
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.instrumentor.util;

import edu.stanford.droidrecord.instrumentor.util.DebugOutput.Sensitivity;

public final class Config {

	public final String inJars;
	public final String outJar;
	public final String libJars;
	public final String inApk;
	public final String androidJar;
	public final String debugJimpleDumpDir;
	public final boolean DBGdumpJimple;
	public final DebugOutput.Sensitivity debugSensitivity;
    public final String staticTemplateOutFile;
    public final boolean methodStartOnly;
    public final boolean dontLog;
    public final boolean enableOnMethodStart;
    public final long initialEventId;
    public final boolean verifyOutput;
    public final RecordingPolicy policy;

	private static Config config;

	public static Config g() {
		if (config == null)
			config = new Config();
		return config;
	}

	private Config() {
	    // General configuration options
		inJars = verboseGetProperty("droidrecord.in.jars");
		outJar = verboseGetProperty("droidrecord.out.jar");
		// Fix paths of the form A.jar:"B.jar" into A.jar:B.jar, this is an issue in some set-ups
        String libJarsLocal = verboseGetProperty("droidrecord.lib.jars");
        String[] libJarsParts = libJarsLocal.split(":");
        libJarsLocal = "";
        for(int i = 0; i < libJarsParts.length; i++) {
        	String part = libJarsParts[i];
        	if(i != 0) libJarsLocal += ":"; 
        	libJarsLocal += part.replace("\"","");
        }
        libJars = libJarsLocal;
        inApk = verboseGetProperty("droidrecord.in.apk");
        androidJar = verboseGetProperty("droidrecord.android.jar");
        if(inApk != null && androidJar == null) {
            System.err.println("Configuration error: Debug option " +
		        "\"droidrecord.in.apk\" is set, but the required property " +
		        " \"droidrecord.android.jar\" is not specified. " +
		        "For APK instrumentation, \"droidrecord.android.jar\" " +
		        "must point to the android.jar platform libary used by " +
		        "the APK to be instrumented.");
		    System.exit(1);
        }
		
		// Debug configuration options
		debugJimpleDumpDir = verboseGetProperty("droidrecord.debug.jimple.dir");
		
		String dbgDumpJimpleStr = verboseGetProperty("droidrecord.debug.dump_jimple");
		if(dbgDumpJimpleStr != null && dbgDumpJimpleStr.equals("true"))
		    DBGdumpJimple = true;
		else
		    DBGdumpJimple = false;
		    
		if(DBGdumpJimple && (debugJimpleDumpDir == null)) {
		    System.err.println("Configuration error: Debug option " +
		        "\"droidrecord.debug.dump_jimple\" is set to true, but the " +
		        "required property \"droidrecord.debug.jimple.dir\" is not " +
		        "specified. Please specify \"droidrecord.debug.jimple.dir\" " +
		        "to be the root directory to which to write the JIMPLE " +
		        "versions of the instrumented classes.");
		    System.exit(1);
		}
		
		String debugSensitivityStr = verboseGetProperty("droidrecord.debug.verbosity");
		if(debugSensitivityStr == null) debugSensitivityStr = "";
		if(debugSensitivityStr.equals("quiet")) {
			debugSensitivity = Sensitivity.QUIET;
		} else if (debugSensitivityStr.equals("normal")) {
			debugSensitivity = Sensitivity.NORMAL;
		} else if(debugSensitivityStr.equals("verbose")) {
			debugSensitivity = Sensitivity.VERBOSE;
		} else if(debugSensitivityStr.equals("super")) {
			debugSensitivity = Sensitivity.SUPERVERBOSE;
		} else {
			System.out.println("droidrecord.debug.verbosity=normal (DEFAULT)");
			debugSensitivity = Sensitivity.NORMAL;
		}
        
        String methodStartOnlyStr = verboseGetProperty("droidrecord.methodStartOnly");
    	if(methodStartOnlyStr != null && methodStartOnlyStr.equals("true"))
		    methodStartOnly = true;
		else
		    methodStartOnly = false;
        
        String dontLogStr = verboseGetProperty("droidrecord.dontLog");
        if(dontLogStr != null && dontLogStr.equals("true"))
    	    dontLog = true;
		else
		    dontLog = false;
        
        String enableOnMethodStartStr = verboseGetProperty("droidrecord.enableOnMethodStart");
        if(enableOnMethodStartStr != null && enableOnMethodStartStr.equals("true"))
		    enableOnMethodStart = true;
		else
		    enableOnMethodStart = false;
        
        String verifyOutputStr = verboseGetProperty("droidrecord.debug.verifyOutput");
        if(verifyOutputStr != null && verifyOutputStr.equals("true"))
		    verifyOutput = true;
		else
		    verifyOutput = false;
		    
        staticTemplateOutFile = verboseGetProperty("droidrecord.out.template",
                                                   "droidrecord.log.template",
                                                   "");
	
	    String initialEventIdStr = verboseGetProperty("droidrecord.initialEventId");
	    if(initialEventIdStr != null)
	        initialEventId = Long.parseLong(initialEventIdStr);
	    else
	        initialEventId = 0;
	
	    String policyStr = verboseGetProperty("droidrecord.policy.file",
	                                          "droidrecord.policy", 
	                                          "");
	    policy = new RecordingPolicy(policyStr);
	}

	private static String verboseGetProperty(String name) {
		return verboseGetProperty(name, null, "");
	}

	private static String verboseGetProperty(String name, String def, String msg) {
		String value = System.getProperty(name, def);
		System.out.println(name + "=" + value + msg);
		return value;
	}
    
}

