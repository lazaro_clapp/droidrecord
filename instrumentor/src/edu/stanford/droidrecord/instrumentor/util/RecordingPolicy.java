
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.instrumentor.util;

import edu.stanford.droidrecord.instrumentor.util.DebugOutput.Sensitivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import soot.SootClass;
import soot.SootMethod;

// TODO: There has to be a way better algorithm than the one used in this class:
// at some point, profile this and if slow make sure to come with a better way 
// to match queries to the policy.
public class RecordingPolicy {

    private static final Pattern entryLineMetaPattern = Pattern.compile("^(Y|N)\\s([^\\s]+)\\s([^\\s]+)\\s(\\*|[A-Za-z](?:[A-Za-z])*)(?:\\s)*(?://.*)?$");
    
    private static class PolicyEntry {
        public final boolean record;
        public final Pattern classPattern;
        public final Pattern methodPattern;
        public final Pattern eventPattern;
        
        public PolicyEntry(String line) {
            Matcher m = entryLineMetaPattern.matcher(line);
            if(!m.matches()) {
                throw new Error(
                    String.format("Failed to match policy file entry %s " +
                                  "(using regular expression: %s)", 
                                  line, entryLineMetaPattern.toString()));
            }
            
            if(m.group(1).equals("Y")) record = true;
            else if(m.group(1).equals("N")) record = false;
            else { assert false; throw new Error("Invalid policy file entry"); }
            
            if(m.group(2).equals("*")) {
            	classPattern = null;
            } else {
            	classPattern = Pattern.compile("^" + m.group(2) + "$");
           	}
            if(m.group(3).equals("*")) {
            	methodPattern = null;
           	} else {
            	methodPattern = Pattern.compile("^" + m.group(3) + "$");
            }
            if(m.group(4).equals("*")) {
            	eventPattern = null;
           	} else {
            	eventPattern = Pattern.compile("^" + m.group(4) + "$");
            }
        }
    }
    
    private final String policyFile;
    private DebugOutput debugOutput;
    private List<PolicyEntry> entries;
    
    private void loadPolicyFile(String policyFile) throws IOException {
    	BufferedReader reader = new BufferedReader(new FileReader(policyFile));
        String line;
        debugOutput.println("Begun loading policy file: " + policyFile);
        while((line = reader.readLine()) != null) {
        	line = line.trim();
           	if(line.equals("")) continue;
            if(line.startsWith("//")) continue;
            
            if(line.startsWith("#")) {
            	// Command directive
            	if(line.startsWith("#include ")) {
            		String includeName = line.split(" ")[1];
            		debugOutput.println("Processing policy include: " + includeName, Sensitivity.VERBOSE);
            		File pF = new File(policyFile);
            		File iF = new File(pF.getParentFile().getAbsolutePath(), 
                                       includeName);
            		loadPolicyFile(iF.getAbsolutePath());
            	} else {
            		throw new Error("Invalid policy file directive: " +
                                    line);
            	}
            	continue;
            }
            
            debugOutput.println("Loading policy rule: " + line, Sensitivity.VERBOSE);
            entries.add(new PolicyEntry(line));
        }
        reader.close();
        debugOutput.println("Finished loading policy file: " + policyFile);
    }
    
    
    public RecordingPolicy(String policyFile) {
    	this.policyFile = policyFile;
    }
    
    public void load() {
    	this.debugOutput = new DebugOutput();
    	this.entries = new ArrayList<PolicyEntry>();
        try {
            loadPolicyFile(policyFile);
        }catch(IOException e){
            throw new Error(e);
        }
    }
    
    private void ensureLoaded() {
    	if(this.entries == null) load();
    }
    
    public boolean match(String classname) {
    	ensureLoaded();
        for(PolicyEntry entry : entries) {
        	Matcher m;
        	if(entry.classPattern != null) {
		    	m = entry.classPattern.matcher(classname);
		    	if(!m.matches()) continue;
        	}
        	
        	if(entry.record) {
        		//Y [TAB] thisclass [TAB] x [TAB] y => match class
        		return true;
        	} else {
        		if((entry.methodPattern == null) &&
        		   (entry.eventPattern == null)) {
        		   	//N [TAB] thisclass [TAB] * [TAB] * => ignore class
        			return false;
        		} else {
        			//N [TAB] thisclass [TAB] x [TAB] y => don't know
        			continue;
        		}
        	}
        	
        }
        return false;
    }
    
    public boolean match(String classname, String methodSubsignature) {
    	ensureLoaded();
        for(PolicyEntry entry : entries) {
        	Matcher m;
        	if(entry.classPattern != null) {
		    	m = entry.classPattern.matcher(classname);
		    	if(!m.matches()) continue;
        	}
        	if(entry.methodPattern != null) {
		    	m = entry.methodPattern.matcher(methodSubsignature);
		    	if(!m.matches()) continue;
        	}
        	
        	if(entry.record) {
        		//Y [TAB] thisclass [TAB] thisMethod [TAB] x => match method
        		return true;
        	} else {
        		if(entry.eventPattern == null) {
        		   	//N [TAB] thisclass [TAB] thisMethod [TAB] * => ignore method
        			return false;
        		} else {
        			//N [TAB] thisclass [TAB] thisMethod [TAB] x => don't know
        			continue;
        		}
        	}
        	
        }
        
        return false;
    }
    
    public boolean match(String classname, String methodSubsignature, String recordType) {
    	ensureLoaded();
        for(PolicyEntry entry : entries) {
        	Matcher m;
        	if(entry.classPattern != null) {
		    	m = entry.classPattern.matcher(classname);
		    	if(!m.matches()) continue;
        	}
        	if(entry.methodPattern != null) {
		    	m = entry.methodPattern.matcher(methodSubsignature);
		    	if(!m.matches()) continue;
        	}
        	if(entry.eventPattern != null) {
		    	m = entry.eventPattern.matcher(recordType);
		    	if(!m.matches()) continue;
        	}
        	
        	return entry.record;
        	
        }
        
        return false;
    }
    
    public boolean match(SootMethod method, String recordType) {
        String classname = method.getDeclaringClass().getName();
        String methodSubsignature = method.getSubSignature();
        return match(classname, methodSubsignature, recordType);
    }
    
    public boolean match(SootMethod method) {
        String classname = method.getDeclaringClass().getName();
        String methodSubsignature = method.getSubSignature();
        return match(classname, methodSubsignature);
    }
    
    public boolean match(SootClass klass) {
        return match(klass.getName());
    }
}
