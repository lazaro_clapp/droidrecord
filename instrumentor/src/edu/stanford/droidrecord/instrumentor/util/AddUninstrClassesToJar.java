
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.instrumentor.util;

import org.apache.tools.ant.taskdefs.Jar;
import org.apache.tools.ant.types.ZipFileSet;
import org.apache.tools.ant.Target;
import org.apache.tools.ant.Project;

import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
   copies the uninstrumented classes from the original
   jar file to the instrumented jar file
*/
public class AddUninstrClassesToJar extends Jar
{
	public AddUninstrClassesToJar(Map<String,List<String>> uninstrumentedClasses,
								  String instrumentedJarName)
	{
		super();
		
		setDestFile(new File(instrumentedJarName));
		setUpdate(true);

		for (Map.Entry<String,List<String>> e : uninstrumentedClasses.entrySet()) {
			String originalJarName = e.getKey();
			ZipFileSet originalJar = new ZipFileSet();
			originalJar.setSrc(new File(originalJarName));
			System.out.println("original jar: " + originalJarName);

			List<String> classes = e.getValue();
			int numFilesToCopy = classes.size();
			String[] array = new String[numFilesToCopy];
			int i = 0;
			for (String className : classes) {
				className = className.replace('.', File.separatorChar) + ".class";
				array[i++] = className;
				System.out.println("copy from original jar: "+ className);
			}
			originalJar.appendIncludes(array);

			addZipfileset(originalJar);
		}
	}

	public void apply()
	{
		Project project = new Project();
		setProject(project);

		Target target = new Target();
		target.setName("addtojar");
		target.addTask(this);
		project.addTarget(target);
		target.setProject(project);

		project.init();
		target.execute();
	}

	public static void main(String[] args)
	{
		String originalJarName = args[0];
		String instrumentedJarName = args[1];
		List<String> uninstrumentedClasses = new ArrayList<String>();
		
		for(int i = 2; i < args.length; i++)
			uninstrumentedClasses.add(args[i]);

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		map.put(originalJarName, uninstrumentedClasses);
		new AddUninstrClassesToJar(map, instrumentedJarName).apply();
	}
}
