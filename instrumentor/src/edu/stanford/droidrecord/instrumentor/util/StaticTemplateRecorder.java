
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.instrumentor.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import soot.ArrayType;
import soot.BooleanType;
import soot.ByteType;
import soot.CharType;
import soot.DoubleType;
import soot.FloatType;
import soot.IntType;
import soot.LongType;
import soot.NullType;
import soot.RefType;
import soot.ShortType;
import soot.Type;
import soot.VoidType;

/**
 * Records the static templates for instrumentation events in a .log.template 
 * file.
 */
public class StaticTemplateRecorder {
    
    private final String STRING_CLASS_NAME = "java.lang.String";
    private final String OBJECT_CLASS_NAME = "java.lang.Object";
    private final String CLASS_CLASS_NAME = "java.lang.Class";
    
    // Magic Token (Base64 for UUID 5bfd384c-2e09-4ca1-9b70-6c6add506a0d)
    // This string is selected as a collision-free search-and-replace 
    // placeholder for dynamic values.
    private static final String PLACE_HOLDER = "_MAGICTOKEN:W/04TC4JTKGbcGxq3VBqDQ==_";
    
    private BufferedWriter writer;
    private long currentIndex;
    
    private DebugOutput debugOutput;
    
    public StaticTemplateRecorder(String filename, DebugOutput debugOutput,
                                 long intialIndex) {
        try{
    		writer = new BufferedWriter(new FileWriter(filename));
		}catch(IOException e){
			throw new Error(e);
		}
        this.debugOutput = debugOutput;
        currentIndex = intialIndex;
    }
    
	private long logEntryTemplate(String entryT) {
		try {
			writer.write(currentIndex + ":" + entryT + "\n");
			writer.flush();
            debugOutput.println("\t\t\tWrote static template entry: " + 
                currentIndex + ":" + entryT.replace("\n\t", "\n\t\t\t\t"), 
                DebugOutput.Sensitivity.SUPERVERBOSE);
            return currentIndex++;
		} catch (IOException e) {
			throw new Error(e);
		}
	}
    
    private String paramTypeToTemplateString(Type type) {
        if(type instanceof LongType) {
            return String.format("long:%s",PLACE_HOLDER);
	    } else if(type instanceof IntType) {
	        return String.format("int:%s",PLACE_HOLDER);
	    } else if(type instanceof ShortType) {
	        return String.format("short:%s",PLACE_HOLDER);
	    } else if(type instanceof ByteType) {
	        return String.format("byte:%s",PLACE_HOLDER);
	    } else if(type instanceof DoubleType) {
	        return String.format("double:%s",PLACE_HOLDER);
	    } else if(type instanceof FloatType) {
	    	return String.format("float:%s",PLACE_HOLDER);
	    } else if(type instanceof CharType) {
	        return String.format("char:%s",PLACE_HOLDER);
	    } else if(type instanceof BooleanType) {
	        return String.format("boolean:%s",PLACE_HOLDER);
	    } else if(type instanceof RefType) {
	        String className = ((RefType)type).getClassName();
	        if(className.equals(STRING_CLASS_NAME)) {
	            return String.format("string:%s",PLACE_HOLDER);
	        } else if(className.equals(CLASS_CLASS_NAME)) {
                return String.format("class:%s:%s",PLACE_HOLDER, PLACE_HOLDER);
            } else {
	            return String.format("obj:%s:%s",className,PLACE_HOLDER);
	        }
	    } else if(type instanceof ArrayType) {
            return String.format("array:%s:%s",type.toString(),PLACE_HOLDER);
	    } else if(type instanceof NullType) {
	        return "other:NULL_TYPE";
        } else if(type instanceof VoidType) {
	        return "other:VOID_TYPE";
	    } else {
	        return "other:UNKNOWN_TYPE";
	    }
    }
    
    private String paramsToTemplateString(List<Type> paramTypes) {
        String params = "[";
        boolean first = true;
        for(Type t : paramTypes) {
            if(!first) params += ",";
            else first = false;
            params += paramTypeToTemplateString(t);
        }
        params += "]";
        return params;
    }
    
    private String listToString(List<?> list) {
        String s = "[";
        boolean first = true;
        for(Object o : list) {
            if(!first) s += ",";
            else first = false;
            s += o.toString();
        }
        s += "]";
        return s;
    }
    
    private String numDimensionsToTemplateString(int numDimensions) {
    	Type[] dimentionTypes = new Type[numDimensions];
        Type t = IntType.v();
        for(int i = 0; i < numDimensions; i++)
        	dimentionTypes[i] = t;
        return dimensionsToTemplateString(dimentionTypes);
    }
    
    private String dimensionsToTemplateString(Type[] dimentionTypes) {
        int numDimensions = dimentionTypes.length;
        String dimStr = "[";
        if(numDimensions != 0) dimStr += paramTypeToTemplateString(dimentionTypes[0]);
        for(int i = 1; i < numDimensions; i++)
            dimStr += "," + paramTypeToTemplateString(dimentionTypes[i]);
        dimStr += "]";
        return dimStr;
    }
    
    public long writeMethodStartTemplate(String fullMethodName, 
        								 long sourcePosition, 
        								 long jimplePosition,
                                         String locals, 
                                         List<Type> paramTypes) {
    	String msg = String.format(
    	        "[MethodStartRecord{Thread: %s,\n\t" +
    	                     "Name: %s,\n\t" +
                             "At: %s:%d:%d,\n\t" +
                             "Parameters: %s,\n\t" +
                             "ParameterLocals: %s,\n\t" +
                             "Height: %s}]",
                PLACE_HOLDER, fullMethodName, fullMethodName,
                sourcePosition, jimplePosition, 
                paramsToTemplateString(paramTypes),
                locals, paramTypeToTemplateString(IntType.v()));
    	return this.logEntryTemplate(msg);
    }
    
    public long writeMethodEndTemplate(String fullMethodName,
                					   long sourcePosition,
                					   long jimplePosition,
                                       Type type,
                                       String local) {
    	String msg = String.format(
    	        "[MethodEndRecord{Thread: %s,\n\t" +
    	                     "Name: %s,\n\t" +
							 "At: %s:%d:%d,\n\t" +
                             "Returns: %s,\n\t" +
                             "ReturnLocal: %s,\n\t" +
                             "Height: %s}]",
                PLACE_HOLDER, fullMethodName, fullMethodName,
                sourcePosition, jimplePosition, 
                paramTypeToTemplateString(type),
                local, paramTypeToTemplateString(IntType.v()));
    	return this.logEntryTemplate(msg);
    }
    
    public long writeMethodCallTemplate(String calleeMethodName,
                                        String callerMethodName,
                                        String locals,
                                        long sourcePosition,
        								long jimplePosition,
                                        List<Type> paramTypes)
    {
	    String msg = String.format(
	        "[MethodCallRecord{Thread: %s,\n\t" +
	                         "Name: %s,\n\t" +
                             "At: %s:%d:%d,\n\t" +
                             "Parameters: %s,\n\t" +
                             "ParameterLocals: %s,\n\t" +
                             "Height: %s}]",
            PLACE_HOLDER, calleeMethodName, callerMethodName, sourcePosition, 
            jimplePosition, paramsToTemplateString(paramTypes), locals,
	        paramTypeToTemplateString(IntType.v()));
		return this.logEntryTemplate(msg);
	}
    
    public long writeMethodReturnTemplate(String calleeMethodName,
                                          String callerMethodName,
                                          long sourcePosition,
        								  long jimplePosition,
                                          Type type,
                                          boolean returnUsed,
                                          String local)
    {
		String msg = String.format(
			"[MethodReturnRecord{Thread: %s,\n\t" +
								"Name: %s,\n\t" +
								"At: %s:%d:%d,\n\t" +
								"Returns: %s,\n\t" +
                                "ReturnLocal: %s,\n\t" +
                                "Height: %s}]",
			PLACE_HOLDER, calleeMethodName, callerMethodName, sourcePosition,
			jimplePosition,
			returnUsed ? paramTypeToTemplateString(type) : "other:UNUSED_VALUE",
            returnUsed ? local : "_unused_",
			paramTypeToTemplateString(IntType.v()));
		return this.logEntryTemplate(msg);
	}
    
    public long writeStoreStmtTemplate(String fieldSig,
                                       String localName,
                                       String fullMethodName,
                                       long sourcePosition,
        							   long jimplePosition,
									   Type instanceType,
									   Type valueType,
									   boolean isInstance,
									   boolean isInitialized) {
        String recordLabel;
		String instanceStr = "";
		if(isInstance && isInitialized) {
			recordLabel = "StoreInstanceRecord";
            assert instanceType != null;
			instanceStr = String.format(
				"Instance: %s,\n\t", 
				paramTypeToTemplateString(instanceType));
		} else if(isInstance && !isInitialized) {
			recordLabel = "StoreUninitializedObjectRecord";
		} else {
			recordLabel = "StoreStaticRecord";
		}
		String msg = String.format(
			"[%s{Thread: %s,\n\t" +
                 "Field: %s,\n\t" +
                 "Local: %s,\n\t" +
                 "At: %s:%d:%d,\n\t" +
                 instanceStr +
                 "Value: %s}]",
            recordLabel, PLACE_HOLDER, fieldSig, localName, fullMethodName,
            sourcePosition, jimplePosition, 
            paramTypeToTemplateString(valueType));
        return this.logEntryTemplate(msg);
    }
	
	public long writeLoadStmtTemplate(String fieldSig,
                                      String localName,
                                      String fullMethodName,
                                      long sourcePosition,
        							  long jimplePosition,
									  Type instanceType,
									  Type valueType,
									  boolean isInstance,
									  boolean isInitialized) {
        String recordLabel;
		String instanceStr = "";
		if(isInstance && isInitialized) {
			recordLabel = "LoadInstanceRecord";
            assert instanceType != null;
			instanceStr = String.format(
				"Instance: %s,\n\t", 
				paramTypeToTemplateString(instanceType));
		} else if(isInstance && !isInitialized) {
			recordLabel = "LoadUninitializedObjectRecord";
		} else {
			recordLabel = "LoadStaticRecord";
		}
		String msg = String.format(
			"[%s{Thread: %s,\n\t" +
                 "Field: %s,\n\t" +
                 "Local: %s,\n\t" +
                 "At: %s:%d:%d,\n\t" +
                 instanceStr +
                 "Value: %s}]",
            recordLabel, PLACE_HOLDER, fieldSig, localName, fullMethodName,
            sourcePosition, jimplePosition, 
            paramTypeToTemplateString(valueType));
        return this.logEntryTemplate(msg);
    }
	
	public long writeNewObjectTemplate(String klassName, 
                                       String localName,
                                       String fullMethodName,
                                       long sourcePosition,
        							   long jimplePosition) {
        String msg = String.format(
            "[NewObjectRecord{Thread: %s,\n\t" +
                             "Local: %s,\n\t" +
                             "At: %s:%d:%d,\n\t" +
                             "Type: %s}]",
            PLACE_HOLDER, localName, fullMethodName,
            sourcePosition, jimplePosition, klassName);
        return this.logEntryTemplate(msg);
    }
    
	public long writeNewArrayTemplate(String arrayLocal,
                                      String fullMethodName,
                                      long sourcePosition,
        							  long jimplePosition,
                                      Type instanceType,
                                      int numDimensions) {
        String msg = String.format(
            "[NewArrayRecord{Thread: %s,\n\t" +
	                     	 "ArrayLocal: %s,\n\t" +
	                     	 "ArrayInstance: %s,\n\t" +
	                     	 "Dimensions: %s,\n\t" +
							 "At: %s:%d:%d}]",
            PLACE_HOLDER, arrayLocal, paramTypeToTemplateString(instanceType), 
            numDimensionsToTemplateString(numDimensions), 
            fullMethodName, sourcePosition, jimplePosition);
        return this.logEntryTemplate(msg);
    }
    
    public long writeArrayLoadTemplate(String arrayLocal,
        							   String valueLocal,
    								   String fullMethodName,
            						   long sourcePosition,
        							   long jimplePosition,
                                       Type instanceType,
                                       Type[] dimentionTypes,
                                       Type valueType) {
    	String msg = String.format(
        	"[LoadArrayRecord{Thread: %s,\n\t" +
	                     	 "ArrayLocal: %s,\n\t" +
	                     	 "ArrayInstance: %s,\n\t" +
	                     	 "Indexes: %s,\n\t" +
    	                     "Local: %s,\n\t" +
							 "At: %s:%d:%d,\n\t" +
                             "Value: %s}]",
            PLACE_HOLDER, arrayLocal, paramTypeToTemplateString(instanceType), 
            dimensionsToTemplateString(dimentionTypes), valueLocal, 
            fullMethodName, sourcePosition, jimplePosition, 
            paramTypeToTemplateString(valueType));
    	return this.logEntryTemplate(msg);
    }
    
    public long writeArrayStoreTemplate(String arrayLocal,
            						    String valueLocal,
    								    String fullMethodName,
            						    long sourcePosition,
        							    long jimplePosition,
                                        Type instanceType,
                                        Type[] dimentionTypes,
                                        Type valueType) {
    	String msg = String.format(
        	"[StoreArrayRecord{Thread: %s,\n\t" +
	                     	 "ArrayLocal: %s,\n\t" +
	                     	 "ArrayInstance: %s,\n\t" +
	                     	 "Indexes: %s,\n\t" +
    	                     "Local: %s,\n\t" +
							 "At: %s:%d:%d,\n\t" +
                             "Value: %s}]",
            PLACE_HOLDER, arrayLocal, paramTypeToTemplateString(instanceType), 
            dimensionsToTemplateString(dimentionTypes), valueLocal, 
            fullMethodName, sourcePosition, jimplePosition, 
            paramTypeToTemplateString(valueType));
    	return this.logEntryTemplate(msg);
    }
    
    public long writeArrayLengthTemplate(String arrayLocal,
                					     String valueLocal,
    								     String fullMethodName,
            						     long sourcePosition,
        							     long jimplePosition,
                                         Type instanceType,
                                         Type valueType) {
    	String msg = String.format(
        	"[ArrayLengthRecord{Thread: %s,\n\t" +
	                     	 "ArrayLocal: %s,\n\t" +
	                     	 "ArrayInstance: %s,\n\t" +
    	                     "Local: %s,\n\t" +
							 "At: %s:%d:%d,\n\t" +
                             "Value: %s}]",
            PLACE_HOLDER, arrayLocal, paramTypeToTemplateString(instanceType), 
            valueLocal, fullMethodName, sourcePosition, jimplePosition, 
            paramTypeToTemplateString(valueType));
    	return this.logEntryTemplate(msg);
    }
    
    public long writeCaughtExceptionTemplate(String localName,
    								     String fullMethodName,
            						     long sourcePosition,
        							     long jimplePosition,
                                         Type exceptionType) {
    	String msg = String.format(
                "[CaughtExceptionRecord{Thread: %s,\n\t" +
	                     	 "Local: %s,\n\t" +
							 "At: %s:%d:%d,\n\t" +
                             "Exception: %s,\n\t" +
                             "Height: %s}]",
                PLACE_HOLDER, localName, fullMethodName, 
                sourcePosition, jimplePosition, 
                paramTypeToTemplateString(exceptionType),
                paramTypeToTemplateString(IntType.v()));
    	return this.logEntryTemplate(msg);
    }
    
    public long writeThrowExceptionTemplate(String fullMethodName,
            						     long sourcePosition,
        							     long jimplePosition,
                                         Type exceptionType) {
    	String msg = String.format(
                "[ThrowExceptionRecord{Thread: %s,\n\t" +
							 "At: %s:%d:%d,\n\t" +
                             "Exception: %s}]",
                PLACE_HOLDER, fullMethodName, sourcePosition, jimplePosition, 
                paramTypeToTemplateString(exceptionType));
    	return this.logEntryTemplate(msg);
    }
    
    public long writeIfStmtTemplate(String conditionalExpr,
                                    String fullMethodName,
                					long sourcePosition,
        							long jimplePosition) {
    	String msg = String.format(
                "[IfRecord{Thread: %s,\n\t" +
						   "Condition: %s,\n\t" +
        				   "At: %s:%d:%d,\n\t" +
    					   "BranchTaken: %s}]",
                PLACE_HOLDER, conditionalExpr, fullMethodName,  sourcePosition, 
                jimplePosition, paramTypeToTemplateString(IntType.v()));
    	return this.logEntryTemplate(msg);
    }
    
    public long writeSwitchStmtTemplate(String key,
                                        List<?> lookupValues,
                                        Type valueType,
                                        String fullMethodName,
                    				    long sourcePosition,
        							    long jimplePosition) {
    	String msg = String.format(
                "[SwitchRecord{Thread: %s,\n\t" +
    					   "Key: %s,\n\t" +
						   "LookupValues: %s,\n\t" +
        				   "At: %s:%d:%d,\n\t" +
            			   "Value: %s,\n\t" +
    					   "BranchTaken: %s}]",
                PLACE_HOLDER, key, listToString(lookupValues), 
                fullMethodName,  sourcePosition, jimplePosition, 
                paramTypeToTemplateString(valueType), 
                paramTypeToTemplateString(IntType.v()));
    	return this.logEntryTemplate(msg);
    }
    
    public long writeSimpleAssignStmtTemplate(String leftLocalName,
                                              String rightLocalName,
                                              String fullMethodName,
                        			          long sourcePosition,
        							          long jimplePosition) {
    	String msg = String.format(
                "[SimpleAssignmentRecord{Thread: %s,\n\t" +
    					   "LeftLocal: %s,\n\t" +
        				   "RightLocal: %s,\n\t" +
        				   "At: %s:%d:%d}]",
                PLACE_HOLDER, leftLocalName, rightLocalName, 
                fullMethodName,  sourcePosition, jimplePosition);
    	return this.logEntryTemplate(msg);
    }
    
    public long writeNegStmtTemplate(String leftLocalName,
                                     String rightLocalName,
                                     String fullMethodName,
                            		 long sourcePosition,
        							 long jimplePosition) {
    	String msg = String.format(
                "[NegationRecord{Thread: %s,\n\t" +
    					   "LeftLocal: %s,\n\t" +
        				   "RightLocal: %s,\n\t" +
        				   "At: %s:%d:%d}]",
                PLACE_HOLDER, leftLocalName, rightLocalName, 
                fullMethodName,  sourcePosition, jimplePosition);
    	return this.logEntryTemplate(msg);
    }
    
    public long writeBinopStmtTemplate(String leftLocalName,
                                       String op1LocalName,
                                       String op2LocalName,
                                       String symbol,
                                       String fullMethodName,
                            		   long sourcePosition,
        							   long jimplePosition) {
    	String msg = String.format(
                "[BinaryOperationRecord{Thread: %s,\n\t" +
    					   "ResultLocal: %s,\n\t" +
        				   "FirstOperandLocal: %s,\n\t" +
            			   "SecondOperandLocal: %s,\n\t" +
            			   "Operator: %s,\n\t" +
        				   "At: %s:%d:%d}]",
                PLACE_HOLDER, leftLocalName, op1LocalName, op2LocalName, 
                symbol, fullMethodName,  sourcePosition, jimplePosition);
    	return this.logEntryTemplate(msg);
    }
    
    public long writeInstanceOfStmtTemplate(String leftLocalName,
                                            String rightLocalName,
                                            String checkType,  
                                            String fullMethodName,
                            		        long sourcePosition,
        							        long jimplePosition) {
    	String msg = String.format(
                "[InstanceOfRecord{Thread: %s,\n\t" +
    					   "LeftLocal: %s,\n\t" +
        				   "RightLocal: %s,\n\t" +
            			   "CheckType: %s,\n\t" +
        				   "At: %s:%d:%d}]",
                PLACE_HOLDER, leftLocalName, rightLocalName, checkType, 
                fullMethodName,  sourcePosition, jimplePosition);
    	return this.logEntryTemplate(msg);
    }
    
    public long writeCastStmtTemplate(String leftLocalName,
                                      String rightLocalName,
                                      String castType,  
                                      String fullMethodName,
                                	  long sourcePosition,
        							  long jimplePosition) {
    	String msg = String.format(
                "[CastRecord{Thread: %s,\n\t" +
    					   "LeftLocal: %s,\n\t" +
        				   "RightLocal: %s,\n\t" +
            			   "CastType: %s,\n\t" +
        				   "At: %s:%d:%d}]",
                PLACE_HOLDER, leftLocalName, rightLocalName, castType, 
                fullMethodName,  sourcePosition, jimplePosition);
    	return this.logEntryTemplate(msg);
    }
    
    public long writeEnterMonitorStmtTemplate(String localName,
                                      String fullMethodName,
                                      long sourcePosition,
        							  long jimplePosition) {
    	String msg = String.format(
                "[EnterMonitorRecord{Thread: %s,\n\t" +
    					   "Local: %s,\n\t" +
        				   "At: %s:%d:%d}]",
                PLACE_HOLDER, localName, fullMethodName,  sourcePosition, 
                jimplePosition);
    	return this.logEntryTemplate(msg);
    }
    
    public long writeExitMonitorStmtTemplate(String localName,
                                      String fullMethodName,
                                      long sourcePosition,
        							  long jimplePosition) {
        String msg = String.format(
                "[ExitMonitorRecord{Thread: %s,\n\t" +
    					   "Local: %s,\n\t" +
        				   "At: %s:%d:%d}]",
                PLACE_HOLDER, localName, fullMethodName,  sourcePosition,
                jimplePosition);
    	return this.logEntryTemplate(msg);
    }
    
    public void chainTemplates(long t1, long t2) {
        try {
            String chainDesc = t1 + ">" + t2;
            writer.write(chainDesc + "\n");
    		writer.flush();
            debugOutput.println("\t\t\tChained static template entries: " + 
                chainDesc, 
                DebugOutput.Sensitivity.SUPERVERBOSE);
		} catch (IOException e) {
			throw new Error(e);
		}
    } 
    
    public void chainTemplates(long t1, long t2, int branch) {
        try {
            String chainDesc = t1 + "[" + branch + "]>" + t2;
            writer.write(chainDesc + "\n");
			writer.flush();
            debugOutput.println("\t\t\tChained static template entries: " + 
                chainDesc, 
                DebugOutput.Sensitivity.SUPERVERBOSE);
		} catch (IOException e) {
			throw new Error(e);
		}
    }
	
	public void close() {
		try {
			writer.close();
		} catch (IOException e) {
			throw new Error(e);
		}	
	}
	
	@Override
	protected void finalize() {
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
    
}
