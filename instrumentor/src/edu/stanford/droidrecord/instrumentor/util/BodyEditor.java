
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.instrumentor.util;

import java.util.Iterator;

import soot.Body;
import soot.Local;
import soot.SootMethod;
import soot.Trap;
import soot.Type;
import soot.jimple.Jimple;
import soot.jimple.Stmt;
import soot.util.Chain;

public class BodyEditor
{
	Iterator stmtIterator;
	int freshCount;
	Chain locals;
	Stmt originalStmt;
	Stmt currentStmt;
	Stmt firstInsertedStmt;
	Chain stmts;
	Chain traps;

    // store the method from which this body is copied from
	SootMethod originalMethod;

	// fields to count # of stmts changed
	boolean removed;
	String lastStmt;
	int stmtsChanged = 0;
	int totalStmts = 0;
	int stmtPosOriginal = 0;
	int stmtPosChanged = 0;
    
    private DebugOutput debugOutput;
    
    public BodyEditor(DebugOutput debugOutput)
    {
        this.debugOutput = debugOutput;
    }

	public void newBody(Body body, SootMethod originalMethod)
	{
		newBody(body, originalMethod, body.getUnits().snapshotIterator());
	}

	public void newEmptyBody( Body body )
	{
		newBody(body, null, null);
	}

	private void newBody(Body body, SootMethod originalMethod, Iterator stmtIterator)
	{
		this.freshCount = 0;
		this.stmtPosChanged = this.stmtPosOriginal = 0;
		this.locals = body.getLocals();
		this.stmtIterator = stmtIterator;
		this.stmts = body.getUnits().getNonPatchingChain();
		this.traps = body.getTraps();
		this.lastStmt = null;
		this.originalMethod = originalMethod;
	}

	public Local freshLocal( Type type )
	{
		freshCount++;
		Local freshL  = Jimple.v().newLocal( new String( "_shadow_tmp_" + freshCount ), type );
		locals.addFirst( freshL );
		return freshL;
	}

	public Local freshLocal(Type type, String name)
	{
		Local freshL  = Jimple.v().newLocal(name, type);
		locals.addFirst(freshL);
		return freshL;
	}

	public boolean hasNext()
	{
		if (lastStmt != null)
			// BUG: This should really be done when next() is called, hasNext()
			// could potentially be called more than once per position.
			checkIfLastStmtChanged();
		return this.stmtIterator.hasNext();
	}

	public Stmt next()
	{
		removed = false;
		currentStmt = originalStmt = (Stmt) this.stmtIterator.next();
		lastStmt = originalStmt.toString();
		firstInsertedStmt = null;
		stmtPosOriginal++;
		stmtPosChanged++;
		return originalStmt;
	}
	
	public boolean moveTo(Stmt s)
	{
		Body originalBody = this.originalMethod.getActiveBody();
		Chain originalStmts = originalBody.getUnits().getNonPatchingChain();
		if(!originalStmts.contains(s)) {
			return false;
		}
		
		this.stmtIterator = originalBody.getUnits().snapshotIterator();
		stmtPosOriginal = stmtPosChanged = 0;
		while((Stmt) this.stmtIterator.next() != s) {
			stmtPosOriginal++; // Advance to s
		}
		Iterator modifiedStmtsIt = this.stmts.snapshotIterator();
		while((Stmt) modifiedStmtsIt.next() != s) {
			stmtPosChanged++; // Advance to s
		}
		
		removed = false;
		currentStmt = originalStmt = s;
		lastStmt = originalStmt.toString();
		firstInsertedStmt = null;
		return true;
	}

	public void insertStmt(Stmt newStmt, boolean redirect)
	{
		stmtPosChanged++;
		if (stmtIterator == null) {
			stmts.addLast(newStmt);
			return;
		}
		stmts.insertBefore( newStmt, originalStmt );
		// if it is the first stmt inserted while processing originalStmt
		if ( originalStmt == currentStmt ) {

			firstInsertedStmt = newStmt;

			// all jumps to originalStmt now jump to newStmt
			if (redirect) {
				originalStmt.redirectJumpsToThisTo( newStmt );
			}
		}
		if (redirect) {
			currentStmt = newStmt;
		}
	}

	public void insertStmtAfter(Stmt newStmt)
	{
		stmts.insertAfter(newStmt, originalStmt);
        debugOutput.println("\t\t\tInserted: " + newStmt.toString(), 
            DebugOutput.Sensitivity.SUPERVERBOSE);
	}

	public void insertStmt(Stmt newStmt)
	{
		insertStmt(newStmt, true);
        debugOutput.println("\t\t\tInserted: " + newStmt.toString(), 
            DebugOutput.Sensitivity.SUPERVERBOSE);
	}

	/** rule is: While processing a stmt, first add all new stmts.
	Finally delete the original one if necessary.
	 */
	public void removeOriginalStmt()
	{
	    removed = true;
		stmts.remove( originalStmt );
	}

	public void addTrap(Trap newTrap)
	{
		traps.addLast(newTrap);
	}
	
	public Chain<Trap> getTraps()
	{
	    return traps;
	}


	private void checkIfLastStmtChanged()
	{
		if (removed)
			stmtsChanged++;
		else{
			if (!lastStmt.equals(originalStmt.toString()))
				stmtsChanged++;
		}
		totalStmts++;
		return;
	}

	public Stmt originalStmt()
	{
		return originalStmt;
	}

	public SootMethod originalMethod()
	{
		return originalMethod;
	}

	public int getTotalStmts() { return totalStmts; }

	public int getChangedStmts() { return stmtsChanged; }

	public int getPosOriginal() { return stmtPosOriginal; }

	public int getPosChanged() { return stmtPosChanged; }
}
