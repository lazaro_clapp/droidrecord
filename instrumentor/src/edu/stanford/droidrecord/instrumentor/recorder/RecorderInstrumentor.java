
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.instrumentor.recorder;

import edu.stanford.droidrecord.instrumentor.util.BodyEditor;
import edu.stanford.droidrecord.instrumentor.util.Config;
import edu.stanford.droidrecord.instrumentor.util.DebugOutput;
import edu.stanford.droidrecord.instrumentor.util.DebugOutput.Sensitivity;
import edu.stanford.droidrecord.instrumentor.util.Printer;
import edu.stanford.droidrecord.instrumentor.util.StaticTemplateRecorder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import soot.ArrayType;
import soot.Body;
import soot.BooleanType;
import soot.ByteType;
import soot.CharType;
import soot.DoubleType;
import soot.FloatType;
import soot.Immediate;
import soot.IntType;
import soot.Local;
import soot.LongType;
import soot.Modifier;
import soot.NullType;
import soot.RefType;
import soot.ShortType;
import soot.SootClass;
import soot.SootMethod;
import soot.SootMethodRef;
import soot.Trap;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.VoidType;
import soot.jimple.AbstractStmtSwitch;
import soot.jimple.ArrayRef;
import soot.jimple.AssignStmt;
import soot.jimple.BinopExpr;
import soot.jimple.CastExpr;
import soot.jimple.CaughtExceptionRef;
import soot.jimple.EnterMonitorStmt;
import soot.jimple.ExitMonitorStmt;
import soot.jimple.Expr;
import soot.jimple.FieldRef;
import soot.jimple.GotoStmt;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.InstanceFieldRef;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InstanceOfExpr;
import soot.jimple.IntConstant;
import soot.jimple.InvokeExpr;
import soot.jimple.Jimple;
import soot.jimple.LookupSwitchStmt;
import soot.jimple.LongConstant;
import soot.jimple.LengthExpr;
import soot.jimple.NegExpr;
import soot.jimple.NewArrayExpr;
import soot.jimple.NewExpr;
import soot.jimple.NewMultiArrayExpr;
import soot.jimple.NullConstant;
import soot.jimple.ReturnStmt;
import soot.jimple.ReturnVoidStmt;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.TableSwitchStmt;
import soot.jimple.ThisRef;
import soot.jimple.ThrowStmt;
import soot.tagkit.LineNumberTag;
import soot.tagkit.SourceLnPosTag;
import soot.tagkit.Tag;
import soot.toolkits.graph.BriefUnitGraph;
import soot.util.Chain;

public class RecorderInstrumentor extends AbstractStmtSwitch {
	public static int MAX_INSTRUCTION_COUNT = 7500;
	
	private SootMethod currentMethod;
	private SootMethod currentOriginalMethod;
	private String currentThisLocalName;
    private List<SootClass> classes; // All classes to instrument

    private RecorderInstrumentorConstants C;
    private BodyEditor editor;
    private Jimple jimple;
    
    // Used for event template chaining:
    private BriefUnitGraph unitGraph;
    private Map<Stmt,Map<Stmt,Integer>> conditionalToTargetToBranch;
    private Map<Stmt,Long> stmtToTemplateId;
    private long methodStartTemplateId;
    private List<Stmt> stmtsToBeChained;
    
    private Map<Unit,Long> stmtToJimplePosMap;
    
    private StaticTemplateRecorder stemplate;
    
    private DebugOutput debugOutput;

    public RecorderInstrumentor(List<SootClass> classes) {
		this.classes = classes;
		this.C = RecorderInstrumentorConstants.g();
    	this.debugOutput = new DebugOutput();
		this.editor = new BodyEditor(debugOutput);
		this.jimple = Jimple.v();
        this.stemplate = new StaticTemplateRecorder(Config.g().staticTemplateOutFile, debugOutput, Config.g().initialEventId);
	}

    public void instrument() {
        for (SootClass klass : classes) {
			klass.setApplicationClass();
			debugOutput.println("Loaded class " + klass.getName());
		}
		
		Config config = Config.g();
		config.policy.load();

		for (SootClass klass : classes) {
		    if(klass.isInterface()) continue;
		    if(!config.policy.match(klass)) continue;
		    
			List<SootMethod> origMethods = klass.getMethods();
			
			// Pass #1: Collect information on constructors
			int nextConstructorArgumentCount = 0;
			for(SootMethod m : origMethods) {
			    if(!m.getName().equals(SootMethod.constructorName))
			        continue;
			    int argCount = m.getParameterCount();
			    if(argCount >= nextConstructorArgumentCount)
			        nextConstructorArgumentCount = argCount + 1;
			}
			
			// Pass #2: Generate instrumented clone methods
			for(SootMethod m : origMethods) {
			    
			    if(!m.isConcrete()) continue;
		        if(!config.policy.match(m)) continue;
			    
			    debugOutput.println("Clonning " + m);
                SootMethod mClone;
                
                int modifiers = m.getModifiers();
                // make __dr_method private
                modifiers &= ~(Modifier.PUBLIC | Modifier.PROTECTED);
                assert(!Modifier.isPublic(modifiers));
                assert(!Modifier.isProtected(modifiers));
                modifiers |= Modifier.PRIVATE;
                assert(Modifier.isPrivate(modifiers));
                
			    if(m.getName().equals(SootMethod.constructorName)) {
			        List<Type> paramTypes = new ArrayList<Type>();
			        for(Type t : m.getParameterTypes()) {
			            paramTypes.add(t);
			        }
			        while(paramTypes.size() <= nextConstructorArgumentCount) {
			            paramTypes.add(RefType.v("java.lang.Object"));
			        }
			        nextConstructorArgumentCount++;
                    mClone = new SootMethod(m.getName(),
                                            paramTypes,
                                            m.getReturnType(), 
                                            modifiers,
                                            m.getExceptions());
			    } else {
                    mClone = new SootMethod("__dr_" + m.getName().replace("<","DR").replace(">","DR"),                 
                                            m.getParameterTypes(),
                                            m.getReturnType(), 
                                            modifiers,
                                            m.getExceptions());
			    }
                klass.addMethod(mClone);
                debugOutput.println("Generated clone method " + mClone);
                
				if (!m.isConcrete())
					continue;
				
                mClone.setActiveBody((Body)m.retrieveActiveBody().clone());
                
                try {
                	stmtToJimplePosMap = getStmtToJimplePosMap(mClone);
    				if(instrument(mClone, m)) {
    				    setJumpToInstrumentedCode(m, mClone);
    				}
    				stmtToJimplePosMap = null;
                } catch(Exception e) {
                    debugOutput.println("Unexpected instrumentation error.");
            		if(Config.g().DBGdumpJimple)
            		    Printer.v().printAll(classes, Config.g().debugJimpleDumpDir);
            		throw new Error(e);
                }
			}
		}
		
		if(config.DBGdumpJimple)
		    Printer.v().printAll(classes, config.debugJimpleDumpDir);
    }
    
    private void setJumpToInstrumentedCode(SootMethod method, SootMethod instMethod) {
        debugOutput.println("Adding jump-to-instrumented prologue for method " + method);
        Body body = method.retrieveActiveBody();
        
        boolean castRequired = false;
        Local drThisLocal = null; // Only used for non-static methods
        
        List<Value> params = new ArrayList<Value>();
        for(int i = 0; i < method.getParameterCount(); i++) {
            params.add(body.getParameterLocal(i));
        }
        
        if(method.getName().equals(SootMethod.constructorName)) {
            while(params.size() < instMethod.getParameterCount()) {
                params.add(NullConstant.v());
            }
        }
        
        Expr invokeInstExpr;
        if(method.isStatic()) {
            invokeInstExpr = jimple.newStaticInvokeExpr(
                                instMethod.makeRef(), 
                                params);
        } else {
            // Doing instMethod.makeRef() breaks in a few cases where the 
            // 'this' local in the Jimple code is not of the type of the 
            // current class (statically) but of the type of a superclass.
            // We solve by adding a cast.
            drThisLocal = body.getThisLocal();
            Type thisT = drThisLocal.getType();
            assert(thisT instanceof RefType);
            SootClass thisRefKlass = ((RefType)thisT).getSootClass();
            castRequired = !(thisRefKlass.getName().equals(instMethod.getDeclaringClass().getName()));
            if(castRequired) {
                // Add casted Local
                Local casted = jimple.newLocal("_dr_this", 
                                            instMethod.getDeclaringClass().getType());
                body.getLocals().add(casted);
                drThisLocal = casted;
            }
            
            // Direct method:
            // Dalvik puts private, static, and constructors into 
            // non-virtual table.
            // See. $ANDROID_SRC/dalvik/vm/oo/Object.h
            invokeInstExpr = jimple.newSpecialInvokeExpr(
                                    drThisLocal, 
                                    instMethod.makeRef(), 
                                    params);
        }
		
		Chain<Unit> units = body.getUnits().getNonPatchingChain();
		for (Unit u : units) {
			Stmt s = (Stmt) u;

			// Capture the list of parameter locals.
			if (paramOrThisIdentityStmt(s))
    		   continue;
            
            // If droidrecord.enableOnMethodStart is true, enable logging
            Config config = Config.g();
            if(config.enableOnMethodStart) {
                units.insertBefore(
                    jimple.newInvokeStmt(
            	        jimple.newStaticInvokeExpr(C.M_WRITE_BIN_ENABLE)), s);
            }
            
            Local enabledLocal = jimple.newLocal("_dr_enabled", BooleanType.v());
            body.getLocals().add(enabledLocal);
            units.insertBefore(
                jimple.newAssignStmt(
                    enabledLocal,
                    jimple.newStaticInvokeExpr(C.M_WRITE_BIN_IS_ENABLED)), 
                s);
                
            units.insertBefore(
                jimple.newIfStmt(
                    jimple.newEqExpr(enabledLocal, IntConstant.v(0)),
                    s), 
                s);
            
            if(castRequired) {
                // Perform cast from original this variable to _dr_this
                units.insertBefore(
                    jimple.newAssignStmt(drThisLocal, 
                        jimple.newCastExpr(body.getThisLocal(),
                            instMethod.getDeclaringClass().getType())), 
                    s);
            }
            
            if(method.getReturnType().equals(VoidType.v())) {
                units.insertBefore(
                    jimple.newInvokeStmt(invokeInstExpr), 
                    s);
                units.insertBefore(
                    jimple.newReturnVoidStmt(), 
                    s);
            } else {
                Local retLocal = jimple.newLocal("_dr_retval", instMethod.getReturnType());
                body.getLocals().add(retLocal);
                units.insertBefore(
                    jimple.newAssignStmt(retLocal, invokeInstExpr), 
                    s);
                units.insertBefore(
                    jimple.newReturnStmt(retLocal), 
                    s);
            }
            
			// Stop once you've reached the start of actual code.
			break;
		}
    }
    
    private Map<Unit,Long> getStmtToJimplePosMap(SootMethod method)
    {
    	debugOutput.println("\tComputing Stmt to jimple position mapping for method " + method, 
        		Sensitivity.VERBOSE);
        Body body = method.retrieveActiveBody();
        Chain<Unit> units = body.getUnits().getNonPatchingChain();
        Map<Unit,Long> map = new HashMap<Unit,Long>();
        long i = 0;
		for (Unit u : units) {
			map.put(u,i++);
		}
		return map;
    }
	
	private boolean instrument(SootMethod method, SootMethod originalMethod)
    {
        debugOutput.println("Instrumenting " + method);
		currentMethod = method;
		currentOriginalMethod = originalMethod;
		currentThisLocalName = null;
		stackHeightLocal = null;

        debugOutput.println(
        		String.format("\tRetriveing active body (%s)", method.getName()), 
        		Sensitivity.VERBOSE);
        Body body = method.retrieveActiveBody();
        //Body oldbody = (Body)body.clone();
        unitGraph = new BriefUnitGraph(body);
        conditionalToTargetToBranch = new HashMap<Stmt,Map<Stmt,Integer>>();
        stmtToTemplateId = new HashMap<Stmt,Long>();
        stmtsToBeChained = new ArrayList<Stmt>();
		
        debugOutput.println(
        		String.format("\tCreating new body (%s)", method.getName()), 
        		Sensitivity.VERBOSE);
		editor.newBody(body, method);
		List<Local> params = new ArrayList<Local>();
		
		addStackHeightLocal();
		
        Config config = Config.g();
        if(!(config.methodStartOnly || config.dontLog)) {
            debugOutput.println(
            		String.format("\tStarting instruction by instruction instrumentation (%s)", method.getName()), 
            		Sensitivity.VERBOSE);
    		while (editor.hasNext()) {
    			Stmt s = editor.next();
    			if (paramOrThisIdentityStmt(s)) {
    				IdentityStmt is = (IdentityStmt) s;
    				params.add((Local) is.getLeftOp());
    				if(is.getRightOp() instanceof ThisRef) {
    					currentThisLocalName = ((Local) is.getLeftOp()).getName();
    				}
    			} else if (s.containsInvokeExpr()) {
    				handleInvokeExpr(s);
    			} else if (!s.branches()) { //Branches are handled by instrumentConds
                    s.apply(this);
    			}
    		}
        }
		
        debugOutput.println(
        		String.format("\tInserting prologue (%s)", method.getName()),
        		Sensitivity.VERBOSE);
		// insertPrologue(...) must be called after instrumenting the
		// statements, doing it otherwise may break some invariants and lead to
		// malformed jimple bodies.
		insertPrologue(body, params);

        if(!(config.methodStartOnly || config.dontLog)) {
    		instrumentConds(body);
            
            // Chain event templates for events that are not dynamically recorded.
            for(Stmt s : stmtsToBeChained) {
                chainToPreviousStmts(s);
            }
        }
		
		body.validate();
		int instructionCount = body.getUnits().size();
		debugOutput.println(
        		String.format("\tFinal (Jimple) instruction count: %d (%s)", 
        				instructionCount, 
        				method.getName()),
        		Sensitivity.VERBOSE);
		if(instructionCount > MAX_INSTRUCTION_COUNT) {
			// Java restricts bytecode method size to 65535 bytes, in order to 
			// prevent generated methods from surpassing this limit, we 
			// conservatively limit the instruction count of the Jimple body 
			// for the method (exact bytecode size seems hard to estimate at 
			// this point in the program and the underlying Jasmin verification 
			// exception from soot.main is non catchable: ie. RuntimeException, 
			// so we are forced to use instruction count as a proxy for bytecode
			// size).
			debugOutput.println(
	        		String.format("\tFinal (Jimple) instruction count too " +
	        				"large (%d). Skiping instrumentation for method: %s.", 
	        				instructionCount, 
	        				method),
	        		Sensitivity.WARNING);
			return false;
			//method.setActiveBody(oldbody);
		}
		
		return true;
    }
	
	private int stmtToSourcePosition(Unit u) {
		for (@SuppressWarnings("rawtypes")
			 Iterator j = u.getTags().iterator(); j.hasNext(); ) {
		    Tag tag = (Tag)j.next();
		    /* TODO: SourceLnPosTag requires directly loading java files, 
		     * left for later.
		    if (tag instanceof SourceLnPosTag) {
		    	SourceLnPosTag posTag = (SourceLnPosTag) tag;
		    	return posTag.startLn();
		    } else */
		    if (tag instanceof LineNumberTag) {
		    	LineNumberTag lnTag = (LineNumberTag) tag;
			    return lnTag.getLineNumber();
		    }
	    }
		return -1; // Line number information unavailable
	}
	
	private long stmtToJimplePosition(Unit u) {
		if(stmtToJimplePosMap == null) return -1;
		else if(stmtToJimplePosMap.get(u) == null) return -1;
		else return stmtToJimplePosMap.get(u).longValue();
	}
    
    private void writeEventHeader(long eventID, boolean insertAfter, boolean redirect) {
    	if(insertAfter)
			editor.insertStmtAfter(
					jimple.newInvokeStmt(
						jimple.newStaticInvokeExpr(C.M_WRITE_BIN_EVENT_HEADER, 
							LongConstant.v(eventID))));

		else
			editor.insertStmt(
					jimple.newInvokeStmt(
						jimple.newStaticInvokeExpr(C.M_WRITE_BIN_EVENT_HEADER, 
							LongConstant.v(eventID))), redirect);
    }
	
	private void writeEventHeader(long eventID, boolean insertAfter) {
		writeEventHeader(eventID, insertAfter, true);
	}
	
	private void writeEventHeader(long eventID) {
		writeEventHeader(eventID, false);
	}
    
    private void writeParamsToBinLog(List<Immediate> params, boolean insertAfter, boolean redirect) {
    	for (Iterator<Immediate> it = params.iterator(); it.hasNext(); ) {
		    Immediate arg = it.next();
		    writeValueToBinLog(arg, insertAfter, redirect);
		}
	}
    
    private void writeParamsToBinLog(List<Immediate> params) {
    	writeParamsToBinLog(params, false, true);
	}
	
    private void writeValueToBinLog(Value arg, boolean insertAfter, boolean redirect, Type type) {
        SootMethodRef m_write_param;
        if(type instanceof LongType) {
	        m_write_param = C.M_WRITE_BIN_PARAM_LONG;
	    } else if(type instanceof IntType) {
	        m_write_param = C.M_WRITE_BIN_PARAM_INT;
	    } else if(type instanceof ShortType) {
	        //m_write_param = C.M_WRITE_BIN_PARAM_SHORT;
            m_write_param = C.M_WRITE_BIN_PARAM_INT;
	    } else if(type instanceof ByteType) {
	        m_write_param = C.M_WRITE_BIN_PARAM_INT;
	    } else if(type instanceof DoubleType) {
	        m_write_param = C.M_WRITE_BIN_PARAM_DOUBLE;
	    } else if(type instanceof FloatType) {
	    	m_write_param = C.M_WRITE_BIN_PARAM_FLOAT;
	    } else if(type instanceof CharType) {
	        m_write_param = C.M_WRITE_BIN_PARAM_CHAR;
	    } else if(type instanceof BooleanType) {
	        m_write_param = C.M_WRITE_BIN_PARAM_BOOL;
	    } else if(type instanceof RefType) {
	        String className = ((RefType)type).getClassName();
	        if(className.equals(C.STRING_CLASS_NAME)) {
	            m_write_param = C.M_WRITE_BIN_PARAM_STR;
	        } else if(className.equals(C.CLASS_CLASS_NAME)) {
                m_write_param = C.M_WRITE_BIN_PARAM_CLASS;
            } else {
                m_write_param = C.M_WRITE_BIN_PARAM_OBJ;
	        }
	    } else if(type instanceof ArrayType) {
            m_write_param = C.M_WRITE_BIN_PARAM_ARRAY;
	    } else if(type instanceof NullType) {
            return;
	    } else {
	        throw new Error("Unnexpected value type: " + type.toString());
	    }
	    if(insertAfter)	    	
		    editor.insertStmtAfter(
	            jimple.newInvokeStmt(
	            	jimple.newStaticInvokeExpr(m_write_param, arg)));
	    else
	    	editor.insertStmt(
		            jimple.newInvokeStmt(
		            	jimple.newStaticInvokeExpr(m_write_param, arg)), redirect);
    }
    
	private void writeValueToBinLog(Value arg, boolean insertAfter, boolean redirect) {
		writeValueToBinLog(arg, insertAfter, redirect, arg.getType());
	}
    
	private void writeValueToBinLog(Value arg, boolean insertAfter) {
		writeValueToBinLog(arg, insertAfter, true, arg.getType());
	}
	
	private void writeValueToBinLog(Value arg) {
		writeValueToBinLog(arg, false);
	}
    
    private void doEnable() {
        editor.insertStmt(
            jimple.newInvokeStmt(
    	        jimple.newStaticInvokeExpr(C.M_WRITE_BIN_ENABLE)), false);
    }
     
    private void insertThrowError(Value arg, boolean insertAfter) {
		if(insertAfter)	    	
		    editor.insertStmtAfter(
	            jimple.newInvokeStmt(
	            	jimple.newStaticInvokeExpr(C.M_THROW_ERROR_STR, arg)));
	    else
	    	editor.insertStmt(
		        jimple.newInvokeStmt(
			        jimple.newStaticInvokeExpr(C.M_THROW_ERROR_STR, arg)), false);
	}
	
    private void insertThrowError() {
    	editor.insertStmt(
            jimple.newInvokeStmt(
    	        jimple.newStaticInvokeExpr(C.M_THROW_ERROR)), false);
    }
    
    
	private Local stackHeightLocal;
	
	// stackHeightLocal Lifecycle:
	// During instrumentation addStackHeightLocal() must be called for each 
	// method before initStackHeightLocal() OR writeStackHeight(...)
	// At runtime initStackHeightLocal() must be before writeStackHeight(...) 
	// in execution order.
	
	private void addStackHeightLocal() {
	    stackHeightLocal = editor.freshLocal(IntType.v(),"_sh_local");
	}
	
	private void initStackHeightLocal() {
	    editor.insertStmt(
	        jimple.newAssignStmt(
	            stackHeightLocal,
			    jimple.newStaticInvokeExpr(C.M_GET_STACK_HEIGHT)),
			false);
	}
	
	private void writeStackHeight(boolean insertAfter, boolean redirect) {
	    writeValueToBinLog(stackHeightLocal, insertAfter, redirect);
	}
	
	private void writeStackHeight(boolean insertAfter) {
	    writeStackHeight(insertAfter, true);
	}
	
	private void writeStackHeight() {
	    writeStackHeight(false);
	}
    
    // Modifies the static template file to chain s (which is not recorded 
    // dynamically) to all previous statements.
    private void chainToPreviousStmts(Stmt s) {
        long templateId = stmtToTemplateId.get(s);
        debugOutput.println("\t\tResolving template chaining for event #" +
            templateId + "...",
            Sensitivity.VERBOSE);
        List<Unit> preds = unitGraph.getPredsOf(s);
        if(preds.size() == 0) {
            stemplate.chainTemplates(methodStartTemplateId, templateId);
        }
        
        List<Stmt> virtualPreds = new ArrayList<Stmt>();
        Queue<Stmt> candidates = new LinkedList<Stmt>();
        for(Unit pred : preds) {
            assert pred instanceof Stmt;
            candidates.add((Stmt)pred);
        }
        
        while(candidates.size() > 0) {
            Stmt c = candidates.poll();
            if(paramOrThisIdentityStmt(c) || 
               c instanceof GotoStmt) {
                List<Unit> preds2 = unitGraph.getPredsOf(c);
                if(preds2.size() == 0) {
                    stemplate.chainTemplates(methodStartTemplateId, templateId);
                    continue;
                }
                for(Unit pred2 : preds2) {
                    assert pred2 instanceof Stmt;
                    candidates.add((Stmt)pred2);
                }
            } else {
                virtualPreds.add(c);
            }
        }
        
        for(Stmt p : virtualPreds) {
            if(!stmtToTemplateId.containsKey(p)) {
                debugOutput.println("\t\tMissing template ID for stmt: [" + p +
                    "] predecessor of [" + s + "].",
                    Sensitivity.WARNING);
            }
            assert stmtToTemplateId.containsKey(p);
            long pTemplateId = stmtToTemplateId.get(p);
            if(p instanceof IfStmt ||
               p instanceof LookupSwitchStmt ||
               p instanceof TableSwitchStmt) {
                try {
                    int branch = conditionalToTargetToBranch.get(p).get(s);
                    stemplate.chainTemplates(pTemplateId, templateId, branch);
                } catch(NullPointerException e) {
                    if(Config.g().DBGdumpJimple)
        			    Printer.v().printAll(classes, Config.g().debugJimpleDumpDir);
        			throw e;
                }
            } else {
                stemplate.chainTemplates(pTemplateId, templateId);
            }
        }
    }
	
	private void insertPrologue(Body body, List<Local> params) {
		SootMethod method = this.currentMethod;
		String methodSignature = this.currentOriginalMethod.getSignature();
        List<String> parameterLocals = new ArrayList<String>();
		
		Chain<Unit> units = body.getUnits().getNonPatchingChain();
		for (Unit u : units) {
			Stmt s = (Stmt) u;

			// Capture the list of parameter locals.
			if (paramOrThisIdentityStmt(s))
    		   continue;
			
			// Move BodyEditor back to just after the parameter statements
			assert editor.moveTo(s);
			
            debugOutput.println("\t\tAdding prologue for method: " + method,
                Sensitivity.VERBOSE);
            
            // If droidrecord.enableOnMethodStart is true, enable logging
            Config config = Config.g();
            if(config.enableOnMethodStart) {
                doEnable();
            }
            
			// Collect arguments and argument types
            List<String> locals = new ArrayList<String>();
			List<Immediate> params_i = new LinkedList<Immediate>();
            List<Type> params_t = new LinkedList<Type>();
			for(Local param : params) {
                locals.add(param.getName());
				if(currentOriginalMethod.getName().equals(SootMethod.constructorName) &&
				   param.getName().equals(this.currentThisLocalName)) {
					continue; // Skip uninitialized 'this' reference if method is a constructor
				}
				params_i.add(param);
                params_t.add(param.getType());
			}
            
            String localsStr = "[";
        	for(int i = 0; i < locals.size()-1; i++) {
    			localsStr += locals.get(i) + ",";
    		}
    		if(locals.size() > 0) {
    			localsStr += locals.get(locals.size()-1);
    		}
    		localsStr += "]";
            
            long pos = stmtToSourcePosition(units.getFirst());
            long jpos = stmtToJimplePosition(units.getFirst());
            
            if(!config.dontLog && 
               (config.policy.match(this.currentOriginalMethod, "MethodCallRecord") ||
               config.policy.match(this.currentOriginalMethod, "MethodStartRecord") ||
               config.policy.match(this.currentOriginalMethod, "MethodEndRecord") ||
               config.policy.match(this.currentOriginalMethod, "MethodReturnRecord") ||
               config.policy.match(this.currentOriginalMethod, "CaughtExceptionRecord"))) {
                // Store current stack height in a local variable only if we 
                // are recording events that need this value.
                initStackHeightLocal();
            }
            
            if(config.policy.match(this.currentOriginalMethod, "MethodStartRecord")) {
                // Write the static template for the method start event
                long eventID = stemplate.writeMethodStartTemplate(methodSignature, pos, jpos, localsStr, params_t);
                methodStartTemplateId = eventID;
                // Inject dynamic instrumentation code to record the event
                if(!config.dontLog) {
                    writeEventHeader(eventID, false, false);
                }
                
                if(!(config.methodStartOnly || config.dontLog)) {
                    writeParamsToBinLog(params_i, false, false);
                    writeStackHeight(false, false);
                }
            }
			
            debugOutput.println("\t\tAdded prologue.",
                Sensitivity.VERBOSE);
            
			// Stop once you've reached the start of actual code, and finished
			// with all the instrumentation.
			break;
		}
		
	}

	private void instrumentConds(Body body) {
        // collect all conditional branches in this method
        List<IfStmt> ifConds = new ArrayList<IfStmt>();
        List<LookupSwitchStmt> lSwitchConds = new ArrayList<LookupSwitchStmt>();
        List<TableSwitchStmt> tSwitchConds = new ArrayList<TableSwitchStmt>();
        Chain<Unit> units = body.getUnits();
        for (Unit u : units) {
            if (u instanceof IfStmt) {
                ifConds.add((IfStmt) u);
            } else if (u instanceof LookupSwitchStmt) {
                lSwitchConds.add((LookupSwitchStmt) u);
            } else if (u instanceof TableSwitchStmt) {
                tSwitchConds.add((TableSwitchStmt) u);
            }
        }
        
        for(IfStmt is : ifConds) {
            assert editor.moveTo(is);
            handleIfStmt(is);
        }
        for(LookupSwitchStmt lss : lSwitchConds) {
            assert editor.moveTo(lss);
            handleLookupSwitchStmt(lss);
        }
        for(TableSwitchStmt tss : tSwitchConds) {
            assert editor.moveTo(tss);
            handleTableSwitchStmt(tss);
        }
    }

	private void handleInvokeExpr(Stmt s) {
		debugOutput.println("\t\tInspecting invoke expression...",
            Sensitivity.VERBOSE);
	    InvokeExpr ie = s.getInvokeExpr();
	    SootMethodRef callee = ie.getMethodRef();
	    SootMethod caller = this.currentOriginalMethod;
		List<Immediate> args = new ArrayList<Immediate>();
    	List<Type> args_t = new ArrayList<Type>();
		List<String> locals = new ArrayList<String>();
		String calleeSignature = callee.getSignature();
		String callerSignature = caller.getSignature();
		
        debugOutput.println("\t\tInstrumenting invoke expression: " + caller + 
                           " calls " + callee + " at " + editor.getPosOriginal(),
                           Sensitivity.VERBOSE);
        // For instance methods other than the constructor, the object itself 
		// is the first argument. Constructors are treated differently, because 
		// passing uninitialized arguments is not allowed.
		if (ie instanceof InstanceInvokeExpr) {
			Immediate base = (Immediate) ((InstanceInvokeExpr) ie).getBase();
			if(!callee.name().equals(SootMethod.constructorName)) {
				args.add(base);
                args_t.add(base.getType());
			}
			assert base instanceof Local;
			locals.add(((Local)base).getName());
		}
		for (@SuppressWarnings("rawtypes") Iterator it = ie.getArgs().iterator(); 
			 it.hasNext();) {
			Immediate arg = (Immediate) it.next();
			args.add(arg);
            args_t.add(arg.getType());
			if(arg instanceof Local) {
				locals.add(((Local)arg).getName());
			} else {
				locals.add("_immediate_");
			}
		}
		
		String localsStr = "[";
		for(int i = 0; i < locals.size()-1; i++) {
			localsStr += locals.get(i) + ",";
		}
		if(locals.size() > 0) {
			localsStr += locals.get(locals.size()-1);
		}
		localsStr += "]";
        
        long pos = stmtToSourcePosition(s);
        long jpos = stmtToJimplePosition(s);
        
        // Write the static template for the method call event
        long eventID;
        if(Config.g().policy.match(this.currentOriginalMethod, "MethodCallRecord")) {
            eventID = stemplate.writeMethodCallTemplate(calleeSignature, 
                                                        callerSignature, 
                                                        localsStr, 
                                                        pos,
                                                        jpos, 
                                                        args_t);
            stmtToTemplateId.put(s, eventID);
            // Inject dynamic instrumentation code to record the event
            writeEventHeader(eventID);
            writeParamsToBinLog(args);
            writeStackHeight();
        }
        
        // Write the static template for the method return event
        
        // Note that insertStmtAfter calls must be given in reverse order
        // so writeValueToBinLog is inserted before writeEventHeader
        // Additionally, we need to know the type of the return value 
        // before encoding the template, as it determines whether a value will 
        // be logged at all.
        if(Config.g().policy.match(this.currentOriginalMethod, "MethodReturnRecord")) {
            writeStackHeight(true);
            Type type;
            boolean returnUsed = true;
            String localName;
            if (ie instanceof InstanceInvokeExpr &&
                callee.name().equals(SootMethod.constructorName)){
    			// For the constructor (object initializer), 
    			// return the constructed object.
    			assert (((InstanceInvokeExpr) ie).getBase()) instanceof Local;
    			assert callee.returnType().equals(VoidType.v());
    			Local base = (Local) ((InstanceInvokeExpr) ie).getBase();
    			writeValueToBinLog(base, true);
                type = base.getType();
                localName = base.getName();
    		} else if (callee.returnType().equals(VoidType.v())) {
    			type = VoidType.v();
                localName = "_void_";
    		} else if(s instanceof AssignStmt) {
    			AssignStmt as = (AssignStmt) s;
    			writeValueToBinLog(as.getLeftOp(), true);
                Value retVal = as.getLeftOp();
                type = retVal.getType();
                assert retVal instanceof Local;
                localName = ((Local)retVal).getName();
    		} else {
                type = null;
                returnUsed = false;
                localName = "";
    		}
            
            eventID = stemplate.writeMethodReturnTemplate(calleeSignature, 
                                                          callerSignature,
                                                          pos,
                                                          jpos,
                                                          type,
                                                          returnUsed,
                                                          localName);
            stmtToTemplateId.put(s, eventID);
            // Inject dynamic instrumentation code to record the event
            writeEventHeader(eventID, true);
        }
        
    	debugOutput.println("\t\tInstrumented invoke expression.",
            Sensitivity.VERBOSE);
	}
    
    private Stmt jumpOverGotos(Stmt s) {
        while(s instanceof GotoStmt) s = (Stmt)((GotoStmt)s).getTarget();
        return s;
    }
    
    private void handleIfStmt(IfStmt is)
    {
        debugOutput.println("\t\tInspecting if statement...",
            Sensitivity.VERBOSE);
        
        Value condition = is.getCondition();
        Stmt target = is.getTarget();
        
        Map<Stmt,Integer> targetToBranch = new HashMap<Stmt,Integer>();
        targetToBranch.put(jumpOverGotos(editor.next()),0);
        editor.moveTo(is); // Reset possition: inefficient but necessary because editor can't go back.
        targetToBranch.put(jumpOverGotos(target),1);
        
        debugOutput.println("\t\tInstrumenting if statement: if(" + 
            condition.toString() + ") goto " + target,
            Sensitivity.VERBOSE);
        
        if(Config.g().policy.match(this.currentOriginalMethod, "IfRecord")) {
    		long eventID = stemplate.writeIfStmtTemplate(condition.toString(),
                                                         this.currentOriginalMethod.getSignature(),
                                                         stmtToSourcePosition(is),
                                                         stmtToJimplePosition(is));
            stmtToTemplateId.put(is, eventID);
            writeEventHeader(eventID);
            // Write 0 after the if statement for branch not taken
            writeValueToBinLog(IntConstant.v(0), true);
            // Write 1 before jumped-to statement for branch taken, patch the jump
            assert editor.moveTo(target);
            Stmt newTarget = 
        	    jimple.newInvokeStmt(
    		        jimple.newStaticInvokeExpr(
    		            C.M_WRITE_BIN_PARAM_INT, 
    		            IntConstant.v(1)));
            editor.insertStmt(jimple.newGotoStmt(target), false); // Required to avoid reading an extra integer from the binlog when the code falls through.
            editor.insertStmt(newTarget, false);
            is.setTarget(newTarget);
            assert editor.moveTo(is);
            
            // Fix catch blocks/traps if needed
            for(Trap trap : editor.getTraps()) {
                if(trap.getEndUnit().equals(target)) {
                    trap.setEndUnit(newTarget);
                }
            }
            
            conditionalToTargetToBranch.put(is, targetToBranch);
        }
        
        debugOutput.println("\t\tInstrumented if statement.",
            Sensitivity.VERBOSE);
    }
    
    private void handleLookupSwitchStmt(LookupSwitchStmt lss)
    {
        debugOutput.println("\t\tInspecting lookup switch statement...",
            Sensitivity.VERBOSE);
        
        List<IntConstant> lookupValues = lss.getLookupValues();
        List<Unit> targets = lss.getTargets();
        Value key = lss.getKey();
        Unit defaultTarget = lss.getDefaultTarget();
        assert lookupValues.size() == targets.size();
        
        Map<Stmt,Integer> targetToBranch = new HashMap<Stmt,Integer>();
        
        debugOutput.println("\t\tInstrumenting lookup switch statement: " +
            "switch(" + key.toString() + ") ",
            Sensitivity.VERBOSE);
        debugOutput.println("\t\t{ //" +
            " # lookup values: " + lookupValues.size() + 
            ", # targets: " + targets.size(),
            Sensitivity.SUPERVERBOSE);
        for(int i = 0; i < lookupValues.size(); i++) {
            debugOutput.println("\t\t\t " + lookupValues.get(i).toString() + 
                " => " + targets.get(i).toString(),
                Sensitivity.SUPERVERBOSE);
        }
        debugOutput.println("\t\t\t DEFAULT => " + defaultTarget.toString(),
            Sensitivity.SUPERVERBOSE);
        debugOutput.println("\t\t}",
            Sensitivity.SUPERVERBOSE);
        
        if(Config.g().policy.match(this.currentOriginalMethod, "SwitchRecord")) {    
            long eventID = stemplate.writeSwitchStmtTemplate(key.toString(),
                                                    lookupValues,
                                                    key.getType(),
                                                    this.currentOriginalMethod.getSignature(),
                                                    stmtToSourcePosition(lss),
                                                    stmtToJimplePosition(lss));
            stmtToTemplateId.put(lss, eventID);
            writeEventHeader(eventID);
            writeValueToBinLog(key);
            
            // Write -1 for the default target
            Stmt target, newTarget;
            if(defaultTarget instanceof Stmt) {
                target = (Stmt)defaultTarget;
                targetToBranch.put(jumpOverGotos(target),-1);
                assert editor.moveTo(target);
                newTarget = 
                    jimple.newInvokeStmt(
        		        jimple.newStaticInvokeExpr(
        		            C.M_WRITE_BIN_PARAM_INT, 
        		            IntConstant.v(-1)));
                editor.insertStmt(jimple.newGotoStmt(target), false); // Required to avoid reading an extra integer from the binlog when the code falls through.
                editor.insertStmt(newTarget, false);
                lss.setDefaultTarget(newTarget);
            }
            // Other branch target codes match their order in the lookupValues list
            for(int i = 0; i < targets.size(); i++) {
                Unit u = lss.getTarget(i);
                if(!(u instanceof Stmt)) {
                    throw new Error("LookupSwitchStmt.getTargets() includes " +
                                    "Unit of unexpected type (not a Stmt): " + u);
                }
                target = (Stmt) u;
                targetToBranch.put(jumpOverGotos(target),i);
                assert editor.moveTo(target);
                newTarget = 
                    jimple.newInvokeStmt(
            	        jimple.newStaticInvokeExpr(
        		            C.M_WRITE_BIN_PARAM_INT, 
        		            IntConstant.v(i)));
                editor.insertStmt(jimple.newGotoStmt(target), false);
                editor.insertStmt(newTarget, false);
                lss.setTarget(i, newTarget);
            }
            assert editor.moveTo(lss);
            
            conditionalToTargetToBranch.put(lss, targetToBranch);
        }
        
        debugOutput.println("\t\tInstrumented lookup switch statement.",
            Sensitivity.VERBOSE);
    }
    
    private void handleTableSwitchStmt(TableSwitchStmt tss)
    {
        debugOutput.println("\t\tInspecting table switch statement...",
            Sensitivity.VERBOSE);
        
        List<IntConstant> lookupValues = new ArrayList<IntConstant>();
        for(int i = tss.getLowIndex(); i <= tss.getHighIndex(); i++) {
            lookupValues.add(IntConstant.v(i));
        }
        List<Unit> targets = tss.getTargets();
        Value key = tss.getKey();
        Unit defaultTarget = tss.getDefaultTarget();
        assert lookupValues.size() == targets.size();
        
        Map<Stmt,Integer> targetToBranch = new HashMap<Stmt,Integer>();
        
        debugOutput.println("\t\tInstrumenting table switch statement: " +
            "switch(" + key.toString() + ") ",
            Sensitivity.VERBOSE);
        debugOutput.println("\t\t{ //" +
            " # table values: " + lookupValues.size() + 
            ", # targets: " + targets.size(),
            Sensitivity.SUPERVERBOSE);
        for(int i = 0; i < lookupValues.size(); i++) {
            debugOutput.println("\t\t\t " + lookupValues.get(i).toString() + 
                " => " + targets.get(i).toString(),
                Sensitivity.SUPERVERBOSE);
        }
        debugOutput.println("\t\t\t DEFAULT => " + defaultTarget.toString(),
            Sensitivity.SUPERVERBOSE);
        debugOutput.println("\t\t}",
            Sensitivity.SUPERVERBOSE);
        
        if(Config.g().policy.match(this.currentOriginalMethod, "SwitchRecord")) {     
            long eventID = stemplate.writeSwitchStmtTemplate(key.toString(),
                                                    lookupValues,
                                                    key.getType(),
                                                    this.currentOriginalMethod.getSignature(),
                                                    stmtToSourcePosition(tss),
                                                    stmtToJimplePosition(tss));
            stmtToTemplateId.put(tss, eventID);
            writeEventHeader(eventID);
            writeValueToBinLog(key);
            
            // Write -1 for the default target
            Stmt target, newTarget;
            if(defaultTarget instanceof Stmt) {
                target = (Stmt)defaultTarget;
                targetToBranch.put(jumpOverGotos(target),-1);
                assert editor.moveTo(target);
                newTarget = 
                    jimple.newInvokeStmt(
            	        jimple.newStaticInvokeExpr(
        		            C.M_WRITE_BIN_PARAM_INT, 
        		            IntConstant.v(-1)));
                editor.insertStmt(jimple.newGotoStmt(target), false);
                editor.insertStmt(newTarget, false);
                tss.setDefaultTarget(newTarget);
            }
            // Other branch target codes match their order in the lookupValues list
            for(int i = 0; i < targets.size(); i++) {
                Unit u = tss.getTarget(i);
                if(!(u instanceof Stmt)) {
                    throw new Error("TableSwitchStmt.getTargets() includes " +
                                    "Unit of unexpected type (not a Stmt): " + u);
                }
                target = (Stmt) u;
                targetToBranch.put(jumpOverGotos(target),i);
                assert editor.moveTo(target);
                newTarget = 
                    jimple.newInvokeStmt(
            	        jimple.newStaticInvokeExpr(
        		            C.M_WRITE_BIN_PARAM_INT, 
        		            IntConstant.v(i)));
                editor.insertStmt(jimple.newGotoStmt(target), false);
                editor.insertStmt(newTarget, false);
                tss.setTarget(i, newTarget);
            }
            assert editor.moveTo(tss);
            
            conditionalToTargetToBranch.put(tss, targetToBranch);
        }
        
        debugOutput.println("\t\tInstrumented table switch statement.",
            Sensitivity.VERBOSE);
    }
	
	@Override public void caseAssignStmt(AssignStmt as)
	{
		Value rightOp = as.getRightOp();
		Value leftOp = as.getLeftOp();

		// we only handle expression types found in Jimple IR
		if (rightOp instanceof BinopExpr) {
			handleBinopStmt(as, (Local) leftOp, (BinopExpr) rightOp);
		}
		else if (rightOp instanceof NegExpr) {
			handleNegStmt(as, (Local) leftOp, (NegExpr) rightOp);
		}
		else if (leftOp instanceof FieldRef) {
			handleStoreStmt(as, (FieldRef) leftOp, (Immediate) rightOp);
		}
		else if (rightOp instanceof FieldRef) {
			handleLoadStmt(as, (Local) leftOp, (FieldRef) rightOp);
		}
		else if (leftOp instanceof ArrayRef) {
			handleArrayStoreStmt(as, (ArrayRef) leftOp, (Immediate) rightOp);
		}
		else if (rightOp instanceof ArrayRef) {
			handleArrayLoadStmt(as, (Local) leftOp, (ArrayRef) rightOp);
		}
		else if (rightOp instanceof LengthExpr) {
			handleArrayLengthStmt(as, (Local) leftOp, (LengthExpr) rightOp);
		}
		else if (rightOp instanceof InstanceOfExpr) {
			handleInstanceOfStmt(as, (Local) leftOp, (InstanceOfExpr) rightOp);
		}
		else if (rightOp instanceof CastExpr) {
			handleCastExpr(as, (Local) leftOp, (CastExpr) rightOp);
		}
		else if (rightOp instanceof NewExpr) {
			handleNewStmt(as, (Local) leftOp, (NewExpr) rightOp);
		}
		else if (rightOp instanceof NewArrayExpr) {
			handleNewArrayStmt(as, (Local) leftOp, (NewArrayExpr) rightOp);
		}
		else if (rightOp instanceof NewMultiArrayExpr) {
			handleNewMultiArrayStmt(as, (Local) leftOp, (NewMultiArrayExpr) rightOp);
		}
		else if (rightOp instanceof Immediate && leftOp instanceof Local) {
			handleSimpleAssignStmt(as, (Local) leftOp, (Immediate) rightOp);
		} else {
			// Is there any other type of assign statement?
			debugOutput.println(
					String.format(
			"ASSERTION ERROR: Unanticipated assignment statement found: " +
			"left operand is of type %s, right operand is of type %s. " +
			"Found at: %s:(src)%d:(original_bytecode)%d:(modified_bytecode)%d.", 
						leftOp.getClass().toString(), 
						rightOp.getClass().toString(),
						this.currentOriginalMethod.getSignature(),
						stmtToSourcePosition(as),
						editor.getPosOriginal(),
						editor.getPosChanged()),
					Sensitivity.ERROR);
			Config config = Config.g();
			if(config.DBGdumpJimple)
			    Printer.v().printAll(classes, config.debugJimpleDumpDir);
			System.exit(1);
		}
	}

	private void handleBinopStmt(Stmt s, Local leftOp, BinopExpr binExpr) {
        debugOutput.println("\t\tInspecting binary operation statement...",
            Sensitivity.VERBOSE);
        
        String symbol = binExpr.getSymbol();
        Value op1 = binExpr.getOp1();
        Value op2 = binExpr.getOp2();
            
	    debugOutput.println("\t\tInstrumenting binary operation statement: " + 
            leftOp.toString() + " = " + op1 + " " + symbol + " " + op2  + 
            " at " + editor.getPosOriginal(),
            Sensitivity.VERBOSE);
        
        String op1Local;
        if(op1 instanceof Local) op1Local = ((Local)op1).getName();
        else op1Local = "_immediate_";
        
        String op2Local;
        if(op2 instanceof Local) op2Local = ((Local)op2).getName();
        else op2Local = "_immediate_";
        
        if(Config.g().policy.match(this.currentOriginalMethod, "BinaryOperationRecord")) { 
            long eventID = stemplate.writeBinopStmtTemplate(leftOp.getName(),
            										op1Local,
                                                    op2Local,
                                                    symbol,
                                                    this.currentOriginalMethod.getSignature(),
                                                    stmtToSourcePosition(s),
                                                    stmtToJimplePosition(s));
    	    stmtToTemplateId.put(s, eventID);
            stmtsToBeChained.add(s);
        }
        
        debugOutput.println("\t\tInstrumented binary operation statement.",
            Sensitivity.VERBOSE);
	}

	private void handleNegStmt(Stmt s, Local leftOp, NegExpr negExpr) {
        
        Value rightOp = negExpr.getOp();
        
        debugOutput.println("\t\tInstrumenting negation assignment statement: " + 
            leftOp.toString() + " = !" + rightOp.toString()  + 
            " at " + editor.getPosOriginal(),
            Sensitivity.VERBOSE);
        
        String var;
        if(rightOp instanceof Local) var = ((Local)rightOp).getName();
        else var = "_immediate_";
        
        if(Config.g().policy.match(this.currentOriginalMethod, "NegationRecord")) {
            long eventID = stemplate.writeNegStmtTemplate(leftOp.getName(),
            										var,
                                                    this.currentOriginalMethod.getSignature(),
                                                    stmtToSourcePosition(s),
                                                    stmtToJimplePosition(s));
    	    stmtToTemplateId.put(s, eventID);
            stmtsToBeChained.add(s);
        }
        
        debugOutput.println("\t\tInstrumented negation assignment statement.",
            Sensitivity.VERBOSE);
	}

	private void handleSimpleAssignStmt(Stmt s, Local leftOp, Immediate rightOp) {
        debugOutput.println("\t\tInstrumenting simple assignment statement: " + 
            leftOp.toString() + " = " + rightOp.toString()  + 
            " at " + editor.getPosOriginal(),
            Sensitivity.VERBOSE);
        
        String var;
        if(rightOp instanceof Local) var = ((Local)rightOp).getName();
        else var = "_immediate_";
        
        if(Config.g().policy.match(this.currentOriginalMethod, "SimpleAssignmentRecord")) {
            long eventID = stemplate.writeSimpleAssignStmtTemplate(leftOp.getName(),
        											var,
                                                    this.currentOriginalMethod.getSignature(),
                                                    stmtToSourcePosition(s),
                                                    stmtToJimplePosition(s));
    	    stmtToTemplateId.put(s, eventID);
            stmtsToBeChained.add(s);
        }
        
        debugOutput.println("\t\tInstrumented simple assignment statement.",
            Sensitivity.VERBOSE);
	}

	private void handleStoreStmt(Stmt s, FieldRef leftOp, Immediate rightOp) {
        debugOutput.println("\t\tInspecting store statement...",
            Sensitivity.VERBOSE);
        
		SootMethodRef recordLoadRef;
		boolean isInstance = leftOp instanceof InstanceFieldRef;
		boolean isInitialized = true;
		Value base = null;
        Type base_t = null;
		
		if(isInstance && 
				this.currentOriginalMethod.getName().equals(SootMethod.constructorName)) {
			base = ((InstanceFieldRef)leftOp).getBase();
			if(base instanceof Local &&
					((Local)base).getName().equals(this.currentThisLocalName)){
				isInitialized = false;				
			}
		}
        
        debugOutput.println("\t\tInstrumenting store statement: " + 
            leftOp.toString() + " = " + rightOp.toString()  + "[" +
            (isInstance ? "instance" : "") + 
            (isInitialized ? ", initialized" : "") + 
            "] at " + editor.getPosOriginal(),
            Sensitivity.VERBOSE);
		
		//// WORKAROUND/HACK:
		// A type coercion error is thrown at the Dx stage whenever soot attempts
		// to process a field assignment where all the following is true:
		// - The field is of type java.lang.Object
		// - The stored value is a string literal
		// - The field has both static and final modifiers
		// To work around this problem, we selectively remove the final 
		// modifier for these fields.
		int modifiers = leftOp.getField().getModifiers();
		if(leftOp.getType().toString().equals("java.lang.Object") && 
			rightOp.getType().toString().equals("java.lang.String") &&
			((modifiers & (soot.Modifier.FINAL|soot.Modifier.STATIC)) != 0)) {
			leftOp.getField().setModifiers(modifiers & ~soot.Modifier.FINAL);
		}
		
        String var;
        if(rightOp instanceof Local) var = ((Local)rightOp).getName();
    	else var = "_immediate_";
        
        if(isInstance && isInitialized){
            base = ((InstanceFieldRef)leftOp).getBase();
    		base_t = base.getType();
        }
		
		if((isInstance && Config.g().policy.match(this.currentOriginalMethod, "StoreInstanceRecord")) ||
		   (!isInstance && Config.g().policy.match(this.currentOriginalMethod, "StoreStaticRecord"))) {
    		long eventID = stemplate.writeStoreStmtTemplate(leftOp.getField().getSignature(),
    														 var,
                                                             this.currentOriginalMethod.getSignature(),
                                                             stmtToSourcePosition(s),
                                                             stmtToJimplePosition(s), 
                                                             base_t,
    														 rightOp.getType(),
    														 isInstance,
    														 isInitialized);
            stmtToTemplateId.put(s, eventID);
            writeEventHeader(eventID);
    		if(isInstance && isInitialized) {
                assert base != null;
    			writeValueToBinLog(base);
    		}
            writeValueToBinLog(rightOp);
        }
		   
        debugOutput.println("\t\tInstrumented store statement.",
            Sensitivity.VERBOSE);
	}

	private void handleLoadStmt(Stmt s, Local leftOp, FieldRef rightOp) {
		// insertAfter as the load needs to have already happened 
		// for the value to be available
		// => add instrumentation in reverse order
        
        debugOutput.println("\t\tInspecting load statement...",
            Sensitivity.VERBOSE);
		SootMethodRef recordLoadRef;
		boolean isInstance = rightOp instanceof InstanceFieldRef;
		boolean isInitialized = true;
		Value base = null;
        Type base_t = null;
		
		if(isInstance && 
		   this.currentOriginalMethod.getName().equals(SootMethod.constructorName)) {
			base = ((InstanceFieldRef)rightOp).getBase();
			if(base instanceof Local &&
				((Local)base).getName().equals(this.currentThisLocalName)){
				isInitialized = false;
			}
		}
        
        debugOutput.println("\t\tInstrumenting load statement: " + 
            leftOp.toString() + " = " + rightOp.toString()  + "[" +
            (isInstance ? "instance" : "") + 
            (isInitialized ? ", initialized" : "") + 
            "] at " + editor.getPosOriginal(),
            Sensitivity.VERBOSE);
        
        if(isInstance && isInitialized) {
    		base = ((InstanceFieldRef)rightOp).getBase();
			base_t = base.getType();
		}
		
		if((isInstance && Config.g().policy.match(this.currentOriginalMethod, "LoadInstanceRecord")) ||
		   (!isInstance && Config.g().policy.match(this.currentOriginalMethod, "LoadStaticRecord"))) {
    		long eventID = stemplate.writeLoadStmtTemplate(rightOp.getField().getSignature(),
    													   leftOp.getName(),
                                                           this.currentOriginalMethod.getSignature(),
                                                           stmtToSourcePosition(s),
                                                           stmtToJimplePosition(s), 
                                                           base_t,
    													   leftOp.getType(),
    													   isInstance,
    													   isInitialized);
            stmtToTemplateId.put(s, eventID);
            if(isInstance) {
                // When doing an instance load, record base first as it might 
                // have changed, then perform instruction, then record result.
        		writeEventHeader(eventID);
        		if(isInitialized) {            
                    assert base != null;
        			writeValueToBinLog(base);
        		}
                writeValueToBinLog(leftOp, true);
            } else {
                // When doing static load, all recording should happen after 
                // the instruction, since a class intializer might be called 
                // in between. There is no base.
                writeValueToBinLog(leftOp, true);
                writeEventHeader(eventID, true);
                
            }
	    }
        
        debugOutput.println("\t\tInstrumented load statement.",
            Sensitivity.VERBOSE);
	}

	private void handleNewStmt(Stmt s, Local leftOp, NewExpr rightOp) {
	    if(Config.g().policy.match(this.currentOriginalMethod, "NewObjectRecord")) {
    		long eventID = stemplate.writeNewObjectTemplate(rightOp.getType().toString(),
    													    leftOp.getName(),
                                                            this.currentOriginalMethod.getSignature(),
                                                            stmtToSourcePosition(s),
                                                            stmtToJimplePosition(s));
    		stmtToTemplateId.put(s, eventID);
            writeEventHeader(eventID);
	    }
	}

	private void handleNewArrayStmt(Stmt s, Local leftOp, NewArrayExpr rightOp) {
	    if(Config.g().policy.match(this.currentOriginalMethod, "NewArrayRecord")) {
    		long eventID = stemplate.writeNewArrayTemplate(leftOp.getName(),
                                                           this.currentOriginalMethod.getSignature(),
                                                           stmtToSourcePosition(s),
                                                           stmtToJimplePosition(s),
                                                           leftOp.getType(),
                                                           1);
            stmtToTemplateId.put(s, eventID);
            writeValueToBinLog(rightOp.getSize(), true);
            writeValueToBinLog(leftOp, true);
            writeEventHeader(eventID, true);
	    }
	}

	private void handleNewMultiArrayStmt(Stmt s, Local leftOp, NewMultiArrayExpr rightOp) {
	    if(Config.g().policy.match(this.currentOriginalMethod, "NewArrayRecord")) {
            long eventID = stemplate.writeNewArrayTemplate(leftOp.getName(),
                                                           this.currentOriginalMethod.getSignature(),
                                                           stmtToSourcePosition(s),
                                                           stmtToJimplePosition(s),
                                                           leftOp.getType(),
                                                           rightOp.getSizeCount());
    		stmtToTemplateId.put(s, eventID);
            // Statements are inyected in reverse order, since we are using
    		// insertAfter.
    		for(int i = rightOp.getSizeCount()-1; i >= 0; i--) {
                writeValueToBinLog(rightOp.getSize(i), true);
    		}
            writeValueToBinLog(leftOp, true);
            writeEventHeader(eventID, true);
	    }
	}

	private void handleCastExpr(Stmt s, Local leftOp, CastExpr expr) {
	    Value rightOp = expr.getOp();
        Type type = expr.getCastType();
        
        debugOutput.println("\t\tInstrumenting cast statement: " + 
            leftOp.toString() + " = (" + type + ") "+ rightOp.toString() + 
            " at " + editor.getPosOriginal(),
            Sensitivity.VERBOSE);
        
        String var;
        if(rightOp instanceof Local) var = ((Local)rightOp).getName();
        else var = "_immediate_";
        
	    if(Config.g().policy.match(this.currentOriginalMethod, "CastRecord")) {
            long eventID = stemplate.writeCastStmtTemplate(leftOp.getName(),
                									var,
                                                    type.toString(),
                                                    this.currentOriginalMethod.getSignature(),
                                                    stmtToSourcePosition(s),
                                                    stmtToJimplePosition(s));
    	    stmtToTemplateId.put(s, eventID);
            stmtsToBeChained.add(s);
	    }
        
        debugOutput.println("\t\tInstrumented cast statement.",
            Sensitivity.VERBOSE);
	}

	private void handleArrayLoadStmt(Stmt s, Local leftOp, ArrayRef rightOp) {
		Local arrayLocal;
		Value base = rightOp;
		ArrayRef baseAR;
        List<Value> arrayIndexes = new ArrayList<Value>();
		while(base instanceof ArrayRef) {
			baseAR = (ArrayRef) base;
            arrayIndexes.add(baseAR.getIndex());
			base = baseAR.getBase();
		}
		assert (base instanceof Local);
		arrayLocal = (Local) base;
		
	    if(Config.g().policy.match(this.currentOriginalMethod, "ArrayLoadRecord")) {
	    	Type[] dimentionTypes = new Type[arrayIndexes.size()];
	    	for(int i = 0; i < arrayIndexes.size(); ++i) {
	    		dimentionTypes[i] = arrayIndexes.get(i).getType();
	    	}
            long eventID = stemplate.writeArrayLoadTemplate(arrayLocal.getName(),
                                                           leftOp.getName(),
                                                           this.currentOriginalMethod.getSignature(),
                                                           stmtToSourcePosition(s),
                                                           stmtToJimplePosition(s),
                                                           base.getType(),
                                                           dimentionTypes,
                                                           leftOp.getType());
            stmtToTemplateId.put(s, eventID);
            writeValueToBinLog(leftOp, true);
            for(int i = arrayIndexes.size()-1; i >= 0; i--) {
                writeValueToBinLog(arrayIndexes.get(i), true);
    		}
            writeValueToBinLog(base, true);
            writeEventHeader(eventID, true);
	    }
	}

	private void handleArrayLengthStmt(Stmt s, Local leftOp, LengthExpr rightOp) {
		Local arrayLocal;
		Value base = rightOp.getOp();
		while(base instanceof ArrayRef) {
			base = ((ArrayRef) base).getBase();
		}
		assert (base instanceof Local);
		arrayLocal = (Local) base;
        base = rightOp.getOp();
		
	    if(Config.g().policy.match(this.currentOriginalMethod, "ArrayLengthRecord")) {
            long eventID = stemplate.writeArrayLengthTemplate(arrayLocal.getName(),
                                                           leftOp.getName(),
                                                           this.currentOriginalMethod.getSignature(),
                                                           stmtToSourcePosition(s),
                                                           stmtToJimplePosition(s),
                                                           base.getType(),
                                                           leftOp.getType());
            stmtToTemplateId.put(s, eventID);
            writeValueToBinLog(leftOp, true);
            writeValueToBinLog(base, true);
            writeEventHeader(eventID, true);
	    }
	}

	private void handleArrayStoreStmt(Stmt s, ArrayRef leftOp, Immediate rightOp) {
		Local arrayLocal;
		Value base = leftOp;
		ArrayRef baseAR;
        List<Value> arrayIndexes = new ArrayList<Value>();
		while(base instanceof ArrayRef) {
			baseAR = (ArrayRef) base;
            arrayIndexes.add(baseAR.getIndex());
			base = baseAR.getBase();
		}
		assert (base instanceof Local);
		arrayLocal = (Local) base;
		
		base = leftOp.getBase();
		
		String var;
		if(rightOp instanceof Local) var = ((Local)rightOp).getName();
		else var = "_immediate_";
        
	    if(Config.g().policy.match(this.currentOriginalMethod, "ArrayStoreRecord")) {
	    	Type[] dimentionTypes = new Type[arrayIndexes.size()];
	    	for(int i = 0; i < arrayIndexes.size(); ++i) {
	    		dimentionTypes[i] = arrayIndexes.get(i).getType();
	    	}
            long eventID = stemplate.writeArrayStoreTemplate(arrayLocal.getName(),
                                                           var,
                                                           this.currentOriginalMethod.getSignature(),
                                                           stmtToSourcePosition(s),
                                                           stmtToJimplePosition(s),
                                                           base.getType(),
                                                           dimentionTypes,
                                                           rightOp.getType());
            stmtToTemplateId.put(s, eventID);
            writeValueToBinLog(rightOp, true);
            for(int i = arrayIndexes.size()-1; i >= 0; i--) {
                writeValueToBinLog(arrayIndexes.get(i), true);
    		}
            writeValueToBinLog(base, true);
            writeEventHeader(eventID, true);
	    }
	}

	private void handleInstanceOfStmt(Stmt s, Local leftOp, InstanceOfExpr expr) {
        Value rightOp = expr.getOp();
        Type type = expr.getCheckType();
        
        debugOutput.println("\t\tInstrumenting instanceof statement: " + 
            leftOp.toString() + " = " + rightOp.toString() + " instanceof " + 
            type + " at " + editor.getPosOriginal(),
            Sensitivity.VERBOSE);
        
        String var;
        if(rightOp instanceof Local) var = ((Local)rightOp).getName();
        else var = "_immediate_";
        
	    if(Config.g().policy.match(this.currentOriginalMethod, "InstanceOfRecord")) {
            long eventID = stemplate.writeInstanceOfStmtTemplate(leftOp.getName(),
            										var,
                                                    type.toString(),
                                                    this.currentOriginalMethod.getSignature(),
                                                    stmtToSourcePosition(s),
                                                    stmtToJimplePosition(s));
    	    stmtToTemplateId.put(s, eventID);
            stmtsToBeChained.add(s);
	    }
        
        debugOutput.println("\t\tInstrumented instanceof statement.",
            Sensitivity.VERBOSE);
	}
	
	@Override public void caseIdentityStmt(IdentityStmt is)
	{
	    Value op = is.getRightOp();
		if (op instanceof CaughtExceptionRef)
		{
		    handleCaughtExceptionStmt(is, (Local) is.getLeftOp(), 
		    						  (CaughtExceptionRef) op);
		}
		else
		{
		    // instrument(SootMethod method) treats IdentityStmt in a special way
	        // when the right operand is not an Exception Reference.
			assert false : "unexpected " + is;
		}
	}

	private void handleCaughtExceptionStmt(Stmt s, Local leftOp, CaughtExceptionRef ref) {
	    if(Config.g().policy.match(this.currentOriginalMethod, "CaughtExceptionRecord")) {
            long eventID = stemplate.writeCaughtExceptionTemplate(
                                        leftOp.getName(),
                                        this.currentOriginalMethod.getSignature(),
                                        stmtToSourcePosition(s),
                                        stmtToJimplePosition(s),
                                        leftOp.getType());
            stmtToTemplateId.put(s, eventID);
            writeStackHeight(true);
            writeValueToBinLog(leftOp, true);
            writeEventHeader(eventID, true);
	    }
	}

	@Override public void caseThrowStmt(ThrowStmt ts)
	{
		Value exception = ts.getOp();
        
	    if(Config.g().policy.match(this.currentOriginalMethod, "ThrowExceptionRecord")) {
            long eventID = stemplate.writeThrowExceptionTemplate(
                                        this.currentOriginalMethod.getSignature(),
                                        stmtToSourcePosition(ts),
                                        stmtToJimplePosition(ts),
                                        exception.getType());
            stmtToTemplateId.put(ts, eventID);
            writeEventHeader(eventID);
            writeValueToBinLog(exception);
	    }
	}
	
	@Override public void caseReturnStmt(ReturnStmt rs)
	{
	    handleReturnStmt(rs);
	}

	private void handleReturnStmt(ReturnStmt rs) {
		SootMethod method = this.currentMethod;
		String methodSignature = this.currentOriginalMethod.getSignature();
		
        Value retVal = rs.getOp();
        String localName;
        if(retVal instanceof Local) localName = ((Local)retVal).getName();
        else localName = "_immediate_";
        
	    if(Config.g().policy.match(this.currentOriginalMethod, "MethodEndRecord")) {
            long eventID = stemplate.writeMethodEndTemplate(methodSignature, 
                                                            stmtToSourcePosition(rs),
                                                            stmtToJimplePosition(rs), 
                                                            method.getReturnType(),
                                                            localName);
            writeEventHeader(eventID);
            writeValueToBinLog(retVal, false, true, method.getReturnType());
            writeStackHeight();
	    }
	}
	
	@Override public void caseReturnVoidStmt(ReturnVoidStmt rs)
	{
	    handleReturnVoidStmt(rs);
	}

	private void handleReturnVoidStmt(ReturnVoidStmt rs) {
        SootMethod method = this.currentMethod;
		String methodSignature = this.currentOriginalMethod.getSignature();
        Local thisP = null;
		Type returnType;
        
        String localName;
		if(method.getName().equals(SootMethod.constructorName)) {
			// In the case of the constructor, return the constructed 
			// object's value.
			thisP = method.getActiveBody().getThisLocal();
			assert thisP.getName().equals(this.currentThisLocalName);
            returnType = thisP.getType();
            localName = thisP.getName();
		} else {
            returnType = VoidType.v();
            localName = "_void_";
		}
        
	    if(Config.g().policy.match(this.currentOriginalMethod, "MethodEndRecord")) {
            long eventID = stemplate.writeMethodEndTemplate(methodSignature, 
                                                            stmtToSourcePosition(rs),
                                                            stmtToJimplePosition(rs),
                                                            returnType,
                                                            localName);
            stmtToTemplateId.put(rs, eventID);
            writeEventHeader(eventID);
            if(thisP != null)
                writeValueToBinLog(thisP, false, true, returnType);
            writeStackHeight();
	    }
	}
    
    @Override public void caseEnterMonitorStmt(EnterMonitorStmt ems)
    {
	    handleEnterMonitorStmt(ems);
	}

	private void handleEnterMonitorStmt(EnterMonitorStmt s) {
        debugOutput.println("\t\tInstrumenting enter monitor statement: " + 
            "entermonitor " + s.getOp().toString()  + 
            " at " + editor.getPosOriginal(),
            Sensitivity.VERBOSE);
        
        String var;
        if(s.getOp() instanceof Local) var = ((Local)s.getOp()).getName();
        else var = "_immediate_";
        
        if(Config.g().policy.match(this.currentOriginalMethod, "EnterMonitorRecord")) {
            long eventID = stemplate.writeEnterMonitorStmtTemplate(var,
                                                    this.currentOriginalMethod.getSignature(),
                                                    stmtToSourcePosition(s),
                                                    stmtToJimplePosition(s));
    	    stmtToTemplateId.put(s, eventID);
            stmtsToBeChained.add(s);
        }
        
        debugOutput.println("\t\tInstrumented enter monitor statement.",
            Sensitivity.VERBOSE);
	}
    
    @Override public void caseExitMonitorStmt(ExitMonitorStmt ems)
    {
        handleExitMonitorStmt(ems);
	}

	private void handleExitMonitorStmt(ExitMonitorStmt s) {
        debugOutput.println("\t\tInstrumenting exit monitor statement: " + 
            "exitmonitor " + s.getOp().toString()  + 
            " at " + editor.getPosOriginal(),
            Sensitivity.VERBOSE);
        
        String var;
        if(s.getOp() instanceof Local) var = ((Local)s.getOp()).getName();
        else var = "_immediate_";
        
        if(Config.g().policy.match(this.currentOriginalMethod, "ExitMonitorRecord")) {
            long eventID = stemplate.writeExitMonitorStmtTemplate(var,
                                                    this.currentOriginalMethod.getSignature(),
                                                    stmtToSourcePosition(s),
                                                    stmtToJimplePosition(s));
    	    stmtToTemplateId.put(s, eventID);
            stmtsToBeChained.add(s);
        }
        
        debugOutput.println("\t\tInstrumented exit monitor statement.",
            Sensitivity.VERBOSE);
	}

	public static boolean paramOrThisIdentityStmt(Stmt s)
	{
		if (!(s instanceof IdentityStmt))
			return false;
		return !(((IdentityStmt) s).getRightOp() instanceof CaughtExceptionRef);
	}
	
	protected boolean isInstrumented(SootClass klass)
	{
		return classes.contains(klass);
	}
}
