
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.instrumentor.recorder;

import soot.RefType;
import soot.SootClass;
import soot.SootMethodRef;
import soot.Scene;
import soot.Type;

public class RecorderInstrumentorConstants {

    private static RecorderInstrumentorConstants recorderInstrumentorConstants;

    public final String RECORDER_CLASS_NAME =
                                    "edu.stanford.droidrecord.droidrecordlib.TraceRecorder";
    public final String STRING_CLASS_NAME = "java.lang.String";
    public final String OBJECT_CLASS_NAME = "java.lang.Object";
    public final String CLASS_CLASS_NAME = "java.lang.Class";
    public final String VMSTACK_CLASS_NAME = "dalvik.system.VMStack";
    private final String VMSTACK_FAST_STACK_HEIGHT_SUBSIGNATURE = "int lazaroGetCurrentThreadStackDepth()";

    public final Type RECORDER_TYPE = RefType.v(RECORDER_CLASS_NAME);
    public final Type STRING_TYPE = RefType.v(STRING_CLASS_NAME);
    public final Type OBJECT_TYPE = RefType.v(OBJECT_CLASS_NAME);
    public final Type CLASS_TYPE = RefType.v(CLASS_CLASS_NAME);
    
    public final SootMethodRef M_WRITE_BIN_EVENT_HEADER;
    public final SootMethodRef M_WRITE_BIN_PARAM_OBJ;
    public final SootMethodRef M_WRITE_BIN_PARAM_ARRAY;
    public final SootMethodRef M_WRITE_BIN_PARAM_CLASS;
    public final SootMethodRef M_WRITE_BIN_PARAM_NULL;
    public final SootMethodRef M_WRITE_BIN_PARAM_STR;
    public final SootMethodRef M_WRITE_BIN_PARAM_LONG;
    public final SootMethodRef M_WRITE_BIN_PARAM_INT;
    public final SootMethodRef M_WRITE_BIN_PARAM_SHORT;
    public final SootMethodRef M_WRITE_BIN_PARAM_BYTE;
    public final SootMethodRef M_WRITE_BIN_PARAM_DOUBLE;
    public final SootMethodRef M_WRITE_BIN_PARAM_FLOAT;
    public final SootMethodRef M_WRITE_BIN_PARAM_CHAR;
    public final SootMethodRef M_WRITE_BIN_PARAM_BOOL;
    public final SootMethodRef M_WRITE_BIN_TICK;
    public final SootMethodRef M_WRITE_BIN_CHECK_ENABLED;
    public final SootMethodRef M_WRITE_BIN_IS_ENABLED;
    public final SootMethodRef M_WRITE_BIN_ENABLE;
    public final SootMethodRef M_WRITE_BIN_STACK_HEIGHT;
    public final SootMethodRef M_GET_STACK_HEIGHT;
    public final SootMethodRef M_THROW_ERROR;
    public final SootMethodRef M_THROW_ERROR_STR;

    private RecorderInstrumentorConstants()
    {
        SootClass recorder_class = Scene.v().getSootClass(RECORDER_CLASS_NAME);
        M_WRITE_BIN_EVENT_HEADER = 
                recorder_class.getMethod(
                    "void recordEventHeader(long)"
                    ).makeRef();
        M_WRITE_BIN_PARAM_OBJ = 
                recorder_class.getMethod(
                    "void writeObjectToBinaryLog(" + OBJECT_TYPE + ")"
                    ).makeRef();
        M_WRITE_BIN_PARAM_CLASS = 
                recorder_class.getMethod(
                    "void writeClassToBinaryLog(" + CLASS_TYPE + ")"
                    ).makeRef();
        M_WRITE_BIN_PARAM_ARRAY = 
                recorder_class.getMethod(
                    "void writeArrayToBinaryLog(" + OBJECT_TYPE + ")"
                    ).makeRef();
        M_WRITE_BIN_PARAM_NULL = 
                recorder_class.getMethod(
                    "void writeNullToBinaryLog(" + STRING_TYPE +")"
                ).makeRef();
        M_WRITE_BIN_PARAM_STR = 
            recorder_class.getMethod(
                "void writeToBinaryLog(" + STRING_TYPE + ")"
                ).makeRef();
        M_WRITE_BIN_PARAM_LONG = 
            recorder_class.getMethod(
                "void writeToBinaryLog(long)"
                ).makeRef();
        M_WRITE_BIN_PARAM_INT = 
                recorder_class.getMethod(
                    "void writeToBinaryLog(int)"
                    ).makeRef();
        M_WRITE_BIN_PARAM_SHORT = 
                recorder_class.getMethod(
                    "void writeToBinaryLog(short)"
                    ).makeRef();
        M_WRITE_BIN_PARAM_BYTE = 
                recorder_class.getMethod(
                    "void writeByteToBinaryLog(int)"
                    ).makeRef();
        M_WRITE_BIN_PARAM_DOUBLE = 
                recorder_class.getMethod(
                    "void writeToBinaryLog(double)"
                    ).makeRef();
        M_WRITE_BIN_PARAM_FLOAT = 
                recorder_class.getMethod(
                    "void writeToBinaryLog(float)"
                    ).makeRef();
        M_WRITE_BIN_PARAM_CHAR = 
                recorder_class.getMethod(
                    "void writeToBinaryLog(char)"
                    ).makeRef();
        M_WRITE_BIN_PARAM_BOOL = 
            recorder_class.getMethod(
                "void writeToBinaryLog(boolean)"
                ).makeRef();
        M_WRITE_BIN_TICK = 
            recorder_class.getMethod(
                "void binaryLogTick()"
                ).makeRef();
        M_WRITE_BIN_CHECK_ENABLED = 
            recorder_class.getMethod(
                "void checkEnabled()"
                ).makeRef();
        M_WRITE_BIN_IS_ENABLED = 
            recorder_class.getMethod(
                "boolean isEnabled()"
                ).makeRef();
        M_WRITE_BIN_ENABLE = 
            recorder_class.getMethod(
                "void enable()"
                ).makeRef();
        M_WRITE_BIN_STACK_HEIGHT = 
            recorder_class.getMethod(
                "void recordStackHeight()"
                ).makeRef();
        boolean fastStackHeight = false;
        SootClass vmstack_class = null;
        if(Scene.v().containsClass(VMSTACK_CLASS_NAME)) {
            vmstack_class = Scene.v().getSootClass(VMSTACK_CLASS_NAME);
            if(vmstack_class.declaresMethod(VMSTACK_FAST_STACK_HEIGHT_SUBSIGNATURE)) {
                fastStackHeight = true;
            }
        }
        if(fastStackHeight) {
            M_GET_STACK_HEIGHT =  
                vmstack_class.getMethod(
                    VMSTACK_FAST_STACK_HEIGHT_SUBSIGNATURE
                    ).makeRef();
        } else {
            M_GET_STACK_HEIGHT =  
                recorder_class.getMethod(
                    "int getStackHeight()"
                    ).makeRef();
        }
        M_THROW_ERROR = 
            recorder_class.getMethod(
                "void throwError()"
                ).makeRef();
        M_THROW_ERROR_STR = 
            recorder_class.getMethod(
                "void throwError(" + STRING_TYPE +")"
                ).makeRef();
    }
    
    /**
	 * Get singleton instance.
	 */
    public static RecorderInstrumentorConstants g() {
        if(recorderInstrumentorConstants == null)
            recorderInstrumentorConstants = new RecorderInstrumentorConstants();
        return recorderInstrumentorConstants;
    }

}
