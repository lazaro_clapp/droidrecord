
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.instrumentor;

import edu.stanford.droidrecord.instrumentor.recorder.RecorderInstrumentor;

import edu.stanford.droidrecord.instrumentor.util.AddUninstrClassesToJar;
import edu.stanford.droidrecord.instrumentor.util.Config;
import edu.stanford.droidrecord.instrumentor.util.OutputVerifier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.Transform;

import org.jf.dexlib2.DexFileFactory;
import org.jf.dexlib2.iface.ClassDef;
import org.jf.dexlib2.iface.DexFile;

public class Main extends SceneTransformer {
	private static Config config;
    private static List<SootClass> classes = new ArrayList<SootClass>();
	private static Map<String,List<String>> uninstrumentedClasses = new HashMap<String,List<String>>();
	private static List<String> allClasses = new ArrayList<String>();
    private static final String dummyMainClassName = "edu.stanford.droidrecord.droidrecordlib.DummyMain";

	private static Pattern excludePat = Pattern.compile(
		// Excluded because of errors:
		"(android\\.support\\.v4\\.app\\.NavUtilsJB)|(com\\.android\\.internal\\.telephony\\.gsm\\.GSMPhone(\\$.*)?)|(com\\.android\\.internal\\.telephony\\.gsm\\.SIMRecords(\\$.*)?)|(android\\.animation\\..*)|(android\\.net\\..*)" + //|(android\\.Manifest(\\$.*)?)
		"|(com\\.android\\.server\\.sip\\.SipService)|(android\\.telephony\\.PhoneNumberUtils)" +
		"|(org\\.apache\\.harmony\\.security\\.asn1\\..*)|(org\\.apache\\.harmony\\.xnet\\.provider\\.jsse\\.PRF)|(javax\\.xml\\.parsers\\.FilePathToURI)" +
		"|(javax\\.xml\\.transform\\.stream\\.FilePathToURI)" +
		// Errors while parsing .apk CTS tests
		"|(SQLite\\.JDBC2z\\..*)|(libcore\\.java\\.net\\.OldDatagramPacketTest\\$1)" +
		"|(libcore\\.javax\\.net\\.ssl\\.SSLEngineTest)|(org\\.apache\\.harmony\\.security\\.tests\\.java\\.security\\.DigestInputStreamTest)" +
		"|(org\\.apache\\.harmony\\.security\\.tests\\.java\\.security\\.DigestOutputStreamTest)|(tests\\.api\\.java\\.lang\\.reflect\\.FieldTest)" +
		"|(tests\\.api\\.java\\.lang\\.reflect\\.MethodTest)|(tests\\.support\\.Support_Configuration)|(tests\\.support\\.Support_StringReader)" +
		"|(tests\\.support\\.Support_TestWebServer\\$Worker)" +
		// Excluded because of dex errors
		"|(android\\.view\\.ViewRootImpl)" +
		"|(com\\.android\\.internal\\.util\\.XmlUtils)" + 
		// Excluded because of Jasmin errors in GitHub app
		//"|(com\\.google\\.inject\\.internal\\.util\\.\\$SourceProvider)" +
		//"|(com\\.viewpagerindicator\\.TabPageIndicator)" + 
        // Excluded because of errors after upgrading to new Soot version:
		"|(com\\.android\\.internal\\.telephony\\.cdma\\.sms\\..*)|(android\\.accessibilityservice\\.IAccessibilityServiceConnection\\$Stub(\\$.*)?)|(android\\.test\\.AndroidTestCase)|(android\\.os\\.BatteryStats)");
    private static Pattern includePat = Pattern.compile(".*"); 

    protected void internalTransform(String phaseName, Map options)
    {
    	RecorderInstrumentor ri = new RecorderInstrumentor(classes);
		ri.instrument();
		Scene.v().getApplicationClasses().remove(Scene.v().getSootClass(dummyMainClassName));
    }

    public static void main(String[] args)
    {
		config = Config.g();
		
        // do whole-program analysis
		soot.options.Options.v().set_whole_program(true);
        // sanity check the IR after optimizations
        soot.options.Options.v().set_validate(true);
        if(config.inApk == null) {
        	// save line information
        	soot.options.Options.v().set_keep_line_number(true);
        	soot.options.Options.v().set_keep_offset(true);
    		// specify where Soot should search for classes
		    Scene.v().setSootClassPath(config.inJars + File.pathSeparator + config.libJars);
        } else {
		    // resolver options
		    soot.options.Options.v().set_full_resolver(true);
		    soot.options.Options.v().set_allow_phantom_refs(true);
		    //prefer Android APK files
		    soot.options.Options.v().set_src_prec(soot.options.Options.src_prec_apk);
		    //use specific android.jar
		    soot.options.Options.v().set_force_android_jar(config.androidJar);
		    // specify where Soot should search for classes
            Scene.v().setSootClassPath(config.inApk + File.pathSeparator + config.libJars);
            // Use Dalvik relaxed exception semantics when checking for locals initialization
            // By default, soot uses very conservative semantics which will refuse to validate 
            // some common Android app classes (including com.google.ads.AdActivity).
            //soot.options.Options.v().set_check_init_throw_analysis(soot.options.Options.check_init_throw_analysis_dalvik);
            // This seems to be required and not done by default
            Scene.v().addBasicClass("dalvik.system.PathClassLoader",SootClass.HIERARCHY);
        }
        
        loadClassesToInstrument();
        loadDroidRecordLibClasses();
		PackManager.v().getPack("wjtp").add(new Transform("wjtp.droidrecord", new Main()));

		StringBuilder builder = new StringBuilder();
		// disable call-graph construction
    	builder.append("-p cg off ");
    	// save the resulting code in a .jar file
    	builder.append("-outjar -d " + config.outJar + " ");
    	// perform intraprocedural optimizations on the application classes
    	builder.append("-O ");
    	// the (dummy) main to operate on
    	builder.append(dummyMainClassName);
    	
		String[] sootArgs = builder.toString().split(" ");
        
        if(classes.size() > 0) {
		    soot.Main.main(sootArgs);
        }

        if(config.inApk == null) {
		    new AddUninstrClassesToJar(uninstrumentedClasses, config.outJar).apply();
		    if(config.verifyOutput) OutputVerifier.run(allClasses);
        }
    }
    
    private static void loadClassesToInstrument()
    {
        if(config.inApk == null) loadClassesToInstrumentSrc();
        else loadClassesToInstrumentApk();
    }
    
    private static void loadClassFromName(String pname, String name)
    {
        allClasses.add(name);
        if(!toBeInstrumented(name)){
			System.out.println("Skipped instrumentation of class: " + name);
			addUninstrumentedClass(pname, name);
			return;
		}
        try{
			System.out.println("Loading class: " + name);
			SootClass klass = Scene.v().loadClassAndSupport(name);
			classes.add(klass);
		}catch(RuntimeException ex) {
			System.out.println("Failed to load class: " + name);
			addUninstrumentedClass(pname, name);
			if (ex.getMessage().startsWith("couldn't find class:")) {
				System.out.println(ex.getMessage());
			} else {
				throw ex;
			}
		}
    }
    
	private static void loadClassesToInstrumentApk()
    {
        try {
            DexFile dexFile = DexFileFactory.loadDexFile(config.inApk, 1/*API version*/);
            for (ClassDef classDefItem: dexFile.getClasses()) {
                String name = classDefItem.getType();
                name = name.replace(";", "").replace(File.separatorChar, '.').substring(1);
                System.out.println(name);
                loadClassFromName(config.inApk, name);
            }
        } catch(IOException e) {
            throw new Error(e);
        }
    }

	private static void loadClassesToInstrumentSrc()
    {
		for (String pname : config.inJars.split(File.pathSeparator)) {
			if (pname.endsWith(".jar")) {
				JarFile jar = null;
				try {
					jar = new JarFile(pname);
				} catch(IOException e) {
					throw new RuntimeException(e.getMessage() + " " + pname);
				}
				for (Enumeration<JarEntry> e = jar.entries(); e.hasMoreElements();) {
					JarEntry entry = e.nextElement();
					String name = entry.getName();
					if (name.endsWith(".class")) {
						name = name.replace(".class", "").replace(File.separatorChar, '.');
						loadClassFromName(pname, name);
					}
				}
			}
			if (pname.endsWith("core.jar")) {
	        	addUninstrumentedClass("jars/droidrecord_lib.jar","edu.stanford.droidrecord.droidrecordlib.TraceRecorder");
	        	addUninstrumentedClass("jars/droidrecord_lib.jar","edu.stanford.droidrecord.droidrecordlib.TraceRecorder$1"); /* ThreadLocal */
		        addUninstrumentedClass("jars/droidrecord_lib.jar","edu.stanford.droidrecord.droidrecordlib.BinLogWriter");
		        addUninstrumentedClass("jars/droidrecord_lib.jar","edu.stanford.droidrecord.droidrecordlib.BinLogWriter$AutoFlusher");
			}
		}
    }
    
    
    private static void loadDroidRecordLibClasses()
    {
		String[] classNames = new String[]{
			"edu.stanford.droidrecord.droidrecordlib.TraceRecorder"
		};
		for(String cname : classNames)
			Scene.v().loadClassAndSupport(cname);
    }

	private static void addUninstrumentedClass(String jarName, String className) {
		List<String> cs = uninstrumentedClasses.get(jarName);
		if (cs == null) {
			cs = new ArrayList<String>();
			uninstrumentedClasses.put(jarName, cs);
		}
		cs.add(className);
	}

    
	private static boolean toBeInstrumented(String className)
	{
	    return includePat.matcher(className).matches() && !excludePat.matcher(className).matches();
	}
}
