
# DroidRecord - Android tracing and dynamic analysis framework.
# 
# Copyright (c) 2015, Stanford University
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Stanford University nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
 
# Author: Lazaro Clapp

import sys

def main():
    runs = list()
    full_avg = [0.0,0.0]

    with open(sys.argv[1]) as f:
        dist_min = {}
        dist_avg = {}
        dist_max = {}
        
        overall_max = 0
        for line in f:
            parts = line.split('\t')
            runs = list()
            for i in range(0,20):
                runs.append(int(parts[i+2]))
                full_avg[0] += int(parts[i+2])
                full_avg[1] += 1
            
            l_min = min(runs)
            if l_min in dist_min:
                dist_min[l_min] += 1
            else:
                dist_min[l_min] = 1
            
            l_avg = sum(runs)/len(runs)
            if l_avg in dist_avg:
                dist_avg[l_avg] += 1
            else:
                dist_avg[l_avg] = 1
            
            l_max = max(runs)
            if l_max in dist_max:
                dist_max[l_max] += 1
            else:
                dist_max[l_max] = 1
                
            if(l_max > overall_max): 
                overall_max = l_max
         
        print "Traces to converge:\tMin\tAvg\tMax"
        for num in range(1, overall_max + 1):
            if num in dist_min:
                s_min = dist_min[num]
            else:
                s_min = 0
            if num in dist_avg:
                s_avg = dist_avg[num]
            else:
                s_avg = 0
            if num in dist_max:
                s_max = dist_max[num]
            else:
                s_max = 0
            
            print "%d\t\t\t%d\t%d\t%d" % (num,s_min,s_avg,s_max)

        print full_avg[0], full_avg[1]
        print "\nFull AVG: %f" % (full_avg[0]/full_avg[1])

if __name__ == '__main__':
    main()
