
# DroidRecord - Android tracing and dynamic analysis framework.
# 
# Copyright (c) 2015, Stanford University
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Stanford University nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
 
# Author: Lazaro Clapp

from lxml import etree
import os
import shutil
import sys

def prepare_app(app_dir):
    android_manifest_path = os.path.join(app_dir, 'AndroidManifest.xml')
    android_build_path = os.path.join(app_dir, 'build.xml')
    android_droidrecord_build_path = os.path.join(app_dir, 'build.droidrecord.xml')
    droidrecord_app_build_xml = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.pardir, os.path.pardir, os.path.pardir, "droidrecord-app.xml")) # '[this script's dir]/../../../droidrecord-app.xml

    xmldoc = etree.parse(android_manifest_path)
    manifest_e = xmldoc.xpath('/manifest')[0]
    if len(manifest_e.xpath('./uses-permission[@android:name="android.permission.WRITE_EXTERNAL_STORAGE"]', 
           namespaces={'android' : 'http://schemas.android.com/apk/res/android'})) == 0:
        wperm_e = etree.SubElement(manifest_e, "uses-permission", nsmap={'android' : 'http://schemas.android.com/apk/res/android'})
        wperm_e.set("{http://schemas.android.com/apk/res/android}name", "android.permission.WRITE_EXTERNAL_STORAGE")
    xmldoc.write(android_manifest_path)
            
    xmldoc = etree.parse(android_build_path)
    project_e = xmldoc.xpath('/project')[0]
    import_e = etree.Element("import")
    import_e.set("file", droidrecord_app_build_xml)
    project_e.insert(0, import_e)
    xmldoc.write(android_droidrecord_build_path)

if __name__ == "__main__":
    prepare_app(sys.argv[1])
