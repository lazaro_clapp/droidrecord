
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.streams;

import java.lang.reflect.Constructor;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import edu.stanford.droidrecord.logreader.CoverageReport;
import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.LogTemplate;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.templates.EventTemplate;
import edu.stanford.droidrecord.logreader.util.HashQueueMultimap;

public class ThreadSieveLogStream extends BaseEventLogStream {

    private final List<Class<? extends EventLogStream>> pipelineClasses;
    private final EventLogStream previous;
    private final Queue<Long> threadOrder;
    private final Map<Long,EventLogStream> streams;
    private final HashQueueMultimap<Long,Event> buffers;
    
    private class InFunnel extends BaseEventLogStream {
        
        private final long threadId;
        
        public LogTemplate getTemplate() { return previous.getTemplate(); }
        
        public InFunnel(long threadId) {
            this.threadId = threadId;
        }
        
        @Override
        public boolean hasEvents() throws IOException {
            while(buffers.peek(threadId) == null) {
                if(!previous.hasEvents()) {
                    return false;
                }
                bufferEvent();
            }
            return true;
        }
        
        @Override
        public Event readNext() throws IOException {
            if(!hasEvents()) {
                return null;
            }
            Event event = buffers.poll(threadId);
            return event;
        }
        
        @Override
        public CoverageReport getCoverageReport() {
            throw new Error("getCoverageReport() called for an event logstream " +
                "inside a private pipeline created by ThreadSieveLogStream. " +
                "This method should never be exposed to users. Call " + 
                "ThreadSieveLogStream.getCoverageReport() to get the " +
                "coverage of the stream being read in by the whole sieve.");
        }
        
    }
    
    private class OutFunnel extends BaseEventLogStream {
        
        private Event head;
        
        public LogTemplate getTemplate() { return previous.getTemplate(); }
        
        public OutFunnel() {
            head = null;
        }
        
        @Override
        public boolean hasEvents() throws IOException {
            if(head != null) return true;
            if(threadOrder.size() == 0) {
                if(!previous.hasEvents()) {
                    return false;
                }
                bufferEvent();
            }
            long nextThread = threadOrder.poll();
            EventLogStream stream = streams.get(nextThread);
            if(stream.hasEvents()) {
                head = stream.readNext();
            } else { // Some events went dead in the pipeline, retry until 
                     // previous is empty
                return hasEvents();
            }
            assert head != null;
            return true;
        }
        
        @Override
        public Event readNext() throws IOException {
            if(!hasEvents()) {
                return null;
            }
            Event event = head;
            head = null;
            return event;
        }
        
        @Override
        public CoverageReport getCoverageReport() {
            throw new Error("getCoverageReport() called for an event logstream " +
                "inside a private pipeline created by ThreadSieveLogStream. " +
                "This method should never be exposed to users. Call " + 
                "ThreadSieveLogStream.getCoverageReport() to get the " +
                "coverage of the stream being read in by the whole sieve.");
        }
        
    }
    private final OutFunnel out;
        
    public LogTemplate getTemplate() { return previous.getTemplate(); }

    public ThreadSieveLogStream(EventLogStream previous,
                    List<Class<? extends EventLogStream>> pipelineClasses) {
        this.pipelineClasses = pipelineClasses;
        this.previous = previous;
        threadOrder = new LinkedBlockingQueue<Long>();
        streams = new HashMap<Long,EventLogStream>();
        out = new OutFunnel();
        buffers = new HashQueueMultimap<Long,Event>();
    }
    
    private void bufferEvent() throws IOException {
        Event event = previous.readNext();
        long eventThreadId = event.getThreadId();
        threadOrder.add(eventThreadId);
        buffers.put(eventThreadId, event);
        if(!streams.containsKey(eventThreadId)) {
            setupNewPerThreadPipeline(eventThreadId);
        }
    }
    
    private void setupNewPerThreadPipeline(long threadId) {
        EventLogStream els = new InFunnel(threadId);
        Class<? extends EventLogStream> elsClass;
        Constructor<? extends EventLogStream> constructor;
        for(int i = pipelineClasses.size() - 1; i >= 0; i--) {
            elsClass = pipelineClasses.get(i);
            try {
                constructor = elsClass.getConstructor(EventLogStream.class);
                els = constructor.newInstance(els);
            } catch(NoSuchMethodException e) {
                throw new Error(String.format("Invalid EventLogStream class in " +
                        "pipeline (%s): No constructor of the form " +
                        "init(EventLogStream).", elsClass), 
                        e);
            } catch(InstantiationException e) {
                throw new Error(e);
            } catch(java.lang.reflect.InvocationTargetException e) {
                throw new Error(e);
            } catch(IllegalAccessException e) {
                throw new Error(e);
            }
        }
        streams.put(threadId, els);
    }
    
    @Override
    public boolean hasEvents() throws IOException {
        return out.hasEvents();
    }
    
    @Override
    public Event readNext() throws IOException {
        return out.readNext();
    }
    
    @Override
    public CoverageReport getCoverageReport() {
        return previous.getCoverageReport();
    }

}
