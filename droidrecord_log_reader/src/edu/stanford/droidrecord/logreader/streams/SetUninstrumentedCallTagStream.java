
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.streams;

import java.io.DataInputStream;
import java.io.IOException;

import edu.stanford.droidrecord.logreader.CoverageReport;
import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.LogTemplate;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;

public class SetUninstrumentedCallTagStream extends BaseEventLogStream {
    
    public static final String TAG_NAME = "uninstrumented_call";
    
    private EventLogStream previousStream;
    private Event lookAhead;
    
    public LogTemplate getTemplate() { return previousStream.getTemplate(); }

    public SetUninstrumentedCallTagStream(EventLogStream previous) {
        previousStream = previous;
    }
    
    private void setUninstrumentedCallTag(MethodCallEvent callE) {
        if(callE.hasTag(TAG_NAME)) return;
        if(lookAhead == null || !(lookAhead instanceof MethodStartEvent)) {
            callE.setTag(TAG_NAME, true);
        } else {
            MethodStartEvent startE = (MethodStartEvent)lookAhead;
            if(startE.matchesCall(callE)) {
                // Finally we check the height of the stack, to ensure that we 
                // are moving from instrumented code to instrumented code, 
                // without uninstrumented code in between.
                int callStackHeight = callE.getStackHeightInt();
                int startStackHeight = startE.getStackHeightInt();
                // +2 Since instrumentation adds one method indirection
                // <= because we also want to support inlining
                if(startStackHeight <= (callStackHeight + 2)) {
                    callE.setTag(TAG_NAME, false);
                } else {
                    System.err.println("Missmatched Stack Heights: ");
                    System.err.println(callE);
                    System.err.println(startE);
                    System.err.println("---------------------------");
                    callE.setTag(TAG_NAME, true);
                }
            } else {
                //System.err.println("\t\tFIXME: Unexpected CALL<->START missmatch");
                callE.setTag(TAG_NAME, true);
            }
        }
    }
    
    @Override
    public boolean hasEvents() throws IOException {
        return (lookAhead != null) || previousStream.hasEvents();
    }
    
    @Override
    public Event readNext() throws IOException {
        if(!hasEvents()) {
            return null;
        }
        Event event;
        if(lookAhead == null) {
            event = previousStream.readNext();
            lookAhead  = previousStream.readNext();
        } else {
            event = lookAhead;
            lookAhead  = previousStream.readNext();
        }
        if(event != null) {
            if(event instanceof MethodCallEvent) {
                setUninstrumentedCallTag((MethodCallEvent)event);
            }
        }
        //System.err.println("\n<SetUninstrumentedCallTagStream>Event[" + event.getID() + "]" + (event.hasTag(TAG_NAME) && (Boolean)event.getTag(TAG_NAME)  ? "[uninstrumented]" : "") + ": " + event);
        return event;
    }
    
    @Override
    public CoverageReport getCoverageReport() {
        return previousStream.getCoverageReport();
    }

}
