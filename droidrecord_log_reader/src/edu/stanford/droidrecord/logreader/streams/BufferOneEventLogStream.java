
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.streams;

import java.io.IOException;

import edu.stanford.droidrecord.logreader.CoverageReport;
import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.LogTemplate;
import edu.stanford.droidrecord.logreader.events.Event;

public class BufferOneEventLogStream extends BaseEventLogStream {

    private EventLogStream previousStream;
    
    private Event buffered;
    
    public LogTemplate getTemplate() { return previousStream.getTemplate(); }
    
    private void bufferOne() throws IOException {
        if(buffered != null) return;
        if(previousStream.hasEvents()) {
            // Can still be null if read an incomplete event from 
            // SimpleEventLogStream. But this will show as an empty stream for 
            // BufferOneEventLogStream's consumers.
            buffered = previousStream.readNext();
        }
    }

    public BufferOneEventLogStream(EventLogStream previous) throws IOException {
        previousStream = previous;
        buffered = null;
        bufferOne();
    }
    
    @Override
    public boolean hasEvents() throws IOException {
        return buffered != null;
    }
    
    @Override
    public Event readNext() throws IOException {
        Event event = buffered;
        buffered = null;
        bufferOne();
        //System.err.println("\n<BufferOneEventLogStream>Event[" + event.getID() + "]: " + event);
        return event;
    }
    
    @Override
    public CoverageReport getCoverageReport() {
        return previousStream.getCoverageReport();
    }

}
