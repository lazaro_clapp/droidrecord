
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.streams;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import edu.stanford.droidrecord.logreader.CoverageReport;
import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.LogTemplate;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodReturnEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;
import edu.stanford.droidrecord.logreader.events.NewObjectEvent;
import edu.stanford.droidrecord.logreader.events.LoadUninitializedObjectEvent;
import edu.stanford.droidrecord.logreader.events.StoreUninitializedObjectEvent;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.util.HashListMultimap;

public class InitializedEventLogStream extends BaseEventLogStream {

    private EventLogStream previousStream;
    
    private Queue<Event> processed;
    private Queue<Event> pending;
    
    private Map<NewObjectEvent,MethodCallEvent> newToCall;
    private Map<MethodCallEvent,MethodReturnEvent> callToReturn;
    private Map<Event,MethodEndEvent> uninitToEnd;
    
    private HashListMultimap<String,NewObjectEvent> unmatchedNews;
    private HashListMultimap<MethodInfo,Event> unmatchedUninit;
    
    public LogTemplate getTemplate() { return previousStream.getTemplate(); }

    public InitializedEventLogStream(EventLogStream previous) {
        previousStream = previous;
        processed = new LinkedBlockingQueue<Event>();
        pending = new LinkedBlockingQueue<Event>();
        unmatchedNews = new HashListMultimap<String,NewObjectEvent>();
        unmatchedUninit = new HashListMultimap<MethodInfo,Event>();
        newToCall = new HashMap<NewObjectEvent,MethodCallEvent>();
        callToReturn = new HashMap<MethodCallEvent,MethodReturnEvent>();
        uninitToEnd = new HashMap<Event,MethodEndEvent>();
    }
    
    private void buffer() throws IOException {
        while(processed.isEmpty()) {
            if(!previousStream.hasEvents()) {
                return;
            }
            Event event = previousStream.readNext();
            assert event != null;
            
            // NewObjectEvents or Uninitialized loads and stores always start 
            // accumulating in the pending buffer.
            if(event instanceof NewObjectEvent) {
                handleNewObject((NewObjectEvent)event);
                continue;
            } else if(event instanceof LoadUninitializedObjectEvent ||
                      event instanceof StoreUninitializedObjectEvent) {
                handleUninitializedLS(event);
                continue;
            }
            
            // MethodCallEvents can start accumulating in the pending buffer in the 
            // special case in which they are calls to a superclass initializer for 
            // an uninitialized object.
            if(event instanceof MethodCallEvent) {
                if(handleMethodCall((MethodCallEvent)event)) continue;
            }
            
            // MethodStartEvents can start accumulating in the pending buffer 
            // when they are initializers.
            if(event instanceof MethodStartEvent) {
                if(handleMethodStart((MethodStartEvent)event)) continue;
            }
            
            // If there are no pending entries, other records cannot start 
            // accumulating in the pending buffer, so we return them 
            // immediately.
            if(pending.isEmpty()) {
                processed.add(event);
                return;
            }
            
            // #Otherwise, we are already accumulating entries, so we store the 
            // current entry in the pending buffer.
            pending.add(event);
            
            // MethodReturnEvents or MethodEndEvents can cause the stream to 
            // start the processing stage.
            if(event instanceof MethodReturnEvent) {
                handleMethodReturn((MethodReturnEvent)event);
                continue;
            } else if(event instanceof MethodEndEvent) {
                handleMethodEnd((MethodEndEvent)event);
                continue;
            }
            
        }
    }
    
    @Override
    public boolean hasEvents() throws IOException {
        buffer();
        return !processed.isEmpty();
    }
    
    @Override
    public Event readNext() throws IOException {
        buffer();
        if(processed.isEmpty()) return null;
        Event event = processed.poll();
        //System.err.println("\n<InitializedEventLogStream>Event[" + event.getID() + "]: " + event);
        return event;
    }
    
    @Override
    public CoverageReport getCoverageReport() {
        return previousStream.getCoverageReport();
    }
    
    private void handleNewObject(NewObjectEvent event) {
        pending.add(event);
        String klass = event.getType();
        unmatchedNews.put(klass, event);
    }
    
    private void handleUninitializedLS(Event event) {
        pending.add(event);
        MethodInfo method = event.getPosition().getMethod();
        unmatchedUninit.put(method, event);
    }
    
    private boolean newMatchesCall(NewObjectEvent newE, MethodCallEvent callE) {
        if(!newE.getPosition().getMethod().equals(
            callE.getPosition().getMethod())) {
                return false;
        } else if(!newE.getType().equals(callE.getMethod().getKlass())) {
            return false;
        } else if(callE.getLocalNames().isEmpty() || 
                  !callE.getLocalNames().get(0).equals(newE.getLocalName())) {
            return false;
        }
        return true;
    }
    
    private NewObjectEvent getUnmatchedNewForCall(MethodCallEvent event) {
    	MethodInfo method = event.getMethod();
        String klass = method.getKlass();
        List<NewObjectEvent> options = unmatchedNews.getAll(klass);
        if(options == null) return null;
        for(NewObjectEvent newE : options) {
        	if(!newE.getPosition().getMethod().equals(event.getPosition().getMethod())) continue;
        	if(event.getLocalNames().size() == 0) continue;
        	if(!newE.getLocalName().equals(event.getLocalName(0))) continue;
        	return newE;
        }
        return null;
    }
    
    private boolean handleMethodCall(MethodCallEvent event) {
        MethodInfo method = event.getMethod();
        if(method.isInitializer()) {
            pending.add(event);
            String klass = method.getKlass();
            // Case 1: [New a -> Call a.<init> -> Ret a.<init>]
            NewObjectEvent newE = getUnmatchedNewForCall(event);
            if(newE != null && newMatchesCall(newE, event)) {
                newToCall.put(newE, event);
                return true;
            }
            // Superclass initializers are threated as uninitialized accesses.
            // Case 2: [Start a.<init> -> Call super(a).<init> -> End a.<init>]
            MethodInfo posMethod = event.getPosition().getMethod();
            unmatchedUninit.put(posMethod, event);
            return true;
        } else {
            return false;
        }
    }
    
    private boolean handleMethodStart(MethodStartEvent event) {
        MethodInfo method = event.getMethod();
        if(method.isInitializer()) {
            pending.add(event);
            unmatchedUninit.put(method, event);
            return true;
        } else {
            return false;
        }
    }
    
    private boolean callMatchesReturn(MethodCallEvent callE, 
                                      MethodReturnEvent retE) {
        if(!callE.getPosition().equals(retE.getPosition())) {
            return false;
        } else if(!callE.getMethod().equals(retE.getMethod())) {
            return false;
        }
        return true;
    }
    
    private NewObjectEvent getUnmatchedNewForReturn(MethodReturnEvent event) {
    	MethodInfo method = event.getMethod();
        String klass = method.getKlass();
        List<NewObjectEvent> options = unmatchedNews.getAll(klass);
        if(options == null) return null;
        for(NewObjectEvent newE : options) {
        	if(!newE.getPosition().getMethod().equals(event.getPosition().getMethod())) continue;
        	if(!newE.getLocalName().equals(event.getLocalName())) continue;
        	return newE;
        }
        return null;
    }
    
    private void handleMethodReturn(MethodReturnEvent event) {
    	MethodInfo method = event.getMethod();
        String klass = method.getKlass();
        NewObjectEvent newE = getUnmatchedNewForReturn(event);
        if(newE == null) return;
        MethodCallEvent callE = newToCall.get(newE);
        if(callE == null) return;
        if(callMatchesReturn(callE, event)) {
            callToReturn.put(callE, event);
            unmatchedNews.remove(klass, newE);
        }
        if(unmatchedNews.size() == 0 && unmatchedUninit.size() == 0) {
            processPending();
        }
    }
    
    private void handleMethodEnd(MethodEndEvent event) {
        MethodInfo method = event.getMethod();
        Event uninitE;
        while((uninitE = unmatchedUninit.poll(method)) != null) {
            uninitToEnd.put(uninitE,event);
        }
        if(unmatchedNews.size() == 0 && unmatchedUninit.size() == 0) {
            processPending();
        }
    }
    
    private void processPending() {
        Event event;
        // Propagate instance value from Ret to Call to New.
        for(Map.Entry<NewObjectEvent,MethodCallEvent> e : newToCall.entrySet()) {
            NewObjectEvent newE = e.getKey();
            MethodCallEvent callE = e.getValue();
            MethodReturnEvent retE = callToReturn.get(callE);
            assert retE != null;
            newE.setInstance(retE.getReturnValue());
            callE.addInstance(retE.getReturnValue());
        }
        newToCall.clear();
        callToReturn.clear();
        // Shift each entry to the processed buffer, transforming uninitialized 
        // accesses into instance accesses.
        while((event = pending.poll()) != null) {
            if(event instanceof LoadUninitializedObjectEvent) {
                MethodEndEvent endE = uninitToEnd.get(event);
                assert endE != null;
                event = 
                    ((LoadUninitializedObjectEvent)event).toLoadInstanceEvent(
                                                        endE.getReturnValue());
            } else if(event instanceof StoreUninitializedObjectEvent) {
                MethodEndEvent endE = uninitToEnd.get(event);
                assert endE != null;
                event = 
                    ((StoreUninitializedObjectEvent)event).toStoreInstanceEvent(
                                                        endE.getReturnValue());
            } else if(event instanceof MethodCallEvent) {
                MethodEndEvent endE = uninitToEnd.get(event);
                if(endE != null) {
                    ((MethodCallEvent)event).addInstance(endE.getReturnValue());
                }
            } else if(event instanceof MethodStartEvent) {
                MethodEndEvent endE = uninitToEnd.get(event);
                if(endE != null) {
                    ((MethodStartEvent)event).addInstance(
                                                endE.getReturnValue());
                }
            }
            processed.add(event);
        }
        uninitToEnd.clear();
    }

}
