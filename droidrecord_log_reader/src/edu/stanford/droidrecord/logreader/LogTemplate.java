
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.BranchEvent;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.events.templates.EventTemplate;
import edu.stanford.droidrecord.logreader.events.templates.ChainableEventTemplate;
import edu.stanford.droidrecord.logreader.events.templates.EventTemplateBuilder;

public class LogTemplate {
    
    private static final Pattern templateChainPattern = Pattern.compile("^(\\d*)(?:\\[(-?\\d+)\\])?>(\\d*)$");
    
    private static class ChainedTemplate {
        private long targetId;
        public long getTargetID() { return targetId; }
        private int branch;
        public int getBranch() { return branch; }
        
        public ChainedTemplate(long targetId, int branch) {
            this.targetId = targetId;
            this.branch = branch;
        }
    }
    
    private static class MutableIndex {
        
        private long counter;
        private Map<Long,Long> innerIdx;
        private Map<Long,Long> reverseIdx;
        private Index immutable;
        
        public MutableIndex() {
            counter = 0;
            innerIdx = new HashMap<Long,Long>();
            reverseIdx = new HashMap<Long,Long>();
            immutable = new Index(this);
        }
        
        public void addValue(long val) {
            innerIdx.put(counter,val);
            reverseIdx.put(val,counter++);
        }
        
        public int count() {
            return innerIdx.size();
        }
        
        public long lookup(long key) {
            return innerIdx.get(key);
        }
        
        public long reverseLookup(long value) {
            return reverseIdx.get(value);
        }
        
        public boolean containsValue(long value) {
            return reverseIdx.containsKey(value);
        }
        
        public Index getImmutable() {
            return immutable;
        }
    }
    
    public static class Index {
        
        private MutableIndex mutable;
        
        public Index(MutableIndex mutable) {
            this.mutable = mutable;
        }
        
        public int count() {
            return mutable.count();
        }
        
        public long lookup(long key) {
            return mutable.lookup(key);
        }
        
        public long reverseLookup(long value) {
            return mutable.reverseLookup(value);
        }
        
        public boolean containsValue(long value) {
            return mutable.containsValue(value);
        }
    }
    
    private Map<Long,EventTemplate> events;
    
    private MutableIndex methodStartRecordIndex;
    public Index getMethodStartRecordIndex() {
        return methodStartRecordIndex.getImmutable();
    }
    private MutableIndex ifRecordIndex;
    public Index getIfRecordIndex() {
        return ifRecordIndex.getImmutable();
    }
    private MutableIndex switchRecordIndex;
    public Index getSwitchRecordIndex() {
        return switchRecordIndex.getImmutable();
    }
    
    private Map<Long,List<ChainedTemplate>> chains = new HashMap<Long,List<ChainedTemplate>>();

    public EventTemplate getEventTemplateById(long id) {
        if(!events.containsKey(id)) {
            throw new Error("Invalid event template ID: " + id);
        }
        return events.get(id);
    }
    
    public Event getNextChainedEvent(Event event) {
        List<ChainedTemplate> chainedTemplates = chains.get(event.getID());
        if(chainedTemplates == null || chainedTemplates.size() == 0) {
            return null;
        }
        
        long targetId = -1;
        if(event instanceof BranchEvent) {
            BranchEvent be = (BranchEvent)event;
            int branch = be.getBranchTakenNum();
            boolean found = false;
            for(ChainedTemplate ct : chainedTemplates) {
                if(ct.getBranch() == branch) {
                    targetId = ct.getTargetID();
                    found = true;
                }
            }
            if(!found) return null;
        } else {
            assert chainedTemplates.size() == 1;
            targetId = chainedTemplates.get(0).getTargetID();
        }
        assert targetId != -1;
        
        EventTemplate targetT = getEventTemplateById(targetId);
        assert targetT instanceof ChainableEventTemplate;
        return ((ChainableEventTemplate)targetT).instantiate(event.getThreadId());
    }

    public List<EventTemplate> getEventTemplatesByLocation(PositionInfo pos) {
        List<EventTemplate> locEvents = new ArrayList<EventTemplate>();
        for(EventTemplate e : events.values()) {
            if(e.getPosition().equals(pos)) {
                locEvents.add(e);
            }
        }
        return locEvents;
    }
    
    public int count() {
        return events.size();
    }
    
    // process id1[branch]>id2
    private void establishTemplateChain(long id1, long id2, int branch) {
        if(!chains.containsKey(id1)) {
            chains.put(id1, new ArrayList<ChainedTemplate>());
        }
        List<ChainedTemplate> chainedTemplates = chains.get(id1);
        chainedTemplates.add(new ChainedTemplate(id2, branch));
    }
    
    private void parseLogFile(String logTemplateFile) {
        try {
            BufferedReader reader = new BufferedReader(
                                        new FileReader(logTemplateFile));
            StringBuilder builder = new StringBuilder();
            String line;
            EventTemplate event;
            while((line = reader.readLine()) != null) {
                Matcher m = templateChainPattern.matcher(line);
                if(m.matches()) {
                    long id1 = Long.parseLong(m.group(1));
                    long id2 = Long.parseLong(m.group(3));
                    int branch;
                    if(m.group(2) == null) {
                        branch = -1;
                    } else {
                        branch = Integer.parseInt(m.group(2));
                    }
                    establishTemplateChain(id1, id2, branch);
                    continue;
                }
                
                while(line != null && !line.endsWith("}]")) {
                    builder.append(line);
                    builder.append("\n");
                    line = reader.readLine();
                }
                if(line != null) builder.append(line);
                
                event = EventTemplateBuilder.makeEventTemplate(builder.toString());
                events.put(event.getID(), event);
                String eventName = event.getEventName();
                if(eventName.equals("MethodStartRecord")) {
                    methodStartRecordIndex.addValue(event.getID());
                } else if(eventName.equals("IfRecord")){
                    ifRecordIndex.addValue(event.getID());
                } else if(eventName.equals("SwitchRecord")){
                    switchRecordIndex.addValue(event.getID());
                }
                
                builder = new StringBuilder();
            }
            reader.close();
        }catch(IOException e){
            throw new Error(e);
        }
    }

    public LogTemplate(String logTemplateFile) {
        events = new HashMap<Long,EventTemplate>();
        methodStartRecordIndex = new MutableIndex();
        switchRecordIndex = new MutableIndex();
        ifRecordIndex = new MutableIndex();
        parseLogFile(logTemplateFile);
    }
}