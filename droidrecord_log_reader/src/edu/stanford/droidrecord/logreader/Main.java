
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.Option;

import edu.stanford.droidrecord.logreader.analysis.AnalysisManager;
import edu.stanford.droidrecord.logreader.analysis.RunnableELSAnalysis;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;

public class Main {
    
    private String templateFile;
    private List<String> binLogFiles = null;
    private String outputFileName;
    private List<String> libPackages = null;
    private List<String> analysisDescriptors = null;
    private boolean ignoreCoverage = false;
    private boolean noDecode = false;
    
    public void printUsage(Options options) {
        System.out.println("USAGE: droidrecord-readlog [opts] droidrecord.log.template droidrecord.log.bin1 [droidrecord.log.bin2...]");
        System.out.println("");
        for(Object o : options.getOptions()) {
            assert o instanceof Option;
            Option opt = (Option)o;
            System.out.println(String.format("\t-%s,--%s\t%s",
                               opt.getOpt(),
                               opt.getLongOpt(),
                               opt.getDescription()));
        }
    }
    
    public void parseCommandLine(String[] args) {
        CommandLineParser parser = new PosixParser();
        // create the Options
        Options options = new Options();
        options.addOption("lib", true, 
                          "The namespace passed to this option should be " + 
                          "considered library code for the purpose of " +
                          "reporting coverage info. This option can be given " +
                          "more than once.");
        options.addOption("analysis", true, 
                          "Run a named analysis and output its results to " +
                          "stdout.");
        options.addOption("o", "out", true, 
                          "Output log file (concatenate all read logs).");
        options.addOption("nocv", false, 
                          "Don't calculate coverage info. Helpful if normal " +
                          "usage results in out-of-memory errors.");
        options.addOption("nodecode", false, 
                          "Don't output decoded log files (*.decoded)");
        
        try {
            CommandLine cli = parser.parse(options, args);
            String[] nonOptArgs = cli.getArgs();
            templateFile = nonOptArgs[0];
            binLogFiles = new ArrayList();
            for(int i = 1; i < nonOptArgs.length; i++) {
                binLogFiles.add(nonOptArgs[i]);
            }
            if(cli.hasOption("lib")) {
                libPackages = Arrays.asList(cli.getOptionValues("lib"));
            }
            if(cli.hasOption("o")) {
                if(cli.getOptionValues("o").length > 1) {
                    System.out.println("Only one output file may be specified using the -o|--out option.");
                    printUsage(options);
                    System.exit(1);
                }
                outputFileName = cli.getOptionValues("o")[0];
            }
            if(cli.hasOption("analysis")) {
                analysisDescriptors = Arrays.asList(cli.getOptionValues("analysis"));
            }
            if(cli.hasOption("nocv")) {
                ignoreCoverage = true;
            }
            if(cli.hasOption("nodecode")) {
                noDecode = true;
            }
            if(noDecode && outputFileName != null) {
            	System.out.println("Incompatible options: -no-decode and -o|--out. Only one is needed. \n" +
            		"You probably want to keep -o|--out: if an output file is manually specified, extra .decode files are never created.");
            }
        } catch(ParseException e) {
            System.out.println(e);
            printUsage(options);
            System.exit(1);
        }
    }
    
    public Main(String[] args) {
        parseCommandLine(args);
    }
    
    private void printCoverageReport(StringBuilder builder, 
                                    CoverageReport coverage, int cellSize,
                                    CoverageReport.CoverageNamespaceFilter f) {
        String format = "%s(%d/%d)";
        if(f == null) {
            String[] strings = new String[]{
                    String.format(format, 
                           coverage.coveredMethodsPercent(),
                           coverage.coveredMethodsCount(),
                           coverage.totalMethodsCount()),
                    String.format(format, 
                           coverage.coveredBranchesPercent(),
                           coverage.coveredBranchesCount(),
                           coverage.totalBranchesCount()),
                    String.format(format, 
                           coverage.coveredStmtsPercent(),
                           coverage.coveredStmtsCount(),
                           coverage.totalStmtsCount()),
                    };
            for(String s : strings) {
                printCell(builder, s, cellSize, true);
            }
        } else {
            String[] strings = new String[]{
                    String.format(format, 
                           coverage.coveredMethodsPercent(f),
                           coverage.coveredMethodsCount(f),
                           coverage.totalMethodsCount(f)),
                    String.format(format, 
                           coverage.coveredBranchesPercent(f),
                           coverage.coveredBranchesCount(f),
                           coverage.totalBranchesCount(f)),
                    String.format(format, 
                           coverage.coveredStmtsPercent(f),
                           coverage.coveredStmtsCount(f),
                           coverage.totalStmtsCount(f)),
                    };
            for(String s : strings) {
                printCell(builder, s, cellSize, true);
            }
        }
        builder.append("\n");
    }
    
    private void printCell(StringBuilder builder, String s, 
                           int cellSize, boolean rightAlign) {
        int paddingSize = cellSize - s.length();
        if(paddingSize <= 0) builder.append(s);
        char[] padding = new char[paddingSize];
        for(int i = 0; i < paddingSize; i++) padding[i] = ' ';
        if(rightAlign) {
            builder.append(padding);
            builder.append(s);
        } else {
            builder.append(s);
            builder.append(padding);
        }
    }
    
    private void printCoverageReports(CoverageReport coverage) {
        int cellSize = 25;
        if(libPackages != null) {
            for(String lib : libPackages) {
                if(lib.length() > cellSize) cellSize = lib.length();
            }
        }
        
        StringBuilder builder = new StringBuilder();
        builder.append("Coverage Report:\n");
        char[] bar = new char[cellSize * 4];
        for(int i = 0; i < bar.length; i++) bar[i] = '=';
        builder.append(bar);
        builder.append("\n");
        printCell(builder, "Package", cellSize, false);
        printCell(builder, "Method Coverage", cellSize, true);
        printCell(builder, "Branch Coverage", cellSize, true);
        printCell(builder, "Instruction Coverage", cellSize, true);
        builder.append("\n");
        printCell(builder, "All", cellSize, false);
        printCoverageReport(builder, coverage, cellSize, null);
        if(libPackages != null) {
            List<CoverageReport.CoverageNamespaceFilter> libFilters = 
                new ArrayList<CoverageReport.CoverageNamespaceFilter>();
            for(String lib : libPackages) {
                CoverageReport.CoverageNamespaceFilter f = 
                    CoverageReport.CoverageNamespaceFilter.getInclusiveFilter(lib);
                printCell(builder, lib, cellSize, false);
                printCoverageReport(builder, coverage, cellSize, f);
                libFilters.add(f);
            }
            CoverageReport.CoverageNamespaceFilter appFilter = 
                CoverageReport.CoverageNamespaceFilter.join(libFilters).neg();
            printCell(builder, "Application code", cellSize, false);
            printCoverageReport(builder, coverage, cellSize, appFilter);
        }
        builder.append(bar);
        System.out.println(builder.toString());
    }
    
    private void run() {
        BinLogReader logReader = new BinLogReader(templateFile);
        logReader.setIgnoreCoverage(ignoreCoverage);
        System.out.println("Template file: " + templateFile);
        for(String binLogFile : binLogFiles) {
            System.out.println("Processing log file: " + binLogFile);
            EventLogStream els = logReader.parseLog(binLogFile);
            try{
		        if(noDecode) {
		        	// Process to get coverage, but don't write output the decode trace
		        	while(els.readNext() != null) {}
		        } else {
		            BufferedWriter writer;
		            if(outputFileName != null) {
		                writer = new BufferedWriter(new FileWriter(outputFileName, true));
		            } else {
		                writer = new BufferedWriter(new FileWriter(binLogFile + ".decoded"));
		            }
		            Event event;
		            while((event = els.readNext()) != null)
		            {
		                writer.write(event.toString());
		                writer.newLine();
		                writer.flush();
		            }
		            writer.close();
                }
            } catch(IOException e){
                throw new Error(e);
            }
        }
        if(!ignoreCoverage) {
            CoverageReport coverage = logReader.getCumulativeCoverageReport();
            printCoverageReports(coverage);
        }
        AnalysisManager am = new AnalysisManager();
        if(analysisDescriptors != null) {
            System.out.println("Running requested analysis:\n\n");
            for(String analysisDescriptor : analysisDescriptors) {
                System.out.println(String.format("===[%s:]===", analysisDescriptor));
                EventLogStream els = logReader.parseLogs(binLogFiles);
                RunnableELSAnalysis analysis = am.getAnalysis(analysisDescriptor, els);
                if(analysis == null) {
                    System.out.println(
                        String.format(
                            "ERROR: Unknown analysis %s requested.",
                            analysisDescriptor));
                } else {
                    analysis.run();
                    while(analysis.shouldRerun()) {
                    	assert !analysis.isReady();
                    	els = logReader.parseLogs(binLogFiles);
                    	analysis.reset(els);
                    	analysis.run();
                    }
                    if(analysis.isReady()) {
                    	String analysisOutputFilename = analysis.getOutputFilename();
                    	if(analysisOutputFilename == null) {
                        	System.out.println(analysis.printableResults());
                        } else {
                        	try{
								BufferedWriter writer;
								writer = new BufferedWriter(new FileWriter(analysisOutputFilename));
								writer.write(analysis.printableResults());
								writer.close();
							} catch(IOException e){
								throw new Error(e);
							}
							System.out.println("Analysis output written to: " + analysisOutputFilename);
                        }
                    } else {
                        System.out.println(
                        String.format(
                            "ERROR: Problem while running requested analysis %s.",
                            analysisDescriptor));
                    }
                }
                System.out.println(String.format("===\n\n"));
            }
        }
    }

    public static void main(String[] args) {
        Main tool = new Main(args);
        tool.run();
        
    }

}
