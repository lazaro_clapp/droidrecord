
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public abstract class ParseUtil {

    private static final Pattern chordMethodPattern = Pattern.compile("^([^:]+):\\(([^\\)]*)\\)([^@]*)@(.*)$");
    
    public static final String MagicToken = "_MAGICTOKEN:W/04TC4JTKGbcGxq3VBqDQ==_";
    
    private static final Map<String, String> typeToBCTypeDict;
    private static final Map<String, String> bcTypeToTypeDict;
    
    public static List<String> parseList(String s) {
        assert s.charAt(0) == '[';
        int len = s.length();
        assert s.charAt(len-1) == ']';
        s = s.substring(1,len-1);
        List<String> list = new ArrayList<String>();
        for(String s1 : s.split(",")) {
            s1 = s1.trim();
            if(!s1.equals("")) list.add(s1);
        }
        return list;
    }
    
    static {
        Map<String, String> m = new HashMap<String, String>();
        m.put("byte","B");
        m.put("char","C");
        m.put("double","D");
        m.put("float","F");
        m.put("int","I");
        m.put("long","J");
        m.put("short","S");
        m.put("void","V");
        m.put("boolean","Z");
        Map<String, String> revM = new HashMap<String, String>();
        for(Map.Entry<String, String> pair : m.entrySet()) {
            revM.put(pair.getValue(), pair.getKey());
        }
        typeToBCTypeDict = Collections.unmodifiableMap(m);
        bcTypeToTypeDict = Collections.unmodifiableMap(revM);
    }
    
    public static boolean isJavaPrimitiveType(String typeStr) {
        return typeToBCTypeDict.containsKey(typeStr);
    }

    public static String typeToChordSignature(String typeStr) {
        if(typeToBCTypeDict.containsKey(typeStr)) {
            return typeToBCTypeDict.get(typeStr);
        } else if(typeStr.endsWith("[]")) {
            return "[" + typeToChordSignature(typeStr.substring(0, typeStr.length() - 2));
        } else {
            return "L" + typeStr.replace('.', '/') + ";";
        }
    }
    
    public static List<String> chordTypeSignatureToSootTypes(String typeStr) {
        List<String> l = new ArrayList<String>();
        int pos = 0;
        int arrayCount = 0;
        while(pos < typeStr.length()) {
            char c = typeStr.charAt(pos);
            String s;
            if(c == 'L') {
                s = typeStr.substring(pos + 1).split(";")[0].replace('/', '.');
                pos += s.length() + 2;
            } else if(c == '[') {
                arrayCount++;
                pos++;
                continue;
            } else {
                s = bcTypeToTypeDict.get(Character.toString(c));
                assert s != null;
                pos++;
            }
            while(arrayCount > 0) {
                s += "[]";
                arrayCount--;
            }
            l.add(s);
        }
        return l; 
    } 
    
    private static String chordToSootMethodSignature(String s, boolean isSubsignature) {
        Matcher m = chordMethodPattern.matcher(s);
        if(!m.matches()) {
            throw new Error(
                String.format("Invalid chord method signature: \'%s\' (doesn't match regular expression \"%s\"",
                        s, chordMethodPattern.toString()));
        }
        String name = m.group(1);
        String paramChordStr = m.group(2);
        List<String> params = chordTypeSignatureToSootTypes(paramChordStr);
        String paramsStr = "";
        for(int i = 0; i < params.size()-1; i++) {
            paramsStr += params.get(i) + ",";
        }
        if(params.size() > 0) {
            paramsStr += params.get(params.size()-1);
        }
        String returnTypeChordStr = m.group(3);
        List<String> returnTypeList = chordTypeSignatureToSootTypes(returnTypeChordStr);
        assert returnTypeList.size() == 1;
        String returnType = returnTypeList.get(0);
        String klass = m.group(4);
        String signature;
        if(isSubsignature) {
            signature = String.format(": %s %s(%s)", returnType, name, paramsStr);
        } else {
            signature = String.format("<%s: %s %s(%s)>", klass, returnType, name, paramsStr);
        }
        return signature;
    }     
    
    public static String chordToSootMethodSignature(String s) {
        return chordToSootMethodSignature(s, false);
    }
    
    public static String chordToSootMethodSubSignature(String s) {
        return chordToSootMethodSignature(s, true);
    }
    
}
