
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events.templates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public abstract class EventTemplateBuilder {
    
    private static final Pattern entryPattern = Pattern.compile("^(?s)(\\d+):\\[(\\w+)\\{(.*)\\}\\]$");
    private static final Pattern attributesPattern = Pattern.compile("(\\w+): ((?:.|\\[(?:.|\\n)*\\]|\"[^\"]*\")+)(?:,\\n\\t|$)");
    
    private static Map<String, String> makeAttributeMap(String attributesString, List<String> orderedAttributes) {
        Map<String, String> attributes = new HashMap<String, String>();
        Matcher m = attributesPattern.matcher(attributesString);
        while(m.find()) {
            attributes.put(m.group(1), m.group(2));
            orderedAttributes.add(m.group(1));
        }
        return attributes;
    }
    
    public static EventTemplate makeEventTemplate(String logTemplateEntry) {
        Matcher m = entryPattern.matcher(logTemplateEntry);
        if(!m.matches()) {
            throw new Error(
                String.format("Failed to match log event template %s " +
                              "(using regular expression: %s)", 
                              logTemplateEntry, entryPattern.toString()));
        }
        Long templateId = Long.parseLong(m.group(1));
        String eventName = m.group(2);
        List<String> orderedAttributes = new ArrayList<String>();
        Map<String, String> eventAttributes = makeAttributeMap(m.group(3), orderedAttributes);
        orderedAttributes = Collections.unmodifiableList(orderedAttributes);
        if(eventName.equals("MethodCallRecord")) {
            return new MethodCallEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("MethodStartRecord")) {
            return new MethodStartEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("MethodEndRecord")) {
            return new MethodEndEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("MethodReturnRecord")) {
            return new MethodReturnEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("NewObjectRecord")) {
            return new NewObjectEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("StoreStaticRecord")) {
            return new StoreStaticEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("LoadStaticRecord")) {
            return new LoadStaticEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("StoreInstanceRecord")) {
            return new StoreInstanceEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("LoadInstanceRecord")) {
            return new LoadInstanceEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("StoreUninitializedObjectRecord")) {
            return new StoreUninitializedObjectEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("LoadUninitializedObjectRecord")) {
            return new LoadUninitializedObjectEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("NewArrayRecord")) {
            return new NewArrayEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("StoreArrayRecord")) {
            return new StoreArrayEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("LoadArrayRecord")) {
            return new LoadArrayEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("ArrayLengthRecord")) {
            return new ArrayLengthEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("IfRecord")) {
            return new IfEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("SwitchRecord")) {
            return new SwitchEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("ThrowExceptionRecord")) {
            return new ThrowExceptionEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("CaughtExceptionRecord")) {
            return new CaughtExceptionEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("SimpleAssignmentRecord")) {
            return new SimpleAssignmentEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("NegationRecord")) {
            return new NegationEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("BinaryOperationRecord")) {
            return new BinaryOperationEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("InstanceOfRecord")) {
            return new InstanceOfEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("CastRecord")) {
            return new CastEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("EnterMonitorRecord")) {
            return new EnterMonitorEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else if(eventName.equals("ExitMonitorRecord")) {
            return new ExitMonitorEventTemplate(templateId, eventName, eventAttributes, orderedAttributes);
        } else {
            throw new Error("Unrecognized Event Type: " + eventName);
        }
    }
}
