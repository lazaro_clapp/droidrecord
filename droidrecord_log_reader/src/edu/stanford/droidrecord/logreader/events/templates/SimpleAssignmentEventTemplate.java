
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events.templates;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import edu.stanford.droidrecord.logreader.events.SimpleAssignmentEvent;

public class SimpleAssignmentEventTemplate extends BaseEventTemplate<SimpleAssignmentEvent> implements ChainableEventTemplate<SimpleAssignmentEvent> {
    
    private final String leftLocal;
    public String getLeftLocalName() { return leftLocal; }
    
    private final String rightLocal;
    public String getRightLocalName() { return rightLocal; }
    
    public SimpleAssignmentEventTemplate(long id, String eventName, 
                                   Map<String,String> attributes, 
                                   List<String> orderedAttributes) {
        super(id, eventName, attributes, orderedAttributes);
        this.leftLocal = getAttribute("LeftLocal");
        this.rightLocal = getAttribute("RightLocal");
    }
    
    public SimpleAssignmentEvent instantiate(long threadId) {
        return new SimpleAssignmentEvent(this, threadId);
    }
    
    public SimpleAssignmentEvent instantiate(DataInputStream dis) {
        throw new Error("Non-Dynamically-Recorded Event! Can't instantiate " +
                        "from DataInputStream. Use instantiate(long threadId) "+
                        "instead.");
    }
}