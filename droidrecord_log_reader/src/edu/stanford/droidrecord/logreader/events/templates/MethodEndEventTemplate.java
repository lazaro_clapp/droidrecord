
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events.templates;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;     
import java.util.Map;

import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;

public class MethodEndEventTemplate extends BaseMethodReturnOrEndEventTemplate<MethodEndEvent> {
    
    private ParamInfo stackHeight;
    public ParamInfo getStackHeight() { return stackHeight; }
    
    public MethodEndEventTemplate(long id, String eventName, 
                                   Map<String,String> attributes, 
                                   List<String> orderedAttributes) {
        super(id, eventName, attributes, orderedAttributes);
        this.stackHeight = ParamInfo.parse(getAttribute("Height"));
    }
    
    public MethodEndEvent instantiate(DataInputStream dis) throws IOException {
        long threadId = dis.readLong();
        ParamInfo returnValue = this.getReturnValue().instantiate(dis);
        ParamInfo stackHeightInst = this.getStackHeight().instantiate(dis);
        return new MethodEndEvent(this, threadId, returnValue, stackHeightInst);
    }
    
}