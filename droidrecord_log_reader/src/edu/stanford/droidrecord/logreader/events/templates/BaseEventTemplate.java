
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events.templates;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import edu.stanford.droidrecord.logreader.events.BaseEvent;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;

public abstract class BaseEventTemplate<T extends BaseEvent> implements EventTemplate<T> {
    
    private final long id;
    public long getID(){return id;}
    
    private final String eventName;
    public String getEventName(){return eventName;}
    
    private final Map<String,String> attributes;
    public String getAttribute(String name) {return attributes.get(name); }
    
    private final List<String> orderedAttributes;
    public List<String> getOrderedAttributeNames() { return orderedAttributes; }
    
    private final PositionInfo position;
    public PositionInfo getPosition() { return position; }
    
    private final Map<String, Object> tags = new HashMap<String, Object>();
    public boolean hasTag(String name) { return tags.containsKey(name); }
    public Object getTag(String name) { return tags.get(name); }
    public void setTag(String name, Object value) { tags.put(name, value); }
    
    public BaseEventTemplate(long id, String eventName, 
                             Map<String,String> attributes, 
                             List<String> orderedAttributes) {
        this.id = id;
        this.eventName = eventName;
        this.orderedAttributes = orderedAttributes;
        this.attributes = Collections.unmodifiableMap(attributes);
        this.position = PositionInfo.parse(getAttribute("At"));
    }
    
    public String toString() {
        String s = String.format("%d:[%s{", this.getID(), this.getEventName());
        for(String name : this.getOrderedAttributeNames()) {
            String value = this.getAttribute(name);
            s += String.format("%s: %s\n\t", name, value);
        }
        s += "}]";
        return s;
    }
}