
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events;

import java.util.HashMap;
import java.util.Map;

import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.events.templates.BaseEventTemplate;
import edu.stanford.droidrecord.logreader.events.templates.EventTemplate;

public abstract class BaseEvent<T extends BaseEventTemplate<?>> implements Event<T> {
    
    private T template;
    private long threadId;
    
    public T getTemplate() { return template; }
    
    public long getID() { return template.getID(); }
    
    public String getEventName() { return template.getEventName(); }
    
    private final Map<String,String> attributes = new HashMap<String, String>();
    public String getAttribute(String name) { 
        if(attributes.containsKey(name)) return attributes.get(name);
        else return template.getAttribute(name); 
    }
    protected void setAttribute(String name, String value) {
        attributes.put(name, value);
    }
    
    public PositionInfo getPosition() { return template.getPosition(); }
    
    private final Map<String, Object> tags = new HashMap<String, Object>();
    public boolean hasTag(String name) { 
        return tags.containsKey(name) || template.hasTag(name); 
    }
    public Object getTag(String name) {
        if(tags.containsKey(name))
            return tags.get(name);
        else
            return template.getTag(name);
    }
    public void setTag(String name, Object value) { tags.put(name, value); }
    
    public long getThreadId() { return threadId; }
    
    public BaseEvent(T template, long threadId) {
        this.template = template;
        this.threadId = threadId;
        setAttribute("Thread",String.format("%d",threadId));
    }
    
    public String toString() {
        return toString(false);
    }
    
    public String toString(boolean includeEventID) {
        String s;
        if(includeEventID) {
            s = String.format("%d:[%s{", this.getID(), this.getEventName());
        } else {
            s = String.format("[%s{", this.getEventName());
        }
        int size = template.getOrderedAttributeNames().size();
        for(int i = 0; i < size; i++) {
            String name = template.getOrderedAttributeNames().get(i);
            String value = this.getAttribute(name);
            s += String.format("%s: %s", name, value);
            if(i != size - 1) {
                s += ",\n\t";
            }
        }
        s += "}]";
        return s;
    }
}