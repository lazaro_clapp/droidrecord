
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import edu.stanford.droidrecord.logreader.events.templates.MethodStartEventTemplate;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;

public class MethodStartEvent extends BaseMethodCallOrStartEvent<MethodStartEventTemplate> {
    
    private ParamInfo stackHeight;
    public ParamInfo getStackHeight() { return stackHeight; }
    public int getStackHeightInt() { return Integer.parseInt(stackHeight.getValue()); }
    
    public MethodStartEvent(MethodStartEventTemplate template, long threadId, 
                            List<ParamInfo> parameterInstances, ParamInfo stackHeight) {
        super(template, threadId, parameterInstances);
        this.stackHeight = stackHeight;
        setAttribute("Height", stackHeight.toString());
    }
    
    public void write(DataOutputStream dos) throws IOException {
        dos.writeLong(this.getID());
        dos.writeLong(this.getThreadId());
        for(ParamInfo p : this.getParameters()) {
            p.write(dos);
        }
        this.getStackHeight().write(dos);
    }
    
    public boolean matchesCall(MethodCallEvent callE) {
        MethodInfo mCall = callE.getMethod();
        MethodInfo mStart = this.getMethod();
        if(!mCall.getName().equals(mStart.getName())) return false;
        List<ParamInfo> plCall = callE.getParameters();
        List<ParamInfo> plStart = this.getParameters();
        if(plCall.size() != plStart.size()) return false;
        for(int i = 0; i < plCall.size(); i++) {
            if(!plCall.get(i).valueMayEqual(plStart.get(i))) return false;
        }
        return true;
    }
}