
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events.info;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class FieldInfo {
    
    private static final Pattern fieldPattern = Pattern.compile("^<([^:]+): ([\\w\\.\\$]+(?:\\[\\])*) ('[\\w<>\\$]+'|[\\w<>\\$]+)>$");
    
    private final String signature;
    public String getSignature() { return signature; }
    private final String klass;
    public String getKlass() { return klass; }
    private final String type;
    public String getFieldType() { return type; }
    private final String name;
    public String getName() { return name; }
    
    public static FieldInfo parse(String s) {
        return new FieldInfo(s);
    }

    public FieldInfo(String fieldSignature) {
        this.signature = fieldSignature;
        Matcher m = fieldPattern.matcher(signature);
        if(!m.matches()) {
            throw new Error(
                String.format("Invalid field signature: \'%s\' (doesn't match regular expression \"%s\"",
                        signature, fieldPattern.toString()));
        }
        this.klass = m.group(1);
        this.type = m.group(2);
        this.name = m.group(3);
    }
    
    public String toString() {
        return signature;
    }
    
    @Override
    public int hashCode() {
        return signature.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        return signature.equals(obj);
    }

}