
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events.info;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import edu.stanford.droidrecord.logreader.events.util.ParseUtil;

public class ParamInfo {
    
    private static final Pattern paramPattern = Pattern.compile(
        "(\\w+):((?:\\w|\"(?:[^\"\\\\]|\\\\.)*\"|\\.|\\$|-| |\\[\\]|" + ParseUtil.MagicToken + ")+)" +
        "(?::((?:\\w|-|\".*\"|\\.|\\$|" + ParseUtil.MagicToken + ")+))?$");

    public static final String INT_TYPE = "int";
    public static final String SHORT_TYPE = "short";
    public static final String LONG_TYPE = "long";
    public static final String BYTE_TYPE = "byte";
    public static final String BOOLEAN_TYPE = "boolean";
    public static final String CHAR_TYPE = "char";
    public static final String STRING_TYPE = "string";
    public static final String OBJ_TYPE = "obj";
    public static final String CLASS_TYPE = "class";
    public static final String ARRAY_TYPE = "array";
    public static final String OTHER_TYPE = "other";
    public static final String NULL_TYPE_VALUE = "NULL_TYPE";
    public static final String VOID_TYPE_VALUE = "VOID_TYPE";
    public static final String UNUSED_TYPE_VALUE = "UNUSED_VALUE";

    private String type;
    public String getType() { return type; }
    
    private String klass;
    public String getKlass() {
        if(isObjectLikeType()) {
            return klass;
        } else {
            throw new Error("Trying to get class of parameter of primitive type: " + this.toString());
        }
    }
    
    private String id;
    public String getId() {
        if(isObjectLikeType()) {
            return id;
        } else {
            throw new Error("Trying to get object id of parameter of primitive type: " + this.toString());
        }
    }
    
    private String value;
    public String getValue() {
        if(isObjectLikeType()) {
            throw new Error("Trying to get primitive value of parameter of object type: " + this.toString());
        } else {
            return value;
        }
    }
    
    public static String parameterListToString(List<ParamInfo> parameters) {
        String s = "[";
        for(int i = 0; i < parameters.size() - 1; i++) {
            s += parameters.get(i).toString() + ", ";
        }
        if(parameters.size() > 0) {
            s += parameters.get(parameters.size() - 1).toString();
        }
        s += "]";
        return s;
    }
    
    public static String arrayIndexesListToString(List<ParamInfo> parameters) {
        String s = "[";
        int i;
        for(i = 0; i < parameters.size() - 1; i++) {
            assert parameters.get(i).isNumericType();
            s += parameters.get(i).getValue() + ", ";
        }
        if(parameters.size() > 0) {
            i = parameters.size() - 1;
            assert parameters.get(i).isNumericType();
            s += parameters.get(i).getValue();
        }
        s += "]";
        return s;
    }
    
    // Format: [p1,p2,p3]
    public static List<ParamInfo> parseList(String s) {
        List<ParamInfo> list = new ArrayList<ParamInfo>();
        s = s.substring(1, s.length() - 1);
        for(String p : s.split(",")) {
            if(p.equals("")) continue;
            list.add(ParamInfo.parse(p));
        }
        return list;
    }
    
    public static ParamInfo parse(String s) {
        return new ParamInfo(s);
    }

    public ParamInfo(String paramDescriptor) {
        Matcher m = paramPattern.matcher(paramDescriptor);
        if(!m.matches()) {
            throw new Error(
                String.format("Invalid parameter descriptor: \'%s\' (doesn't match regular expression \"%s\"",
                        paramDescriptor, paramPattern.toString()));
        }
        this.type = m.group(1);
        if(type.equals(OBJ_TYPE) || type.equals(ARRAY_TYPE) || type.equals(CLASS_TYPE)) {
            this.klass = m.group(2);
            this.id = m.group(3);
            this.value = null;
        } else {
            this.klass = null;
            this.id = null;
            this.value = m.group(2);
        }
    }
    
    private ParamInfo(String type, String klass, String id) {
        assert type.equals(OBJ_TYPE) || type.equals(ARRAY_TYPE) || type.equals(CLASS_TYPE);
        this.type = type;
        this.klass = klass;
        this.id = id;
        this.value = null;
    }
    
    private ParamInfo(String type, String value) {
        assert !(type.equals(OBJ_TYPE) || type.equals(ARRAY_TYPE) || type.equals(CLASS_TYPE));
        this.type = type;
        this.klass = null;
        this.id = null;
        this.value = value;
    }
    
    public boolean isPrimitiveType() {
        return !isObjectLikeType() && !isSpecialType();
    }
    
    public boolean isNumericType() {
        return type.equals(LONG_TYPE) || type.equals(INT_TYPE) || 
        	type.equals(SHORT_TYPE) || type.equals(CHAR_TYPE) || 
        	type.equals(BYTE_TYPE);
    }
    
    public boolean isObject() {
        return type.equals(OBJ_TYPE);
    }
    
    public boolean isObjectLikeType() {
        return (type.equals(OBJ_TYPE) || type.equals(ARRAY_TYPE) || type.equals(CLASS_TYPE));
    }
    
    public boolean isNonNullObjectLikeType() {
    	return isObjectLikeType() && !isNull();
    }
    
    public boolean isSpecialType() {
        return type.equals(OTHER_TYPE);
    }
    
    public boolean isNullType() {
        return type.equals(OTHER_TYPE) && value.equals(NULL_TYPE_VALUE);
    }
    
    public boolean isNull() {
    	return (type.equals(OTHER_TYPE) && value.equals(NULL_TYPE_VALUE)) || 
    			(isObjectLikeType() && this.id.equals("0"));
    }
    
    public boolean isVoidType() {
        return type.equals(OTHER_TYPE) && value.equals(VOID_TYPE_VALUE);
    }
    
    public boolean isUnusedType() {
        return type.equals(OTHER_TYPE) && value.equals(UNUSED_TYPE_VALUE);
    }
    
    public String toString() {
        return String.format("%s:%s", type, toSimpleString());
    }
    
    public String toSimpleString() {
        if(isObjectLikeType()) {
            return String.format("%s:%s", klass, id);
        } else if(type.equals(STRING_TYPE) || type.equals(CHAR_TYPE)) {
            String encValue = value;
            encValue = encValue.replace("\\","\\\\");
            encValue = encValue.replace("\"","\\\"");
            encValue = encValue.replace("\'","\\\'");
            encValue = encValue.replace("\t","\\t");
            encValue = encValue.replace("\n","\\n");
            encValue = encValue.replace("\r","\\r");
            encValue = encValue.replace("\b","\\b");
            encValue = encValue.replace("\f","\\f");
            return String.format("\"%s\"", encValue);
        } else {
            return String.format("%s", value);
        }
    }
    
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof ParamInfo)) return false;
        ParamInfo other = (ParamInfo)obj;
        if(type != other.type) return false;
        if(isObjectLikeType()) {
            return (klass == other.klass) && (id == other.id);
        } else {
            return (value == other.value);
        }
    }
    
    /* 
     * Return true only if obj is a ParamInfo object of the exact same type 
     * and value as the parameter represented by this ParamInfo object. 
     * Allows subclasses but uses the object id as the value to compare.
     *
     * IMPORTANT: Might fail to detect that an java.lang.Object parameter 
     *            is the same as that same parameter treated as a String.
     */
    public boolean valueMustEqual(Object obj) {
        if(!(obj instanceof ParamInfo)) return false;
        ParamInfo other = (ParamInfo)obj;
        if(isObjectLikeType() && other.isObjectLikeType()) {
            return (id.equals(other.id));
        } else {
            if(!type.equals(other.type)) return false;
            return (value.equals(other.value));
        }
    }
    
    /*
     * Return true if obj is a ParamInfo object of the exact same type and 
     * value as the parameter represented by this ParamInfo object.
     * Allows subclasses but uses the object id as the value to compare.
     *
     * Also return true if one of the compared ParamInfo is of type 
     * java.lang.Object and the other one is a String. In this case we can't 
     * say for certain whether or not they are the same value.
     *
     * Finally, correct for cases in which the same value can be represented 
     * in different ways (NULL_TYPE vs obj:X:0, int:0 vs boolean:false, etc).
     */
    public boolean valueMayEqual(Object obj) {
        if(!(obj instanceof ParamInfo)) return false;
        ParamInfo other = (ParamInfo)obj;
        // string:"..." could be equal to obj:java.lang.Object:... or obj:java.lang.CharSequence:...
        if((type.equals(STRING_TYPE) && other.type.equals(OBJ_TYPE) && other.klass.equals("java.lang.Object")) ||
           (other.type.equals(STRING_TYPE) && type.equals(OBJ_TYPE) && klass.equals("java.lang.Object")) ||
           (type.equals(STRING_TYPE) && other.type.equals(OBJ_TYPE) && other.klass.equals("java.lang.CharSequence")) ||
           (other.type.equals(STRING_TYPE) && type.equals(OBJ_TYPE) && klass.equals("java.lang.CharSequence"))) {
                return true;
        }
        // other:NULL_TYPE == obj:X:0
        if((this.isNullType() && other.type.equals(OBJ_TYPE) && other.id.equals("0")) || 
           (other.isNullType() && this.type.equals(OBJ_TYPE) && this.id.equals("0"))) {
            return true;
        }
        // other:NULL_TYPE == string:"_null_"
        if((this.isNullType() && other.type.equals(STRING_TYPE) && other.value.equals("_null_")) || 
           (other.isNullType() && this.type.equals(STRING_TYPE) && this.value.equals("_null_"))) {
            return true;
        }
        // int:0 == false && int:1 == true
        if((type.equals(INT_TYPE) && other.type.equals(BOOLEAN_TYPE) && value.equals("0") && other.value.equals("false")) ||
           (type.equals(INT_TYPE) && other.type.equals(BOOLEAN_TYPE) && value.equals("1") && other.value.equals("true")) ||
           (other.type.equals(INT_TYPE) && type.equals(BOOLEAN_TYPE) && other.value.equals("0") && value.equals("false")) ||
           (other.type.equals(INT_TYPE) && type.equals(BOOLEAN_TYPE) && other.value.equals("1") && value.equals("true"))) {
            return true;
        }
        return valueMustEqual(obj);
    }
    
    public ParamInfo instantiate(DataInputStream dis) throws IOException {
        if(type.equals(OBJ_TYPE) || type.equals(ARRAY_TYPE)) {
            if(!id.equals(ParseUtil.MagicToken)) {
                throw new Error("Parameter already instantiated: " + toString());
            }
            return new ParamInfo(type, klass, String.format("%d",dis.readInt()));
        } else if(type.equals(CLASS_TYPE)) {
            if(!id.equals(ParseUtil.MagicToken)) {
                throw new Error("Parameter already instantiated: " + toString());
            }
            return new ParamInfo(type, dis.readUTF(), String.format("%d",dis.readInt()));
        } else if(type.equals(OTHER_TYPE)) {
            return new ParamInfo(type, value);
        } else {
            if(!value.equals(ParseUtil.MagicToken)) {
                throw new Error("Parameter already instantiated: " + toString());
            }
            if(type.equals("long")) {
                return new ParamInfo(type, String.format("%d",dis.readLong()));
            }
            if(type.equals("int") || type.equals("short") || type.equals("byte")) {
                return new ParamInfo(type, String.format("%d",dis.readInt()));
            }
            if(type.equals("double")) {
                return new ParamInfo(type, String.format("%f",dis.readDouble()));
            }
            if(type.equals("float")) {
                return new ParamInfo(type, String.format("%f",dis.readFloat()));
            }
            if(type.equals("char")) {
                return new ParamInfo(type, String.format("%s",dis.readChar()));
            }
            if(type.equals("boolean")) {
                return new ParamInfo(type, String.format("%b",dis.readBoolean()));
            }
            if(type.equals("string")) {
            	String utf = dis.readUTF();
            	if(utf.equals("_long:10959006-7446-11e4-889a-fb6449f28a74_")) {
            		// Special case: long, multi-part string data
            		String longString = "";
            		while(true) {
            			utf = dis.readUTF();
            			if(utf.equals("_long:10959006-7446-11e4-889a-fb6449f28a74_")) break;
            			longString += utf;
            		}
            		return new ParamInfo(type, longString);
            	} else {
                	return new ParamInfo(type, utf);
                }
            }
            throw new Error("Unrecognized parameter type.");
        }
    }
    
    public void write(DataOutputStream dos) throws IOException {
        if(type.equals(OBJ_TYPE) || type.equals(ARRAY_TYPE)) {
            if(id.equals(ParseUtil.MagicToken)) {
                throw new Error("Parameter NOT instantiated: " + toString());
            }
            dos.writeInt(Integer.parseInt(id));
        } else if(type.equals(CLASS_TYPE)) {
            dos.writeUTF(klass);
            dos.writeInt(Integer.parseInt(id));
        } else if(type.equals(OTHER_TYPE)) {
            // Skip
        }  else {
            if(value.equals(ParseUtil.MagicToken)) {
                throw new Error("Parameter NOT instantiated: " + toString());
            }
            if(type.equals("long")) {
                dos.writeLong(Long.parseLong(value));
            } else if(type.equals("int") || type.equals("short") || type.equals("byte")) {
                dos.writeInt(Integer.parseInt(value));
            } else if(type.equals("double")) {
                dos.writeDouble(Double.parseDouble(value));
            } else if(type.equals("float")) {
                dos.writeFloat(Float.parseFloat(value));
            } else if(type.equals("char")) {
                dos.writeChar(value.charAt(0));
            } else if(type.equals("boolean")) {
                dos.writeBoolean(Boolean.parseBoolean(value));
            } else if(type.equals("string")) {
                dos.writeUTF(value);
            }
        }
    }
}
