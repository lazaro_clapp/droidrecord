
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events.info;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import edu.stanford.droidrecord.logreader.events.util.ParseUtil;

public class MethodInfo {
    
    public static final String INIT_METHOD_NAME = "<init>";
    
    private static final Pattern methodPattern = Pattern.compile("^<([^:]+): ([\\w\\.\\$]+(?:\\[\\])*) ('[\\w<>\\$]+'|[\\w<>\\$]+)\\((.*)\\)>$");
    
    private final String signature;
    public String getSignature() { return signature; }
    private final String klass;
    public String getKlass() { return klass; }
    private final String returnType;
    public String getReturnType() { return returnType; }
    private final String name;
    public String getName() { return name; }
    private final List<String> arguments;
    public List<String> getArguments() { return arguments; }
    
    public static MethodInfo parseChordSignature(String s) {
        return parse(ParseUtil.chordToSootMethodSignature(s));
    }
    
    public static MethodInfo parse(String s) {
        return new MethodInfo(s);
    }

    public MethodInfo(String methodSignature) {
        this.signature = methodSignature;
        Matcher m = methodPattern.matcher(signature);
        if(!m.matches()) {
            throw new Error(
                String.format("Invalid method signature: \'%s\' (doesn't match regular expression \"%s\")",
                        signature, methodPattern.toString()));
        }
        this.klass = m.group(1);
        this.returnType = m.group(2);
        this.name = m.group(3);
        String argStr = m.group(4);
        if(argStr.equals("")) {
            this.arguments = Collections.unmodifiableList(Arrays.asList(new String[0])); 
        } else {
            this.arguments = Collections.unmodifiableList(Arrays.asList(argStr.split(",")));
        }
    }
    
    public boolean isInitializer() {
        return name.equals(INIT_METHOD_NAME);
    }
    
    public String toQualifiedName() {
        return this.klass + "." + this.name;
    }
    
    public String toChordSubsignature() {
        String chordSig = this.name + ":(";
        for(String a : this.arguments) {
            chordSig += ParseUtil.typeToChordSignature(a);
        }
        chordSig += ")" + ParseUtil.typeToChordSignature(this.returnType);
        return chordSig;
    }
    
    public String toChordSignature() {
        return this.toChordSubsignature() + "@" + this.klass;
    }
    
    public String toPrettySignature() {
        String s = getReturnType() + " " + getKlass() + "." + getName() + "(";
        if(arguments.size() > 0) s += arguments.get(0);
        for(int i = 1; i < arguments.size(); i++) {
            s += ", " + arguments.get(i);
        }
        s += ")";
        return s;
    }
    
    public String toString() {
        return signature;
    }
    
    @Override
    public int hashCode() {
        return signature.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        return signature.equals(obj.toString());
    }

}
