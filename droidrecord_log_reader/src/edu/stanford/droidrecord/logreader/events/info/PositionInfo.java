
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events.info;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class PositionInfo {
    
    private static final Pattern positionPattern = Pattern.compile("^(<.*>):([-]?\\d+):([-]?\\d+)$");
    
    private MethodInfo method;
    public MethodInfo getMethod() { return method; }
    private long line;
    public long getLine() { return line; }
    private long jimplestmt;
    public long getJimpleStmtIndex() { return jimplestmt; }
    
    public static PositionInfo parse(String s) {
        Matcher m = positionPattern.matcher(s);
        if(!m.matches()) {
            throw new Error(
                String.format("Invalid position descriptor: \'%s\' (doesn't match regular expression \"%s\"",
                        s, positionPattern.toString()));
        }
        return new PositionInfo(m.group(1), Long.parseLong(m.group(2)), Long.parseLong(m.group(3)));
    }

    public PositionInfo(String methodSignature, long line, long jimplestmt) {
        this.method = new MethodInfo(methodSignature);
        this.line = line;
        this.jimplestmt = jimplestmt;
    }

    public PositionInfo(String methodSignature, long line) {
        this(methodSignature, line, -1L);
    }
    
    public String getFile() {
        String filename = "";
        String klass = this.method.getKlass();
        String[] klassParts = klass.split("\\.");
        int i = 0;
        for(; i < klassParts.length - 1; i++) {
            filename += klassParts[i] + "/";
        }
        filename += klassParts[i].split("\\$")[0] + ".java";
        return filename;
    }
    
    public String toString() {
        return String.format("%s:%d:%d", method.toString(), line, jimplestmt);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 5;
        hash = prime * hash + (this.method != null ? this.method.hashCode() : 0);
        hash = prime * hash + (int) (this.line ^ (this.line >>> 32));
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(obj == this) return true;
        if(!(obj instanceof PositionInfo)) {
            return false;
        }
        PositionInfo objAsPI = (PositionInfo)obj;
        // Not all PositionInfo objects have a jimple stmt value, but those 
        // that do are only equal if that value is also the same.
        // PositionInfo values without a jimple stmt value are often used to 
        // query existing logs for elements at a particular source line position.
        if(this.jimplestmt != -1 && objAsPI.jimplestmt != -1 &&
           this.jimplestmt != objAsPI.jimplestmt) return false;
        return this.getMethod().equals(objAsPI.getMethod()) && 
               (this.getLine() == objAsPI.getLine());
    }

}
