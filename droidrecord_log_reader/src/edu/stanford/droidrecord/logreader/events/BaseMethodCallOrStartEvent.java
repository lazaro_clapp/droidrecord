
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.events;

import java.util.List;
import java.util.Collections;

import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;
import edu.stanford.droidrecord.logreader.events.templates.BaseMethodCallOrStartEventTemplate;

public abstract class BaseMethodCallOrStartEvent<T extends BaseMethodCallOrStartEventTemplate<?>> extends BaseMethodEvent<T> {
    
    private List<ParamInfo> parameters;
    public ParamInfo getParameter(int i) { return parameters.get(i); }
    public List<ParamInfo> getParameters() { return Collections.unmodifiableList(parameters); }
    
    public String getLocalName(int i) { return this.getTemplate().getLocalName(i); }
    public List<String> getLocalNames() { return this.getTemplate().getLocalNames(); }
    
    public BaseMethodCallOrStartEvent(T template, 
                                      long threadId, 
                                      List<ParamInfo> parameterInstances) {
        super(template, threadId);
        this.parameters = parameterInstances;
        setAttribute("Parameters", ParamInfo.parameterListToString(parameters));
    }
    
    public boolean isInstanceMethod() {
        int signatureArgumentCount = this.getMethod().getArguments().size();
        int parameterCount = this.getParameters().size();
        assert signatureArgumentCount <= parameterCount;
        return signatureArgumentCount != parameterCount;
    }
    
    /**
     * Add the object instance to this method call or start record. 
     * This should only be called once for the constructor to add the 
     * this instance parameter that cannot be captured at recording time
    **/
    public void addInstance(ParamInfo instance) {
        if(isInstanceMethod()) {
            throw new Error("Instance parameter already present");
        } else if(!this.getMethod().isInitializer()) {
            throw new Error("Not a constructor. addInstance(...) should be " +
                            "called only for constructors.");
        } else {
            this.parameters.add(0, instance);
        	setAttribute("Parameters", ParamInfo.parameterListToString(parameters));
        }
    }
    
}
