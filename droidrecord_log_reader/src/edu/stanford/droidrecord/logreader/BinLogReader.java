
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;

import edu.stanford.droidrecord.logreader.streams.BufferOneEventLogStream;
import edu.stanford.droidrecord.logreader.streams.InitializedEventLogStream;
import edu.stanford.droidrecord.logreader.streams.ThreadSieveLogStream;
import edu.stanford.droidrecord.logreader.streams.SetUninstrumentedCallTagStream;
import edu.stanford.droidrecord.logreader.streams.SimpleEventLogStream;

public class BinLogReader {
    
    private final LogTemplate template;
    private boolean ignoreCoverage;
    public void setIgnoreCoverage(boolean flag) {
        ignoreCoverage = flag;
    }
    private final List<EventLogStream> trackedEventLogStreams;

    public BinLogReader(String logTemplateFile) {
        // Load the template file into a LogTemplate object
        this.template = new LogTemplate(logTemplateFile);
        trackedEventLogStreams = new LinkedList<EventLogStream>();
        ignoreCoverage = false;
    }
    
    public EventLogStream parseLogs(List<String> binLogFiles) {
        // Generate a new EventLogStream object using the current LogTemplate 
        // and the particular traces (binLogFile).
        EventLogStream logStream;
        try{
            SimpleEventLogStream sels = new SimpleEventLogStream(template);
            for(String binLogFile : binLogFiles) {
                sels.concatBinLog(
                    new DataInputStream(
                        new FileInputStream(binLogFile)));
            }
            BufferOneEventLogStream boels = new BufferOneEventLogStream(sels);
            List<Class<? extends EventLogStream>> pipeline = 
                new ArrayList<Class<? extends EventLogStream>>();
            pipeline.add(InitializedEventLogStream.class);
            pipeline.add(SetUninstrumentedCallTagStream.class);
            logStream = new ThreadSieveLogStream(boels, pipeline);
        }catch(IOException e){
            throw new Error(e);
        }
        
        // Track this EventLogStream in order to be able to report cumulative 
        // coverage.
        if(!ignoreCoverage)
            trackedEventLogStreams.add(logStream);
        return logStream;
    }
    
    public EventLogStream parseLog(String binLogFile) {
        List<String> list = new ArrayList<String>(1);
        list.add(binLogFile);
        return parseLogs(list);
    }
    
    public EventLogStream parseLogsSimple(List<String> binLogFiles) {
        EventLogStream logStream;
        try{
            SimpleEventLogStream sels = new SimpleEventLogStream(template);
            for(String binLogFile : binLogFiles) {
                sels.concatBinLog(
                    new DataInputStream(
                        new FileInputStream(binLogFile)));
            }
            logStream = sels;
        }catch(IOException e){
            throw new Error(e);
        }
        
        // Track this EventLogStream in order to be able to report cumulative 
        // coverage.
        if(!ignoreCoverage)
            trackedEventLogStreams.add(logStream);
        return logStream;
    }
    
    public EventLogStream parseLogSimple(String binLogFile) {
        List<String> list = new ArrayList<String>(1);
        list.add(binLogFile);
        return parseLogsSimple(list);
    }
    
    public CoverageReport getCumulativeCoverageReport() {
        // Get the coverage reports from each EventLogStream created using 
        // this reader and add them together.
        CoverageReport coverage = new CoverageReport(template);
        for(EventLogStream els: trackedEventLogStreams) {
            coverage.add(els.getCoverageReport());
        }
        return coverage;
    }

}
