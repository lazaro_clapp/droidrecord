
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.events.templates.MethodCallEventTemplate;

public class CallArgumentValueAnalysis {
    
    private final EventLogStream els;
    private final Map<MethodInfo, List<MethodCallEventTemplate>> methodToCalls;
    private final Map<MethodCallEventTemplate, List<MethodCallEvent>> templateToEvents;
    private boolean ready;
    
    public CallArgumentValueAnalysis(EventLogStream els) {
        this.els = els;
        methodToCalls = new HashMap<MethodInfo, List<MethodCallEventTemplate>>();
        templateToEvents = new HashMap<MethodCallEventTemplate, List<MethodCallEvent>>();
    }
    
    public boolean isReady() {
        return ready;
    }
    
    private void processMethodCallEvent(MethodCallEvent e) {
        MethodCallEventTemplate t = e.getTemplate();
        MethodInfo m = e.getMethod();
        if(!methodToCalls.containsKey(m)) {
            methodToCalls.put(m,new ArrayList());
        }
        methodToCalls.get(m).add(t);
        if(!templateToEvents.containsKey(t)) {
            templateToEvents.put(t,new ArrayList());
        }
        templateToEvents.get(t).add(e);
    }
    
    public void run() {
        if(isReady()) return;
        methodToCalls.clear();
        templateToEvents.clear();
        try{
            Event event;
            while((event = els.readNext()) != null)
            {
                if(event instanceof MethodCallEvent) {
                    processMethodCallEvent((MethodCallEvent)event);
                }
            }
        } catch(IOException e){
            throw new Error(e);
        }
        ready = true;
    }
    
    private MethodCallEventTemplate getUniqueMethodCallEventTemplate(MethodInfo method, 
                                                               PositionInfo position) {
        MethodCallEventTemplate template = null;
        if(!methodToCalls.containsKey(method)) return null;
        for(MethodCallEventTemplate t : methodToCalls.get(method)) {
            if(t.getPosition().equals(position)) {
                // We assume Method+Position uniquely identifies a call-site.
                // This is not necesarily true, if this assertion evet gets 
                // trigered, considering adding an "int callSite" parameter to
                // queryArgumentValues to disambiguate between call-sites for 
                // the same method at the same source line.
                assert template == null;
                template = t;
            }
        }
        return template;
    }
    
    private List<ParamInfo> getDynamicValues(MethodCallEventTemplate t, 
                                             int argNum) {
        List<ParamInfo> values = new ArrayList<ParamInfo>();
        if(t == null) return values;
        if(!templateToEvents.containsKey(t)) return values;
        for(MethodCallEvent e : templateToEvents.get(t)) {
            values.add(e.getParameter(argNum));
        }
        return values;
    }
    
    private List<List<ParamInfo>> getDynamicValues(MethodCallEventTemplate t) {
        List<List<ParamInfo>> values = new ArrayList<List<ParamInfo>>();
        if(t == null) return values;
        if(!templateToEvents.containsKey(t)) return values;
        for(MethodCallEvent e : templateToEvents.get(t)) {
            values.add(e.getParameters());
        }
        return values;
    }
    
    public List<ParamInfo> queryArgumentValues(MethodInfo method, 
                                               PositionInfo position, 
                                               int argNum) {
        MethodCallEventTemplate t = 
            getUniqueMethodCallEventTemplate(method, position);
        return getDynamicValues(t, argNum);
    }
    
    public List<List<ParamInfo>> queryArgumentValues(MethodInfo method, 
                                               PositionInfo position) {
        MethodCallEventTemplate t = 
            getUniqueMethodCallEventTemplate(method, position);
        return getDynamicValues(t);
    }
    
    public List<ParamInfo> queryArgumentValues(String methodSignature,
                                               String parentMethodSignature,
                                               long srcLineNumber, 
                                               int argNum) {
        MethodInfo method = MethodInfo.parse(methodSignature);
        PositionInfo position = new PositionInfo(parentMethodSignature, 
                                                 srcLineNumber);
        return queryArgumentValues(method, position, argNum); 
    }
    
    public List<List<ParamInfo>> queryArgumentValues(String methodSignature,
                                               String parentMethodSignature,
                                               long srcLineNumber) {
        MethodInfo method = MethodInfo.parse(methodSignature);
        PositionInfo position = new PositionInfo(parentMethodSignature, 
                                                 srcLineNumber);
        return queryArgumentValues(method, position); 
    }
}
