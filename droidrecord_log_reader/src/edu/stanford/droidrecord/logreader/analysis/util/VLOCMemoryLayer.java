
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModel;
import edu.stanford.droidrecord.logreader.events.ArrayLengthEvent;
import edu.stanford.droidrecord.logreader.events.BinaryOperationEvent;
import edu.stanford.droidrecord.logreader.events.CastEvent;
import edu.stanford.droidrecord.logreader.events.CaughtExceptionEvent;
import edu.stanford.droidrecord.logreader.events.EnterMonitorEvent;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.ExitMonitorEvent;
import edu.stanford.droidrecord.logreader.events.IfEvent;
import edu.stanford.droidrecord.logreader.events.InstanceOfEvent;
import edu.stanford.droidrecord.logreader.events.LoadArrayEvent;
import edu.stanford.droidrecord.logreader.events.LoadInstanceEvent;
import edu.stanford.droidrecord.logreader.events.LoadStaticEvent;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodReturnEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;
import edu.stanford.droidrecord.logreader.events.NegationEvent;
import edu.stanford.droidrecord.logreader.events.NewArrayEvent;
import edu.stanford.droidrecord.logreader.events.NewObjectEvent;
import edu.stanford.droidrecord.logreader.events.SimpleAssignmentEvent;
import edu.stanford.droidrecord.logreader.events.StoreArrayEvent;
import edu.stanford.droidrecord.logreader.events.StoreInstanceEvent;
import edu.stanford.droidrecord.logreader.events.StoreStaticEvent;
import edu.stanford.droidrecord.logreader.events.SwitchEvent;
import edu.stanford.droidrecord.logreader.events.info.FieldInfo;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;

public class VLOCMemoryLayer {

	private static class AbstractCallstackFrame {
        private final MethodInfo method;
        public MethodInfo getMethod() { return method; }
        
        private final int height;
        public int getHeight() { return height; }
        
        private final Map<String,Long> regVLOCMap;
        
        public long getLocalVLOC(String local) {
			if(regVLOCMap.containsKey(local)) {
				return regVLOCMap.get(local);
			} else {
				return -1;
			}
		}
	
		public void setLocalVLOC(String local, long loc) {
			regVLOCMap.put(local,loc);
		}
        
        public AbstractCallstackFrame(MethodInfo method, int height) {
            this.method = method;
            this.height = height;
            this.regVLOCMap = new HashMap<String,Long>();
        }
        
        public String toString() {
        	String s = "CF[" + method + "|" + height + "]={";
        	List<String> keys = new ArrayList<String>(regVLOCMap.keySet());
        	int size = keys.size();
        	for(int i = 0; i < size; ++i) {
        		String key = keys.get(i);
        		long value = regVLOCMap.get(key);
        		s += key + ":" + value;
        		if(i != size-1) s += ",";
        	}
        	s += "}";
        	return s;
        }
    }

	private long nextLoc = 0;
	private long newLoc() { return nextLoc++; }
	
	private final ParamInfo globalStaticObject = new ParamInfo("obj:global.static.Object:fdc2ffa624444f1cbf2f7bc9188a43e7");
	public ParamInfo getGlobalStaticObjectReferece() { return globalStaticObject; }
	
	private final Map<String,Long> paramIdToVLOCMap = new HashMap<String,Long>();
	private final Map<String,Map<String,Long>> fieldToVLOCMap = new HashMap<String,Map<String,Long>>();
    private final Stack<AbstractCallstackFrame> abstractCallstack = new Stack<AbstractCallstackFrame>();
	
	public long getVLOC(ParamInfo param) {
		assert param.isNonNullObjectLikeType();
		String paramId = param.getId();
		if(paramIdToVLOCMap.containsKey(paramId)) {
			return paramIdToVLOCMap.get(paramId);
		} else {
			return -1;
		}
	}
	
	public void setVLOC(ParamInfo param, long loc) {
		assert param.isNonNullObjectLikeType();
		String paramId = param.getId();
		if(paramIdToVLOCMap.containsKey(paramId)) {
			throw new Error("Virtual locations for reference types cannot be overwritten.");
		}
		paramIdToVLOCMap.put(paramId,loc);
	}
	
	public long getVLOC(ParamInfo param, String fieldSignature) {
		assert param.isNonNullObjectLikeType();
		String paramId = param.getId();
		if(fieldToVLOCMap.containsKey(paramId)) {
			Map<String,Long> f2VLOC = fieldToVLOCMap.get(paramId);
			if(f2VLOC.containsKey(fieldSignature)) {
				return f2VLOC.get(fieldSignature);
			} else {
				return -1;
			}
		} else {
			return -1;
		}
	}
	
	public void setVLOC(ParamInfo param, String fieldSignature, long loc) {
		assert param.isNonNullObjectLikeType();
		String paramId = param.getId();
		if(!fieldToVLOCMap.containsKey(paramId)) {
			fieldToVLOCMap.put(paramId, new HashMap<String,Long>());
		}
		Map<String,Long> f2VLOC = fieldToVLOCMap.get(paramId);
		f2VLOC.put(fieldSignature,loc);
	}
	
	public long getVLOC(String local) {
		return abstractCallstack.peek().getLocalVLOC(local);
	}
	
	public void setVLOC(String local, long loc) {
		abstractCallstack.peek().setLocalVLOC(local,loc);
	}
	
	// Give the parameter and/or local the same (possibly new) virtual location. 
	public void initVLOC(ParamInfo param, String local) {
		if(param.isNonNullObjectLikeType()) {
			long paramVLOC = getVLOC(param);
        	if(paramVLOC == -1) {
            	long loc = newLoc();
            	setVLOC(param,loc);
            	setVLOC(local,loc);
            } else {
            	setVLOC(local,paramVLOC);
            }
	    } else {
			setVLOC(local,newLoc());
		}
		assert !param.isNonNullObjectLikeType() || getVLOC(param) == getVLOC(local);
		assert getVLOC(local) != -1;
	}
    
    public VLOCMemoryLayer() {
    	setVLOC(globalStaticObject,newLoc());
    }
    
    public String arrayFieldSignature(List<ParamInfo> indexes) {
        String fsig = "arrayIdx";
        for(ParamInfo index : indexes) {
            fsig += "_" + index;
        }
        return fsig;
    }
    
    private void handleAbstractLoad(ParamInfo instance, String fsig,
                                    ParamInfo value, String valueLocal) {
        assert getVLOC(instance) != -1;
        long vloc;
        
        if(value.isNonNullObjectLikeType()) {
        	if(getVLOC(value) == -1) {
				setVLOC(value, newLoc());
        	}
        	setVLOC(instance, fsig, getVLOC(value));
        } else if(getVLOC(instance, fsig) == -1) {
		    setVLOC(instance, fsig, newLoc());
        }
        assert !value.isNonNullObjectLikeType() || getVLOC(value) != -1;
        assert getVLOC(instance, fsig) != -1;
        assert !value.isNonNullObjectLikeType() || getVLOC(value) == getVLOC(instance, fsig);
        
        vloc = getVLOC(instance, fsig);
        setVLOC(valueLocal, vloc);
    }
    
    public void handleLoadInstanceEvent(LoadInstanceEvent event) {
    	ParamInfo instance = event.getInstance();
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        
        handleAbstractLoad(instance, fsig, event.getValue(), event.getLocalName());
    }
    
    public void handleLoadStaticEvent(LoadStaticEvent event) {
    	FieldInfo field = event.getField();
        String fsig = field.getSignature();
        
        handleAbstractLoad(globalStaticObject, fsig, event.getValue(), event.getLocalName());
    }
    
    public void handleLoadArrayEvent(LoadArrayEvent event) {
    	ParamInfo instance = event.getArrayInstance();
        List<ParamInfo> indexes = event.getIndexes();
        String fsig = arrayFieldSignature(indexes);
        
        handleAbstractLoad(instance, fsig, event.getValue(), event.getLocalName());
    }
    
    private void handleAbstractStore(ParamInfo instance, String fsig,
                                    ParamInfo value, String valueLocal) {
        // Sanity checks
        assert getVLOC(instance) != -1;
        long vloc;
        if(!valueLocal.equals("_immediate_")) {
        	vloc = getVLOC(valueLocal);
        } else {
        	vloc = newLoc();
        }
        assert vloc != -1;
        assert !value.isNonNullObjectLikeType() || getVLOC(value) == getVLOC(valueLocal);
        
        setVLOC(instance, fsig, vloc);
    }
    
    public void handleStoreInstanceEvent(StoreInstanceEvent event) {
    	ParamInfo instance = event.getInstance();
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        
        handleAbstractStore(instance, fsig, event.getValue(), event.getLocalName());
    }
    
    public void handleStoreStaticEvent(StoreStaticEvent event) {
    	FieldInfo field = event.getField();
        String fsig = field.getSignature();
        
        handleAbstractStore(globalStaticObject, fsig, event.getValue(), event.getLocalName());
    }
    
    public void handleStoreArrayEvent(StoreArrayEvent event) {
    	ParamInfo instance = event.getArrayInstance();
        List<ParamInfo> indexes = event.getIndexes();
        String fsig = arrayFieldSignature(indexes);
        
        handleAbstractStore(instance, fsig, event.getValue(), event.getLocalName());
    }
    
    public void handleNewObjectEvent(NewObjectEvent event) {
        String local = event.getLocalName();
        ParamInfo param = event.getInstance();
        assert param.isNonNullObjectLikeType();
        initVLOC(param,local);
    }
    
    public void handleNewArrayEvent(NewArrayEvent event) {
    	String local = event.getArrayLocalName();
        ParamInfo param = event.getArrayInstance();
        assert param.isNonNullObjectLikeType();
        initVLOC(param,local);
    }
    
    public void handleSimpleAssignmentEvent(SimpleAssignmentEvent event) {
    	String lhs = event.getLeftLocalName();
    	String rhs = event.getRightLocalName();
    	if(rhs.equals("_immediate_")) {
			setVLOC(lhs,newLoc());
    	} else {
			assert getVLOC(rhs) != -1;
			setVLOC(lhs,getVLOC(rhs));
    	}
    }
    
    public void handleBinaryOperationEvent(BinaryOperationEvent event) {
    	setVLOC(event.getResultLocalName(),newLoc());
    }
    
    public void handleNegationEvent(NegationEvent event) {
    	String lhs = event.getLeftLocalName();
    	String rhs = event.getRightLocalName();
    	assert getVLOC(rhs) != -1;
    	setVLOC(lhs,getVLOC(rhs));
    }
    
    public void handleArrayLengthEvent(ArrayLengthEvent event) {
    	setVLOC(event.getLocalName(),newLoc());
    }
    
    public void handleCastEvent(CastEvent event) {
    	String lhs = event.getLeftLocalName();
    	String rhs = event.getRightLocalName();
    	assert getVLOC(rhs) != -1;
    	setVLOC(lhs,getVLOC(rhs));
    }
    
    public void handleInstanceOfEvent(InstanceOfEvent event) {
    	String lhs = event.getLeftLocalName();
    	String rhs = event.getRightLocalName();
    	assert getVLOC(rhs) != -1;
    	setVLOC(lhs,newLoc()); // New boolean value
    }
    
    public void handleCaughtExceptionEvent(CaughtExceptionEvent event) {
    	String local = event.getLocalName();
        ParamInfo param = event.getException();
        assert param.isNonNullObjectLikeType();
        initVLOC(param,local);
    }
    
    public void handleIfEvent(IfEvent event) {
    	// Pass
    }
    
    public void handleInstrumentedMethodCall(MethodCallEvent callE, MethodStartEvent startE) {
    	assert abstractCallstack.peek().getHeight() == callE.getStackHeightInt();
    	AbstractCallstackFrame callerFrame = abstractCallstack.peek();
    	AbstractCallstackFrame calleeFrame = new AbstractCallstackFrame(startE.getMethod(), startE.getStackHeightInt());
    	abstractCallstack.push(calleeFrame);
    	
    	// Copy the virtual locations from call arguments into callee parameters.
    	// Plenty of sanity checks added!
    	List<String> callLocals = callE.getLocalNames();
        List<ParamInfo> callParams = callE.getParameters();
    	List<String> calleeLocals = startE.getLocalNames();
    	List<ParamInfo> calleeParams = startE.getParameters();
        assert callParams.size() == calleeLocals.size();
        
        for(int i = 0; i < callLocals.size(); ++i) {
        	String callLocal = callLocals.get(i);
        	ParamInfo callParam = callParams.get(i);
        	String calleeLocal = calleeLocals.get(i);
        	ParamInfo calleeParam = calleeParams.get(i);
        	long vloc;
        	if(callLocal.equals("_immediate_")) {
        		vloc = newLoc();
        	} else {
        		vloc = callerFrame.getLocalVLOC(callLocal);
        	}
        	assert vloc != -1;
        	if(callParam.getType().equals(ParamInfo.STRING_TYPE) && calleeParam.isNonNullObjectLikeType()) {
        		// Special case, when passing a string to a method that treats it like an object.
        		// FIXME: Add proper Object ID information to recorded strings.
        		assert calleeParam.getKlass().equals("java.lang.Object");
        		if(getVLOC(calleeParam) == -1) {
        			setVLOC(calleeParam, vloc);
        		}
        		setVLOC(calleeLocal,getVLOC(calleeParam));
        	} else if(callParam.isNonNullObjectLikeType()) {
        		assert calleeParam.isNonNullObjectLikeType();
        		assert vloc == getVLOC(callParam);
				calleeFrame.setLocalVLOC(calleeLocal,vloc);
			} else {
				calleeFrame.setLocalVLOC(calleeLocal,vloc);
			}
        }
        
        // Sanity check of startE event:
        List<String> locals = startE.getLocalNames();
        List<ParamInfo> params = startE.getParameters();
        assert locals.size() == params.size();
        for(int i = 0; i < locals.size(); ++i) {
        	String local = locals.get(i);
        	ParamInfo param = params.get(i);
        	assert !param.isNonNullObjectLikeType() || getVLOC(param) == getVLOC(local);
        }
    }
    
    public void handleInstrumentedMethodReturn(MethodEndEvent endE, MethodReturnEvent retE) {
        assert abstractCallstack.peek().getHeight() == endE.getStackHeightInt();
        String calleeLocal = endE.getLocalName();
    	ParamInfo calleeParam = endE.getReturnValue();
    	long vloc;
    	if(calleeLocal.equals("_immediate_") || calleeLocal.equals("_void_")) {
    		vloc = newLoc();
    	} else {
    		vloc = getVLOC(calleeLocal);
    	}
    	assert vloc != -1;
    	assert !calleeParam.isNonNullObjectLikeType() || getVLOC(calleeParam) == vloc;
    	
    	abstractCallstack.pop();
    	assert abstractCallstack.peek().getHeight() == retE.getStackHeightInt();
    	
    	String callLocal = retE.getLocalName();
    	setVLOC(callLocal,vloc);
    	
    	// Sanity check of retE event:
    	ParamInfo callParam = retE.getReturnValue();
    	assert !callParam.isNonNullObjectLikeType() || getVLOC(callParam) == getVLOC(callLocal);
    }
    
    public void handleUninstrumentedMethodCall(MethodCallEvent callE, MethodReturnEvent retE) {
    	assert abstractCallstack.peek().getHeight() == callE.getStackHeightInt();
    	assert abstractCallstack.peek().getHeight() == retE.getStackHeightInt();
    	String local = retE.getLocalName();
    	if(!local.equals("_unused_")) {
			ParamInfo param = retE.getReturnValue();
		    initVLOC(param,local);
        }
    }
    
    public void handleUninstrumentedMethodCall(MethodCallEvent callE, CaughtExceptionEvent exceptE) {
        int newHeight = exceptE.getStackHeightInt();
        while(abstractCallstack.peek().getHeight() > newHeight) {
        	abstractCallstack.pop();
        }
        String local = exceptE.getLocalName();
        ParamInfo param = exceptE.getException();
        assert param.isNonNullObjectLikeType();
        initVLOC(param,local);
    }
    
    public void handleInitialMethodStartEvent(MethodStartEvent event) {
        abstractCallstack.push(new AbstractCallstackFrame(event.getMethod(), event.getStackHeightInt()));
        List<String> locals = event.getLocalNames();
        List<ParamInfo> params = event.getParameters();
        assert locals.size() == params.size();
        for(int i = 0; i < locals.size(); ++i) {
        	String local = locals.get(i);
        	ParamInfo param = params.get(i);
        	initVLOC(param,local);
        }
    }
    
}
