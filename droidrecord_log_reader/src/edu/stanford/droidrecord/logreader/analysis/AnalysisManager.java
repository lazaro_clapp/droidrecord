
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis;

import java.util.HashMap;
import java.util.Map;
import java.util.Collections;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.analysis.taintpropagation.LoginDetectionAnalysis;

public class AnalysisManager {
    
    public static class AnalysisDescriptor {
        private static final Pattern descriptorPattern = Pattern.compile(
            "^([^{]*)(?:\\{((?:[^=]*=(?:\".*\"|[^\",]*),?)*)\\})?$");
        private static final Pattern descriptorParamPattern = Pattern.compile(
            "([^=]*)=(\".*\"|[^\",]*),?");
        
        private final String name;
        public String getName() { return name; }
        
        private final Map<String, String> parameters;
        public Map<String, String> getParameters() {
            return Collections.unmodifiableMap(parameters);
        }
        
        public static AnalysisDescriptor parse(String s) {
            return new AnalysisDescriptor(s);
        }
    
        public AnalysisDescriptor(String descriptorStr) {
            this.parameters = new HashMap<String, String>();
            Matcher m = descriptorPattern.matcher(descriptorStr);
            if(!m.matches()) {
                throw new Error(
                    String.format("Invalid analysis descriptor string: \'%s\' (doesn't match regular expression \"%s\"",
                            descriptorStr, descriptorPattern.toString()));
            }
            this.name = m.group(1);
            String parametersStr = m.group(2);
            if(parametersStr != null) {
                m = descriptorParamPattern.matcher(parametersStr);
                while(m.find()) {
                    parameters.put(m.group(1), m.group(2));
                }
            }
        }
    }
    
    public AnalysisManager() {
        // No state
    }
    
    public RunnableELSAnalysis getAnalysis(AnalysisDescriptor descriptor,
                                           EventLogStream els) {
        RunnableELSAnalysis analysis;
        String name = descriptor.getName();
        if(name.equals("heap-model") || name.equals("heap-model-analysis")) {
            analysis = new MethodHeapModelAnalysis(els);
        } else if(name.equals("points-to") || name.equals("points-to-analysis")) {
            analysis = new DynamicPointsToAndCallgraphAnalysis(els);
        } else if(name.equals("callsite-points-to") || name.equals("callsite-points-to-analysis")) {
            analysis = new DynamicPointsToOnCallAnalysis(els);
        } else if(name.equals("login-detection") || name.equals("login-detection-analysis")) {
            analysis = new LoginDetectionAnalysis(els);
        } else {
            return null;
        }
        for(Map.Entry<String,String> entry : descriptor.getParameters().entrySet()) {
            analysis.setParameter(entry.getKey(), entry.getValue());
        }
        return analysis;
    }
    
    public RunnableELSAnalysis getAnalysis(String descriptor,
                                           EventLogStream els) {
        return getAnalysis(AnalysisDescriptor.parse(descriptor), els);
    }
}
