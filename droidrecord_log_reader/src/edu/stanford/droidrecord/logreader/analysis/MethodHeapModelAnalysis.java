
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.json.simple.JSONValue;
import org.json.simple.JSONAware;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.events.ArrayLengthEvent;
import edu.stanford.droidrecord.logreader.events.BinaryOperationEvent;
import edu.stanford.droidrecord.logreader.events.CastEvent;
import edu.stanford.droidrecord.logreader.events.CaughtExceptionEvent;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.InstanceOfEvent;
import edu.stanford.droidrecord.logreader.events.LoadArrayEvent;
import edu.stanford.droidrecord.logreader.events.LoadInstanceEvent;
import edu.stanford.droidrecord.logreader.events.LoadStaticEvent;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodReturnEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;
import edu.stanford.droidrecord.logreader.events.NewArrayEvent;
import edu.stanford.droidrecord.logreader.events.NewObjectEvent;
import edu.stanford.droidrecord.logreader.events.SimpleAssignmentEvent;
import edu.stanford.droidrecord.logreader.events.StoreArrayEvent;
import edu.stanford.droidrecord.logreader.events.StoreInstanceEvent;
import edu.stanford.droidrecord.logreader.events.StoreStaticEvent;
import edu.stanford.droidrecord.logreader.events.info.FieldInfo;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.streams.SetUninstrumentedCallTagStream;
import edu.stanford.droidrecord.logreader.util.HashSetMultimap;


public class MethodHeapModelAnalysis extends RunnableELSAnalysis {
    
    public static class MethodHeapModelEntry implements JSONAware {
        private final String from;
        public String getFrom() { return from; }
        private final String to;
        public String getTo() { return to; }
        
        public MethodHeapModelEntry(String from, String to) {
            this.from = from;
            this.to = to;
        }
        
        protected Map<String, Object> toJSONMap() {
            Map<String, Object> obj = new LinkedHashMap<String, Object>();
            obj.put("from", from);
            obj.put("to", to);
            return obj;
        }
        
        public String toJSONString() {
            return JSONValue.toJSONString(this.toJSONMap());
        }
        
        public boolean equals(Object o) {
            if(!(o instanceof MethodHeapModelEntry)) return false;
            MethodHeapModelEntry other = (MethodHeapModelEntry)o;
            return this.from.equals(other.from) && this.to.equals(other.to);
        }
    }
    
    public static class MethodHeapModel implements Iterable<MethodHeapModelEntry>, JSONAware {
        private final List<MethodHeapModelEntry> entries;
        
        public MethodHeapModel() {
            entries = new ArrayList<MethodHeapModelEntry>();
        }
        
        // Preserves order and uniqueness
        public void add(MethodHeapModelEntry entry) {
            for(int i = 0; i < entries.size(); i++) {
                MethodHeapModelEntry e = entries.get(i);
                int compare = entry.getFrom().compareTo(e.getFrom());
                if(compare < 0) continue;
                else if(compare > 0) {
                    entries.add(i, entry);
                    return;
                } 
                assert compare == 0;
                
                compare = entry.getTo().compareTo(e.getTo());
                if(compare < 0) continue;
                else if(compare > 0) {
                    entries.add(i, entry);
                    return;
                }
                assert compare == 0;
                
                return; // duplicated entry
            }
            // Add last
            entries.add(entry);
        }
        
        public void add(String from, String to) {
            add(new MethodHeapModelEntry(from, to));
        }
        
        public void add(MethodHeapModel model) {
            for(MethodHeapModelEntry entry : model)
                this.add(entry);
        }
        
        public Iterator<MethodHeapModelEntry> iterator() {
            return Collections.unmodifiableList(entries).iterator();
        }
        
        public String toJSONString() {
            return JSONValue.toJSONString(entries);
        }
        
        public boolean equals(Object o) {
            if(!(o instanceof MethodHeapModel)) return false;
            MethodHeapModel other = (MethodHeapModel)o;
            if(this.entries.size() != other.entries.size()) return false;
            for(int i = 0; i < entries.size(); i++) {
                if(!entries.get(i).equals(other.entries.get(i))) return false;
            }
            return true;
        }
    }
    
    private static abstract class HeapEventEdge implements JSONAware {
        
        private final String fromId;
        private final String toId;
        private final long timeStep;
        
        public HeapEventEdge(String fromId, String toId, long timeStep) {
            this.fromId = fromId;
            this.toId = toId;
            this.timeStep = timeStep;
        }
        
        protected Map<String, Object> toJSONMap() {
            Map<String, Object> obj = new LinkedHashMap<String, Object>();
            obj.put("step", timeStep);
            obj.put("from", fromId);
            obj.put("to", toId);
            return obj;
        }
        
        public String toJSONString() {
            return JSONValue.toJSONString(this.toJSONMap());
        }
        
    }
    
    private static class HeapLoadEdge extends HeapEventEdge {
        
        private final String field;
        
        public HeapLoadEdge(String fromId, String toId, String field, long timeStep) {
            super(fromId, toId, timeStep);
            this.field = field;
        }
        
        @Override
        protected Map<String, Object> toJSONMap() {
            Map<String, Object> obj = super.toJSONMap();
            obj.put("type", "load");
            obj.put("field", field);
            return obj;
        }
    }
    
    private static class HeapStoreEdge extends HeapEventEdge {
        
        private final String field;
        
        public HeapStoreEdge(String fromId, String toId, String field, long timeStep) {
            super(fromId, toId, timeStep);
            this.field = field;
        }
        
        @Override
        protected Map<String, Object> toJSONMap() {
            Map<String, Object> obj = super.toJSONMap();
            obj.put("type", "store");
            obj.put("field", field);
            return obj;
        }
    }
    
    private static class HeapCreateEdge extends HeapEventEdge {
        
        private final String field;
        
        public HeapCreateEdge(String fromId, String toId, String field, long timeStep) {
            super(fromId, toId, timeStep);
            this.field = field;
        }
        
        @Override
        protected Map<String, Object> toJSONMap() {
            Map<String, Object> obj = super.toJSONMap();
            obj.put("type", "create");
            obj.put("field", field);
            return obj;
        }
    }
    
    private static class HeapModelEdge extends HeapEventEdge {
        
        public HeapModelEdge(String fromId, String toId, long timeStep) {
            super(fromId, toId, timeStep);
        }
        
        @Override
        protected Map<String, Object> toJSONMap() {
            Map<String, Object> obj = super.toJSONMap();
            obj.put("type", "model");
            return obj;
        }
    }
    
    private static class JSONMethodParameterInfo implements JSONAware {
        
        private final String id;
        private final String name;
        public String getName() { return name; }
        private final String klass;
        public String getKlass() { return klass; }
        
        public JSONMethodParameterInfo(String id, String name, String klass) {
            this.id = id;
            this.name = name;
            this.klass = klass;
        }
        
        protected Map<String, Object> toJSONMap() {
            Map<String, Object> obj = new LinkedHashMap<String, Object>();
            obj.put("id", id);
            obj.put("name", name);
            obj.put("klass", klass);
            return obj;
        }
        
        public String toJSONString() {
            return JSONValue.toJSONString(this.toJSONMap());
        }
    }
    
    private class AbstractCallstackFrame {
        private MethodInfo method;
        public MethodInfo getMethod() { return method; }
        
        // Register to SDA map for the current method abstract activation record
        private Map<String,String> regSDAMap;
        public Map<String,String> getRegSDAMap() { return regSDAMap; }
        
        // Parameters to pass to callee. Set on CALL. Read on START of 
        // next AbstractCallstackFrame
        private List<String> paramSDAMap;
        public List<String> getParamSDAMap() { return paramSDAMap; }
        public void setParamSDAMap(List<String> params) { 
            paramSDAMap = params; 
        }
        public void clearParamSDAMap() { paramSDAMap = null; }
        
        // Return value to pass to caller. Set on END of next 
        // AbstractCallstackFrame. Read on RETURN of this 
        // AbstractCallstackFrame
        private String returnSDA;
        public String getReturnSDA() { return returnSDA; }
        public void setReturnSDA(String returnSDA) { 
            this.returnSDA = returnSDA; 
        }
        public void clearReturnSDA() { returnSDA = null; }
        
        public AbstractCallstackFrame(MethodInfo method) {
            this.method = method;
            this.regSDAMap = new LinkedHashMap<String,String>();
            this.paramSDAMap = null;
            this.returnSDA = null;
        }
    }
    private Stack<AbstractCallstackFrame> abstractCallstack = new Stack<AbstractCallstackFrame>();
    
    private Set<MethodInfo> uninstrumentedMethods = new HashSet<MethodInfo>();
    private Set<MethodInfo> uninstrumentedMethodsWException = new HashSet<MethodInfo>();
    
    private abstract class UninstrumentedCallStrategy {
        protected MethodCallEvent callE;
        public void setCall(MethodCallEvent event) { callE = event; }
        protected MethodReturnEvent retE;
        public void setReturn(MethodReturnEvent event) { retE = event; }
        protected CaughtExceptionEvent exceptE;
        public void setException(CaughtExceptionEvent event) { exceptE = event; }
        
        public abstract void process();
    }
    
    private class UninstrumentedCallWorstCaseStrategy extends UninstrumentedCallStrategy {
        
        public UninstrumentedCallWorstCaseStrategy() {
            callE = null;
            retE = null;
        }
        
        public void process() {
            assert callE != null;
            assert retE != null ^ exceptE != null;
            List<String> localNames = callE.getLocalNames();
            MethodInfo method = callE.getMethod();
            List<String> inputIds = new ArrayList<String>();
            List<String> inputNames = new ArrayList<String>();
            List<String> outputIds = new ArrayList<String>();
            List<String> outputNames = new ArrayList<String>();
            
            // Collect location ids:
            for(int i = 0; i < localNames.size(); i++) {
                ParamInfo param = callE.getParameter(i);
                
                String name;
                if(i == 0 && callE.isInstanceMethod()) {
                    name = "this";
                } else {
                    name = "arg#" + i;
                }
            
                if(param.isPrimitiveType()) {
                    String localName = localNames.get(i);
                    if(localName.equals("_immediate_")) continue;
                    String sdaLocalName = getRegSDAMap().get(localName);
                    if(sdaLocalName == null) {
                        System.err.println("Unknown sdaLocalName");
                        System.err.println(callE);
                        System.err.println(localName);
                        // FIXME: Temp code to handle yet-to-be interpreted events
                        continue;
                    }
                    assert sdaLocalName != null;
                    inputIds.add(sdaLocalName);
                    inputNames.add(name);
                    outputIds.add(sdaLocalName);
                    outputNames.add(name);
                } else if(param.isObjectLikeType()) {
                    String paramId = param.getId();
                    if(paramId.equals("0")) continue; // Null value
                    inputIds.add(paramId);
                    inputNames.add(name);
                    outputIds.add(paramId);
                    outputNames.add(name);
                } else if(param.isNullType() || param.isVoidType()) {
                    // Do nothing
                } else {
                    throw new Error("Unexpected parameter type: " + param + 
                                    " in \n" + callE);
                }
                
            }
            
            // Normal return case
            if(retE != null) {
                if(!uninstrumentedMethods.contains(method)) {
                    System.out.println("Uninstrumented method called: " + method + 
                                    " (processing with worst-case assumptions).");
                }
                
                String retLocalName = retE.getLocalName();
                ParamInfo retVal = retE.getReturnValue();
                if(retVal.isPrimitiveType()) {
                    assert !retLocalName.equals("_unused_");
                    String retLocalNameSDA = newSDA();
                    getRegSDAMap().put(retLocalName, retLocalNameSDA);
                    heapLocToColor.put(retLocalNameSDA, nextColor++);
                    outputIds.add(retLocalNameSDA);
                    outputNames.add("return");
                } else if(retVal.isObjectLikeType()) {
                    if(retVal.getId() != null) {
                        outputIds.add(retVal.getId());
                        outputNames.add("return");
                    }
                } else if(retVal.isUnusedType()) {
                    assert retLocalName.equals("_unused_");
                } else if(retVal.isNullType() || retVal.isVoidType()) {
                    // Do nothing
                } else {
                    throw new Error("Unexpected return type: " + retVal + 
                                    " in \n" + retE);
                }
            }
            
            // Exceptional termination case
            if(exceptE != null) {
                if(!uninstrumentedMethodsWException.contains(method)) {
                    System.out.println("Uninstrumented method threw an exception: " + method + 
                                    " (processing with worst-case assumptions).");
                }
                
                String exceptLocalName = exceptE.getLocalName();
                ParamInfo exception = exceptE.getException();
                assert exception.isObjectLikeType();
                assert exception.getId() != null; // Is this true?
                outputIds.add(exception.getId());
                outputNames.add("exception");
            }
            
            // Propagate colors and create "model" edges
            if(!uninstrumentedMethods.contains(method)) {
                System.out.println("\t Model:");
            }
            for(int i = 0; i < inputIds.size(); i++) {
                String inId = inputIds.get(i);
                String inName = inputNames.get(i);
                for(int j = 0; j < outputIds.size(); j++) {
                    String outId = outputIds.get(j);
                    String outName = outputNames.get(j);
                    if(inId.equals(outId)) continue;
                    if(!uninstrumentedMethods.contains(method)) {
                        System.out.println(
                            String.format("\t%s->%s", inName, outName));
                    }
                    if(inName.equals("this")) {
                        // Pseudo-load
                        propagateColors(inId, outId);
                    } else {
                        // Pseudo-store
                        for(Integer inColor : heapLocToColor.getAll(inId)) {
                            for(Integer outColor : heapLocToColor.getAll(outId)) {
                                colorToColor.put(inColor, outColor);
                            }
                        }
                    }
                    HeapModelEdge edge = new HeapModelEdge( 
                        inId,
                        outId, 
                        currentStep++);
                    heapEdges.add(edge);
                }
            }
            
            if(retE != null) {
                String retLocalName = retE.getLocalName();
                ParamInfo retVal = retE.getReturnValue();
                // General purpose return handling:
                if(retVal.isObjectLikeType() && retVal.getId() != null) {
                    String retLocalNameSDA = newSDA();
                    getRegSDAMap().put(retLocalName, retLocalNameSDA);
                    propagateColors(retVal.getId(), retLocalNameSDA);
                }
                uninstrumentedMethods.add(method);
            }
            
            if(exceptE != null) {
                String exceptLocalName = exceptE.getLocalName();
                ParamInfo exception = exceptE.getException();
                assert exception.isObjectLikeType();
                assert exception.getId() != null; // Is this true?
                String exceptLocalNameSDA = newSDA();
                getRegSDAMap().put(exceptLocalName, exceptLocalNameSDA);
                propagateColors(exception.getId(), exceptLocalNameSDA);
                uninstrumentedMethodsWException.add(method);
            }
        }
        
    }
    
    private class UninstrumentedCallBaseModelStrategy extends UninstrumentedCallStrategy {
        
        private final MethodHeapModel baseModel;
        
        public UninstrumentedCallBaseModelStrategy(MethodHeapModel baseModel) {
            callE = null;
            retE = null;
            this.baseModel = baseModel;
        }
        
        public void process() {
            assert callE != null;
            assert retE != null ^ exceptE != null;
            List<String> localNames = callE.getLocalNames();
            MethodInfo method = callE.getMethod();
            
            Map<String,String> nameToId = new HashMap<String,String>();
            
            // Collect location ids for parameters:
            for(int i = 0; i < localNames.size(); i++) {
                ParamInfo param = callE.getParameter(i);
                
                String name;
                if(i == 0 && callE.isInstanceMethod()) {
                    name = "this";
                } else {
                    name = "arg#" + i;
                }
            
                if(param.isPrimitiveType()) {
                    String localName = localNames.get(i);
                    if(localName.equals("_immediate_")) continue;
                    String sdaLocalName = getRegSDAMap().get(localName);
                    if(sdaLocalName == null) {
                        System.err.println("Unknown sdaLocalName");
                        System.err.println(callE);
                        System.err.println(localName);
                        // FIXME: Temp code to handle yet-to-be interpreted events
                        continue;
                    }
                    assert sdaLocalName != null;
                    nameToId.put(name, sdaLocalName);
                } else if(param.isObjectLikeType()) {
                    String paramId = param.getId();
                    if(paramId.equals("0")) continue; // Null value
                    nameToId.put(name, paramId);
                } else if(param.isNullType() || param.isVoidType()) {
                    // Do nothing
                } else {
                    throw new Error("Unexpected parameter type: " + param + 
                                    " in \n" + callE);
                }
            }
            
            // Normal return case: Collect location id for return parameter
            if(retE != null) {
                if(!uninstrumentedMethods.contains(method)) {
                    System.out.println("Uninstrumented method called: " + method + 
                                    " (processing with base model).");
                }
                
                String retLocalName = retE.getLocalName();
                ParamInfo retVal = retE.getReturnValue();
                if(retVal.isPrimitiveType()) {
                    assert !retLocalName.equals("_unused_");
                    String retLocalNameSDA = newSDA();
                    getRegSDAMap().put(retLocalName, retLocalNameSDA);
                    heapLocToColor.put(retLocalNameSDA, nextColor++);
                    nameToId.put("return", retLocalNameSDA);
                } else if(retVal.isObjectLikeType()) {
                    if(retVal.getId() != null) {
                        assert !retVal.getId().equals("0");
                        nameToId.put("return", retVal.getId());
                    }
                } else if(retVal.isUnusedType()) {
                    assert retLocalName.equals("_unused_");
                } else if(retVal.isNullType() || retVal.isVoidType()) {
                    // Do nothing
                } else {
                    throw new Error("Unexpected return type: " + retVal + 
                                    " in \n" + retE);
                }
            }
            
            // Exceptional termination case: Collect location id for exception parameter
            if(exceptE != null) {
                if(!uninstrumentedMethodsWException.contains(method)) {
                    System.out.println("Uninstrumented method threw an exception: " + method + 
                                    " (processing with base model).");
                }
                
                String exceptLocalName = exceptE.getLocalName();
                ParamInfo exception = exceptE.getException();
                assert exception.isObjectLikeType();
                assert exception.getId() != null; // Is this true?
                nameToId.put("exception", exception.getId());
            }
            
            if((!uninstrumentedMethods.contains(method) && retE != null) ||
               (!uninstrumentedMethodsWException.contains(method) && exceptE != null)){
                System.out.println("\t Model:");
            }
            for(MethodHeapModelEntry entry : baseModel) {
                String from = entry.getFrom();
                String to = entry.getTo();
                String fromId = nameToId.get(from); 
                String toId = nameToId.get(to);
                if(fromId == null || toId == null) {
                    // This is expected if one is an immediate parameter or an 
                    // exception happened instead of a return (or viceversa)
                    continue;
                }
                if(fromId.equals(toId)) continue;
                if((!uninstrumentedMethods.contains(method) && retE != null) ||
                   (!uninstrumentedMethodsWException.contains(method) && exceptE != null)) {
                    System.out.println(
                        String.format("\t%s->%s", from, to));
                }
                if(from.equals("this")) {
                    // Pseudo-load
                    propagateColors(fromId, toId);
                } else {
                    // Pseudo-store
                    for(Integer fromColor : heapLocToColor.getAll(fromId)) {
                        for(Integer toColor : heapLocToColor.getAll(toId)) {
                            colorToColor.put(fromColor, toColor);
                        }
                    }
                }
                HeapModelEdge edge = new HeapModelEdge( 
                    fromId,
                    toId, 
                    currentStep++);
                heapEdges.add(edge);
            }
            
            // General purpose return handling:
            if(retE != null) {
                String retLocalName = retE.getLocalName();
                ParamInfo retVal = retE.getReturnValue();
                if(retVal.isObjectLikeType() && retVal.getId() != null) {
                    String retLocalNameSDA = newSDA();
                    getRegSDAMap().put(retLocalName, retLocalNameSDA);
                    propagateColors(retVal.getId(), retLocalNameSDA);
                }
                uninstrumentedMethods.add(method);
            }
            
            if(exceptE != null) {
                String exceptLocalName = exceptE.getLocalName();
                ParamInfo exception = exceptE.getException();
                assert exception.isObjectLikeType();
                assert exception.getId() != null; // Is this true?
                String exceptLocalNameSDA = newSDA();
                getRegSDAMap().put(exceptLocalName, exceptLocalNameSDA);
                propagateColors(exception.getId(), exceptLocalNameSDA);
                uninstrumentedMethodsWException.add(method);
            }
        }
    }
    
    private interface TraceHoleStrategy {
        public boolean finished(Event event);
    }
    
    private class ConsumeUntilTraceHoleEndStrategy implements TraceHoleStrategy {
        private MethodCallEvent callE;
        public void setCall(MethodCallEvent event) { callE = event; }
        
        public ConsumeUntilTraceHoleEndStrategy() {
            callE = null;
        }
        
        public boolean finished(Event event) {
            int heightCall, height;
            if(event instanceof MethodReturnEvent) {
                heightCall = callE.getStackHeightInt();
                height = ((MethodReturnEvent)event).getStackHeightInt();
                assert heightCall <= height;
                if(heightCall == height) {
                    assert ((MethodReturnEvent)event).getMethod().equals(callE.getMethod());
                    return true;
                }
            } else if(event instanceof CaughtExceptionEvent) {
                heightCall = callE.getStackHeightInt();
                height = ((CaughtExceptionEvent)event).getStackHeightInt();
                if(heightCall >= height) return true;
            }
            
            return false;
        }
    }
    
    /* A NewObjectEvent can prompt an implicit call to <clinit>, we use the 
     * following strategy object to consume any instrumented methods called 
     * within that initializer.
     */
    private class ConsumeNewObjectGeneratedTraceHoleStrategy implements TraceHoleStrategy {
        
        private MethodStartEvent startE;
        
        public ConsumeNewObjectGeneratedTraceHoleStrategy() {
            startE = null;
        }
        
        public boolean finished(Event event) {
            int startHeight, height;
            if(startE == null) {
                // Hole hasn't started yet.
                if(event instanceof MethodStartEvent) {
                    // There is a hole after this new, start consuming.
                    startE = (MethodStartEvent)event;
                    return false;
                } else {
                    return true; // No hole
                }
            } else {
                // Hole consumption in progress
                if(event instanceof MethodEndEvent) {
                    startHeight = startE.getStackHeightInt();
                    height = ((MethodEndEvent)event).getStackHeightInt();
                    assert startHeight <= height;
                    if(startHeight == height) {
                        // Found the matching end, reset the state of this 
                        // strategy to no-hole, but still consume this event 
                        // and keep an eye for another potential MethodStartEvent
                        // just after this.
                        assert ((MethodEndEvent)event).getMethod().equals(startE.getMethod());
                        startE = null;
                    }
                    return false;
                } else if(event instanceof CaughtExceptionEvent) {
                    throw new Error("WARNING: Exception detected after " +
                    "NewObjectEvent and caught on instrumented code. " + 
                    "Unfortunatelly, we currently have no way of knowing " +
                    "whether this instrumented code is inside a trace hole or " +
                    "not, since NewObjectEvent doesn't record it's stack " +
                    "height. We could infer it from the last seen " +
                    "MethodStartEvent, but we should only think of doing this " +
                    "if this condition ever happens in practice.");
                } else {
                    return false;
                }
            }
        }
    }
    
    private TraceHoleStrategy traceHole = null;
    
    private String outputString = "";
    private String modeledMethodName = null;
    
    private boolean scanning = false;
    private int recursionDepth = 0;
    private Set<String> seenEventTypes = new HashSet<String>();
    private Set<String> unhandledEventTypes = new HashSet<String>();
    
    private List<JSONMethodParameterInfo> parameters;
    private List<JSONMethodParameterInfo> posibleReturnValues;
    private List<HeapEventEdge> heapEdges = new ArrayList<HeapEventEdge>();
    private long currentStep = 0;
    private long sdaCounter = 0;
    private UninstrumentedCallStrategy uninstrumentedCallStrategy;
    
    private HashSetMultimap<String,Integer> heapLocToColor = new HashSetMultimap<String,Integer>();
    private HashSetMultimap<String,String> destructivelyUpdatedFields = new HashSetMultimap<String,String>();
    private Set<String> newLocations = new HashSet<String>();
    private HashSetMultimap<Integer,Integer> colorToColor = new HashSetMultimap<Integer,Integer>();
    private Map<Integer,String> colorToLabel = new LinkedHashMap<Integer,String>();
    private int nextColor = 0;
    
    private Map<MethodInfo, MethodHeapModel> baseModels = new HashMap<MethodInfo, MethodHeapModel>();
    
    private void loadBaseModels(String baseModelsFilename) {
        Pattern modelEntryPattern = Pattern.compile("^\t([a-zA-Z0-9#]+)->([a-zA-Z0-9#]+)$");
        try {
            BufferedReader br = new BufferedReader(new FileReader(baseModelsFilename));
            String line = br.readLine();
            while (line != null) {
                assert !line.startsWith("\t");
                MethodInfo method = MethodInfo.parse(line);
                MethodHeapModel model = new MethodHeapModel();
                line = br.readLine();
                
                while(line != null && (line.startsWith("\t") || line.startsWith(" "))) {
                    // tabs to spaces corrective magic:
                    if(line.startsWith("        ")) line = line.replace("        ","\t");
                    else if(line.startsWith("    ")) line = line.replace("    ","\t");
                    
                    Matcher m = modelEntryPattern.matcher(line);
                    if(!m.matches()) {
                        throw new Error(
                            String.format("Invalid base models file (%s): Model line \'%s\' (in method \'%s\') doesn't match regular expression \"%s\".",
                                    baseModelsFilename, line, method.getSignature(), modelEntryPattern.toString()));
                    }
                    model.add(m.group(1), m.group(2));
                    line = br.readLine();
                }
                baseModels.put(method, model);
            }
            br.close();
        } catch(IOException e) {
            throw new Error(e);
        }
    }
    
    public MethodHeapModelAnalysis(EventLogStream els) {
        super(els);
        posibleReturnValues = new ArrayList<JSONMethodParameterInfo>();
    }
    
    private Map<String,String> getRegSDAMap() {
        return abstractCallstack.peek().getRegSDAMap();
    }
    
    private String newSDA() {
        return "sdaReg" + (sdaCounter++);
    }
    
    private String fieldToSDA(ParamInfo instance, String fieldSignature) {
        if(instance == null) return "static." + fieldSignature;
        return instance.getId() + "." + fieldSignature;
    }
    
    private String arrayFieldSignature(List<ParamInfo> indexes) {
        String fsig = "arrayIdx";
        for(ParamInfo index : indexes) {
            fsig += "_" + index;
        }
        return fsig;
    }
    
    private void startScanning(MethodStartEvent event) {
        //System.err.println("START SCANNING: " + event);
        MethodInfo method = event.getMethod();
        abstractCallstack.push(new AbstractCallstackFrame(method));
        parameters = new ArrayList<JSONMethodParameterInfo>();
        for(int i = 0; i < event.getParameters().size(); i++) {
            String name;
            if(i == 0 && event.isInstanceMethod()) {
                name = "this";
            } else {
                name = "arg#" + i;
            }
            ParamInfo param = event.getParameter(i);
            String targetId = null, returnType = null;
            if(param.isPrimitiveType()) {
                String paramLocalNameSDA = newSDA();
                String paramLocalName = event.getLocalName(i);
                getRegSDAMap().put(paramLocalName, paramLocalNameSDA);
                targetId = paramLocalNameSDA;
                returnType = param.getType();
            } else if(param.isObjectLikeType()) {
                targetId = param.getId();
                returnType = param.getKlass();
            } else {
                continue;
            }
            heapLocToColor.put(targetId, nextColor);
            colorToLabel.put(nextColor, name);
            nextColor++;
            JSONMethodParameterInfo jsonP = new JSONMethodParameterInfo(
                targetId,
                name,
                returnType);
            parameters.add(jsonP);
        }
        
        scanning = true;
        outputString += "Scanning method "+method.toString()+"\n";
    }
    
    private void stopScanning(MethodEndEvent event) {
        ParamInfo param = event.getReturnValue();
        String targetId = null, returnType = null;
        if(param.isPrimitiveType()) {
            targetId = getRegSDAMap().get(event.getLocalName());
            returnType = param.getType();
        } else if(param.isObjectLikeType()) {
            targetId = param.getId();
            returnType = param.getKlass();
        }
        colorToLabel.put(nextColor, "return");
        for(Integer color : heapLocToColor.getAll(targetId)) {
            colorToColor.put(color, nextColor);
        }
        nextColor++;
        JSONMethodParameterInfo jsonP = new JSONMethodParameterInfo(
            targetId,
            "return",
            returnType);
        posibleReturnValues.add(jsonP);
        
        abstractCallstack.pop();
        scanning = false;
    }
    
    public void run() {
        modeledMethodName = this.getParameter("method");
        if(modeledMethodName == null) {
            throw new Error("Missing analysis parameter: " + modeledMethodName);
        }
        String baseModelsFilename = this.getParameter("baseModelsFilename");
        if(baseModelsFilename != null) {
            loadBaseModels(baseModelsFilename);
        }
        super.run();
    }
    
    protected void handleEvent(Event event) {
        PositionInfo pos = event.getPosition();
        MethodInfo method;
        
        //System.err.println("handleEvent: " + event);
        
        // Consume mid-trace holes 
        // (e.g. m1 calls m2 calls m3, m1 and m3 instrumented, m2 not instrumented => consume m3 )
        if(scanning && traceHole != null) {
            System.err.println("handleEvent_InHole: " + event);
            if(!traceHole.finished(event)) return;
            traceHole = null;
        }
        
        //System.err.println("handleEvent_NoHole: " + event);
        
        if(event instanceof MethodStartEvent) {
            MethodStartEvent msEvent = (MethodStartEvent)event;
            method = msEvent.getMethod();
            if(method.toString().contains(modeledMethodName)) {
                recursionDepth++;
                if(recursionDepth == 1) {
                    startScanning(msEvent);
                    return;
                }
            }
        } else if(event instanceof MethodEndEvent) {
            MethodEndEvent meEvent = (MethodEndEvent)event;
            method = meEvent.getMethod();
            if(method.toString().contains(modeledMethodName)) {
                recursionDepth--;
                if(recursionDepth == 0) {
                    stopScanning(meEvent);
                    return;
                }
            }
        }
        if(!scanning) {
            return;
        }
        
        seenEventTypes.add(event.getEventName());
        
        //System.err.println("handleEvent_switching: " + event);
        
        if(event instanceof LoadInstanceEvent) {
            handleLoadInstanceEvent((LoadInstanceEvent)event);
        } else if(event instanceof LoadStaticEvent) {
            handleLoadStaticEvent((LoadStaticEvent)event);
        } else if(event instanceof LoadArrayEvent) {
            handleLoadArrayEvent((LoadArrayEvent)event);
        } else if(event instanceof StoreInstanceEvent) {
            handleStoreInstanceEvent((StoreInstanceEvent)event);
        } else if(event instanceof StoreStaticEvent) {
            handleStoreStaticEvent((StoreStaticEvent)event);
        } else if(event instanceof StoreArrayEvent) {
            handleStoreArrayEvent((StoreArrayEvent)event);
        } else if(event instanceof NewObjectEvent) {
            handleNewObjectEvent((NewObjectEvent)event);
        } else if(event instanceof NewArrayEvent) {
            handleNewArrayEvent((NewArrayEvent)event);
        } else if(event instanceof MethodCallEvent) {
            handleMethodCallEvent((MethodCallEvent)event);
        } else if(event instanceof MethodStartEvent) {
            handleMethodStartEvent((MethodStartEvent)event);
        } else if(event instanceof MethodEndEvent) {
            handleMethodEndEvent((MethodEndEvent)event);
        } else if(event instanceof MethodReturnEvent) {
            handleMethodReturnEvent((MethodReturnEvent)event);
        } else if(event instanceof SimpleAssignmentEvent) {
            handleSimpleAssignmentEvent((SimpleAssignmentEvent)event);
        } else if(event instanceof BinaryOperationEvent) {
            handleBinaryOperationEvent((BinaryOperationEvent)event);
        } else if(event instanceof ArrayLengthEvent) {
            handleArrayLengthEvent((ArrayLengthEvent)event);
        } else if(event instanceof CastEvent) {
            handleCastEvent((CastEvent)event);
        } else if(event instanceof InstanceOfEvent) {
            handleInstanceOfEvent((InstanceOfEvent)event);
        } else if(event instanceof CaughtExceptionEvent) {
            handleCaughtExceptionEvent((CaughtExceptionEvent)event);
        } else {
            unhandledEventTypes.add(event.getEventName());
        }
    }
    
    private void propagateColors(String fromId, String toId) {
        for(Integer color : heapLocToColor.getAll(fromId)) {
            heapLocToColor.put(toId, color);
        }
    }
    
    private void handleAbstractLoad(String instanceId, ParamInfo instance,
                                    ParamInfo value, String valueLocal, 
                                    String fsig) {
        //System.err.println("Performing abstract load");
        if(!heapLocToColor.containsKey(instanceId)) {
            // FIXME: Imprecise 
            // This is required for special cases where this instance was 
            // assigned inside a trace hole and therefore not properly 
            // initialized.
            heapLocToColor.put(instanceId,nextColor++);
        }
        // assert heapLocToColor.containsKey(instanceId)
        Set<Integer> colors = heapLocToColor.getAll(instanceId);
        String targetId;
        String sdaLocalName;
        if(value.isObjectLikeType()) {
            if(value.getId().equals("0")) return; // null value
            targetId = value.getId();
            
            sdaLocalName = fieldToSDA(instance, fsig);
            getRegSDAMap().put(valueLocal,sdaLocalName);
            propagateColors(targetId, sdaLocalName);
            
        } else {
            sdaLocalName = fieldToSDA(instance, fsig);
            getRegSDAMap().put(valueLocal,sdaLocalName);
            targetId = sdaLocalName;
        }
        for(Integer color : colors) {
            heapLocToColor.put(targetId, color);
        }
        HeapLoadEdge edge = new HeapLoadEdge(
            instanceId, 
            targetId, 
            fsig, 
            currentStep++);
        heapEdges.add(edge);
    }
    
    private void handleLoadInstanceEvent(LoadInstanceEvent event) {
        //System.err.println("Load Instance: " + event);
        ParamInfo instance = event.getInstance();
        assert instance.getId() != null;
        String instanceId = instance.getId();
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        if(destructivelyUpdatedFields.getAll(instanceId).contains(fsig)) return;
        
        handleAbstractLoad(instanceId, instance, event.getValue(), 
                           event.getLocalName(), fsig);
    }
    
    private void handleLoadStaticEvent(LoadStaticEvent event) {
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        String instanceId = fieldToSDA(null, fsig);
        if(destructivelyUpdatedFields.getAll("static").contains(fsig)) return;
        
        if(!heapLocToColor.containsKey(instanceId)) {
            heapLocToColor.put(instanceId, nextColor);
            colorToLabel.put(nextColor, fsig);
            nextColor++;
        }
        
        handleAbstractLoad(instanceId, null, event.getValue(), 
                           event.getLocalName(), fsig);
    }
    
    private void handleLoadArrayEvent(LoadArrayEvent event) {
        //System.err.println("Load Array: " + event);
        ParamInfo instance = event.getArrayInstance();
        assert instance.getId() != null;
        String instanceId = instance.getId();
        List<ParamInfo> indexes = event.getIndexes();
        String fsig = arrayFieldSignature(indexes);
        if(destructivelyUpdatedFields.getAll(instanceId).contains(fsig)) return;
        
        handleAbstractLoad(instanceId, instance, event.getValue(), 
                           event.getLocalName(), fsig);
    }
    
    private void handleAbstractStore(String instanceId, ParamInfo value,
                                    String valueLocal, String fsig) {
        String targetId;
        String sdaLocalName;
        if(value.isObjectLikeType()) {
            if(value.getId().equals("0")) return; // null value
            targetId = value.getId();
        } else {
            if(valueLocal.equals("_immediate_")) {
                sdaLocalName = newSDA();
                newLocations.add(sdaLocalName);
            } else {
                sdaLocalName = getRegSDAMap().get(valueLocal);
            }
            if(sdaLocalName == null) {
                // FIXME: Temp code to handle yet-to-be interpreted events
                sdaLocalName = newSDA();
            }
            targetId = sdaLocalName;
        }
        Set<Integer> instanceColors = heapLocToColor.getAll(instanceId);
        HeapEventEdge edge;
        for(Integer colorInstance : instanceColors) {
            for(Integer colorValue : heapLocToColor.getAll(targetId)) {
                colorToColor.put(colorValue, colorInstance);
            }
        }
        if(newLocations.contains(targetId)) {
            newLocations.remove(targetId);
            edge = new HeapCreateEdge( 
                instanceId,
                targetId, 
                fsig, 
                currentStep++);
        } else {
            edge = new HeapStoreEdge( 
                targetId,
                instanceId,
                fsig, 
                currentStep++);
        }
        heapEdges.add(edge);
        
    }
    
    private void handleStoreInstanceEvent(StoreInstanceEvent event) {
        ParamInfo instance = event.getInstance();
        assert instance.getId() != null;
        String instanceId = instance.getId();
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        
        destructivelyUpdatedFields.put(instanceId, fsig);
        
        handleAbstractStore(instanceId, event.getValue(), event.getLocalName(), 
                            fsig);
    }
    
    private void handleStoreStaticEvent(StoreStaticEvent event) {
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        String instanceId = fieldToSDA(null, fsig);
        
        destructivelyUpdatedFields.put("static", fsig);
        
        
        if(!heapLocToColor.containsKey(instanceId)) {
            heapLocToColor.put(instanceId, nextColor);
            colorToLabel.put(nextColor, fsig);
            nextColor++;
        }
        
        handleAbstractStore(instanceId, event.getValue(), event.getLocalName(), 
                            fsig);
    }
    
    private void handleStoreArrayEvent(StoreArrayEvent event) {
        ParamInfo instance = event.getArrayInstance();
        assert instance.getId() != null;
        String instanceId = instance.getId();
        List<ParamInfo> indexes = event.getIndexes();
        String fsig = arrayFieldSignature(indexes);
        
        destructivelyUpdatedFields.put(instanceId, fsig);
        
        handleAbstractStore(instanceId, event.getValue(), event.getLocalName(), 
                            fsig);
    }
    
    private void handleNewObjectEvent(NewObjectEvent event) {
        //System.err.println("New Object: " + event);
        
        //FIXME: Should this even be a possible event recorded?
        // (currently is, potentially need to fix instrumentation)
        if(event.getType().equals("java.lang.String")) {
            return; // skip
        } 
        ParamInfo instance = event.getInstance();
        assert instance.getId() != null;
        heapLocToColor.put(instance.getId(),nextColor++);
        newLocations.add(instance.getId());
        
        // <clinit> might be called after a NewObjectEvent, for instrumented or 
        // uninstrumented code. Consume this call as a hole for now.
        traceHole = new ConsumeNewObjectGeneratedTraceHoleStrategy();
    }
    
    private void handleNewArrayEvent(NewArrayEvent event) {
        //System.err.println("New Array: " + event);
        ParamInfo instance = event.getArrayInstance();
        assert instance.getId() != null;
        heapLocToColor.put(instance.getId(),nextColor++);
        newLocations.add(instance.getId());
    }
    
    private void handleInstrumentedMethodCallEvent(MethodCallEvent event) {
        //System.err.println("Call: " + event);
        List<String> paramSDAMap = new ArrayList<String>();
        List<String> localNames = event.getLocalNames();
        for(int i = 0; i < localNames.size(); i++) {
            ParamInfo param = event.getParameter(i);
            String localName = localNames.get(i);
            String sdaLocalName;
            if(param.isPrimitiveType()) {
                if(localName.equals("_immediate_")) {
                    sdaLocalName = newSDA();
                    newLocations.add(sdaLocalName);
                } else {
                    sdaLocalName = getRegSDAMap().get(localName);
                }
                if(sdaLocalName == null) {
                    System.err.println(event);
                    System.err.println(localName);
                    // FIXME: Temp code to handle yet-to-be interpreted events
                    sdaLocalName = newSDA();
                }
                //assert sdaLocalName != null;
                paramSDAMap.add(sdaLocalName);
            } else if(param.isObjectLikeType()) {
                sdaLocalName = newSDA();
                paramSDAMap.add(sdaLocalName);
                propagateColors(param.getId(), sdaLocalName);
            } else {
                paramSDAMap.add(null); // empty marker
            }
        }
        abstractCallstack.peek().setParamSDAMap(paramSDAMap);
    }
    
    private void handleUninstrumentedMethodCallEvent(MethodCallEvent event) {
        System.err.println("Uninstrumented Call: " + event);
        MethodInfo m = event.getMethod();
        for(MethodInfo m1 : baseModels.keySet()) {
            System.err.println(m + " is not " + m1);
        }
        if(baseModels.containsKey(m)) {
            uninstrumentedCallStrategy = new UninstrumentedCallBaseModelStrategy(baseModels.get(m));
        } else {
            uninstrumentedCallStrategy = new UninstrumentedCallWorstCaseStrategy();
        }
        uninstrumentedCallStrategy.setCall(event);
        ConsumeUntilTraceHoleEndStrategy callTraceHole = new ConsumeUntilTraceHoleEndStrategy();
        callTraceHole.setCall(event);
        traceHole = callTraceHole;
    }
    
    private void handleMethodCallEvent(MethodCallEvent event) {
        if(!event.hasTag(SetUninstrumentedCallTagStream.TAG_NAME)) {
            handleInstrumentedMethodCallEvent(event);
            return;
        }
        boolean uninstrumented = (Boolean)event.getTag(SetUninstrumentedCallTagStream.TAG_NAME);
        if(uninstrumented) {
            handleUninstrumentedMethodCallEvent(event);
        } else {
            handleInstrumentedMethodCallEvent(event);
        }
    }
    
    private void handleMethodStartEvent(MethodStartEvent event) {
        //System.err.println("Start: " + event);
        MethodInfo method = event.getMethod();
        List<String> localNames = event.getLocalNames();
        List<String> paramSDAMap = abstractCallstack.peek().getParamSDAMap();
        assert paramSDAMap.size() == localNames.size();
        abstractCallstack.push(new AbstractCallstackFrame(method));
        for(int i = 0; i < paramSDAMap.size(); i++) {
            String localName = localNames.get(i);
            String sdaLocalName = paramSDAMap.get(i);
            if(sdaLocalName == null) continue;
            getRegSDAMap().put(localName, sdaLocalName);
        }
    }
    
    private void handleMethodEndEvent(MethodEndEvent event) {
        //System.err.println("End: " + event);
        String sdaLocalName = null;
        ParamInfo retVal = event.getReturnValue();
        if(retVal.isPrimitiveType()) {
            String localName = event.getLocalName();
            if(localName.equals("_immediate_")) {
                sdaLocalName = newSDA();
                newLocations.add(sdaLocalName);
            } else {
                sdaLocalName = getRegSDAMap().get(localName);
            }
            if(sdaLocalName == null) {
                System.err.println(event);
                // FIXME: Temp code to handle yet-to-be interpreted events
                sdaLocalName = newSDA();
            }
            //assert sdaLocalName != null;
        }
        abstractCallstack.pop();
        abstractCallstack.peek().setReturnSDA(sdaLocalName);
    }
    
    private void handleInstrumentedMethodReturnEvent(MethodReturnEvent event) {
        //System.err.println("Return: " + event);
        ParamInfo retVal = event.getReturnValue();
        String localName = event.getLocalName();
        String sdaLocalName;
        if(retVal.isPrimitiveType()) {
            if(localName.equals("_unused_")) return;
            sdaLocalName = abstractCallstack.peek().getReturnSDA();
            assert sdaLocalName != null;
            getRegSDAMap().put(localName, sdaLocalName);
        } else if(retVal.isObjectLikeType()) {
            sdaLocalName = newSDA();
            getRegSDAMap().put(localName, sdaLocalName);
            propagateColors(retVal.getId(), sdaLocalName);
        }
        abstractCallstack.peek().clearReturnSDA();
        abstractCallstack.peek().clearParamSDAMap();
    }
    
    private void handleUninstrumentedMethodReturnEvent(MethodReturnEvent event) {
        //System.err.println("Uninstrumented Return: " + event);
        ParamInfo retVal = event.getReturnValue();
        if(retVal.isObjectLikeType() && retVal.getId() != null) {
            String instanceId = retVal.getId();
            if(!heapLocToColor.containsKey(instanceId)) {
                heapLocToColor.put(instanceId,nextColor++);
                newLocations.add(instanceId);
            }
        }
        uninstrumentedCallStrategy.setReturn(event);
        uninstrumentedCallStrategy.process();
        uninstrumentedCallStrategy = null;
    }
    
    private void handleMethodReturnEvent(MethodReturnEvent event) {
        if(uninstrumentedCallStrategy != null) {
            handleUninstrumentedMethodReturnEvent(event);
        } else {
            handleInstrumentedMethodReturnEvent(event);
        }
    }
    
    private void handleSimpleAssignmentEvent(SimpleAssignmentEvent event) {
        String leftLocalName = event.getLeftLocalName();
        String leftLocalNameSDA = getRegSDAMap().get(leftLocalName);
        if(leftLocalNameSDA == null) {
            leftLocalNameSDA = newSDA();
            getRegSDAMap().put(leftLocalName, leftLocalNameSDA);
        }
        
        String rightLocalName = event.getRightLocalName();
        if(rightLocalName.equals("_immediate_")) return;
        String rightLocalNameSDA  = getRegSDAMap().get(rightLocalName);
        if(rightLocalNameSDA == null) {
            return; // Skip. Could be a reference type or the result of some event we don't handle yet.
        }
        
        propagateColors(rightLocalNameSDA, leftLocalNameSDA);
    }
    
    private void handleBinaryOperationEvent(BinaryOperationEvent event) {
        String resultLocalName = event.getResultLocalName();
        String resultLocalNameSDA = getRegSDAMap().get(resultLocalName);
        if(resultLocalNameSDA == null) {
            resultLocalNameSDA = newSDA();
            getRegSDAMap().put(resultLocalName, resultLocalNameSDA);
        }
        
        String firstOpLocalName = event.getFirstOperandLocalName();
        if(!firstOpLocalName.equals("_immediate_")) {
            String firstOpLocalNameSDA  = getRegSDAMap().get(firstOpLocalName);
            if(firstOpLocalNameSDA != null) {
                propagateColors(firstOpLocalNameSDA, resultLocalNameSDA);
            }
        }
        
        String secondOpLocalName = event.getSecondOperandLocalName();
        if(!secondOpLocalName.equals("_immediate_")) {
            String secondOpLocalNameSDA  = getRegSDAMap().get(secondOpLocalName);
            if(secondOpLocalNameSDA != null) {
                propagateColors(secondOpLocalNameSDA, resultLocalNameSDA);
            }
        }
    }
    
    private void handleArrayLengthEvent(ArrayLengthEvent event) {
        String localName = event.getLocalName();
        String localNameSDA = newSDA();
        getRegSDAMap().put(localName, localNameSDA);
    }
    
    private void handleCastEvent(CastEvent event) {
        String leftLocalName = event.getLeftLocalName();
        String leftLocalNameSDA = getRegSDAMap().get(leftLocalName);
        if(leftLocalNameSDA == null) {
            leftLocalNameSDA = newSDA();
            getRegSDAMap().put(leftLocalName, leftLocalNameSDA);
        }
        
        String rightLocalName = event.getRightLocalName();
        if(rightLocalName.equals("_immediate_")) return;
        String rightLocalNameSDA  = getRegSDAMap().get(rightLocalName);
        if(rightLocalNameSDA == null) {
            return; // Skip. Could be a reference type or the result of some event we don't handle yet.
        }
        
        propagateColors(rightLocalNameSDA, leftLocalNameSDA);
    }
    
    private void handleInstanceOfEvent(InstanceOfEvent event) {
        String leftLocalName = event.getLeftLocalName();
        String leftLocalNameSDA = newSDA();
        getRegSDAMap().put(leftLocalName, leftLocalNameSDA);
    }
    
    private void handleCaughtExceptionEvent(CaughtExceptionEvent event) {
        MethodInfo currentMethod = event.getPosition().getMethod();
        
        while(!abstractCallstack.peek().getMethod().equals(currentMethod)) {
            assert abstractCallstack.size() > 1;
            abstractCallstack.pop();
            recursionDepth--;
            assert abstractCallstack.size() == recursionDepth;
        }
        abstractCallstack.peek().clearReturnSDA();
        abstractCallstack.peek().clearParamSDAMap();
        
        if(uninstrumentedCallStrategy != null) {
            // Exception occurred inside uninstrumented code, we need to 
            // process with worst-case assumptions
            uninstrumentedCallStrategy.setException(event);
            uninstrumentedCallStrategy.process();
            uninstrumentedCallStrategy = null;
        }
    }
    
    private Map<String, Object> toJSONMap() {
        Map<String, Object> obj = new LinkedHashMap<String, Object>();
        obj.put("heapEdges", heapEdges);
        obj.put("parameters", parameters);
        obj.put("returns", posibleReturnValues);
        Map<String, Object> colorMap = new LinkedHashMap<String, Object>();
        colorMap.put("heapLocToColor", heapLocToColor);
        colorMap.put("colorToColor", colorToColor);
        colorMap.put("colorToLabel", colorToLabel);
        obj.put("colors", colorMap);
        return obj;
    }
    
    private Map<String,Integer> getLabelToColorMap() {
        Map<String,Integer> labelToColor = new LinkedHashMap<String,Integer>();
        for(Map.Entry<Integer,String> entry : colorToLabel.entrySet()) {
            assert !labelToColor.containsKey(entry.getValue());
            labelToColor.put(entry.getValue(), entry.getKey());
        }
        return labelToColor;
    }
    
    public void reachableColors(Set<Integer> reachable, int color) {
        reachable.add(color);
        for(int color2 : colorToColor.getAll(color)) {
            if(reachable.contains(color2)) continue;
            reachableColors(reachable, color2);
        }
    }
    
    public Set<Integer> reachableColors(int color) {
        Set<Integer> reachable = new HashSet<Integer>();
        reachableColors(reachable, color);
        reachable.remove(color);
        return reachable;
    }
    
    public MethodHeapModel getModel() {
        MethodHeapModel model = new MethodHeapModel();
        Map<String,Integer> labelToColor = getLabelToColorMap();
        for(JSONMethodParameterInfo jParam : parameters) {
            String name = jParam.getName();
            int color = labelToColor.get(name);
            for(int color2 : reachableColors(color)) {
                if(colorToLabel.containsKey(color2)) {
                    String pName2 = colorToLabel.get(color2);
                    model.add(name, pName2);
                }
            }
        }
        return model;
    }
    
    public String printableResults() {
        outputString += "\nAll seen event types: ";
        for(String et : seenEventTypes) outputString += et + ", ";
        outputString += "\nIgnored event types: ";
        for(String et : unhandledEventTypes) outputString += et + ", ";
        outputString += "\n\nJSON Encoded Analysis Data (for visualization):";
        outputString += JSONValue.toJSONString(this.toJSONMap());
        outputString += "\n\nGenerated Model";
        for(MethodHeapModelEntry entry : getModel()) {
            outputString += "\n\t" + entry.getFrom() + "->" + entry.getTo();
        }
        outputString += "\n";
        return outputString;
    }
}
