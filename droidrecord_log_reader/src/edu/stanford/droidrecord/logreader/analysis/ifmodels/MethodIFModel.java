
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis.ifmodels;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONValue;
import org.json.simple.JSONAware;

public class MethodIFModel implements Iterable<MethodIFModelEntry>, JSONAware {
    private final List<MethodIFModelEntry> entries;
    
    public MethodIFModel() {
        entries = new ArrayList<MethodIFModelEntry>();
    }
    
    // Preserves order and uniqueness
    public void add(MethodIFModelEntry entry) {
        for(int i = 0; i < entries.size(); i++) {
            MethodIFModelEntry e = entries.get(i);
            int compare = entry.getFrom().compareTo(e.getFrom());
            if(compare < 0) continue;
            else if(compare > 0) {
                entries.add(i, entry);
                return;
            } 
            assert compare == 0;
            
            compare = entry.getTo().compareTo(e.getTo());
            if(compare < 0) continue;
            else if(compare > 0) {
                entries.add(i, entry);
                return;
            }
            assert compare == 0;
            
            return; // duplicated entry
        }
        // Add last
        entries.add(entry);
    }
    
    public void add(String from, String to) {
        add(new MethodIFModelEntry(from, to));
    }
    
    public void add(MethodIFModel model) {
        for(MethodIFModelEntry entry : model)
            this.add(entry);
    }
    
    public Iterator<MethodIFModelEntry> iterator() {
        return Collections.unmodifiableList(entries).iterator();
    }
    
    public String toJSONString() {
        return JSONValue.toJSONString(entries);
    }
    
    public String toString() {
    	String s = "";
    	for(MethodIFModelEntry entry : entries) {
    		s += "\t" + entry + "\n";
    	}
    	return s;
    }
    
    public boolean equals(Object o) {
        if(!(o instanceof MethodIFModel)) return false;
        MethodIFModel other = (MethodIFModel)o;
        if(this.entries.size() != other.entries.size()) return false;
        for(int i = 0; i < entries.size(); i++) {
            if(!entries.get(i).equals(other.entries.get(i))) return false;
        }
        return true;
    }
}
