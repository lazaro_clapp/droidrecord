
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis.ifmodels;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import edu.stanford.droidrecord.logreader.events.info.MethodInfo;

public class MethodIFModels {
    
    private MethodIFModels() {
    }
    
    public static Map<MethodInfo, MethodIFModel> loadIFModelsFromFile(String baseModelsFilename) {
    	Map<MethodInfo, MethodIFModel> models = new HashMap<MethodInfo, MethodIFModel>();
        Pattern modelEntryPattern = Pattern.compile("^\t([a-zA-Z0-9#]+)->([a-zA-Z0-9#]+)$");
        try {
            BufferedReader br = new BufferedReader(new FileReader(baseModelsFilename));
            String line = br.readLine();
            while (line != null) {
                assert !line.startsWith("\t");
                MethodInfo method = MethodInfo.parse(line);
                MethodIFModel model = new MethodIFModel();
                line = br.readLine();
                
                while(line != null && (line.startsWith("\t") || line.startsWith(" "))) {
                    // tabs to spaces corrective magic:
                    if(line.startsWith("        ")) line = line.replace("        ","\t");
                    else if(line.startsWith("    ")) line = line.replace("    ","\t");
                    
                    Matcher m = modelEntryPattern.matcher(line);
                    if(!m.matches()) {
                        throw new Error(
                            String.format("Invalid base models file (%s): Model line \'%s\' (in method \'%s\') doesn't match regular expression \"%s\".",
                                    baseModelsFilename, line, method.getSignature(), modelEntryPattern.toString()));
                    }
                    model.add(m.group(1), m.group(2));
                    line = br.readLine();
                }
                models.put(method, model);
            }
            br.close();
        } catch(IOException e) {
            throw new Error(e);
        }
        return models;
    }
}
