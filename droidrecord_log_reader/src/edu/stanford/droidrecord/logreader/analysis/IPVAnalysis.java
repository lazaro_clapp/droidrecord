
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.analysis.RunnableELSAnalysis;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModel;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModelEntry;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModels;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.UninstrumentedCallStrategy;
import edu.stanford.droidrecord.logreader.events.ArrayLengthEvent;
import edu.stanford.droidrecord.logreader.events.BinaryOperationEvent;
import edu.stanford.droidrecord.logreader.events.CastEvent;
import edu.stanford.droidrecord.logreader.events.CaughtExceptionEvent;
import edu.stanford.droidrecord.logreader.events.EnterMonitorEvent;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.ExitMonitorEvent;
import edu.stanford.droidrecord.logreader.events.IfEvent;
import edu.stanford.droidrecord.logreader.events.InstanceOfEvent;
import edu.stanford.droidrecord.logreader.events.LoadArrayEvent;
import edu.stanford.droidrecord.logreader.events.LoadInstanceEvent;
import edu.stanford.droidrecord.logreader.events.LoadStaticEvent;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodReturnEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;
import edu.stanford.droidrecord.logreader.events.NegationEvent;
import edu.stanford.droidrecord.logreader.events.NewArrayEvent;
import edu.stanford.droidrecord.logreader.events.NewObjectEvent;
import edu.stanford.droidrecord.logreader.events.SimpleAssignmentEvent;
import edu.stanford.droidrecord.logreader.events.StoreArrayEvent;
import edu.stanford.droidrecord.logreader.events.StoreInstanceEvent;
import edu.stanford.droidrecord.logreader.events.StoreStaticEvent;
import edu.stanford.droidrecord.logreader.events.SwitchEvent;
import edu.stanford.droidrecord.logreader.events.info.FieldInfo;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.streams.SetUninstrumentedCallTagStream;

public abstract class IPVAnalysis extends RunnableELSAnalysis {

	public enum DefaultModelMode {
		WORST_CASE, BEST_CASE, NULL_MODEL
	}
    
    private class IPVUninstrumentedCallStrategy extends UninstrumentedCallStrategy {
    
    	public IPVUninstrumentedCallStrategy() {
    	}
    	
    	@Override
    	public void process() {
    		assert callE != null;
            assert retE != null ^ exceptE != null;
            if(retE != null) {
            	MethodInfo m = callE.getMethod();
            	MethodIFModel model = models.get(m);
            	if(model == null) model = getDefaultModel(callE);
            	handleUninstrumentedMethodCall(callE, retE, model);
            } else {
            	handleUninstrumentedMethodCall(callE, exceptE);
            }
    	}
    }
	
    private IPVUninstrumentedCallStrategy uninstrumentedCallStrategy;
    
    private interface TraceHoleStrategy {
        public boolean finished(Event event);
    }
    
    private class ConsumeUntilTraceHoleEndStrategy implements TraceHoleStrategy {
        private MethodCallEvent callE;
        public void setCall(MethodCallEvent event) { callE = event; }
        
        public ConsumeUntilTraceHoleEndStrategy() {
            callE = null;
        }
        
        public boolean finished(Event event) {
            int heightCall, height;
            if(event instanceof MethodReturnEvent) {
                heightCall = callE.getStackHeightInt();
                height = ((MethodReturnEvent)event).getStackHeightInt();
                assert heightCall <= height;
                if(heightCall == height) {
                    assert ((MethodReturnEvent)event).getMethod().equals(callE.getMethod());
                    return true;
                }
            } else if(event instanceof CaughtExceptionEvent) {
                heightCall = callE.getStackHeightInt();
                height = ((CaughtExceptionEvent)event).getStackHeightInt();
                if(heightCall >= height) return true;
            }
            
            return false;
        }
    }
    
    /* A NewObjectEvent can prompt an implicit call to <clinit>, we use the 
     * following strategy object to consume any instrumented methods called 
     * within that initializer.
     */
    private class ConsumeNewObjectGeneratedTraceHoleStrategy implements TraceHoleStrategy {
        
        private MethodStartEvent startE;
        
        public ConsumeNewObjectGeneratedTraceHoleStrategy() {
            startE = null;
        }
        
        public boolean finished(Event event) {
            int startHeight, height;
            if(startE == null) {
                // Hole hasn't started yet.
                if(event instanceof MethodStartEvent) {
                    // There is a hole after this new, start consuming.
                    startE = (MethodStartEvent)event;
                    return false;
                } else {
                    return true; // No hole
                }
            } else {
                // Hole consumption in progress
                if(event instanceof MethodEndEvent) {
                    startHeight = startE.getStackHeightInt();
                    height = ((MethodEndEvent)event).getStackHeightInt();
                    assert startHeight <= height;
                    if(startHeight == height) {
                        // Found the matching end, reset the state of this 
                        // strategy to no-hole, but still consume this event 
                        // and keep an eye for another potential MethodStartEvent
                        // just after this.
                        assert ((MethodEndEvent)event).getMethod().equals(startE.getMethod());
                        startE = null;
                    }
                    return false;
                } else if(event instanceof CaughtExceptionEvent) {
                    throw new Error("WARNING: Exception detected after " +
                    "NewObjectEvent and caught on instrumented code. " + 
                    "Unfortunatelly, we currently have no way of knowing " +
                    "whether this instrumented code is inside a trace hole or " +
                    "not, since NewObjectEvent doesn't record it's stack " +
                    "height. We could infer it from the last seen " +
                    "MethodStartEvent, but we should only think of doing this " +
                    "if this condition ever happens in practice.");
                } else {
                    return false;
                }
            }
        }
    }
    
    private long currentThread = -1;
    private TraceHoleStrategy traceHole = null;
	
	private Map<MethodInfo, MethodIFModel> models;
	private DefaultModelMode defaultModelMode = DefaultModelMode.WORST_CASE;
	
	private MethodCallEvent previousCallEvent;
	private MethodEndEvent previousEndEvent;
	
	public void setDefaultModelMode(DefaultModelMode mode) {
		this.defaultModelMode = mode;
	}
	
	private MethodIFModel getDefaultModel(MethodCallEvent event) {
		switch (defaultModelMode) {
            case WORST_CASE:
                MethodIFModel model = new MethodIFModel();
                MethodInfo method = event.getMethod();
		        int numArgs = event.getLocalNames().size();
		        boolean isInstanceMethod = false;
		        if(numArgs != method.getArguments().size()) {
		        	assert (method.getArguments().size()+1) == numArgs;
		        	isInstanceMethod = true;
		        }
		        for(int i = (isInstanceMethod ? 1 : 0); i < numArgs; ++i) {
		        	if(isInstanceMethod) {
		        		model.add("arg#" + i,"this");
		        		model.add("this","arg#" + i);
		        	}
		        	for(int j = (isInstanceMethod ? 1 : 0); j < numArgs; ++j) {
		        		if(i == j) continue;
		        		model.add("arg#" + i, "arg#" + j);
		        	}
		        	model.add("arg#" + i, "return");
		        }
		        if(isInstanceMethod) {
		        	model.add("this", "return");
		        }
                return model;
            case BEST_CASE:
                return new MethodIFModel();
            case NULL_MODEL:
            default:
                return null;
        }
	}

	public IPVAnalysis(EventLogStream els) {
        super(els);
	}
	
    public void run() {
    	if(models == null) {
		    String modelsFilename = this.getParameter("modelsFilename");
		    if(modelsFilename != null) {
		    	models = MethodIFModels.loadIFModelsFromFile(modelsFilename);
		    } else {
		    	models = new HashMap<MethodInfo, MethodIFModel>();
		    }
        }
        super.run();
    }

	protected void handleEvent(Event event) {
	
		if(event.getThreadId() != currentThread) {
			// Thread/trace switch, clean up pending/ongoing method calls.
			// This is needed because capture or execution might have been 
			// stopped before the top most call in a particular thread returns.
			traceHole = null;
			uninstrumentedCallStrategy = null;
			previousCallEvent = null;
			previousEndEvent = null;
			currentThread = event.getThreadId();
		}
	
		// Consume mid-trace holes 
        // (e.g. m1 calls m2 calls m3, m1 and m3 instrumented, m2 not instrumented => consume m3 )
        if(traceHole != null) {
            //System.err.println("handleEvent_InHole: " + event);
            if(!traceHole.finished(event)) return;
            traceHole = null;
        }
        
        //System.err.println("\n<IPVAnalysis_Dispatcher[run:" + this.getCurrentRunPass() + "]>Event[" + event.getID() + "]: " +  event);
        
    	if(event instanceof LoadInstanceEvent) {
            handleLoadInstanceEvent((LoadInstanceEvent)event);
        } else if(event instanceof LoadStaticEvent) {
            handleLoadStaticEvent((LoadStaticEvent)event);
        } else if(event instanceof LoadArrayEvent) {
            handleLoadArrayEvent((LoadArrayEvent)event);
        } else if(event instanceof StoreInstanceEvent) {
            handleStoreInstanceEvent((StoreInstanceEvent)event);
        } else if(event instanceof StoreStaticEvent) {
            handleStoreStaticEvent((StoreStaticEvent)event);
        } else if(event instanceof StoreArrayEvent) {
            handleStoreArrayEvent((StoreArrayEvent)event);
        } else if(event instanceof NewObjectEvent) {
            handleInternalNewObjectEvent((NewObjectEvent)event);
        } else if(event instanceof NewArrayEvent) {
            handleNewArrayEvent((NewArrayEvent)event);
        } else if(event instanceof MethodCallEvent) {
            handleMethodCallEvent((MethodCallEvent)event);
        } else if(event instanceof MethodStartEvent) {
            handleMethodStartEvent((MethodStartEvent)event);
        } else if(event instanceof MethodEndEvent) {
            handleMethodEndEvent((MethodEndEvent)event);
        } else if(event instanceof MethodReturnEvent) {
            handleMethodReturnEvent((MethodReturnEvent)event);
        } else if(event instanceof SimpleAssignmentEvent) {
            handleSimpleAssignmentEvent((SimpleAssignmentEvent)event);
        } else if(event instanceof BinaryOperationEvent) {
            handleBinaryOperationEvent((BinaryOperationEvent)event);
        } else if(event instanceof NegationEvent) {
            handleNegationEvent((NegationEvent)event);
        } else if(event instanceof ArrayLengthEvent) {
            handleArrayLengthEvent((ArrayLengthEvent)event);
        } else if(event instanceof CastEvent) {
            handleCastEvent((CastEvent)event);
        } else if(event instanceof InstanceOfEvent) {
            handleInstanceOfEvent((InstanceOfEvent)event);
        } else if(event instanceof CaughtExceptionEvent) {
            handleInternalCaughtExceptionEvent((CaughtExceptionEvent)event);
        } else if(event instanceof IfEvent) {
            handleIfEvent((IfEvent)event);
        } else if(event instanceof SwitchEvent) {
            handleSwitchEvent((SwitchEvent)event);
        } else if(event instanceof EnterMonitorEvent) {
            handleEnterMonitorEvent((EnterMonitorEvent)event);
        } else if(event instanceof ExitMonitorEvent) {
            handleExitMonitorEvent((ExitMonitorEvent)event);
        } else {
            throw new Error("Unexpected event type: " + event.getEventName());
        }
    }
    
    protected void handleLoadInstanceEvent(LoadInstanceEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleLoadStaticEvent(LoadStaticEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleLoadArrayEvent(LoadArrayEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleStoreInstanceEvent(StoreInstanceEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleStoreStaticEvent(StoreStaticEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleStoreArrayEvent(StoreArrayEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleNewObjectEvent(NewObjectEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleNewArrayEvent(NewArrayEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleSimpleAssignmentEvent(SimpleAssignmentEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleBinaryOperationEvent(BinaryOperationEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleNegationEvent(NegationEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleArrayLengthEvent(ArrayLengthEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleCastEvent(CastEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleInstanceOfEvent(InstanceOfEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleCaughtExceptionEvent(CaughtExceptionEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleIfEvent(IfEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleSwitchEvent(SwitchEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleEnterMonitorEvent(EnterMonitorEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleExitMonitorEvent(ExitMonitorEvent event) {
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleInstrumentedMethodCall(MethodCallEvent callE, MethodStartEvent startE) {
        // Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleInstrumentedMethodReturn(MethodEndEvent endE, MethodReturnEvent retE) {
        // Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleUninstrumentedMethodCall(MethodCallEvent callE, MethodReturnEvent retE, MethodIFModel model) {
        // Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleUninstrumentedMethodCall(MethodCallEvent callE, CaughtExceptionEvent exceptE) {
        // Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleInitialMethodStartEvent(MethodStartEvent event) {
        // Do nothing, subclasses must override if they need to handle this event.
    }
    
    private void handleInternalNewObjectEvent(NewObjectEvent event) {
        //FIXME: Should this even be a possible event recorded?
        // (currently is, potentially need to fix instrumentation)
        if(event.getType().equals("java.lang.String")) {
            return; // skip
        }
        
        handleNewObjectEvent(event);
        
        // <clinit> might be called after a NewObjectEvent, for instrumented or 
        // uninstrumented code. Consume this call as a hole for now.
        traceHole = new ConsumeNewObjectGeneratedTraceHoleStrategy();
    }
    
    private void handleUninstrumentedMethodCallEvent(MethodCallEvent event) {
        //System.err.println("Uninstrumented Call: " + event);
        MethodInfo m = event.getMethod();
        uninstrumentedCallStrategy = new IPVUninstrumentedCallStrategy();
        uninstrumentedCallStrategy.setCall(event);
        ConsumeUntilTraceHoleEndStrategy callTraceHole = new ConsumeUntilTraceHoleEndStrategy();
        callTraceHole.setCall(event);
        traceHole = callTraceHole;
    }
    
    private void handleInstrumentedMethodCallEvent(MethodCallEvent event) {
    	assert this.previousCallEvent == null;
    	this.previousCallEvent = event;
    }
    
    private void handleMethodCallEvent(MethodCallEvent event) {
        if(!event.hasTag(SetUninstrumentedCallTagStream.TAG_NAME)) {
            handleInstrumentedMethodCallEvent(event);
            return;
        }
        boolean uninstrumented = (Boolean)event.getTag(SetUninstrumentedCallTagStream.TAG_NAME);
        if(uninstrumented) {
            handleUninstrumentedMethodCallEvent(event);
        } else {
            handleInstrumentedMethodCallEvent(event);
        }
    }
    
    private void handleMethodStartEvent(MethodStartEvent event) {
    	if(this.previousCallEvent == null) {
    		handleInitialMethodStartEvent(event);
    		this.previousEndEvent = null;
        } else {
        	handleInstrumentedMethodCall(previousCallEvent, event);
        	this.previousCallEvent = null;
        }
    }
    
    private void handleMethodEndEvent(MethodEndEvent event) {
    	assert this.previousEndEvent == null;
    	this.previousEndEvent = event;
    }
    
    private void handleInstrumentedMethodReturnEvent(MethodReturnEvent event) {
    	assert this.previousEndEvent != null;
        handleInstrumentedMethodReturn(previousEndEvent, event);
        this.previousEndEvent = null;
    }
    
    private void handleUninstrumentedMethodReturnEvent(MethodReturnEvent event) {
        uninstrumentedCallStrategy.setReturn(event);
        uninstrumentedCallStrategy.process();
        uninstrumentedCallStrategy = null;
    }
    
    private void handleMethodReturnEvent(MethodReturnEvent event) {
        if(uninstrumentedCallStrategy != null) {
            handleUninstrumentedMethodReturnEvent(event);
        } else {
            handleInstrumentedMethodReturnEvent(event);
        }
    }
    
    private void handleInternalCaughtExceptionEvent(CaughtExceptionEvent event) {
    	if(uninstrumentedCallStrategy != null) {
            uninstrumentedCallStrategy.setException(event);
        	uninstrumentedCallStrategy.process();
        	uninstrumentedCallStrategy = null;
        } else {
            handleCaughtExceptionEvent(event);
        }
    }
    
}
