
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis.taintpropagation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.analysis.IPVAnalysis;
import edu.stanford.droidrecord.logreader.analysis.RunnableELSAnalysis;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModel;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModelEntry;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModels;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.UninstrumentedCallStrategy;
import edu.stanford.droidrecord.logreader.analysis.util.VLOCHeapModel;
import edu.stanford.droidrecord.logreader.analysis.util.VLOCMemoryLayer;
import edu.stanford.droidrecord.logreader.events.ArrayLengthEvent;
import edu.stanford.droidrecord.logreader.events.BinaryOperationEvent;
import edu.stanford.droidrecord.logreader.events.CastEvent;
import edu.stanford.droidrecord.logreader.events.CaughtExceptionEvent;
import edu.stanford.droidrecord.logreader.events.EnterMonitorEvent;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.ExitMonitorEvent;
import edu.stanford.droidrecord.logreader.events.IfEvent;
import edu.stanford.droidrecord.logreader.events.InstanceOfEvent;
import edu.stanford.droidrecord.logreader.events.LoadArrayEvent;
import edu.stanford.droidrecord.logreader.events.LoadInstanceEvent;
import edu.stanford.droidrecord.logreader.events.LoadStaticEvent;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodReturnEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;
import edu.stanford.droidrecord.logreader.events.NegationEvent;
import edu.stanford.droidrecord.logreader.events.NewArrayEvent;
import edu.stanford.droidrecord.logreader.events.NewObjectEvent;
import edu.stanford.droidrecord.logreader.events.SimpleAssignmentEvent;
import edu.stanford.droidrecord.logreader.events.StoreArrayEvent;
import edu.stanford.droidrecord.logreader.events.StoreInstanceEvent;
import edu.stanford.droidrecord.logreader.events.StoreStaticEvent;
import edu.stanford.droidrecord.logreader.events.info.FieldInfo;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.streams.SetUninstrumentedCallTagStream;

public abstract class AbstractTaintPropagationAnalysis extends IPVAnalysis {
	
    private UninstrumentedCallStrategy uninstrumentedCallStrategy;

	protected VLOCMemoryLayer vlocs = new VLOCMemoryLayer();
	protected VLOCHeapModel vheap = new VLOCHeapModel();
	private Map<Long,Set<Taint>> taintMap = new HashMap<Long,Set<Taint>>();

	public AbstractTaintPropagationAnalysis(EventLogStream els) {
        super(els);
        setDefaultModelMode(IPVAnalysis.DefaultModelMode.WORST_CASE);
	}
	
	@Override
	public void reset(EventLogStream els) {
		uninstrumentedCallStrategy = null;
		// Because the numbering of virtual locations is a determinist algorithm, 
		// we can re-run it with every pass and still get consistent mappings of 
		// objects and locals to vlocs to taint sets.
		// This a time vs storage trade-off compared to maintaing the VLOC mapping 
		// for locals at different program points.
		vlocs = new VLOCMemoryLayer();
		super.reset(els);
	}
	
	private void addTaint(long vloc, Taint taint) {
		if(!taintMap.containsKey(vloc)) {
			taintMap.put(vloc, new HashSet<Taint>());
		}
		Set<Taint> taintSet = taintMap.get(vloc);
		if(!taintSet.contains(taint)) {
			taintSet.add(taint);
			this.requestRerun();
		}
	}

	protected void addTaint(ParamInfo param, Taint taint) {
		long vloc = vlocs.getVLOC(param);
		addTaint(vloc, taint);
	}

	protected Set<Taint> getTaintMarks(ParamInfo param) {
		long vloc = vlocs.getVLOC(param);
	
		if(!taintMap.containsKey(vloc)) {
			return new HashSet<Taint>();
		}
		return taintMap.get(vloc);
	}
	
	protected void addTaint(String localName, Taint taint) {
		long vloc = vlocs.getVLOC(localName);
		addTaint(vloc, taint);
	}
	
	protected Set<Taint> getTaintMarks(String localName) {
		long vloc = vlocs.getVLOC(localName);
	
		if(!taintMap.containsKey(vloc)) {
			return new HashSet<Taint>();
		}
		return taintMap.get(vloc);
	}
	
	private void propagateTaint(long srcVLOC, long dstVLOC) {
		if(!taintMap.containsKey(srcVLOC)) return;
		if(!taintMap.containsKey(dstVLOC)) {
			taintMap.put(dstVLOC, new HashSet<Taint>());
		}
		Set<Taint> srcTaintSet = taintMap.get(srcVLOC);
		for(Taint t : srcTaintSet) {
			addTaint(dstVLOC, t);
		}
	}
	
	private void propagateTaint(String srcLocal, String dstLocal) {
		propagateTaint(vlocs.getVLOC(srcLocal),vlocs.getVLOC(dstLocal));
	}
	
	protected Set<Taint> getTaintMarksForAllReachableHeapObjects(ParamInfo root) {
		Set<Taint> taints = new HashSet<Taint>();
		for(long vloc : vheap.reachableLocations(vlocs.getVLOC(root))) {
			if(taintMap.containsKey(vloc)) {
				taints.addAll(taintMap.get(vloc));
			}
		}
		return taints;
	}
	
	private void handleAbstractLoad(ParamInfo instance, String fsig, String valueLocal) {
        long instanceVLOC = vlocs.getVLOC(instance);
        long valueVLOC = vlocs.getVLOC(valueLocal);
        long fieldVLOC = vlocs.getVLOC(instance, fsig);
        assert valueVLOC == fieldVLOC;
        vheap.addRefLink(instanceVLOC,fieldVLOC); // track locations reachable from o (in particular o.f/v is now reachable from o).
    }
    
    protected void handleLoadInstanceEvent(LoadInstanceEvent event) {
    	vlocs.handleLoadInstanceEvent(event);
    	ParamInfo instance = event.getInstance();
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        handleAbstractLoad(instance, fsig, event.getLocalName());
    }
    
    protected void handleLoadStaticEvent(LoadStaticEvent event) {
    	vlocs.handleLoadStaticEvent(event);
    	FieldInfo field = event.getField();
        String fsig = field.getSignature();
        handleAbstractLoad(vlocs.getGlobalStaticObjectReferece(), fsig, event.getLocalName());
    }
    
    protected void handleLoadArrayEvent(LoadArrayEvent event) {
    	vlocs.handleLoadArrayEvent(event);
    	ParamInfo instance = event.getArrayInstance();
        List<ParamInfo> indexes = event.getIndexes();
        String fsig = vlocs.arrayFieldSignature(indexes);
        handleAbstractLoad(instance, fsig, event.getLocalName());
    }
    
    private void handleAbstractStore(ParamInfo instance, String fsig, String valueLocal) {
        long valueVLOC = vlocs.getVLOC(valueLocal);
        long fieldVLOC = vlocs.getVLOC(instance, fsig);
        long instanceVLOC = vlocs.getVLOC(instance);
        propagateTaint(valueVLOC,fieldVLOC); // o.f = v; v taints o.f
        vheap.addRefLink(instanceVLOC,fieldVLOC); // track locations reachable from o (in particular o.f is now reachable from o).
    }
    
    protected void handleStoreInstanceEvent(StoreInstanceEvent event) {
    	vlocs.handleStoreInstanceEvent(event);
    	ParamInfo instance = event.getInstance();
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        handleAbstractStore(instance, fsig, event.getLocalName());
    }
    
    protected void handleStoreStaticEvent(StoreStaticEvent event) {
    	vlocs.handleStoreStaticEvent(event);
    	FieldInfo field = event.getField();
        String fsig = field.getSignature();
        handleAbstractStore(vlocs.getGlobalStaticObjectReferece(), fsig, event.getLocalName());
    }
    
    protected void handleStoreArrayEvent(StoreArrayEvent event) {
    	vlocs.handleStoreArrayEvent(event);
    	ParamInfo instance = event.getArrayInstance();
        List<ParamInfo> indexes = event.getIndexes();
        String fsig = vlocs.arrayFieldSignature(indexes);
        handleAbstractStore(instance, fsig, event.getLocalName());
    }
    
    protected void handleNewObjectEvent(NewObjectEvent event) {
    	vlocs.handleNewObjectEvent(event);
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleNewArrayEvent(NewArrayEvent event) {
    	vlocs.handleNewArrayEvent(event);
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleSimpleAssignmentEvent(SimpleAssignmentEvent event) {
    	vlocs.handleSimpleAssignmentEvent(event);
        String lhs = event.getLeftLocalName();
        String rhs = event.getRightLocalName();
        propagateTaint(rhs,lhs);
    }
    
    protected void handleBinaryOperationEvent(BinaryOperationEvent event) {
    	vlocs.handleBinaryOperationEvent(event);
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleNegationEvent(NegationEvent event) {
    	vlocs.handleNegationEvent(event);
        String lhs = event.getLeftLocalName();
        String rhs = event.getRightLocalName();
        propagateTaint(rhs,lhs);
    }
    
    protected void handleArrayLengthEvent(ArrayLengthEvent event) {
    	vlocs.handleArrayLengthEvent(event);
        String lhs = event.getLocalName();
        String rhs = event.getArrayLocalName();
        propagateTaint(rhs,lhs);
    }
    
    protected void handleCastEvent(CastEvent event) {
    	vlocs.handleCastEvent(event);
        String lhs = event.getLeftLocalName();
        String rhs = event.getRightLocalName();
        assert getTaintMarks(lhs).equals(getTaintMarks(rhs));
    }
    
    protected void handleInstanceOfEvent(InstanceOfEvent event) {
    	vlocs.handleInstanceOfEvent(event);
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleCaughtExceptionEvent(CaughtExceptionEvent event) {
    	vlocs.handleCaughtExceptionEvent(event);
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleIfEvent(IfEvent event) {
    	vlocs.handleIfEvent(event);
    	// Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleInstrumentedMethodCall(MethodCallEvent callE, MethodStartEvent startE) {
    	vlocs.handleInstrumentedMethodCall(callE,startE);
    	// Propagation handled by VLOC layer
        
        // Sanity checks:
        int numArgs = startE.getLocalNames().size();
        for(int i = 0; i < numArgs; ++i) {
        	if(startE.getParameter(i).isNonNullObjectLikeType()) {
        		assert getTaintMarks(startE.getParameter(i)).equals(getTaintMarks(startE.getLocalName(i)));
        		assert callE.getParameter(i).getType().equals(ParamInfo.STRING_TYPE) /*FIXME: Add object ID info to recorded String parameters*/ || getTaintMarks(startE.getParameter(i)).equals(getTaintMarks(callE.getParameter(i)));
        	}
        }
    }
    
    protected void handleInstrumentedMethodReturn(MethodEndEvent endE, MethodReturnEvent retE) {
    	vlocs.handleInstrumentedMethodReturn(endE,retE);
    	// Propagation handled by VLOC layer
        
        // Sanity checks:
        if(retE.getReturnValue().isNonNullObjectLikeType()) {
        	assert getTaintMarks(retE.getReturnValue()).equals(getTaintMarks(retE.getLocalName()));
        	assert getTaintMarks(retE.getReturnValue()).equals(getTaintMarks(endE.getReturnValue()));
        }
    }
    
    protected void handleUninstrumentedMethodCall(MethodCallEvent callE, MethodReturnEvent retE, MethodIFModel model) {
    
    	// Since executing a method might overwritte the VLOCs of one of its arguments, if 
    	// the same local is used to store the return address, we compute taint propagation
    	// *before* updating the VLOC layer.
    	// We cache the taint set of the return value and apply it after VLOC layer update.
    	Set<Taint> returnTaintSet = new HashSet<Taint>();
    	
    	String localSrc, localDst;
        for(MethodIFModelEntry entry : model) {
        	String from = entry.getFrom();
        	String to = entry.getTo();
        	if(from.equals("this")) {
        		assert callE.isInstanceMethod();
        		localSrc = callE.getLocalName(0);
        	} else {
        		assert from.startsWith("arg#");
        		int fromInt = Integer.parseInt(from.substring(4,from.length()));
        		localSrc = callE.getLocalName(fromInt);
        	}
        	if(to.equals("return")) {
        		returnTaintSet.addAll(getTaintMarks(localSrc));
        		continue;
        	} else if(to.equals("this")) {
        		assert callE.isInstanceMethod();
        		localDst = callE.getLocalName(0);
        	} else{
        		assert to.startsWith("arg#");
        		int toInt = Integer.parseInt(to.substring(4,to.length()));
        		localDst = callE.getLocalName(toInt);
        	}
        	propagateTaint(localSrc,localDst);
        }
        
        // Update VLOC layer
        vlocs.handleUninstrumentedMethodCall(callE,retE);
    	
    	// Propagate cached return value taints
		localDst = retE.getLocalName();
    	if(!localDst.equals("_unused_")) {
			long dstVLOC = vlocs.getVLOC(localDst);
			for(Taint t : returnTaintSet) {
				addTaint(dstVLOC, t);
			}
		}
        
        // Sanity checks:
        int numArgs = callE.getLocalNames().size();
        for(int i = 0; i < numArgs; ++i) {
        	if(callE.getParameter(i).isNonNullObjectLikeType()) {
        		assert getTaintMarks(callE.getParameter(i)).equals(getTaintMarks(callE.getLocalName(i)));
        	}
        }
        if(retE.getReturnValue().isNonNullObjectLikeType()) {
        	assert getTaintMarks(retE.getLocalName()).equals(getTaintMarks(retE.getReturnValue()));
        }
    }
    
    protected void handleUninstrumentedMethodCall(MethodCallEvent callE, CaughtExceptionEvent exceptE) {
    	vlocs.handleUninstrumentedMethodCall(callE,exceptE);
        // Do nothing, subclasses must override if they need to handle this event.
    }
    
    protected void handleInitialMethodStartEvent(MethodStartEvent event) {
    	vlocs.handleInitialMethodStartEvent(event);
        // Nothing to do, propagation handled by VLOC layer.
    }
    
    
}
