
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis.taintpropagation;

import java.util.TreeMap;
import java.util.Map;
import java.util.Set;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModel;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.IfEvent;
import edu.stanford.droidrecord.logreader.events.LoadInstanceEvent;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodReturnEvent;
import edu.stanford.droidrecord.logreader.events.StoreInstanceEvent;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;

public class LoginDetectionAnalysis extends AbstractTaintPropagationAnalysis {
	
	private Map<Long,IfEvent> taintedConditionals = new TreeMap<Long,IfEvent>(); // TreeMap is sorted by keys, not by hash, so it's more useful for printing out the results.

	public LoginDetectionAnalysis(EventLogStream els) {
        super(els);
	}
    
    protected void handleUninstrumentedMethodCall(MethodCallEvent callE, MethodReturnEvent retE, MethodIFModel model) {
    	super.handleUninstrumentedMethodCall(callE,retE,model);
    	String atMethodSig;
		Set<Taint> taintMarks;
    	
    	// DEBUG: Print method tainted arguments
    	if(callE.getMethod().getSignature().equals("<org.apache.http.client.methods.HttpUriRequest: java.net.URI getURI()>") || 
    		callE.getMethod().getSignature().equals("<java.util.concurrent.ThreadPoolExecutor: java.util.concurrent.Future submit(java.lang.Runnable)>") ||
    		callE.getMethod().getSignature().equals("<com.android.asynchttp.AsyncHttpRequest: void run()>") ||
    		callE.getMethod().getSignature().equals("<org.apache.http.client.methods.HttpGet: void <init>(java.lang.String)>") || 
    		callE.getMethod().getSignature().equals("<org.apache.http.impl.client.AbstractHttpClient: org.apache.http.HttpResponse execute(org.apache.http.client.methods.HttpUriRequest,org.apache.http.protocol.HttpContext)>") ||
    		callE.getMethod().getSignature().equals("<org.apache.http.HttpResponse: org.apache.http.HttpEntity getEntity()>") ||
    		callE.getMethod().getSignature().equals("<java.util.concurrent.Executors: java.util.concurrent.ExecutorService newCachedThreadPool()>")){
			System.err.println("Method CALL: " + callE.getMethod());
			System.err.println("Run: " + this.getCurrentRunPass());
			atMethodSig = callE.getPosition().getMethod().getSignature();
			for(int i = 0; i < callE.getLocalNames().size(); ++i) {
				ParamInfo p = callE.getParameter(i);
				String local = callE.getLocalName(i);
				if(p.isNonNullObjectLikeType()) {
					taintMarks = getTaintMarks(p);
				} else {
					taintMarks = getTaintMarks(local);
				}
				System.err.println("Taints: " + taintMarks.size());
				for(Taint t : taintMarks) {
					if(t instanceof TypedTaint) {
						System.err.println(p + " tainted with " + (TypedTaint)t);
					}
				}
			}
			if(retE.getReturnValue().isNonNullObjectLikeType()) {
				taintMarks = getTaintMarks(retE.getReturnValue());
			} else {
				taintMarks = getTaintMarks(retE.getLocalName());
			}
			for(Taint t : taintMarks) {
				if(t instanceof TypedTaint) {
					System.err.println(retE.getReturnValue() + " tainted with " + (TypedTaint)t);
				}
			}
    	}
    	
    	// Introduce new taint
    	// TODO: config from file
    	String methodSig = retE.getMethod().getSignature();
    	if(methodSig.equals("<android.widget.EditText: android.text.Editable getText()>")) {
    		String local = retE.getLocalName();
    		atMethodSig = retE.getPosition().getMethod().getSignature();
    		addTaint(local, new TypedTaint("TextInput", Long.parseLong(callE.getParameter(0).getId())));
    	} else if(methodSig.equals("<org.apache.http.impl.client.AbstractHttpClient: org.apache.http.HttpResponse execute(org.apache.http.client.methods.HttpUriRequest,org.apache.http.protocol.HttpContext)>")) {
    		String retLocal = retE.getLocalName();
    		ParamInfo requestParam = callE.getParameter(1);
    		assert requestParam.isNonNullObjectLikeType();
    		int countEditTaintMarks = 0;
    		taintMarks = getTaintMarksForAllReachableHeapObjects(requestParam);
    		for(Taint t : taintMarks) {
				if(t instanceof TypedTaint) {
					if(((TypedTaint)t).getType().equals("TextInput")) {
						countEditTaintMarks++;
					}
				}
			}
			if(countEditTaintMarks >= 2) {
				addTaint(retLocal, new TypedTaint("HttpResponse", Long.parseLong(retE.getReturnValue().getId())));
			}
    	}
    }
    
    protected void handleIfEvent(IfEvent event) {
    	super.handleIfEvent(event);
    	boolean taintedWithResponse = false;
    	Set<Taint> taintMarks;
    	for(String local : event.getLocals()) {
    		taintMarks = getTaintMarks(local);
    		for(Taint t : taintMarks) {
    			if(t instanceof TypedTaint && ((TypedTaint)t).getType().equals("HttpResponse")) {
    				taintedWithResponse = true;
    			}
    		}
    	}
    	if(taintedWithResponse && !taintedConditionals.containsKey(event.getID())) {
    		taintedConditionals.put(event.getID(), event);
    	}
    }
    
    public String printableResults() {
        StringBuilder sb = new StringBuilder();;
     	for (Long key : taintedConditionals.keySet()) { 
		   String value = taintedConditionals.get(key).toString();
		   sb.append(value);
		   sb.append("\n");
		}
     	return sb.toString();
    }
}
