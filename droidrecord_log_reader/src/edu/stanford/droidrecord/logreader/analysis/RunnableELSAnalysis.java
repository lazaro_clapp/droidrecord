
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.events.Event;

public abstract class RunnableELSAnalysis {
    
    private EventLogStream els;
    
    private boolean ready;
    public boolean isReady() {
        return ready;
    }
    
    private final Map<String,String> parameters;
    public void setParameter(String name, String parameter) {
        parameters.put(name, parameter);
    }
    public String getParameter(String name) {
        return parameters.get(name);
    }
    
    private boolean rerun = false;
    public boolean shouldRerun() { return rerun; }
    protected void requestRerun() { rerun = true; }
    
    private int passes = 0;
    protected int getCurrentRunPass() { return passes; }
    protected boolean isFirstPass() { return passes == 0; }
    
    private String outputFilename;
    public String getOutputFilename() { return outputFilename; }
    
    public RunnableELSAnalysis(EventLogStream els) {
        this.els = els;
        this.ready = false;
        this.parameters = new HashMap<String,String>();
    }
    
    public void reset(EventLogStream els) {
    	this.els = els;
        this.ready = false;
        this.rerun = false;
        ++passes;
    }
    
    protected abstract void handleEvent(Event event);
    
    public void run() {
        if(isReady()) return;
        if(outputFilename == null) outputFilename = this.getParameter("outfile");
        try{
            Event event;
            while((event = els.readNext()) != null)
            {
                //System.err.println("RunnableELSAnalysis read: " + event);
                handleEvent(event);
            }
        } catch(IOException e){
            throw new Error(e);
        }
        if(!shouldRerun()) ready = true;
    }
    
    public abstract String printableResults();
}
