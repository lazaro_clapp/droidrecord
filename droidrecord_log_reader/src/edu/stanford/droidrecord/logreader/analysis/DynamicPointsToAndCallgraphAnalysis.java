
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.CastEvent;
import edu.stanford.droidrecord.logreader.events.CaughtExceptionEvent;
import edu.stanford.droidrecord.logreader.events.LoadArrayEvent;
import edu.stanford.droidrecord.logreader.events.LoadInstanceEvent;
import edu.stanford.droidrecord.logreader.events.LoadStaticEvent;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodReturnEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;
import edu.stanford.droidrecord.logreader.events.NewArrayEvent;
import edu.stanford.droidrecord.logreader.events.NewObjectEvent;
import edu.stanford.droidrecord.logreader.events.SimpleAssignmentEvent;
import edu.stanford.droidrecord.logreader.events.StoreArrayEvent;
import edu.stanford.droidrecord.logreader.events.StoreInstanceEvent;
import edu.stanford.droidrecord.logreader.events.StoreStaticEvent;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.events.templates.MethodCallEventTemplate;

public class DynamicPointsToAndCallgraphAnalysis extends RunnableELSAnalysis {

    private String outDir;

	private static final class CallContext {
		private final PositionInfo position;
		public PositionInfo getPosition() { return position; }
		private final CallContext parentContext;
		private final int k;
		private final int stackHeight;
		public int getStackHeight() { return stackHeight; }
		
		private CallContext(int k) {
			this.k = k;
			this.parentContext = null;
			this.position = null;
			this.stackHeight = -1;
		}
		
		private CallContext(PositionInfo position, CallContext parentContext, int stackHeight) {
			this.position = position;
			this.parentContext = parentContext;
			this.k = parentContext.k;
			this.stackHeight = stackHeight;
		}
		
		public boolean isRootContext() {
		    return parentContext == null;
		}
		
		public boolean isHole() {
		    return parentContext != null && stackHeight == -1;
		}
		
		public static CallContext newEmptyContext(int k) {
			return new CallContext(k);
		}
		
		public CallContext getParentContext() {
			return parentContext;
		}
		
		public CallContext getContextAfterCalling(PositionInfo p, int stackHeight) {
		    assert stackHeight >= 0;
			return new CallContext(p, this, stackHeight);
		}
		
		public CallContext getContextAfterHole() {
		    return new CallContext(null, this, -1);
		}
		
		private static String callsiteToString(PositionInfo p) {
		    return p.getMethod().toString() + ":" + p.getJimpleStmtIndex();
		}
		
		public String toString() {
        	List<String> l = new ArrayList<String>();
        	CallContext c = this;
        	for(int i = 0; i < k; i++) {
        	    if(c.isHole()) {
        	        l.add("HOLE");
        	    } else if(c.isRootContext()) {
        	        break;
        	    } else {
            		l.add(callsiteToString(c.position));
        		}
            	c = c.parentContext;
        	}
        	return Arrays.toString(l.toArray());
		}
		
		private boolean hashCodeCacheValid = false;
		private int hashCodeCache = -1;
		@Override
		public int hashCode() {
			if(!hashCodeCacheValid) {
				List<PositionInfo> l = new ArrayList<PositionInfo>();
				CallContext c = this;
				while(!c.isRootContext()) {
					l.add(c.position);
					c = c.parentContext;
					assert c.k == this.k;
				}
				hashCodeCache = 37 * l.hashCode() + k;
				hashCodeCacheValid = true;
			}
		    return hashCodeCache;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof CallContext) {
				CallContext other = (CallContext) obj;
				if(this == other) return true;
				else if(this.isRootContext()) return other.isRootContext();
				else if(this.isHole()) return (this.k == other.k) && 
							other.isHole() && 
							(this.parentContext.equals(other.parentContext));
				else return (this.k == other.k) && 
							(this.position.equals(other.position)) && 
							(this.parentContext.equals(other.parentContext));
			} else {
				return false;
			}
		}
	}
	private CallContext currentContext;
	
	private static final class CallGraphEntry {
	    private final CallContext context;
	    public CallContext getContext() { return context; }
	    private final MethodInfo method;
	    public MethodInfo getMethod() { return method; }
	    
	    public CallGraphEntry(CallContext context, MethodInfo method) {
	        this.context = context;
	        this.method = method;
	    }
	    
	    public String toString() {
	        return this.toString("|");
	    }
	    
	    public String toString(String separator) {
        	return context.toString() + separator + method.toString();
		}
	    
	    @Override
		public int hashCode() {
		    return 37 * context.hashCode() + method.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof CallGraphEntry) {
				CallGraphEntry other = (CallGraphEntry) obj;
				if(this == other) return true;
				else return (this.context.equals(other.context)) && 
							(this.method.equals(other.method));
			} else {
				return false;
			}
		}
	}
	private final Set<CallGraphEntry> callgraphEntries = new HashSet<CallGraphEntry>();
	
	private static final class StaticAbstractObject {
	    private final CallContext context;
	    public CallContext getContext() { return context; }
	    private final PositionInfo allocationInst; // allocation instruction
	    public PositionInfo getAllocationInstruction() { return allocationInst; }
	    
	    public StaticAbstractObject(CallContext context, PositionInfo allocationInst) {
	        this.context = context;
	        this.allocationInst = allocationInst;
	    }
	    
	    @Override
		public int hashCode() {
		    return 37 * context.hashCode() + allocationInst.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof StaticAbstractObject) {
				StaticAbstractObject other = (StaticAbstractObject) obj;
				if(this == other) return true;
				else return (this.context.equals(other.context)) && 
							(this.allocationInst.equals(other.allocationInst));
			} else {
				return false;
			}
		}
	}
	private final Map<String,StaticAbstractObject> refIdToStaticAbstractObject = new HashMap<String,StaticAbstractObject>();
	
	private static final class PtEntry {
	    private final CallContext context;
	    public CallContext getContext() { return context; } 
	    private final String local;
	    public String getLocal() { return local; }
	    private final StaticAbstractObject abstractObj;
	    public StaticAbstractObject getAbstractObject() { return abstractObj; }
	    
	    public PtEntry(CallContext context, String local, StaticAbstractObject abstractObj) {
	        this.context = context;
	        this.local = local;
	        this.abstractObj = abstractObj;
	    }
	    
	    public String toString() {
	        return this.toString("|");
	    }
	    
	    public String toString(String separator) {
        	return context.toString() + separator + local + separator + 
        	       abstractObj.getContext().toString() + separator +
        	       abstractObj.getAllocationInstruction().getMethod().toString() + ":" +
        	       abstractObj.getAllocationInstruction().getJimpleStmtIndex();
		}
	    
	    @Override
		public int hashCode() {
		    int r = context.hashCode();
		    r = 37 * r + local.hashCode();
		    r = 37 * r + abstractObj.hashCode();
		    return r;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj instanceof PtEntry) {
				PtEntry other = (PtEntry) obj;
				if(this == other) return true;
				else return (this.context.equals(other.context)) && 
							(this.local.equals(other.local)) && 
							(this.abstractObj.equals(other.abstractObj));
			} else {
				return false;
			}
		}
	}
	private final Set<PtEntry> ptEntries = new HashSet<PtEntry>();

	public DynamicPointsToAndCallgraphAnalysis(EventLogStream els) {
        super(els);
 		currentContext = CallContext.newEmptyContext(Integer.MAX_VALUE);
	}
    
    protected void handleEvent(Event event) {
    	if(event instanceof CastEvent) {
            handleCastEvent((CastEvent)event);
    	} else if(event instanceof CaughtExceptionEvent) {
            handleCaughtExceptionEvent((CaughtExceptionEvent)event);
        } else if(event instanceof LoadInstanceEvent) {
            handleLoadInstanceEvent((LoadInstanceEvent)event);
        } else if(event instanceof LoadStaticEvent) {
            handleLoadStaticEvent((LoadStaticEvent)event);
        } else if(event instanceof LoadArrayEvent) {
            handleLoadArrayEvent((LoadArrayEvent)event);
        } else if(event instanceof MethodCallEvent) {
            handleMethodCallEvent((MethodCallEvent)event);
        } else if(event instanceof MethodStartEvent) {
            handleMethodStartEvent((MethodStartEvent)event);
        } else if(event instanceof MethodReturnEvent) {
        	handleMethodReturnEvent((MethodReturnEvent)event);
        } else if(event instanceof NewArrayEvent) {
            handleNewArrayEvent((NewArrayEvent)event);
        } else if(event instanceof NewObjectEvent) {
            handleNewObjectEvent((NewObjectEvent)event);
        } else if(event instanceof SimpleAssignmentEvent) {
            handleSimpleAssignmentEvent((SimpleAssignmentEvent)event);
        } else if(event instanceof StoreInstanceEvent) {
            handleStoreInstanceEvent((StoreInstanceEvent)event);
        } else if(event instanceof StoreStaticEvent) {
            handleStoreStaticEvent((StoreStaticEvent)event);
        } else if(event instanceof StoreArrayEvent) {
            handleStoreArrayEvent((StoreArrayEvent)event);
        }
    }
    
    private void handleCastEvent(CastEvent event) {
        // TODO: Maybe add pt facts, although this should be quickly discovered by static analysis
    }
    
    private void setLocalToValue(PositionInfo pos, String local, ParamInfo val) {
        if(!val.isObjectLikeType()) return;
        
        String id = val.getId();
        StaticAbstractObject absObj = refIdToStaticAbstractObject.get(id);
        if(absObj == null) return; // e.g. r1 = null; or an object defined in platform code
        
        local = local + "@" + pos.getMethod().toString();
        PtEntry ptE = new PtEntry(currentContext, local, absObj);
        ptEntries.add(ptE);
    }
    
    private void handleCaughtExceptionEvent(CaughtExceptionEvent event) {
        // Unwind stack/context
        int h = event.getStackHeightInt();
        while(currentContext.getStackHeight() >= h) {
            currentContext = currentContext.getParentContext();
        }
    
        setLocalToValue(event.getPosition(), 
                        event.getLocalName(), 
                        event.getException());
    }
    
    private void handleLoadInstanceEvent(LoadInstanceEvent event) {
        setLocalToValue(event.getPosition(), 
                        event.getLocalName(), 
                        event.getValue());
    }
    
    private void handleLoadStaticEvent(LoadStaticEvent event) {
        setLocalToValue(event.getPosition(), 
                        event.getLocalName(), 
                        event.getValue());
    }
    
    private void handleLoadArrayEvent(LoadArrayEvent event) {
        setLocalToValue(event.getPosition(), 
                        event.getLocalName(), 
                        event.getValue());
    }
    
    private void handleMethodCallEvent(MethodCallEvent event) {
        MethodInfo method = event.getMethod();
    	PositionInfo pos = event.getPosition();
    	int h = event.getStackHeightInt();
    	currentContext = currentContext.getContextAfterCalling(pos, h);
    	callgraphEntries.add(new CallGraphEntry(currentContext, method));
    }
    
    private void handleMethodStartEvent(MethodStartEvent event) {
        if(!currentContext.isRootContext()) {
        	int h = event.getStackHeightInt();
        	assert currentContext.getStackHeight() < h;
        	if(currentContext.getStackHeight() + 2 < h) {
        	    // hole detected, mark it in context
        	    currentContext = currentContext.getContextAfterHole();
        	}
    	}
    	PositionInfo pos = event.getPosition();
    	assert event.getParameters().size() == event.getLocalNames().size();
    	for(int i = 0; i < event.getLocalNames().size(); i++) {
    	    setLocalToValue(pos, 
                        event.getLocalName(i), 
                        event.getParameter(i));
    	}
    }
    
    private void handleMethodEndEvent(MethodEndEvent event) {
    	// remove ONE hole if needed
    	if(currentContext.isHole()) {
    	    currentContext = currentContext.getParentContext();
    	}
    }
    
    private void handleMethodReturnEvent(MethodReturnEvent event) {
        MethodInfo method = event.getMethod();
    	PositionInfo pos = event.getPosition();
    	int h = event.getStackHeightInt();
    	if(currentContext.getStackHeight() != h) System.err.println(event);
    	assert currentContext.getStackHeight() == h;
    	assert callgraphEntries.contains(new CallGraphEntry(currentContext, method));
    	currentContext = currentContext.getParentContext();
    	
    	setLocalToValue(pos, 
                        event.getLocalName(), 
                        event.getReturnValue());
    }
    
    private void handleNewArrayEvent(NewArrayEvent event) {
        ParamInfo instance = event.getArrayInstance();
    	PositionInfo pos = event.getPosition();
    	StaticAbstractObject absObj = new StaticAbstractObject(currentContext, pos);
    	assert instance.isObjectLikeType();
    	refIdToStaticAbstractObject.put(instance.getId(), absObj);
    	
    	// In r = new ...; we also want to record the straightforward points-to fact
    	setLocalToValue(pos, event.getArrayLocalName(), instance);
    }
    
    private void handleNewObjectEvent(NewObjectEvent event) {
        ParamInfo instance = event.getInstance();
    	PositionInfo pos = event.getPosition();
    	StaticAbstractObject absObj = new StaticAbstractObject(currentContext, pos);
    	assert instance.isObjectLikeType();
    	refIdToStaticAbstractObject.put(instance.getId(), absObj);
    	
    	// In r = new ...; we also want to record the straightforward points-to fact
    	setLocalToValue(pos, event.getLocalName(), instance);
    }
    
    private void handleSimpleAssignmentEvent(SimpleAssignmentEvent event) {
        // TODO: Maybe add pt facts, although this should be quickly discovered by static analysis
    }
    
    private void handleStoreInstanceEvent(StoreInstanceEvent event) {
        // TODO
    }
    
    private void handleStoreStaticEvent(StoreStaticEvent event) {
        // TODO
    }
    
    private void handleStoreArrayEvent(StoreArrayEvent event) {
        // TODO
    }
    
    public void run() {
        outDir = this.getParameter("outdir");
        super.run();
        
        if(outDir != null) writeOutputFiles(outDir);
    }
    
    public String printableResults() {
        String s;
     	if(outDir == null) {
		    s = "\nCallgraph:\n";
		    for(CallGraphEntry entry : callgraphEntries) {
         		s += entry.toString() + "\n";
         	}
         	s += "\nPointsTo Relation:\n";
		    for(PtEntry entry : ptEntries) {
         		s += entry.toString() + "\n";
         	}
     	} else {
     	    s = "\nAnalysis output writen to: " + outDir + "\n";
     	}
     	return s;
    }
    
    public void writeOutputFiles(String dir) {
        File d = new File(dir);
        d.mkdirs();
        try {
            // CallGraph
            BufferedWriter writer = new BufferedWriter(new FileWriter(dir + "/cg.dat"));
            for(CallGraphEntry entry : callgraphEntries) {
         		writer.write(entry.toString("\t"));
         		writer.newLine();
         	}
            writer.close();
            
            // points-to
            writer = new BufferedWriter(new FileWriter(dir + "/pt.dat"));
            for(PtEntry entry : ptEntries) {
         		writer.write(entry.toString("\t"));
         		writer.newLine();
         	}
            writer.close();
        } catch(IOException e) {
            throw new Error(e);
        }
    }
}
