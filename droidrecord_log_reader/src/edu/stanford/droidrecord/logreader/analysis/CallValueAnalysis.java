
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodReturnEvent;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;

public class CallValueAnalysis {
    
    public static class CallValueResult {
        
        private final MethodInfo method;
        public MethodInfo getMethod() { return method; }
        private final List<ParamInfo> arguments;
        public List<ParamInfo> getArguments() { 
            return Collections.unmodifiableList(arguments); 
        }
        private ParamInfo result;
        public ParamInfo getReturnValue() { return result; }
        
        public CallValueResult(MethodInfo method, List<ParamInfo> arguments) {
            this.method = method;
            this.arguments = arguments;
            this.result = null;
        }
        
        public void setResult(ParamInfo result) {
            this.result = result;
        }
    }
    
    private static class CallSite {
        
        private final MethodInfo method;
        public MethodInfo getMethod() { return method; }
        private final PositionInfo pos;
        private final List<CallValueResult> calls;
        public List<CallValueResult> getCalls() { 
            return Collections.unmodifiableList(calls); 
        }
        private final Map<Long,Stack<CallValueResult>> openCalls;
        
        public CallSite(MethodInfo method, PositionInfo pos) {
            this.method = method;
            this.pos = pos;
            this.calls = new ArrayList<CallValueResult>();
            this.openCalls = new HashMap<Long,Stack<CallValueResult>>();
        }
    
        private void processMethodCallEvent(MethodCallEvent e) {
            assert e.getPosition().equals(pos);
            assert e.getMethod().equals(method);
            List<ParamInfo> args = e.getParameters();
            CallValueResult call = new CallValueResult(method, args);
            calls.add(call);
            long threadId = e.getThreadId();
            if(!openCalls.containsKey(threadId)) {
                openCalls.put(threadId, new Stack<CallValueResult>());
            }
            openCalls.get(threadId).push(call);
        }
    
        private void processMethodReturnEvent(MethodReturnEvent e) {
            assert e.getPosition().equals(pos);
            assert e.getMethod().equals(method);
            long threadId = e.getThreadId();
            CallValueResult call = openCalls.get(threadId).pop();
            if(call == null) {
                System.err.println("WARNING: Unmatched return.");
            }
            call.setResult(e.getReturnValue());
        }
        
    }
    
    private final EventLogStream els;
    private final Map<PositionInfo,List<CallSite>> callsites;
    private boolean ready;
    
    public CallValueAnalysis(EventLogStream els) {
        this.els = els;
        callsites = new HashMap<PositionInfo,List<CallSite>>();
    }
    
    public boolean isReady() {
        return ready;
    }
    
    private CallSite getCallsite(PositionInfo pos, MethodInfo method) {
        if(!callsites.containsKey(pos)) {
            callsites.put(pos, new ArrayList<CallSite>());
        }
        CallSite callsite = null;
        for(CallSite cs : callsites.get(pos)) {
            if(cs.getMethod().equals(method)) {
                callsite = cs;
            }
        }
        if(callsite == null) {
            callsite = new CallSite(method,pos);
            callsites.get(pos).add(callsite);
        }
        return callsite;
    }
    
    private List<CallSite> getCallsites(PositionInfo pos) {
        return callsites.get(pos);
    }
    
    public void run() {
        if(isReady()) return;
        callsites.clear();
        CallSite callsite;
        try{
            Event event;
            while((event = els.readNext()) != null)
            {
                if(event instanceof MethodCallEvent) {
                    MethodCallEvent mcEvent = (MethodCallEvent)event;
                    callsite = getCallsite(mcEvent.getPosition(), 
                                           mcEvent.getMethod());
                    callsite.processMethodCallEvent(mcEvent);
                } else if(event instanceof MethodReturnEvent) {
                    MethodReturnEvent mrEvent = (MethodReturnEvent)event;
                    callsite = getCallsite(mrEvent.getPosition(), 
                                           mrEvent.getMethod());
                    callsite.processMethodReturnEvent(mrEvent);
                }
            }
        } catch(IOException e){
            throw new Error(e);
        }
        ready = true;
    }
    
    public List<CallValueResult> queryCallInfo(MethodInfo method, 
                                               PositionInfo position) {
        List<CallSite> csByPos = getCallsites(position);
        if(csByPos != null) {
            for(CallSite cs : csByPos) {
                if(cs.getMethod().equals(method)) {
                    return cs.getCalls();
                }
            }
        }
        return new ArrayList<CallValueResult>();
    }
    
    public List<CallValueResult> queryCallInfo(String methodSubSignature, 
                                               PositionInfo position) {
        List<CallValueResult> results = new ArrayList<CallValueResult>();
        List<CallSite> csByPos = getCallsites(position);
        if(csByPos != null) {
            for(CallSite cs : csByPos) {
                if(cs.getMethod().getSignature().contains(methodSubSignature)) {
                    for(CallValueResult call : cs.getCalls()) {
                        results.add(call);
                    }
                }
            }
        }
        return results;
    }
    
    public List<CallValueResult> queryCallInfo(String methodSubSignature,
                                               String parentMethodSignature,
                                               long srcLineNumber) {
        PositionInfo position = new PositionInfo(parentMethodSignature, 
                                                 srcLineNumber);
        return queryCallInfo(methodSubSignature, position); 
    }
}
