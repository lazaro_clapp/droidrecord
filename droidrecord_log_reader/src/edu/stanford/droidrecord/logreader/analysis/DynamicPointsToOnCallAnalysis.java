
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.analysis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.analysis.IPVAnalysis;
import edu.stanford.droidrecord.logreader.analysis.RunnableELSAnalysis;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModel;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModelEntry;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.MethodIFModels;
import edu.stanford.droidrecord.logreader.analysis.ifmodels.UninstrumentedCallStrategy;
import edu.stanford.droidrecord.logreader.analysis.util.VLOCMemoryLayer;
import edu.stanford.droidrecord.logreader.events.ArrayLengthEvent;
import edu.stanford.droidrecord.logreader.events.BinaryOperationEvent;
import edu.stanford.droidrecord.logreader.events.CastEvent;
import edu.stanford.droidrecord.logreader.events.CaughtExceptionEvent;
import edu.stanford.droidrecord.logreader.events.EnterMonitorEvent;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.ExitMonitorEvent;
import edu.stanford.droidrecord.logreader.events.IfEvent;
import edu.stanford.droidrecord.logreader.events.InstanceOfEvent;
import edu.stanford.droidrecord.logreader.events.LoadArrayEvent;
import edu.stanford.droidrecord.logreader.events.LoadInstanceEvent;
import edu.stanford.droidrecord.logreader.events.LoadStaticEvent;
import edu.stanford.droidrecord.logreader.events.MethodCallEvent;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodReturnEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;
import edu.stanford.droidrecord.logreader.events.NegationEvent;
import edu.stanford.droidrecord.logreader.events.NewArrayEvent;
import edu.stanford.droidrecord.logreader.events.NewObjectEvent;
import edu.stanford.droidrecord.logreader.events.SimpleAssignmentEvent;
import edu.stanford.droidrecord.logreader.events.StoreArrayEvent;
import edu.stanford.droidrecord.logreader.events.StoreInstanceEvent;
import edu.stanford.droidrecord.logreader.events.StoreStaticEvent;
import edu.stanford.droidrecord.logreader.events.info.FieldInfo;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;
import edu.stanford.droidrecord.logreader.events.info.ParamInfo;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.streams.SetUninstrumentedCallTagStream;

public class DynamicPointsToOnCallAnalysis extends IPVAnalysis {

	private static class AliasedLocationRecord {
	
		private String type;
		public String getType() { return type; }
		
		private PositionInfo position;
		public PositionInfo getPosition() { return position; }
		
		private String local;
		public String getLocal() { return local; }
		
		private ParamInfo value;
		public ParamInfo getValue() { return value; }
	
		private String comment;
		public String getComment() { return comment; }
	
		public AliasedLocationRecord(String type, PositionInfo position, String local, ParamInfo value, String comment) {
			this.type = type;
			this.position = position;
			this.local = local;
			this.value = value;
			this.comment = comment;
		}
	}

	private Map<String,PositionInfo> idToNewMap = new HashMap<String,PositionInfo>();
	private List<AliasedLocationRecord> callLocations = new ArrayList<AliasedLocationRecord>();

	public DynamicPointsToOnCallAnalysis(EventLogStream els) {
        super(els);
        setDefaultModelMode(IPVAnalysis.DefaultModelMode.BEST_CASE);
	}
	
	private boolean isPlatformField(FieldInfo field) {
		String klass = field.getKlass();
		if(klass.startsWith("java.")) return true;
		if(klass.startsWith("android.")) return true;
		if(klass.startsWith("com.android.")) return true;
		if(klass.startsWith("org.apache.")) return true;
		return false;
	}
	
	protected void handleLoadInstanceEvent(LoadInstanceEvent event) {
		PositionInfo position = event.getPosition();
		String local = event.getLocalName();
		ParamInfo value = event.getValue();
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        
        if(value.isObjectLikeType() && isPlatformField(field)) {
    		AliasedLocationRecord location = new AliasedLocationRecord("LOAD", position, local, value, fsig);
        	callLocations.add(location);
        }
    }
    
    protected void handleLoadStaticEvent(LoadStaticEvent event) {
		PositionInfo position = event.getPosition();
		String local = event.getLocalName();
		ParamInfo value = event.getValue();
        FieldInfo field = event.getField();
        String fsig = field.getSignature();
        
        if(value.isObjectLikeType() && isPlatformField(field)) {
    		AliasedLocationRecord location = new AliasedLocationRecord("LOAD", position, local, value, fsig);
        	callLocations.add(location);
        }
    }
    
    protected void handleNewObjectEvent(NewObjectEvent event) {
    	String instanceId = event.getInstance().getId();
    	assert !idToNewMap.containsKey(instanceId);
    	idToNewMap.put(instanceId,event.getPosition());
    }
    
    protected void handleNewArrayEvent(NewArrayEvent event) {
    	String instanceId = event.getArrayInstance().getId();
    	assert !idToNewMap.containsKey(instanceId);
    	idToNewMap.put(instanceId,event.getPosition());
    }
    
    protected void handleUninstrumentedMethodCall(MethodCallEvent callE, MethodReturnEvent retE, MethodIFModel model) {
    	MethodInfo callee = callE.getMethod();
    	PositionInfo callsite = callE.getPosition();
    	assert callsite.equals(retE.getPosition());
    	
    	int numArgs = callE.getLocalNames().size();
        for(int i = 0; i < numArgs; ++i) {
        	String local = callE.getLocalName(i);
        	ParamInfo value = callE.getParameter(i);
        	if(value.isObjectLikeType()) {
        		AliasedLocationRecord location = new AliasedLocationRecord("CALLSITE", callsite, local, value, callee.toString());
        		callLocations.add(location);
        	}
        }
        
        String local = retE.getLocalName();
        ParamInfo value = retE.getReturnValue();
        if(value.isObjectLikeType()) {
        	AliasedLocationRecord location = new AliasedLocationRecord("CALLSITE", callsite, local, value, callee.toString());
        	callLocations.add(location);
        }
    }
    
    protected void handleUninstrumentedMethodCall(MethodCallEvent callE, CaughtExceptionEvent exceptE) {
    	MethodInfo callee = callE.getMethod();
    	PositionInfo callsite = callE.getPosition();
    	
    	int numArgs = callE.getLocalNames().size();
        for(int i = 0; i < numArgs; ++i) {
        	String local = callE.getLocalName(i);
        	ParamInfo value = callE.getParameter(i);
        	if(value.isObjectLikeType()) {
        		AliasedLocationRecord location = new AliasedLocationRecord("CALLSITE", callsite, local, value, callee.toString());
        		callLocations.add(location);
        	}
        }
        
        String exceptLocal = exceptE.getLocalName();
        ParamInfo exceptValue = exceptE.getException();
        if(exceptValue.isObjectLikeType()) {
        	AliasedLocationRecord location = new AliasedLocationRecord("EXCEPTION", exceptE.getPosition(), exceptLocal, exceptValue, callee.toString());
        	callLocations.add(location);
        } 
    }
    
    public String printableResults() {
        StringBuilder sb = new StringBuilder();
     	for(AliasedLocationRecord location : callLocations) {
     		sb.append(location.getType() + " ");
     		sb.append(location.getPosition());
     		sb.append(",");
     		sb.append(location.getLocal());
     		sb.append("->");
     		String instanceId = location.getValue().getId();
     		if(idToNewMap.containsKey(instanceId)) {
     			sb.append(idToNewMap.get(instanceId));
     		} else {
     			sb.append(instanceId);
     		}
     		sb.append("\t// ");
     		sb.append(location.getComment());
     		sb.append("\n");
     	}
     	return sb.toString();
    }
}
