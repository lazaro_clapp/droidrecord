
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
    
public class HashListMultimap<K,V> {
        
    private Map<K, List<V>> inner;
    private int size;
        
    public HashListMultimap(){
        inner = new HashMap<K, List<V>>();
        size = 0;
    }
        
    public void put(K k, V v) {
        if(!inner.containsKey(k)) {
            inner.put(k, new ArrayList<V>()); 
        }
        inner.get(k).add(v);
        size++;
    }
        
    public boolean containsKey(K k) {
        return inner.containsKey(k);
    }
    
    public V peek(K k) {
        if(!inner.containsKey(k)) {
           return null; 
        }
        return inner.get(k).get(0);
    }
    
    public List<V> getAll(K k) {
        List<V> result = new ArrayList<V>();
        if(!inner.containsKey(k)) {
           return null; 
        }
        for(V v : inner.get(k)) {
        	result.add(v);
        }
        return result;
    }
    
    public V poll(K k) {
        if(!inner.containsKey(k)) {
           return null; 
        }
        if(inner.get(k).size() == 0) {
            return null;
        }
        size--;
        return inner.get(k).remove(0);
    }
    
    public boolean remove(K k, V v) {
    	boolean removed = inner.get(k).remove(v);
    	if(removed) size--;
    	return removed;
    }
    
    public int size() {
        return size;
    }
    
    public String toString() {
    	String s = "{";
    	List<K> nonemptyKeysList = new ArrayList<K>();
    	for(K k : inner.keySet()) {
    		if(inner.get(k).size() == 0) continue;
    		nonemptyKeysList.add(k);
    	}
    	for(int i = 0; i < nonemptyKeysList.size(); ++i) {
    		K k = nonemptyKeysList.get(i);
    		List<V> values = inner.get(k);
    		s += k.toString() + " => [";
    		for(int j = 0; j < values.size() && j <= 5; ++j) {
    			V v = values.get(j);
    			if(j != 0) s += ", ";
    			s += v.toString();
    		}
    		if(values.size() > 5) {
    			s += "...+" + (values.size()-5);
    		}
    		if(i == nonemptyKeysList.size()-1) s += "]";
    		else s += "], ";
    	}
    	s+= "}";
    	return s;
    }
}
