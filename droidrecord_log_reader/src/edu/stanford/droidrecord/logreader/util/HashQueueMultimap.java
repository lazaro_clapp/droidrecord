
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.util;

import java.util.HashMap;
import java.util.Queue;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
    
public class HashQueueMultimap<K,V> {
        
    private Map<K, Queue<V>> inner;
    private int size;
        
    public HashQueueMultimap(){
        inner = new HashMap<K, Queue<V>>();
        size = 0;
    }
        
    public void put(K k, V v) {
        if(!inner.containsKey(k)) {
            inner.put(k, new LinkedBlockingQueue<V>()); 
        }
        inner.get(k).add(v);
        size++;
    }
        
    public boolean containsKey(K k) {
        return inner.containsKey(k);
    }
    
    public V peek(K k) {
        if(!inner.containsKey(k)) {
           return null; 
        }
        return inner.get(k).peek();
    }
    
    public V poll(K k) {
        if(!inner.containsKey(k)) {
           return null; 
        }
        if(inner.get(k).size() == 0) {
            return null;
        }
        size--;
        return inner.get(k).poll();
    }
    
    public int size() {
        return size;
    }
    
    public String toString() {
    	String s = "{";
    	Queue<K> nonemptyKeysQueue = new LinkedBlockingQueue<K>();
    	for(K k : inner.keySet()) {
    		if(inner.get(k).size() == 0) continue;
    		nonemptyKeysQueue.add(k);
    	}
    	K[] keys = (K[])nonemptyKeysQueue.toArray();
    	for(int i = 0; i < keys.length; ++i) {
    		K k = keys[i];
    		Queue<V> q = inner.get(k);
    		V[] values = (V[])q.toArray();
    		s += k.toString() + " => [";
    		for(int j = 0; j < values.length && j <= 5; ++j) {
    			V v = values[j];
    			if(j != 0) s += ", ";
    			s += v.toString();
    		}
    		if(values.length > 5) {
    			s += "...+" + (values.length-5);
    		}
    		if(i == keys.length-1) s += "]";
    		else s += "], ";
    	}
    	s+= "}";
    	return s;
    }
}
