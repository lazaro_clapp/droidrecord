
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.BitSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.IfEvent;
import edu.stanford.droidrecord.logreader.events.SwitchEvent;
import edu.stanford.droidrecord.logreader.events.info.PositionInfo;
import edu.stanford.droidrecord.logreader.events.templates.EventTemplate;
import edu.stanford.droidrecord.logreader.events.templates.IfEventTemplate;
import edu.stanford.droidrecord.logreader.events.templates.MethodStartEventTemplate;
import edu.stanford.droidrecord.logreader.events.templates.SwitchEventTemplate;


public class CoverageReport {
    
    /**
     * BranchTracker: Internal use of CoverageReport
     * 
     * Maps if.taken, if.notTaken, switch.nth-branchTaken and 
     * switch.default-branchTaken to sequential unique branch numbers.
     * 
     * Usage:
     *  BranchTracker bt = new BranchTracker();
     *  bt.addIfTemplate(eventTemplate1);
     *  ...
     *  bt.addSwitchTemplate(eventTemplateK);
     *  ...
     *  int total = bt.totalBranches()
     *  int ifEventBranchNum = bt.ifToBranchNum(e1);
     *  int switchEventBranchNum = bt.switchToBranchNum(e1);
     **/
    private static class BranchTracker {
        
        private int total;
        private Map<Long, Integer> numberer;
        private Map<Integer, Long> reverse_map;
        
        public BranchTracker() {
            total = 0;
            numberer = new HashMap<Long, Integer>();
            reverse_map = new HashMap<Integer, Long>();
        }
        
        public int totalBranches() {
            return total;
        }
        
        public void addIfTemplate(IfEventTemplate t) {
            long id = t.getID();
            numberer.put(id,total);
            reverse_map.put(total, id);
            reverse_map.put(total + 1, id);
            total += 2; // [false,true]
        }
        
        public void addSwitchTemplate(SwitchEventTemplate t) {
            long id = t.getID();
            numberer.put(id,total);
            for(int i = 0; i <= t.getLookupValues().length; i++) {
                reverse_map.put(total + i, id);
            }
            total += t.getLookupValues().length + 1; // lookupVals + default
        }
        
        public int ifToBranchNum(IfEvent e) {
            long id = e.getID();
            int base = numberer.get(id); // [...,base = false,true,...]
            if(e.tookTrueBranch()) return base + 1;
            else return base;
        }
        
        public int switchToBranchNum(SwitchEvent e) {
            long id = e.getID();
            int base = numberer.get(id);
            int branchTaken = e.getBranchTakenNum();
            if(branchTaken == e.DEFAULT_BRANCH_NUM) {
                return base + e.getLookupValues().length;
            } else {
                return base + branchTaken;
            }
        }
        
        public long branchNumToEventID(int num) {
            return reverse_map.get(num);
        }
    }
    
    // TODO: This class can probably be generalized and used for things 
    // other than coverage reports
    public static class CoverageNamespaceFilter {
        
        private List<String> namespaces;
        
        public static CoverageNamespaceFilter getInclusiveFilter(String... namespaces) {
            return new CoverageNamespaceFilter(namespaces);
        }
        
        public static CoverageNamespaceFilter getExclusiveFilter(String... namespaces) {
            return new CoverageNamespaceFilter(namespaces).neg();
        }
        
        private CoverageNamespaceFilter(String... namespaces) {
            this.namespaces = Arrays.asList(namespaces);
        }
        
        public boolean includes(Event e) {
            String eKlass = e.getPosition().getMethod().getKlass();
            return includes(eKlass);
        }
        
        public boolean includes(EventTemplate e) {
            String eKlass = e.getPosition().getMethod().getKlass();
            return includes(eKlass);
        }
        
        public boolean includes(String klass) {
            for(String ns : namespaces) {
                if(klass.startsWith(ns)) return true;
            }
            return false;
        }
        
        public CoverageNamespaceFilter neg() {
            final CoverageNamespaceFilter f = this;
            return new CoverageNamespaceFilter() {
                public boolean includes(String klass) {
                    return !f.includes(klass);
                }
            };
        }
        
        public static CoverageNamespaceFilter join(List<CoverageNamespaceFilter> filters) {
            CoverageNamespaceFilter[] filtersArray = 
                new CoverageNamespaceFilter[filters.size()];
            filters.toArray(filtersArray);
            return join(filtersArray);
        }
        
        public static CoverageNamespaceFilter join(CoverageNamespaceFilter... filters) {
            final CoverageNamespaceFilter[] fs = filters;
            return new CoverageNamespaceFilter() {
                public boolean includes(String klass) {
                    boolean result = false;
                    for(CoverageNamespaceFilter f : fs) {
                        result = result || f.includes(klass);
                    }
                    return result;
                }
            };
        }
        
        public static CoverageNamespaceFilter intersect(List<CoverageNamespaceFilter> filters) {
            CoverageNamespaceFilter[] filtersArray = 
                new CoverageNamespaceFilter[filters.size()];
            filters.toArray(filtersArray);
            return intersect(filtersArray);
        }
        
        public static CoverageNamespaceFilter intersect(CoverageNamespaceFilter... filters) {
            final CoverageNamespaceFilter[] fs = filters;
            return new CoverageNamespaceFilter() {
                public boolean includes(String klass) {
                    boolean result = true;
                    for(CoverageNamespaceFilter f : fs) {
                        result = result && f.includes(klass);
                    }
                    return result;
                }
            };
        }
    }
    
    private final LogTemplate template;
    
    private final int statementCount;
    private final BitSet coveredStatementSet;
    
    private final LogTemplate.Index methodStartIndex;
    private final BitSet coveredMethodSet;
    
    private final BranchTracker branchTracker;
    private final BitSet coveredBranchesSet;
    
    public int coveredStmtsCount() {
        return coveredStatementSet.cardinality();
    }
    
    public int coveredStmtsCount(CoverageNamespaceFilter filter) {
        int count = 0;
        for(int i = 0; i < statementCount; i++) {
            if(coveredStatementSet.get(i)) {
                EventTemplate e = template.getEventTemplateById(i);
                if(filter.includes(e)) count++;
            }
        }
        return count;
    }
    
    public int totalStmtsCount() {
        return statementCount;
    }
    
    public int totalStmtsCount(CoverageNamespaceFilter filter) {
        int count = 0;
        for(int i = 0; i < statementCount; i++) {
            EventTemplate e = template.getEventTemplateById(i);
            if(filter.includes(e)) count++;
        }
        return count;
    }
    
    public String coveredStmtsPercent() {
        if(totalStmtsCount() == 0) return "100%";
        double percent = (100.0 * coveredStmtsCount()) / totalStmtsCount();
        return String.format("%.0f%%",percent);
    }
    
    public String coveredStmtsPercent(CoverageNamespaceFilter filter) {
        if(totalStmtsCount() == 0) return "100%";
        double percent = (100.0 * coveredStmtsCount(filter)) / totalStmtsCount(filter);
        return String.format("%.0f%%",percent);
    }
    
    public int coveredMethodsCount() {
        return coveredMethodSet.cardinality();
    }
    
    public int coveredMethodsCount(CoverageNamespaceFilter filter) {
        int count = 0;
        for(int i = 0; i < methodStartIndex.count(); i++) {
            if(coveredMethodSet.get(i)) {
                EventTemplate e = template.getEventTemplateById(
                    methodStartIndex.lookup(i));
                if(filter.includes(e)) count++;
            }
        }
        return count;
    }
    
    public int totalMethodsCount() {
        return methodStartIndex.count();
    }
    
    public int totalMethodsCount(CoverageNamespaceFilter filter) {
        int count = 0;
        for(int i = 0; i < methodStartIndex.count(); i++) {
            EventTemplate e = template.getEventTemplateById(
                methodStartIndex.lookup(i));
            if(filter.includes(e)) count++;
        }
        return count;
    }
    
    public String coveredMethodsPercent() {
        if(totalMethodsCount() == 0) return "100%";
        double percent = (100.0 * coveredMethodsCount()) / totalMethodsCount();
        return String.format("%.0f%%",percent);
    }
    
    public String coveredMethodsPercent(CoverageNamespaceFilter filter) {
        if(totalMethodsCount() == 0) return "100%";
        double percent = (100.0 * coveredMethodsCount(filter)) / totalMethodsCount(filter);
        return String.format("%.0f%%",percent);
    }
    
    public int coveredBranchesCount() {
        return coveredBranchesSet.cardinality();
    }
    
    public int coveredBranchesCount(CoverageNamespaceFilter filter) {
        int count = 0;
        for(int i = 0; i < branchTracker.totalBranches(); i++) {
            if(coveredBranchesSet.get(i)) {
                EventTemplate e = template.getEventTemplateById(
                    branchTracker.branchNumToEventID(i));
                if(filter.includes(e)) count++;
            }
        }
        return count;
    }
    
    public int totalBranchesCount() {
        return branchTracker.totalBranches();
    }
    
    public int totalBranchesCount(CoverageNamespaceFilter filter) {
        int count = 0;
        for(int i = 0; i < branchTracker.totalBranches(); i++) {
            EventTemplate e = template.getEventTemplateById(
                branchTracker.branchNumToEventID(i));
            if(filter.includes(e)) count++;
        }
        return count;
    }
    
    public String coveredBranchesPercent() {
        if(totalBranchesCount() == 0) return "100%";
        double percent = (100.0 * coveredBranchesCount()) / totalBranchesCount();
        return String.format("%.0f%%",percent);
    }
    
    public String coveredBranchesPercent(CoverageNamespaceFilter filter) {
        if(totalBranchesCount() == 0) return "100%";
        double percent = (100.0 * coveredBranchesCount(filter)) / totalBranchesCount(filter);
        return String.format("%.0f%%",percent);
    }
    
    public void setStatementCovered(Event event) {
        assert event.getID() <= Integer.MAX_VALUE;
        int id = (int)event.getID();
        coveredStatementSet.set(id);
        if(methodStartIndex.containsValue(id)) {
            coveredMethodSet.set((int)methodStartIndex.reverseLookup(id));
        }
        if(event instanceof IfEvent) {
            coveredBranchesSet.set(
                (int)branchTracker.ifToBranchNum((IfEvent)event));
        } else if(event instanceof SwitchEvent) {
            coveredBranchesSet.set(
                (int)branchTracker.switchToBranchNum((SwitchEvent)event));
        }
    }
    
    public boolean isCoveredLocation(String methodSig, long lineNum) {
        PositionInfo pos = new PositionInfo(methodSig, lineNum);
        for(EventTemplate e : template.getEventTemplatesByLocation(pos)) {
            if(coveredStatementSet.get((int)e.getID())) {
                return true;
            }
        }
        return false;
    }

    public CoverageReport(LogTemplate template) {
        this.template = template;
        statementCount = template.count();
        coveredStatementSet = new BitSet(statementCount);
        methodStartIndex = template.getMethodStartRecordIndex();
        coveredMethodSet = new BitSet(methodStartIndex.count());
        LogTemplate.Index ifIndex = template.getIfRecordIndex();
        LogTemplate.Index switchIndex = template.getSwitchRecordIndex();
        branchTracker = new BranchTracker();
        for(int i = 0; i < ifIndex.count(); i++) {
            branchTracker.addIfTemplate(
                (IfEventTemplate)template.getEventTemplateById(
                    ifIndex.lookup(i)));
        }
        for(int i = 0; i < switchIndex.count(); i++) {
            branchTracker.addSwitchTemplate(
                (SwitchEventTemplate)template.getEventTemplateById(
                    switchIndex.lookup(i)));
        }
        coveredBranchesSet = new BitSet(branchTracker.totalBranches());
    }
    
    public void add(CoverageReport other) {
        if(statementCount != other.statementCount)
            throw new Error("Adding incompatible CoverageReport objects: different statement count.");
        coveredStatementSet.or(other.coveredStatementSet);
        coveredMethodSet.or(other.coveredMethodSet);
        coveredBranchesSet.or(other.coveredBranchesSet);
    }
    
    public String toString() {
        return String.format("Instrumented Statement Coverage: %d/%d (%s)\n" +
                             "Branch Coverage: %d/%d (%s)\n" +
                             "Method Coverage: %d/%d (%s)",
                             coveredStmtsCount(),
                             totalStmtsCount(),
                             coveredStmtsPercent(),
                             coveredBranchesCount(),
                             totalBranchesCount(),
                             coveredBranchesPercent(),
                             coveredMethodsCount(),
                             totalMethodsCount(),
                             coveredMethodsPercent());
    }
    
    private void writerOutMethodByIndex(BufferedWriter writer, int i) throws IOException {
        long id = methodStartIndex.lookup(i);
        MethodStartEventTemplate methodStartTemplate = 
            (MethodStartEventTemplate)template.getEventTemplateById(id);
        writer.write(methodStartTemplate.getMethod().toString());
        writer.newLine();
    }
    
    public void writeOutCoveredMethods(BufferedWriter writer) throws IOException {
        for(int i = 0; i < methodStartIndex.count(); i++) {
            if(coveredMethodSet.get(i)) {
                writerOutMethodByIndex(writer, i);
            }
        }
    }
    
    public void writeOutCoveredMethods(String filename) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        writeOutCoveredMethods(writer);
        writer.close();
    }
    
    public void writeOutNotCoveredMethods(BufferedWriter writer) throws IOException {
        for(int i = 0; i < methodStartIndex.count(); i++) {
            if(!coveredMethodSet.get(i)) {
                writerOutMethodByIndex(writer, i);
            }
        }
    }
    
    public void writeOutNotCoveredMethods(String filename) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        writeOutNotCoveredMethods(writer);
        writer.close();
    }
}