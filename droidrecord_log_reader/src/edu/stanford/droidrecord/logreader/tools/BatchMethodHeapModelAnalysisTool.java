
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.Option;

import edu.stanford.droidrecord.logreader.BinLogReader;
import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.analysis.MethodHeapModelAnalysis;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;

public class BatchMethodHeapModelAnalysisTool {
    
    private String templateFileName;
    private String experimentDir;
    private String baseModelsFileName;
    
    private MethodHeapModelAnalysis.MethodHeapModel model = null;
    
    public void printUsage() {
        System.out.println("USAGE: droidrecord-batch-modelgen [--baseModels=droidrecord-modelgen.basemodels] droidrecord.log.template experiments_dir");
    }
    
    public void printConfig() {
        System.out.println("Template file: " + templateFileName);
        System.out.println("Experiment root directory: " + experimentDir);
    }
    
    public void printUsage(Options options) {
        System.out.println("USAGE: droidrecord-batch-modelgen [opts] droidrecord.log.template experiments_dir");
        System.out.println("");
        for(Object o : options.getOptions()) {
            assert o instanceof Option;
            Option opt = (Option)o;
            System.out.println(String.format("\t-%s,--%s\t%s",
                               opt.getOpt(),
                               opt.getLongOpt(),
                               opt.getDescription()));
        }
    }
    
    public void parseCommandLine(String[] args) {
        CommandLineParser parser = new PosixParser();
        // create the Options
        Options options = new Options();
        options.addOption("m", "basemodels", true, 
                          "Use the given basemodels file to model " +
                          "uninstrumented calls to know methods, instead " +
                          "of assuming worst-case behavior.");
        
        try {
            CommandLine cli = parser.parse(options, args);
            String[] nonOptArgs = cli.getArgs();
            if(nonOptArgs.length != 2) {
                printUsage(options);
                System.exit(1);
            }
            templateFileName = nonOptArgs[0];
            experimentDir = nonOptArgs[1];
            if(cli.hasOption("m")) {
                if(cli.getOptionValues("m").length > 1) {
                    System.out.println("Only one base models file may be specified using the -m|--basemodels option.");
                    printUsage(options);
                    System.exit(1);
                }
                baseModelsFileName = cli.getOptionValues("m")[0];
            }
        } catch(ParseException e) {
            System.out.println(e);
            printUsage(options);
            System.exit(1);
        }
    }
    
    public BatchMethodHeapModelAnalysisTool(String[] args) {
        parseCommandLine(args);
    }
    
    private void crawl(BinLogReader logReader, File dir) throws IOException {
        model = null;
        for(File f : dir.listFiles()) {
            if(f.isDirectory()) {
                crawl(logReader, f);
            } else if(f.getName().endsWith(".bin")) {
                processBinLog(logReader, f.getCanonicalPath());
            }
        }
        if(model != null) {
            writeModel(dir.getPath() + "/modelgen.out");
            model = null;
        }
    }
    
    private void writeToFile(String filename, String text) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        writer.write(text);
        writer.newLine();
        writer.close();
    }
    
    private void writeModel(String filename) throws IOException {
        String modelStr = "";
        for(MethodHeapModelAnalysis.MethodHeapModelEntry entry : model) {
            modelStr += entry.getFrom() + "->" + entry.getTo() + "\n";
        }
        writeToFile(filename, modelStr);
    }
    
    private void processBinLog(BinLogReader logReader, String binLogFile) throws IOException {
        System.out.println("Processing log file: " + binLogFile);
        System.err.println("Processing log file: " + binLogFile);
        EventLogStream els = logReader.parseLog(binLogFile);
        BufferedWriter writer;
        writer = new BufferedWriter(new FileWriter(binLogFile + ".decoded"));
        Event event;
        while((event = els.readNext()) != null) {
            writer.write(event.toString());
            writer.newLine();
        }
        writer.close();
        
        System.out.println("Started method heap model analysis log file: " + binLogFile);
        System.err.println("Started method heap model analysis log file: " + binLogFile);
        els = logReader.parseLog(binLogFile);
        MethodHeapModelAnalysis analysis = new MethodHeapModelAnalysis(els);
        analysis.setParameter("method", "");
        analysis.setParameter("baseModelsFilename", baseModelsFileName);
        try {
            analysis.run();
            if(analysis.isReady()) {
                writeToFile(binLogFile + ".analysis.out", analysis.printableResults());
                if(model == null) model = new MethodHeapModelAnalysis.MethodHeapModel();
                model.add(analysis.getModel());
            } else {
                System.out.println(
                    String.format("ERROR: Problem while running requested analysis."));
            }
        } catch(Exception e) {
            System.out.println("Exception thrown while modeling log file: " + binLogFile);
            e.printStackTrace(System.out);
        } catch(AssertionError e) {
            System.out.println("Exception thrown while modeling log file: " + binLogFile);
            e.printStackTrace(System.out);
        }
    }
    
    public void run() {
        BinLogReader logReader = new BinLogReader(templateFileName);
        System.out.println("Loaded template file.");
        logReader.setIgnoreCoverage(true);
        File dir = new File(experimentDir);
        assert dir.isDirectory();
        try{
            crawl(logReader, dir);
        } catch(IOException e){
            throw new Error(e);
        }
    }
    
    public static void main(String[] args) {
        BatchMethodHeapModelAnalysisTool tool = new BatchMethodHeapModelAnalysisTool(args);
        tool.run();
    }
}