
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONValue;
import org.json.simple.JSONAware;

import edu.stanford.droidrecord.logreader.analysis.MethodHeapModelAnalysis.MethodHeapModel;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;

public class ModelgenResultsGatherer {
    
    private static class ResultsNode {
        private Map<String,ResultsNode> subnodes;
        public Map<String,ResultsNode> getSubnodes() { return subnodes; }
        
        private MethodHeapModel stampModel;
        public MethodHeapModel getStampModel() { return stampModel; }
        
        private MethodHeapModel modelgenModel;
        public MethodHeapModel getModelgenModel() { return modelgenModel; }
        
        public ResultsNode() {
            this.subnodes = null;
            this.stampModel = null;
            this.modelgenModel = null;
        }
        
        public ResultsNode getOrMakeSubNode(String s) {
            if(subnodes == null) {
                subnodes = new HashMap<String,ResultsNode>();
                assert stampModel == null;
                assert modelgenModel == null;
            }
            if(subnodes.containsKey(s)) {
                return subnodes.get(s);
            }
            ResultsNode node = new ResultsNode();
            subnodes.put(s, node);
            return node;
        }
        
        public void addStampModelLine(String from, String to) {
            if(stampModel == null) {
                stampModel = new MethodHeapModel();
                assert subnodes == null;
            }
            stampModel.add(from, to);
        }
        
        public void addModelgenModelLine(String from, String to) {
            if(modelgenModel == null) {
                modelgenModel = new MethodHeapModel();
                assert subnodes == null;
            }
            modelgenModel.add(from, to);
        }
    }
    
    public static class ModelgenResults {
        private final ResultsNode root;
        public ResultsNode getRoot() { return root; }
        
        public ModelgenResults() {
            root = new ResultsNode();
        }
        
        private ResultsNode leafForMethod(MethodInfo method) {
            ResultsNode node = root;
            String[] ns = method.getKlass().split("\\.");
            for(String s : ns) {
                node = node.getOrMakeSubNode(s);
            }
            node = node.getOrMakeSubNode(method.toPrettySignature());
            return node;
        }
        
        public void addStampModelLine(MethodInfo method, String from, String to) {
            ResultsNode leaf = leafForMethod(method);
            leaf.addStampModelLine(from, to);
        }
        
        public void addModelgenModelLine(MethodInfo method, String from, String to) {
            ResultsNode leaf = leafForMethod(method);
            leaf.addModelgenModelLine(from, to);
        }
    }
    
    private final String stampAnnotationsFilename;
    private final String cutlogDirFilename;
    private final String outputFilename;
    
    private ModelgenResults results;
    
    public void printUsage() {
        System.out.println("USAGE: droidrecord-modelgen-gather-results stamp_annotations.txt batch_modelgen_dir output.json");
    }
    
    public void printConfig() {
        System.out.println("Stamp models (file): " + stampAnnotationsFilename);
        System.out.println("Droidrecord/Modelgen models (file): " + cutlogDirFilename);
        System.out.println("Output file: " + outputFilename);
    }
    
    public ModelgenResultsGatherer(String[] args) {
        results = new ModelgenResults();
        if(args.length != 3) {
            printUsage();
            System.exit(1);
        }
        stampAnnotationsFilename = args[0];
        cutlogDirFilename = args[1];
        outputFilename = args[2];
        printConfig();
    }
    
    public static boolean isInteger(String s) {
        return isInteger(s,10);
    }
    
    public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }
    
    private String parseStampTarget(String target) {
        if(isInteger(target)) {
            int val = Integer.parseInt(target);
            if(val == -1) return "return";
            else if(val == 0) return "this";
            else if(val > 0) return "arg#" + val;
            else throw new Error("Unexpected argument identifier: " + val);
        }
        return target;
    }
    
    private void loadStampModels() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(stampAnnotationsFilename));
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.trim().split(" ");
                assert parts.length == 3;
                MethodInfo method = MethodInfo.parseChordSignature(parts[0]);
                String from = parseStampTarget(parts[1]);
                String to = parseStampTarget(parts[2]);
                results.addStampModelLine(method, from, to);
            }
            br.close();
        } catch(IOException e) {
            throw new Error(e);
        }
    }
    
    private void modelgenCrawl(File dir) throws IOException {
        String modelgenOutPath = null;
        String methodInfoPath = null;
        for(File f : dir.listFiles()) {
            if(f.isDirectory()) {
                modelgenCrawl(f);
            } else if(f.getName().equals("modelgen.out")) {
                modelgenOutPath = f.getPath();
            } else if(f.getName().equals("method.info")) {
                methodInfoPath = f.getPath();
            }
        }
        if(modelgenOutPath != null) {
            assert methodInfoPath != null;
            String line;
            MethodInfo method = null;
            BufferedReader br = new BufferedReader(new FileReader(methodInfoPath));
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if(line.startsWith("Signature: ")) {
                    method = MethodInfo.parse(line.substring(11,line.length()));
                    break;
                }
            }
            assert method != null;
            br.close();
            br = new BufferedReader(new FileReader(modelgenOutPath));
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if(line.equals("")) continue;
                String[] parts = line.split("->");
                assert parts.length == 2;
                String from = parts[0];
                String to = parts[1];
                results.addModelgenModelLine(method, from, to);
            }
            br.close();
        }
    }
    
    private void loadModelgentModels() {
        File dir = new File(cutlogDirFilename);
        assert dir.isDirectory();
        try {
            modelgenCrawl(dir);
        } catch(IOException e) {
            throw new Error(e);
        }
    }
    
    private int matchCounter = 0;
    private int mismatchCounter = 0;
    private int stampOnly = 0;
    private int modelgenOnly = 0;
    private boolean prune = false; // TEMP
    
    private Map<String,Object> leafNodeToJsonMap(String methodSig, ResultsNode node) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("name", methodSig);
        map.put("type", "method");
        assert node.getSubnodes() == null;
        MethodHeapModel stampModel = node.getStampModel();
        MethodHeapModel modelgenModel = node.getModelgenModel();
        if(stampModel == null) {
            map.put("matches", "modelgen_only");
            modelgenOnly++;
            prune = true;
        } else if(modelgenModel == null) {
            map.put("matches", "stamp_only");
            stampOnly++;
            prune = true;
        } else {
            if(stampModel.equals(modelgenModel)) {
                map.put("matches", "yes");
                matchCounter++;
            } else {
                map.put("matches", "no");
                mismatchCounter++;
            }
        }
        if(stampModel != null) {
            map.put("stamp_model", stampModel);
        }
        if(modelgenModel != null) {
            map.put("modelgen_model", modelgenModel);
        }
        return map;
    }
    
    private Map<String,Object> nonLeafNodeToJsonMap(String namespace, ResultsNode node) {
        boolean prune = true;
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("name", namespace);
        map.put("type", "namespace");
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        map.put("subnodes",list);
        Map<String,ResultsNode> subnodes = node.getSubnodes();
        assert subnodes != null;
        for(Map.Entry<String,ResultsNode> entry : subnodes.entrySet()) {
            String key = entry.getKey();
            ResultsNode subnode = entry.getValue();
            Map<String,Object> subnodeAsJson = null;
            if(subnode.getSubnodes() != null) {
                subnodeAsJson = nonLeafNodeToJsonMap(key, subnode);
            } else {
                subnodeAsJson = leafNodeToJsonMap(key, subnode);
            }
            if(this.prune) {
                this.prune = false;
                continue;
            }
            prune = false;
            list.add(subnodeAsJson);
        }
        this.prune = prune;
        return map;
    }
    
    private void writeOuptut() {
        ResultsNode node = results.getRoot();
        String outputJson = JSONValue.toJSONString(nonLeafNodeToJsonMap("root",node));
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilename));
            writer.write(outputJson);
            writer.newLine();
            writer.close();
        } catch(IOException e) {
            throw new Error(e);
        }
        System.out.println("Models matching: " + matchCounter);
        System.out.println("Models NOT matching: " + mismatchCounter);
        System.out.println("Models only in STAMP: " + stampOnly);
        System.out.println("Models only generated by Droidrecord/Modelgen: " + modelgenOnly);
    }
    
    public void run() {
        loadStampModels();
        loadModelgentModels();
        writeOuptut();
    }
    
    public static void main(String[] args) {
        ModelgenResultsGatherer tool = new ModelgenResultsGatherer(args);
        tool.run();
    }
}