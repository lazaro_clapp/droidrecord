
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.tools.extra.fse2014;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.stanford.droidrecord.logreader.events.info.MethodInfo;

public class FSE2014CountAllModels {
    
    private final String cutlogDirFilename;
    private final Set<MethodInfo> methodsWithTrace;
    private final Set<MethodInfo> methodsWithNonEmptyModel;
    private int sanityCountWithRepeat = 0;
    
    public void printUsage() {
        System.out.println("USAGE: droidrecord-modelgen-fse2014-countall dir");
    }
    
    public void printConfig() {
        System.out.println("Droidrecord/Modelgen models (file): " + cutlogDirFilename);
    }
    
    public FSE2014CountAllModels(String[] args) {
        if(args.length != 1) {
            printUsage();
            System.exit(1);
        }
        cutlogDirFilename = args[0];
        printConfig();
        methodsWithTrace = new HashSet<MethodInfo>();
        methodsWithNonEmptyModel = new HashSet<MethodInfo>();
    }
    
    private void modelgenCrawl(File dir) throws IOException {
        String modelgenOutPath = null;
        String methodInfoPath = null;
        
        for(File f : dir.listFiles()) {
            if(f.isDirectory()) {
                modelgenCrawl(f);
            } else if(f.getName().equals("modelgen.out")) {
                modelgenOutPath = f.getPath();
            } else if(f.getName().equals("method.info")) {
                methodInfoPath = f.getPath();
            }
        }
        
        if(methodInfoPath == null) {
        	return;
        }
        
        String line;
        MethodInfo method = null;
        BufferedReader br = new BufferedReader(new FileReader(methodInfoPath));
        while ((line = br.readLine()) != null) {
        	line = line.trim();
            if(line.startsWith("Signature: ")) {
            	method = MethodInfo.parse(line.substring(11,line.length()));
                break;
            }
		}
        assert method != null;
        br.close();
        
        methodsWithTrace.add(method);
        sanityCountWithRepeat++;
        
        if(modelgenOutPath != null) {
        
        	boolean nonEmpty = false;
            
            br = new BufferedReader(new FileReader(modelgenOutPath));
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if(line.equals("")) continue;
                String[] parts = line.split("->");
                assert parts.length == 2;
                nonEmpty = true;
            }
            br.close();
            
            if(nonEmpty) methodsWithNonEmptyModel.add(method);
        }
    }
    
    private void loadModelgentModels() {
        File dir = new File(cutlogDirFilename);
        assert dir.isDirectory();
        try {
            modelgenCrawl(dir);
        } catch(IOException e) {
            throw new Error(e);
        }
    }
    
    public void printOuptut() {
        System.out.println("# of trace directories (w/ method name repetition): " + sanityCountWithRepeat);
        System.out.println("Total # of method with traces: " + methodsWithTrace.size());
        System.out.println("Total # of method with non-empty models: " + methodsWithNonEmptyModel.size());
    }
    
    public void run() {
        loadModelgentModels();
        printOuptut();
    }
    
    public static void main(String[] args) {
        FSE2014CountAllModels tool = new FSE2014CountAllModels(args);
        tool.run();
    }
}
