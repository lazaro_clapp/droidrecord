
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.tools.extra.fse2014;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.json.simple.JSONValue;
import org.json.simple.JSONAware;

import edu.stanford.droidrecord.logreader.analysis.MethodHeapModelAnalysis.MethodHeapModel;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;

public class FSE2014TestSuiteEffect {

    private final Random randomGenerator = new Random();
    
    private final String stampAnnotationsFilename;
    private final String cutlogDirFilename;
    private final String outputFilename;
    
    private class ModelSet {
        private MethodHeapModel fullModel;
        public MethodHeapModel getFullModel() { return fullModel; }
        public void setFullModel(MethodHeapModel model) { fullModel = model; }
        private final List<MethodHeapModel> models;
        public void addPartialModel(MethodHeapModel model) { models.add(model); }
        
        public ModelSet() {
            this.models = new ArrayList<MethodHeapModel>();
        }
        
        public int countPartialModels() {
            return models.size();
        }
        
        public int[] combinePartialUntilMatchingFull(int repeat) {
            int[] result = new int[repeat];
            for(int i = 0; i < repeat; i++) {
                result[i] = combinePartialUntilMatchingFull();
            }
            return result;
        }
        
        public int combinePartialUntilMatchingFull() {
            MethodHeapModel current = new MethodHeapModel();
            List<MethodHeapModel> remainingModels = new ArrayList<MethodHeapModel>();
            for(MethodHeapModel m : models) remainingModels.add(m);
            int count = 0;
            while(!current.equals(fullModel)) {
                assert remainingModels.size() != 0;
                int index = randomGenerator.nextInt(remainingModels.size());
                current.add(remainingModels.remove(index));
                count++;
            }
            return count;
        }
    }
    
    private final Map<MethodInfo,ModelSet> modelSets = new HashMap<MethodInfo,ModelSet>();
    
    public void printUsage() {
        System.out.println("USAGE: droidrecord-modelgen-fse2014-testsuiteeffect stamp_annotations.txt batch_modelgen_dir output.txt");
    }
    
    public void printConfig() {
        System.out.println("Stamp models (file): " + stampAnnotationsFilename);
        System.out.println("Droidrecord/Modelgen models (file): " + cutlogDirFilename);
        System.out.println("Output file: " + outputFilename);
    }
    
    public FSE2014TestSuiteEffect(String[] args) {
        if(args.length != 3) {
            printUsage();
            System.exit(1);
        }
        stampAnnotationsFilename = args[0];
        cutlogDirFilename = args[1];
        outputFilename = args[2];
        printConfig();
    }
    
    private void loadStampModels() {
        try {
            BufferedReader br = new BufferedReader(new FileReader(stampAnnotationsFilename));
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.trim().split(" ");
                assert parts.length == 3;
                MethodInfo method = MethodInfo.parseChordSignature(parts[0]);
                modelSets.put(method, new ModelSet());
            }
            br.close();
        } catch(IOException e) {
            throw new Error(e);
        }
    }
    
    private void modelgenCrawl(File dir) throws IOException {
        String modelgenOutPath = null;
        String methodInfoPath = null;
        List<String> modelgenOutFiles = new ArrayList<String>();
        MethodHeapModel model;
        
        for(File f : dir.listFiles()) {
            if(f.isDirectory()) {
                modelgenCrawl(f);
            } else if(f.getName().equals("modelgen.out")) {
                modelgenOutPath = f.getPath();
            } else if(f.getName().equals("method.info")) {
                methodInfoPath = f.getPath();
            } else if(f.getName().endsWith("analysis.out")) {
                modelgenOutFiles.add(f.getPath());
            }
        }
        if(modelgenOutPath != null) {
            assert methodInfoPath != null;
            String line;
            MethodInfo method = null;
            BufferedReader br = new BufferedReader(new FileReader(methodInfoPath));
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if(line.startsWith("Signature: ")) {
                    method = MethodInfo.parse(line.substring(11,line.length()));
                    break;
                }
            }
            assert method != null;
            br.close();
            
            if(!modelSets.containsKey(method)) return; // Skip methods without manual models
            ModelSet modelSet = modelSets.get(method);
            
            model = new MethodHeapModel();
            br = new BufferedReader(new FileReader(modelgenOutPath));
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if(line.equals("")) continue;
                String[] parts = line.split("->");
                assert parts.length == 2;
                String from = parts[0];
                String to = parts[1];
                model.add(from,to);
            }
            br.close();
            modelSet.setFullModel(model);
            model = null;
            
            for(String fpath : modelgenOutFiles) {
                model = new MethodHeapModel();
                br = new BufferedReader(new FileReader(fpath));
                boolean readingModel = false;
                while ((line = br.readLine()) != null) {
                    line = line.trim();
                    if(line.equals("")) continue;
                    if(line.equals("Generated Model")) {
                        readingModel = true;
                        continue;
                    }
                    if(readingModel == false) continue;
                    String[] parts = line.split("->");
                    assert parts.length == 2;
                    String from = parts[0];
                    String to = parts[1];
                    model.add(from,to);
                }
                br.close();
                modelSet.addPartialModel(model);
                model = null;
            }
        }
    }
    
    private void loadModelgentModels() {
        File dir = new File(cutlogDirFilename);
        assert dir.isDirectory();
        try {
            modelgenCrawl(dir);
        } catch(IOException e) {
            throw new Error(e);
        }
    }
    
    public void writeOuptut() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilename));
            for(MethodInfo method : modelSets.keySet()) {
                ModelSet modelSet = modelSets.get(method);
                if(modelSet.countPartialModels() == 0) continue; // No Modelgen models
                writer.write(method.toString());
                writer.write("\t(#Traces:" + modelSet.countPartialModels() + ")");
                for(int i = 0; i < 20; i++) {
                    writer.write("\t" + modelSet.combinePartialUntilMatchingFull());
                }
                writer.newLine();
            }
            writer.close();
        } catch(IOException e) {
            throw new Error(e);
        }
    }
    
    public void run() {
        loadStampModels();
        loadModelgentModels();
        writeOuptut();
    }
    
    public static void main(String[] args) {
        FSE2014TestSuiteEffect tool = new FSE2014TestSuiteEffect(args);
        tool.run();
    }
}
