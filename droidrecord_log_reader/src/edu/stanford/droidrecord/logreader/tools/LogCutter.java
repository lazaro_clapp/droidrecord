
/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.logreader.tools;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.stanford.droidrecord.logreader.BinLogReader;
import edu.stanford.droidrecord.logreader.EventLogStream;
import edu.stanford.droidrecord.logreader.events.Event;
import edu.stanford.droidrecord.logreader.events.MethodEndEvent;
import edu.stanford.droidrecord.logreader.events.MethodStartEvent;
import edu.stanford.droidrecord.logreader.events.info.MethodInfo;

public class LogCutter {
    
    private static class CutRecord {
        private int recursiveDepth;
        
        public CutRecord() {
            this.recursiveDepth = 1;
        }
        
        public void newStart() {
            this.recursiveDepth++;
        }
        
        public boolean newEnd() {
            this.recursiveDepth--;
            return (this.recursiveDepth == 0);
        }
    }
    
    private class Cutter {
        private final Map<MethodInfo, CutRecord> openRecords;
        private final Map<MethodInfo, Integer> counters;
        private final Map<MethodInfo, DataOutputStream> streams;
        private final Map<MethodInfo, String> filenames;
        
        public Cutter() {
            openRecords = new HashMap<MethodInfo, CutRecord>();
            counters = new HashMap<MethodInfo, Integer>();
            streams = new HashMap<MethodInfo, DataOutputStream>();
            filenames = new HashMap<MethodInfo, String>();
        }
        
        private void writeInfoFile(MethodInfo method, String filename) {
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
                System.out.println(filename);
                System.out.println("Signature: " + method.getSignature());
                writer.write("Signature: " + method.getSignature());
                writer.close();
            } catch(IOException e){
                throw new Error(e);
            }
        }
        
        public void processStartEvent(MethodStartEvent event) {
            MethodInfo m = event.getMethod();
            if(openRecords.containsKey(m)) {
                openRecords.get(m).newStart();
            } else {
                openRecords.put(m, new CutRecord());
                if(!counters.containsKey(m)) {
                    counters.put(m,0);
                } else {
                    counters.put(m,counters.get(m)+1);
                }
                String outFileName = methodToPath(m) + "/" + counters.get(m) + ".bin";
                //System.out.println(outFileName);
                filenames.put(m, outFileName);
                try {
                    (new File(outFileName)).getParentFile().mkdirs();
                    if(counters.get(m) == 0) {
                        writeInfoFile(m, methodToPath(m) + "/method.info");
                    }
                    DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outFileName)));
                    streams.put(m, dos);
                } catch(IOException e) {
                    throw new Error(e);
                }
            }
            processOtherEvent(event);
        }
        
        public void processEndEvent(MethodEndEvent event) {
            processOtherEvent(event);
            MethodInfo m = event.getMethod();
            if(openRecords.containsKey(m)) {
                if(openRecords.get(m).newEnd()) {
                    openRecords.remove(m);
                    try {
                        streams.get(m).flush();
                        streams.get(m).close();
                    } catch(IOException e){
                        throw new Error(e);
                    }
                    streams.remove(m);
                    filenames.remove(m);
                }
            }
        }
        
        public void processOtherEvent(Event event) {
            //System.err.println("\n<LogCutter_write>Event[" + event.getID() + "]: " + event);
            for(DataOutputStream dos : streams.values()) {
                try {
                    event.write(dos);
                } catch(IOException e) {
                    throw new Error(e);
                }
            }
        }
        
        public void cleanup() {
            MethodInfo[] keyArray = new MethodInfo[streams.keySet().size()];
            streams.keySet().toArray(keyArray); // Avoid modify-during-iteration-exception
            for(MethodInfo m : keyArray) {
                openRecords.remove(m);
                counters.put(m,counters.get(m)-1);
                try {
                    streams.get(m).flush();
                    streams.get(m).close();
                } catch(IOException e) {
                    throw new Error(e);
                }
                streams.remove(m);
                (new File(filenames.get(m))).delete();
                filenames.remove(m);
            }
        }
    }
    
    private final String templateFileName;
    private final String[] binLogFileNames;
    private final String outDir;
    
    private final Cutter cutter;
    
    public void printUsage() {
        System.out.println("USAGE: droidrecord-cutlog droidrecord.log.template droidrecord.log.bin1 [droidrecord.log.bin2...] output_dir");
        System.out.println("or: droidrecord-cutlog droidrecord.log.template -d input_dir output_dir");
    }
    
    public void printConfig() {
        System.out.println("Template file: " + templateFileName);
        for(String s : binLogFileNames)
            System.out.println("Binary log: " + s);
        System.out.println("Output directory: " + outDir);
    }
    
    private String methodToPath(MethodInfo m) {
        String klass = m.getKlass();
        String name = m.getName();
        String argdesc = "_" + m.getReturnType().replace(".","-") + "_";
        for(String arg : m.getArguments()) {
            argdesc += "_" + arg.replace(".","-");
        }
        if(argdesc.length() > 64) {
            argdesc = String.format("_%X",argdesc.hashCode());
        }
        return outDir + "/" + klass.replace(".","/") + "/" + name + argdesc;
    }
    
    public LogCutter(String[] args) {
        if(args.length < 3) {
            printUsage();
            System.exit(1);
        }
        int i = 0;
        templateFileName = args[i++];
        if(args[i].equals("-d")) {
            i++;
            String inDir = args[i++];
            List<String> binLogFileList = new ArrayList<String>();
            File dir = new File(inDir);
            assert dir.isDirectory();
            for(File f : dir.listFiles()) {
                if(f.getName().startsWith("droidrecord.log.bin.")) {
                    binLogFileList.add(f.getPath());
                }
            }
            binLogFileNames = new String[binLogFileList.size()];
            int j = 0;
            for(String s : binLogFileList) binLogFileNames[j++] = s;
        } else {
            binLogFileNames = new String[args.length - 2];
            while(i < args.length - 1) { binLogFileNames[i-1] = args[i]; i++; }
        }
        outDir = args[i];
        printConfig();
        cutter = new Cutter();
    }
    
    public void run() {
        BinLogReader logReader = new BinLogReader(templateFileName);
        System.out.println("Loaded template file.");
        logReader.setIgnoreCoverage(true);
        for(String binLogFile : binLogFileNames) {
            System.out.println("Processing log file: " + binLogFile);
            EventLogStream els = logReader.parseLogSimple(binLogFile);
            try{
                Event event;
                while((event = els.readNext()) != null)
                {
                    if(event instanceof MethodStartEvent) {
                        cutter.processStartEvent((MethodStartEvent)event);
                    } else if(event instanceof MethodEndEvent) {
                        cutter.processEndEvent((MethodEndEvent)event);
                    } else {
                        cutter.processOtherEvent(event);
                    }
                }
                cutter.cleanup();
            } catch(IOException e){
                throw new Error(e);
            }
        }
    }
    
    public static void main(String[] args) {
        LogCutter tool = new LogCutter(args);
        tool.run();
    }
}
