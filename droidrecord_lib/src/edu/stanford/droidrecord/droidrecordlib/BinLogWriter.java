/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */

package edu.stanford.droidrecord.droidrecordlib;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BinLogWriter {
    private static final String LOG_FILE_PREFIX = "/data/droidrecord/droidrecord.log.bin";
    
    private static final int AUTO_FLUSH_TIME = 1000;
    private static class AutoFlusher implements Runnable {
        
        private int time;
        private BinLogWriter writer;
        
        public AutoFlusher(BinLogWriter writer, int time) {
            this.writer = writer;
            this.time = time;
        }
        
        public void run() {
            while(true) {
                try {
                    Thread.sleep(time);
                } catch(InterruptedException e) {
                    return;
                }
                writer.flush();
            }
        }
    }
    private Thread autoFlusherThreadHandle;
	
	private DataOutputStream dos;
    private boolean closed;
    private boolean writeable;
    private boolean recursive;
    private boolean initStarted;
    private long runId;
    private long threadId;
    public long getThreadId() { return threadId; }
    private long subId; // To avoid overwriting files in case of thread ID re-use
	
	public BinLogWriter(long runId, long threadId, long subId) {
        this.runId = runId;
        this.threadId = threadId;
        this.subId = subId;
        this.closed = false;
        this.recursive = false;
        this.initStarted = false;
	}
	
	private static final int CANARY_LENGTH_BYTES = 10000;
	private synchronized void writeCanary() {
		try{
            String filename = LOG_FILE_PREFIX + ".run" + runId + ".thread" + threadId + "." + subId + ".canary";
			dos = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(filename)));
            for(int i = 0; i<CANARY_LENGTH_BYTES; ++i) {
            	dos.writeByte(1);
            }
            dos.flush();
            //dos.close();
		} catch(IOException e) {
			throw new Error(e);
		}
	}
	
	public synchronized void init() {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
	    if(recursive) return; recursive = true;
	    initStarted = true;
	    
        File f = new File(LOG_FILE_PREFIX);
        f.getParentFile().mkdirs();
        //writeCanary(); // FIXME: For disk-writting debugging purposes
        try{
            String filename = LOG_FILE_PREFIX + ".run" + runId + ".thread" + threadId + "." + subId;
			dos = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(filename)));
            writeable = true;
		} catch(IOException e) {
			throw new Error(e);
		}
		autoFlusherThreadHandle = new Thread(new AutoFlusher(this, AUTO_FLUSH_TIME));
        autoFlusherThreadHandle.start();
        
        recursive = false;
	}
    
    public synchronized void writeLong(long l) {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) init();
        if(!writeable) return;
	    if(recursive) return; recursive = true;
        try {
            dos.writeLong(l);
    	} catch (IOException e) {
			throw new Error(e);
		}
		recursive = false;
    }
    
    public synchronized void writeInt(int i) {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) init();
        if(!writeable) return;
	    if(recursive) return; recursive = true;
        try {
            dos.writeInt(i);
        } catch (IOException e) {
			throw new Error(e);
		}
		recursive = false;
    }
    
    public synchronized void writeDouble(double d) {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) init();
        if(!writeable) return;
	    if(recursive) return; recursive = true;
        try {
            dos.writeDouble(d);
        } catch (IOException e) {
    		throw new Error(e);
		}
		recursive = false;
    }
    
    public synchronized void writeFloat(float f) {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) init();
        if(!writeable) return;
	    if(recursive) return; recursive = true;
        try {
            dos.writeFloat(f);
        } catch (IOException e) {
    		throw new Error(e);
		}
		recursive = false;
    }
    
    public synchronized void writeChar(char c) {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) init();
        if(!writeable) return;
	    if(recursive) return; recursive = true;
        try {
            dos.writeChar(c);
        } catch (IOException e) {
        	throw new Error(e);
		}
		recursive = false;
    }
    
    public synchronized void writeBoolean(boolean b) {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) init();
        if(!writeable) return;
	    if(recursive) return; recursive = true;
        try {
            dos.writeBoolean(b);
        } catch (IOException e) {
            throw new Error(e);
		}
		recursive = false;
    }
    
    public synchronized void writeString(String s) {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) init();
        if(!writeable) return;
	    if(recursive) return; recursive = true;
        try {
            if(s == null) {
            	dos.writeUTF("_null_");
    			dos.flush();
            } else {
				int slen = s.length();
		        if(slen > 16383) {
		        	// Write long, multi-part String, since writeUTF hardcodes a 65535 bytes limit 
		        	// (we use 16383 characters to make sure we don't go over, without incurring 
		        	// the cost of calculating the byte size of the string, since UTF-8 chars are 
		        	// at most 4-bytes long).
		        	dos.writeUTF("_long:10959006-7446-11e4-889a-fb6449f28a74_");
		        	int i = 0;
		        	int j = 16383;
		        	while(j < slen) {
		        		dos.writeUTF(s.substring(i,j));
		        		i = j; j += 16383;
		        	}
		        	dos.writeUTF(s.substring(i));
		        	dos.writeUTF("_long:10959006-7446-11e4-889a-fb6449f28a74_");
		        } else {
		        	dos.writeUTF(s);
		        }
            }
        } catch (IOException e) {
            throw new Error(e);
    	}
    	recursive = false;
    }
    
    public synchronized void flush() {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) init();
        if(!writeable) return;
	    if(recursive) return; recursive = true;
        try {
            if(!closed) dos.flush();
		} catch (IOException e) {
			throw new Error(e);
		}
		recursive = false;
    }
	
	public synchronized void close() {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) return;
        if(!writeable) return;
	    if(recursive) return; recursive = true;
		try {
            closed = true;
            autoFlusherThreadHandle.interrupt();
            dos.close();
		} catch (IOException e) {
			throw new Error(e);
		}
		recursive = false;
	}
	
	@Override
	protected void finalize() {
		//if(threadId != 1) return; // FIXME: Force single thread recording for debugging.
        if(!initStarted) return;
        if(!writeable) return;
	    if(recursive) return; recursive = true;
		try {
            closed = true;
            autoFlusherThreadHandle.interrupt();
			dos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		recursive = false;
	}
}
