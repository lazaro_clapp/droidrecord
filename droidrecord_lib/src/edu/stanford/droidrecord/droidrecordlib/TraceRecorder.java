/* DroidRecord - Android tracing and dynamic analysis framework.
 * 
 * Copyright (c) 2015, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stanford University nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/* @author Lazaro Clapp */
 
package edu.stanford.droidrecord.droidrecordlib;

import java.io.File;

/**
 * Provides the runtime library for the DroidRecord execution 
 * recording system for Android.
 */
public class TraceRecorder {
    
    private static boolean enabled;
    private static final String MARKER_READY_FILE = "/sdcard/.droidrecord.mark";
    private static final File markerReadyFile = new File(MARKER_READY_FILE);
    private static long previousTick = -100000;
    
    public static void checkEnabled() {
        if(enabled) return;
        long currentTick = System.currentTimeMillis();
        if(currentTick < previousTick+1000) return;
        previousTick = currentTick;
        if(markerReadyFile.exists()) {
            enabled = true;
        } else {
            enabled = false;
        }
    }
    
    public static boolean isEnabled() {
        return enabled;
    }
    
    public static void enable() {
        if(!enabled) enabled = true;
    }
    
    private static long runId = System.nanoTime();
    private static final ThreadLocal<BinLogWriter> binlog = 
         new ThreadLocal <BinLogWriter> () {
             @Override protected BinLogWriter initialValue() {
                 long threadId = Thread.currentThread().getId();
                 long subId = System.nanoTime();
                 return new BinLogWriter(runId, threadId, subId);
         }
     };
    
    public static void writeToBinaryLog(long param) {
        if(!enabled) return;
        binlog.get().writeLong(param);
    }
    
    public static void writeToBinaryLog(int param) {
        if(!enabled) return;
        binlog.get().writeInt(param);
    }
    
    public static void writeToBinaryLog(short param) {
        if(!enabled) return;
        binlog.get().writeInt(param);
    }
    
    public static void writeByteToBinaryLog(int param) {
        if(!enabled) return;
        binlog.get().writeInt(param);
    }
    
    public static void writeToBinaryLog(double param) {
        if(!enabled) return;
        binlog.get().writeDouble(param);
    }
    
    public static void writeToBinaryLog(float param) {
        if(!enabled) return;
        binlog.get().writeFloat(param);
    }
    
    public static void writeToBinaryLog(char param) {
        if(!enabled) return;
        binlog.get().writeChar(param);
    }
    
    public static void writeToBinaryLog(boolean param) {
        if(!enabled) return;
        binlog.get().writeBoolean(param);
    }
    
    public static void writeToBinaryLog(String param) {
        if(!enabled) return;
        binlog.get().writeString(param);
    }
    
    public static void writeObjectToBinaryLog(Object param) {
        if(!enabled) return;
        int id = System.identityHashCode(param);
        binlog.get().writeInt(id);
    }
    
    public static void writeClassToBinaryLog(Class param) {
        if(!enabled) return;
        String klass = null;
        if(param == null) {
            klass = "NULL_CLASS"; 
        } else {
            klass = param.getCanonicalName();
            if(klass == null) {
                klass = "ANONYMOUS_CLASS";
            } else {
                klass += ".class";
            }
        }
        int id = System.identityHashCode(param);
        BinLogWriter bwriter = binlog.get();
        bwriter.writeString(klass);
        bwriter.writeInt(id);
    }
    
    public static void writeArrayToBinaryLog(Object param) {
        if(!enabled) return;
        int id = System.identityHashCode(param);
        binlog.get().writeInt(id);
    }
    
    public static void writeNullToBinaryLog(String type) {
        if(!enabled) return;
        // Write '0' for the address of a null object
        // this has the added advantage that, when read as a String, it will 
        // result in "\0" == "", the empty string.
        //binlog.get().writeString(type);
        binlog.get().writeInt(0);
    }
    
    public static void recordEventHeader(long eventID) {
        if(!enabled) return;
        BinLogWriter bwriter = binlog.get();
        bwriter.writeLong(eventID);
        bwriter.flush();
        //if(eventID == 8227) throw new Error("Writting event 8227.");
        bwriter.writeLong(Thread.currentThread().getId());
        binlog.get().flush();
    }
    
    public static void binaryLogTick() {
        //if(!enabled) return;
        //binlog.get().tick();
        return;
    }
    
    public static int getStackHeight() {
        if(!enabled) return 0;
        else return Thread.currentThread().getStackTrace().length;
    }
    
    public static void recordStackHeight() {
        if(!enabled) return;
        BinLogWriter bwriter = binlog.get();
        bwriter.writeInt(Thread.currentThread().getStackTrace().length);
    }
    
    // For debugging purposes only: throws error if recording is enabled
    // Allows inspecting stack
    public static void throwError() {
        if(!enabled) return;
        binlog.get().flush();
        throw new Error("TraceRecorder.throwError() called (thread: " + binlog.get().getThreadId() +")");
    }
    
    public static void throwError(String message) {
        if(!enabled) return;
        binlog.get().flush();
        throw new Error("TraceRecorder.throwError() called (thread: " + binlog.get().getThreadId() +", message: \'" + message + "\')");
    }

}
