# DroidRecord - Android tracing and dynamic analysis framework.
# 
# Copyright (c) 2015, Stanford University
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Stanford University nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
 
# Author: Lazaro Clapp

from lxml import etree
from optparse import OptionParser
import sys

import logparser
import timestamps

def parseCmdLine():
    parser = OptionParser()
    parser.add_option("-s", "--static", dest="xmlfile",
                      help="use static analysis results to find the source or sink methods we are interested in.", metavar="[ArgSinkFlow.xml]")
    return parser.parse_args()
    
class CallInstanceValues(object):

    def __init__(self, instance, arguments, retV):
        self.instance = instance
        self.arguments = arguments
        self.retV = retV
    
    def __hash__(self):
        if self.instance:
            return hash((self.instance, self.retV))
        else:
            return hash(self.retV)
        
    def __eq__(self, other):
        if self.instance != other.instance: return False
        if self.retV != other.retV: return False
        if len(self.arguments) != len(self.arguments): return False
        for i in range(0, len(self.arguments)):
            if self.arguments[i] != other.arguments[i]: return False
        return True

def collect_methodsigs(static_info_file):
    methodsigs = []
    xmldoc = etree.parse(static_info_file)
    for e in xmldoc.xpath('//value[@type="method"]'):
        methodsigs.append(e.get("chordsig"))
    return methodsigs

def get_call_instance_values(log, methodsigs):
    sigToCIV = {}
    sigToLastCallRecord = {}
    for entry in log:
        if isinstance(entry, logparser.MethodCallRecord):
            sig = entry.method.toChordSignature()
            if methodsigs and (sig not in methodsigs): continue
            sigToLastCallRecord[sig] = entry
        elif isinstance(entry, logparser.MethodReturnRecord):
            sig = entry.method.toChordSignature()
            if methodsigs and (sig not in methodsigs): continue
            callE = sigToLastCallRecord[sig]
            if callE.isInstanceMethod():
                instance = callE.parameters[0]
                params = callE.parameters[1:]
            else:
                instance = None
                params = callE.parameters
            retV = entry.retval
            civ = CallInstanceValues(instance, params, retV)
            if sig not in sigToCIV:
                sigToCIV[sig] = []
            sigToCIV[sig].append(civ)
        else: continue
    return sigToCIV

def print_call_instance_values(sigToCIV):
    for sig, civs in sigToCIV.items():
        civs = set(civs)
        for civ in civs:
            argStr = "["
            for arg in civ.arguments[:-1]: argStr += str(arg) + ', '
            if len(civ.arguments) > 0:  argStr += str(civ.arguments[-1])
            argStr += "]"
            
            print 'Method: %s\n\t' \
                  'Instance Object: %s\n\t' \
                  'Arguments: %s\n\t' \
                  'Return Value: %s\n\n' % \
                  (sig, civ.instance, argStr, civ.retV)  
    
def main():
    (options, args) = parseCmdLine()
    if options.xmlfile == None:
        methodsigs = None
    else:
        methodsigs = collect_methodsigs(options.xmlfile)
    log = logparser.Log(args[0])
    sigToCIV = get_call_instance_values(log, methodsigs)
    print_call_instance_values(sigToCIV)
    
        

if __name__ == "__main__":
    main()
