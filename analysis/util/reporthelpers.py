# DroidRecord - Android tracing and dynamic analysis framework.
# 
# Copyright (c) 2015, Stanford University
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Stanford University nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
 
# Author: Lazaro Clapp

from lxml import etree

from structures import *

class MethodsReportXmlHelper(object):
    '''
    Provides methods for writting left-bar analysis reports, involving 
    method call information.
    '''
    
    def __init__(self):
        pass
    
    def method_to_namespace_node(self, parent_e, ns_struct, chordsig, 
                                   srcFile, klassLineNum, 
                                   make_tuple_node=False):
            klass = chordsig.split("@")[-1]
            e = parent_e
            for ns in klass.split('.'):
                if ns not in ns_struct:
                    category_e = etree.SubElement(e, "category")
                    value_e = etree.SubElement(category_e, "value")
                    label_e = etree.SubElement(value_e, "label")
                    label_e.text = etree.CDATA(ns)
                    if ns == klass.split('.')[-1]:
                        category_e.set("type", "class")
                        value_e.set("srcFile", srcFile)
                        value_e.set("lineNum", klassLineNum)
                    ns_struct[ns] = (category_e, {})
                e, ns_struct = ns_struct[ns]
            
            
            if(make_tuple_node):
                node_e = etree.SubElement(e, "tuple")
            else:
                node_e = etree.SubElement(e, "category")
                node_e.set("type", "method")
            return node_e

    def callaggregate_to_namespace_node(self, parent_e, ns_struct, 
                                        callaggregate, make_tuple_node=False):
        assert len(callaggregate.invocations) > 0
        filename = callaggregate.invocations[0].entry.position.getFile()
        chordsig = callaggregate.method.toChordSignature()
        return self.method_to_namespace_node(
                                    parent_e,
                                    ns_struct,
                                    chordsig, 
                                    filename,
                                    "0", # FIXME: True for most classes, but only an approximation for inner classes
                                    make_tuple_node)

    def make_method_value_element(self, parent_e, chordsig, srcFile, lineNum):
        value_e = etree.SubElement(parent_e, "value")
        value_e.set("type", "method")
        value_e.set("srcFile", srcFile)
        value_e.set("lineNum", lineNum)
        value_e.set("chordsig", chordsig)
        return value_e
    
    def make_callaggregate_value_element(self, parent_e, callaggregate):
        assert len(callaggregate.invocations) > 0
        chordsig = callaggregate.method.toChordSignature()
        position = callaggregate.invocations[0].entry.position
        srcFile = position.getFile()
        lineNum = str(position.getLine())
        return self.make_method_value_element(parent_e, chordsig, 
                                              srcFile, lineNum)
            
    def _valid_XML_char_ordinal(self, i):
        return ( # conditions ordered by presumed frequency
            0x20 <= i <= 0xD7FF 
            or i in (0x9, 0xA, 0xD)
            or 0xE000 <= i <= 0xFFFD
            or 0x10000 <= i <= 0x10FFFF
        )
    
    def _string_to_valid_xml_string(self, string):
        return ''.join(
            c for c in string if self._valid_XML_char_ordinal(ord(c))
        )
    
    def method_basic_info_to_xml(self, parent_e, method, position):
        rec_e = etree.SubElement(parent_e, "record")
        rec_e.set("key", "method_name")
        rec_e.set("value", method.name)
        rec_e = etree.SubElement(parent_e, "record")
        rec_e.set("key", "srcfile")
        rec_e.set("value", position.getFile())
        rec_e = etree.SubElement(parent_e, "record")
        rec_e.set("key", "linenum")
        rec_e.set("value", str(position.getLine()))
        rec_e = etree.SubElement(parent_e, "record")
        rec_e.set("key", "method_return_type")
        rec_e.set("value", method.returnType)
        
        rlist_e = etree.SubElement(parent_e, "recordlist")
        rlist_e.set("key", "parameters")
        rec_e = etree.SubElement(rlist_e, "record")
        rec_e.set("key", "0")
        rec_e.set("value", method.klass)
        for k in range(0, len(method.args)):
            rec_e = etree.SubElement(rlist_e, "record")
            rec_e.set("key", str(k+1))
            rec_e.set("value", method.args[k])
            
    def method_invocations_to_xml(self, parent_e, invocations):
        rlist_e = etree.SubElement(parent_e, "recordlist")
        rlist_e.set("key", "invocations")
        i=0
        for invocation in invocations:
            rlist_invc_e = etree.SubElement(rlist_e, "recordlist")
            rlist_invc_e.set("key", str(i))
            
            position = invocation.entry.position
            rec_e = etree.SubElement(rlist_invc_e, "record")
            rec_e.set("key", "srcfile")
            rec_e.set("value", position.getFile())
            rec_e = etree.SubElement(rlist_invc_e, "record")
            rec_e.set("key", "linenum")
            rec_e.set("value", str(position.getLine()))
            
            rlist_param_e = etree.SubElement(rlist_invc_e, "recordlist")
            rlist_param_e.set("key", "parameters")
            i+=1; j=0
            #TODO: figure out appdesc
            # print invocation.entry
            for param in invocation.entry.parameters:
                rec_e = etree.SubElement(rlist_param_e, "record")
                rec_e.set("key", str(j))
                sparam = str(param)
                try:
                    rec_e.set("value", sparam)
                except ValueError as e:
                    print "WARNING: Encountered non-xml encodable (control?) character."
                    rec_e.set("value",self._string_to_valid_xml_string(sparam))
                j+=1
