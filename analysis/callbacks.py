# DroidRecord - Android tracing and dynamic analysis framework.
# 
# Copyright (c) 2015, Stanford University
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Stanford University nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
 
# Author: Lazaro Clapp

from lxml import etree
from optparse import OptionParser
import sys

import logparser
import timestamps

from util.reporthelpers import MethodsReportXmlHelper
from util.structures import CallAggregate
from util.structures import CallInvocation

def parseCmdLine():
    parser = OptionParser()
    parser.add_option("-s", "--static", dest="xmlfile",
                      help="use static analysis results to find overriden callback names and other information", metavar="[PotentialCallbacks.xml]")
    parser.add_option("-r", "--registrations",
                      action="store_true", dest="getRegistrations", default=False,
                      help="get callback registrations as well as callbacks")
    parser.add_option("-c", "--include-constructors",
                      action="store_true", dest="includeConstructors", default=False,
                      help="also list <init> and <cinit> methods as callbacks where appropriate")
    parser.add_option("--tuples",
                      action="store_true", dest="printAsTuples", default=False,
                      help="also list <init> and <cinit> methods as callbacks where appropriate")
    parser.add_option("-o", "--output", dest="outfile",
                      help="write output XML file. This option requires -s|--static.", metavar="[PotentialCallbacks.xml]")
    return parser.parse_args()



class CallbackInvocation(CallInvocation):
    '''
    Represents a single invocation of a callback.
    '''
    
    def __init__(self, entry):
        CallInvocation.__init__(self, entry)
        self.uses = []
        
    def checkAndAddUse(self, invocation):
        param_map = {}
        for i in range(0,len(self.entry.parameters)):
            p1 = self.entry.parameters[i]
            for j in range(0,len(invocation.entry.parameters)):
                p2 = invocation.entry.parameters[j]
                if p1 == p2:
                    # Normalize to instance always being param 0 even when 
                    # there is no instance.
                    if not self.entry.isInstanceMethod(): i += 1
                    if not invocation.entry.isInstanceMethod(): j += 1
                    param_map[j] = i
        if len(param_map) == 0:
            return # Not an use
        use = CallbackInvocationToParamUse(self, invocation, param_map)
        self.uses.append(use)
                    

class CallbackInvocationToParamUse(object):
    '''
    Relates a single callback invocation to a previous method with a 
    specific parameter in common.
    '''
    
    def __init__(self, callback_inv, apicall_inv, param_map):
        self.callback_inv = callback_inv
        self.apicall_inv = apicall_inv
        self.param_map = param_map

    def isPrevious(self):
        t1 = self.callback_inv.entry.getTag('time')
        t2 = self.apicall_inv.entry.getTag('time')
        return t2.happensbefore(t1)

    def isSubsequent(self):
        t1 = self.callback_inv.entry.getTag('time')
        t2 = self.apicall_inv.entry.getTag('time')
        return t2.happensafter(t1)
        
    def onlySameThisObject(self):
        return self.apicall_inv.entry.isInstanceMethod() and \
               self.param_map == {0 : 0}

class CallbackAggregate(CallAggregate):
    '''
    Aggregates invocations of the same callback
    '''
    
    def __init__(self, method):
        CallAggregate.__init__(self, method)
        self._related_cache = None
        self._static_info = None

    def checkAndAddUses(self, call):
        if self._related_cache:
            self._related_cache = None # Invalidate
        for callback_inv in self.invocations:
            for invocation in call.invocations:
                callback_inv.checkAndAddUse(invocation)
    
    def addStaticInfo(self, static_callback_info):
        self._static_info = static_callback_info
    
    def _generateRelatedMethodInfo(self):
        # Generate related methods
        related = []
        methodUsesAggregateMap = {}
        for callback in self.invocations:
            for use in callback.uses:
                if use.isSubsequent():
                    continue    # Use after callback
                if use.onlySameThisObject():
                    continue    # o.m(...) use for callback c with o.c(...)
                methodSig = use.apicall_inv.entry.method.signature
                if methodSig not in methodUsesAggregateMap:
                    methodUsesAggregateMap[methodSig] = []
                methodUsesAggregateMap[methodSig].append(use)
        for methodSig, uses in methodUsesAggregateMap.items():
            apimethod = uses[0].apicall_inv.entry.method
            related.append(RelatedMethodInfo(self.method, apimethod, uses))
        # Compute the number key of each parameter
        param_map_keys = {}
        for r in related:
            for i, li in r.param_map.items():
                for j in li:
                    param_map_keys[j] = 1
        counter = 0
        param_num_map = {}
        for j in param_map_keys:
            param_num_map[j] = counter
            counter += 1
        self._param_num_map = param_num_map
        assert param_num_map != None
        for r in related:
            r.setCallbackNumMap(param_num_map)
        # Cache
        self._related_cache = related
            
    def getRelatedMethods(self):
        if not self._related_cache:
            self._generateRelatedMethodInfo()
        return self._related_cache

    def getPotentialRegistrations(self):
        registrations = []
        for related in self.getRelatedMethods():
            if related.isPotentialRegistration():
                registrations.append(related)
        return registrations

    def getStaticCallbackInfo(self):
        return self._static_info 
    
    def getCallbackNumMap(self):
        return self._param_num_map
        
    def toAnnotatedSignature(self):
        if not self._related_cache:
            self._generateRelatedMethodInfo()
        method = self.method
        param_num_map = self.getCallbackNumMap()
        s = "<" + method.klass
        if 0 in param_num_map:
            num = param_num_map[0]
            s += '{' + str(num) + '}'
        s += ": " + method.returnType + " " + method.name + "("
        for i in range(0, len(method.args)-1):
            s += method.args[i]
            j = i + 1
            if j in param_num_map:
                num = param_num_map[j]
                s += '{' + str(num) + '}'
            s += ", "
        if len(method.args) > 0:
            i = len(method.args)-1
            s += method.args[i]
            j = i + 1
            if j in param_num_map:
                num = param_num_map[j]
                s += '{' + str(num) + '}'
        s += ")>"
        return s
        
class RelatedMethodInfo(object):
    '''
    Provides the information relating a callback and a related method.
    '''
    
    def __init__(self, callbackmethod, apimethod, uses):
        self.callbackmethod = callbackmethod
        self.apimethod = apimethod
        assert len(uses) > 0
        for use in uses:
            assert use.callback_inv.entry.method == self.callbackmethod
            assert use.apicall_inv.entry.method == self.apimethod
        self._uses = uses
        self.param_map = {}
        for use in uses:
            for i, j in use.param_map.items():
                if i not in self.param_map:
                    self.param_map[i] = set()
                self.param_map[i].add(j)    
        self._cb_num_map = None
    
    def getParamNumMap(self):
        return self.param_map
        
    def getApiCallInvocations(self):
        return [use.apicall_inv for use in self._uses]
        
    def isPotentialRegistration(self):
        return self._uses[0].callback_inv.entry.isInstanceMethod() and \
               0 in [item for sublist in self.param_map.values() for item in sublist]
    
    def setCallbackNumMap(self, cb_num_map):
        self._cb_num_map = cb_num_map
        
    def toAnnotatedSignature(self):
        method = self.apimethod
        param_num_map = self.param_map
        if self._cb_num_map == None:
            raise Exception("Callback num map not set.")
        cb_num_map = self._cb_num_map
        s = "<" + method.klass
        if 0 in param_num_map:
            for num in param_num_map[0]:
                num = cb_num_map[num]
                s += '{' + str(num) + '}'
        s += ": " + method.returnType + " " + method.name + "("
        for i in range(0, len(method.args)-1):
            s += method.args[i]
            j = i + 1
            if j in param_num_map:
                for num in param_num_map[j]:
                    num = cb_num_map[num]
                    s += '{' + str(num) + '}'
            s += ", "
        if len(method.args) > 0:
            i = len(method.args)-1
            s += method.args[i]
            j = i + 1
            if j in param_num_map:
                for num in param_num_map[j]:
                    num = cb_num_map[num]
                    s += '{' + str(num) + '}'
        s += ")"
        return s

class StaticCallbackInfo(object):
    '''
    Represents the information for a single callback as computed by STAMP's 
    static callback analysis.
    '''

    def __init__(self, chordsig, srcFile, lineNum, parent):
        self.chordsig = chordsig
        self.srcFile = srcFile
        self.lineNum = lineNum
        self.parent = parent

class StaticCallbackParentInfo(object):
    '''
    Represents the information for a single callback parent method 
    (API defined callback) as computed by STAMP's static callback analysis.
    '''
    
    def __init__(self, chordsig, srcFile, lineNum, klassLineNum):
        self.chordsig = chordsig
        self.srcFile = srcFile
        self.lineNum = lineNum
        self.klassLineNum = klassLineNum
        self.children = []
        
    def addChild(self, chordsig, srcFile, lineNum):
        self.children.append(
                StaticCallbackInfo(chordsig, srcFile, lineNum, self))
        
class CallbackAnalysis(object):
    '''
    Provides the execution of the stand-alone callback analysis.
    '''

    def __init__(self, options):
        self.options = options
        self.callbacks = {}
        self.apicalls = {}
    
    def _addCallbackEntry(self, entry):
        method = entry.method
        if not self.options.includeConstructors:
            if method.name == "<init>" or method.name == "<clinit>":
                return

        if method not in self.callbacks:
            self.callbacks[method] = CallbackAggregate(method)
        call = CallbackInvocation(entry)
        self.callbacks[method].addInvocation(call)
    
    def _addApiCallEntry(self, entry):
        method = entry.method
        if method not in self.apicalls:
            self.apicalls[method] = CallAggregate(method)
        call = CallInvocation(entry)
        self.apicalls[method].addInvocation(call)
    
    def _setCallbackList(self, log):
        unmatchedcall = {}
        clinit_special_stacks = {}
        for entry in log.filtered(timestamps.TimeTagFilter()):
            if isinstance(entry, logparser.MethodCallRecord):
                # If there is a previous unmatched call, it must be an API call.
                # Call after call means the first call is an api call, otherwise it 
                # would have a corresponding start record in between:
                if entry.thread in unmatchedcall:
                    self._addApiCallEntry(unmatchedcall[entry.thread])
                unmatchedcall[entry.thread] = entry
            elif isinstance(entry, logparser.MethodStartRecord):
                # Special case: the class initializer can run between method 
                # invocation and method start, so if this MethodStartRecord is 
                # for <clinit>, we store the previous unmatched call and restore 
                # it when <clinit> finishes.
                if entry.method.name == '<clinit>':
                    if entry.thread not in clinit_special_stacks:
                        clinit_special_stacks[entry.thread] = []
                    if entry.thread in unmatchedcall:
                        unmatched_entry = unmatchedcall[entry.thread]
                        del unmatchedcall[entry.thread]
                    else:
                        unmatched_entry = None
                    clinit_special_stacks[entry.thread].append(unmatched_entry)
                
                # If we have a start record without a matching preceeding call 
                # record, it must be a callback.
                if entry.thread in unmatchedcall:
                    match_to = unmatchedcall[entry.thread]
                    matched = entry.matchesCall(match_to)
                    if not matched:
                        # If we have a preceeding call that doesn't match the 
                        # starting method, then it was an API call. Furthermore 
                        # it was an API call that calls this callback:
                        # CALL m1 -> START m2, implies that m1 is not 
                        # instrumeted and calls m2.
                        # TODO: This actually gives fairly direct 
                        # "registration" info: m1 is the registration for m2.
                        # Maybe we can make use of this?
                        self._addApiCallEntry(unmatchedcall[entry.thread])
                    del unmatchedcall[entry.thread]
                else:
                    matched = False
                if not matched:
                    self._addCallbackEntry(entry)
            elif isinstance(entry, logparser.MethodEndRecord):
                # Restore state after the end of a class initializer method.
                # (see comment above)
                if entry.method.name == '<clinit>':
                    assert entry.thread in clinit_special_stacks
                    assert len(clinit_special_stacks[entry.thread]) > 0
                    unmatched_entry = clinit_special_stacks[entry.thread].pop()
                    if unmatched_entry != None:
                        unmatchedcall[entry.thread] = unmatched_entry
    
    def _collectParameterUses(self):
        for callback in self.callbacks.values():
            for apicall in self.apicalls.values():
                callback.checkAndAddUses(apicall)
        
    def run(self, log):
        self._setCallbackList(log)
        self._collectParameterUses()

    def printCallbackInfo(self):
        for callback in self.callbacks.values():
            print "CALLBACK ", callback.toAnnotatedSignature()
            if self.options.getRegistrations: 
                for registration in callback.getPotentialRegistrations():
                    print "\tCANDIDATE REGISTRATION ",
                    print registration.toAnnotatedSignature()
                for relatedm in callback.getRelatedMethods():
                    if relatedm.isPotentialRegistration(): continue
                    print "\tRELATED METHOD ",
                    print relatedm.toAnnotatedSignature()

    def printCallbackInfoAsTuples(self):
        for callback in self.callbacks.values():
            cb_signature = callback.toAnnotatedSignature()
            found_related = False
            if self.options.getRegistrations: 
                for registration in callback.getPotentialRegistrations():
                    reg_signature = registration.toAnnotatedSignature()
                    print "(%s,%s)" %(cb_signature, reg_signature)
                    found_related = True
                for relatedm in callback.getRelatedMethods():
                    if relatedm.isPotentialRegistration(): continue
                    rel_signature = relatedm.toAnnotatedSignature()
                    print "(%s,%s)" %(cb_signature, rel_signature)
                    found_related = True
            if not found_related:
                print "(%s,_)" %(cb_signature)
          
class STAMPCallbackAnalysis(CallbackAnalysis):
    '''
    Provides the execution of the stamp-integrated callback analysis.
    
    Requires reading the PotentialCallbacks.xml generated by STAMP static 
    callback analysis.
    '''
    
    def __init__(self, options, staticCBFile):
        CallbackAnalysis.__init__(self, options)
        self._static_info_file = staticCBFile
    
    def _readStaticCBFile(self):
        xmldoc = etree.parse(self._static_info_file)
        scb_parent_list = []
        for e in xmldoc.xpath('//category[@type="method"]/value[@type="method"]'):
            scb_parent = StaticCallbackParentInfo(
                            e.get("chordsig"),
                            e.get("srcFile"),
                            e.get("lineNum"),
                            e.xpath('../../value')[0].get("lineNum"))
            for instance in e.xpath('../tuple/value[@type="method"]'):
                scb_parent.addChild(instance.get("chordsig"),
                                    instance.get("srcFile"),
                                    instance.get("lineNum"))
            scb_parent_list.append(scb_parent)
        return scb_parent_list
    
    def _addStaticInfoToCallbacks(self, static_cb_info_l):
        for method, callback in self.callbacks.items():
            cb_chord_subsig = method.toChordSubsignature()
            for scb_parent in static_cb_info_l:
                if scb_parent.chordsig.startswith(cb_chord_subsig):
                    child_matched = False
                    for scb in scb_parent.children:
                        if scb.chordsig == method.toChordSignature():
                            child_matched = True
                            callback.addStaticInfo(scb)
                    if not child_matched:
                        # If the callback parent is present in the static 
                        # information but not the callback itself, that means 
                        # that STAMP deemed the callback reachable, which 
                        # means there is already a model for it.
                        # In this case, we ommit that callback from further 
                        # consideration.
                        del self.callbacks[method]
                        break
    
    def run(self, log):
        CallbackAnalysis.run(self, log)
        static_cb_info_l = self._readStaticCBFile()
        self._addStaticInfoToCallbacks(static_cb_info_l)

    def _structuredCallbackInfo(self):
        parent_scb_to_callbacks = {}
        unmatched_callbacks = []
        for callback in self.callbacks.values():
            scb = callback.getStaticCallbackInfo()
            if scb:
                parent_scb = scb.parent
                if parent_scb not in parent_scb_to_callbacks:
                    parent_scb_to_callbacks[parent_scb] = []
                parent_scb_to_callbacks[parent_scb].append(callback)
            else:
               unmatched_callbacks.append(callback) 
        return (parent_scb_to_callbacks, unmatched_callbacks)
        
    def printCallbackInfo(self):
        (parent_scb_to_callbacks, 
         unmatched_callbacks) = self._structuredCallbackInfo()
        
        def print_instance(callback):
            print "\tINSTANCE ", callback.toAnnotatedSignature()
            if self.options.getRegistrations: 
                for registration in callback.getPotentialRegistrations():
                    print "\t\tCANDIDATE REGISTRATION ",
                    print registration.toAnnotatedSignature()
                for relatedm in callback.getRelatedMethods():
                    if relatedm.isPotentialRegistration(): continue
                    print "\t\tRELATED METHOD ",
                    print relatedm.toAnnotatedSignature()

        for scb_parent, callbacks in parent_scb_to_callbacks.items():
            print "PARENT CALLBACK ", scb_parent.chordsig
            for callback in callbacks: print_instance(callback)
        if len(unmatched_callbacks) > 0:
            print "UNRESOLVED CALLBACKS"
            for callback in unmatched_callbacks: print_instance(callback)
        
    def toXml(self):
        (parent_scb_to_callbacks, 
         unmatched_callbacks) = self._structuredCallbackInfo()
        
        helper = MethodsReportXmlHelper()
        
        xmldoc = etree.XML("<root><title>Possibly-missing Callback Methods (Dynamic)</title></root>")
        root = xmldoc.xpath("/root")[0]
        
        def make_unresolved_callbacks_category_node(e):
            category_e = etree.SubElement(e, "category")
            value_e = etree.SubElement(category_e, "value")
            label_e = etree.SubElement(value_e, "label")
            label_e.text = etree.CDATA("Unresolved Callbacks")
            return category_e
        
        def callback_num_map_to_xml(parent_e, num_map):
            rlist_e = etree.SubElement(parent_e, "recordlist")
            rlist_e.set("key", "param_num_map")
            for k, v in num_map.items():
                rec_e = etree.SubElement(rlist_e, "record")
                rec_e.set("key", str(k))
                rec_e.set("value", str(v))
        
        def related_num_map_to_xml(parent_e, callback_num_map, related_num_map):
            rlist_e = etree.SubElement(parent_e, "recordlist")
            rlist_e.set("key", "param_num_map")
            for k, v in related_num_map.items():
                s = ",".join([str(callback_num_map[v_e]) for v_e in v])
                rec_e = etree.SubElement(rlist_e, "record")
                rec_e.set("key", str(k))
                rec_e.set("value", s)
        
        def callback_to_xml(parent_e, callback):
            # Write basic callback information
            value_e = helper.make_callaggregate_value_element(parent_e, callback)
            
            # Make this item show a custom report page in STAMP
            short_name = callback.method.klass.split('.')[-1] + "." + \
                         callback.method.name
            value_e.set("showReport", "dyncallbacksreport")
            value_e.set("reportNodeID", callback.method.toChordSignature())
            value_e.set("reportNodeShortName", "Callback: " + callback.method.name)
            label_e = etree.SubElement(value_e, "label")
            label_e.text = etree.CDATA(short_name)
            
            # Write additional (analysis specific) data
            data_e = etree.SubElement(value_e, "data")
            data_e.set("type", "dr_analysis_callbacks")
            position = callback.invocations[0].entry.position
            helper.method_basic_info_to_xml(data_e, callback.method, position)
            
            # Write the callback's invocations and parameters
            helper.method_invocations_to_xml(data_e, callback.invocations)
            
            # Write callback's param_num_map
            callback_num_map = callback.getCallbackNumMap()
            callback_num_map_to_xml(data_e, callback.getCallbackNumMap())
             
            # Write related methods info
            rlist_e = etree.SubElement(data_e, "recordlist")
            rlist_e.set("key", "related_methods")
            for relatedm in callback.getRelatedMethods():
                rec_e = etree.SubElement(rlist_e, "record")
                rec_e.set("key", str(relatedm.apimethod.name))
                rec_e.set("value", str(relatedm.apimethod.toChordSignature()))
                position = relatedm.getApiCallInvocations()[0].entry.position
                helper.method_basic_info_to_xml(rec_e, relatedm.apimethod, position)
                subrec_e = etree.SubElement(rec_e, "record")
                subrec_e.set("key", "relation_type")
                if relatedm.isPotentialRegistration():
                    subrec_e.set("value", "registration")
                else:
                    subrec_e.set("value", "related")
                related_num_map_to_xml(rec_e, callback_num_map, relatedm.getParamNumMap())
                helper.method_invocations_to_xml(rec_e, relatedm.getApiCallInvocations())
        
        scb_ns_struct = {}
        for scb_parent, callbacks in parent_scb_to_callbacks.items():
            category_e = helper.method_to_namespace_node(
                                                root,
                                                scb_ns_struct,
                                                scb_parent.chordsig, 
                                                scb_parent.srcFile, 
                                                scb_parent.klassLineNum)
            
            value_e = etree.SubElement(category_e, "value")
            value_e.set("type", "method")
            value_e.set("srcFile", scb_parent.srcFile)
            value_e.set("lineNum", scb_parent.lineNum)
            value_e.set("chordsig", scb_parent.chordsig)
            label_e = etree.SubElement(value_e, "label")
            label_e.text = etree.CDATA(scb_parent.chordsig.split(":")[0])
            tuple_e = etree.SubElement(category_e, "tuple")
            
            for callback in callbacks:
                callback_to_xml(tuple_e, callback)
        
        category_e = make_unresolved_callbacks_category_node(root)
        unresolved_ns_struct = {}
        for callback in unmatched_callbacks:
            cb_category_e = helper.callaggregate_to_namespace_node(
                                                category_e,
                                                unresolved_ns_struct,
                                                callback,
                                                make_tuple_node=True)
            callback_to_xml(cb_category_e, callback)
                           
        return xmldoc

def callbackAnalysis(log, options):
    if options.xmlfile == None:
        analysis = CallbackAnalysis(options)
    else:
        analysis = STAMPCallbackAnalysis(options, options.xmlfile)
    analysis.run(log)
    if options.printAsTuples:
        analysis.printCallbackInfoAsTuples()
    else:
        analysis.printCallbackInfo()
    if options.xmlfile != None and options.outfile != None:
        xmldoc = analysis.toXml()
        with open(options.outfile, 'w') as f:
            f.write(etree.tostring(xmldoc, pretty_print=True))

def main():
    (options, args) = parseCmdLine()
    if options.xmlfile == None and options.outfile != None:
        print "ERROR: Option -o|--output only valid in conjunction with -s|--static"
        exit(1)
    
    log = logparser.Log(args[0])
    callbackAnalysis(log, options)
    

if __name__ == "__main__":
    main()
