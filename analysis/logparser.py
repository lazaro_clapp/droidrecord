# DroidRecord - Android tracing and dynamic analysis framework.
# 
# Copyright (c) 2015, Stanford University
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Stanford University nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
 
# Author: Lazaro Clapp

''' Deprecated code, don't modify except for maintaining compatibility with 
	log format, braving this code requires a safety pig:

                         _
 _._ _..._ .-',     _.._(`))
'-. `     '  /-._.-'    ',/
   )         \            '.
  / _    _    |             \
 |  a    a    /              |
 \   .-.                     ;  
  '-('' ).-'       ,'       ;
     '-;           |      .'
        \           \    /
        | 7  .__  _.-\   \
        | |  |  ``/  /`  /
       /,_|  |   /,_/   /
          /,_/      '`-' 

Ref. http://qr.ae/3hMJ7
'''

from collections import deque
import re

# Logfile parser code, primitive API
def parse_list(list_string):
    l = []
    for e in list_string.split(','):
        e = e.strip()
        if(e == ""):
            continue
        l.append(e)
    return l

entry_re = re.compile(r'^\[(?P<er_type>\w+){(?P<er_contents>(?:.|\n)*)}\]$')
attr_pair_re = re.compile(r'(\w+): ((?:.|\[(?:.|\n)*\]|"[^"]*")+)(?:,\n\t|$)')
list_parse_re = re.compile(r'(?:(?:[^,"]|"(?:[^"\\]|\\.)*")+)')

def entry_to_dict(entry):
    entry_dict = {}
    entry_match = entry_re.match(entry)
    entry_dict["type"] = entry_match.group("er_type")
    e_contents = entry_match.group("er_contents")
    for (k,v) in attr_pair_re.findall(e_contents):
        v = v.strip()
        if v[0] == '[' and v[-1] == ']':
            entry_dict[k] = []
            for val in list_parse_re.findall(v[1:-1]):
                entry_dict[k].append(val.strip())
        else:
            entry_dict[k] = v
    return entry_dict

def log_entries(logfile):
    entry = ""
    line = logfile.readline()
    while(line != ""):
        line.strip()
        entry += line
        if(line.endswith('}]\n') or line.endswith('}]')):
            yield entry_to_dict(entry)
            entry = ""
        line = logfile.readline()

methodsig_re = re.compile(r"^<(?P<ms_class>[^:]+): (?P<ms_ret>[\w\.\$]+(?:\[\])*) (?P<ms_name>'[\w<>\$]+'|[\w<>\$]+)\((?P<ms_args>.*)\)>$")

def method_to_dict(methodsig):
    method_dict = {}
    methodsig_match = methodsig_re.match(methodsig)
    if not methodsig_match:
        raise Exception("Doesn't match method signature RE: " + methodsig)
    method_dict["class"] = methodsig_match.group("ms_class")
    method_dict["retval"] = methodsig_match.group("ms_ret")
    method_dict["name"] = methodsig_match.group("ms_name")
    ms_args = methodsig_match.group("ms_args")
    method_dict["args"] = parse_list(ms_args)
    return method_dict

fieldsig_re = re.compile(r"^<(?P<fs_class>[^:]+): (?P<fs_type>[\w\.\$]+(?:\[\])*) (?P<fs_name>'[\w<>\$]+'|[\w<>\$]+)>$")

def field_to_dict(fieldsig):
    field_dict = {}
    fieldsig_match = fieldsig_re.match(fieldsig)
    if not fieldsig_match:
        raise Exception("Doesn't match field signature RE: " + fieldsig)
    field_dict["class"] = fieldsig_match.group("fs_class")
    field_dict["type"] = fieldsig_match.group("fs_type")
    field_dict["name"] = fieldsig_match.group("fs_name")
    return field_dict

param_re = re.compile(r'(?P<p_type>\w+):(?P<p_val>(?:\w|"(?:[^"\\]|\\.)*"|\.|\$|-| |\[\])+)(?::(?P<p_extra>(?:\w|-|".*"|\.|\$)+))?$')

def param_to_dict(param):
    param_d = {}
    param_match = param_re.match(param)
    if not param_match:
        raise Exception("Doesn't match parameter RE: " + param)
    param_d["type"] = param_match.group("p_type")
    if param_d["type"] == "obj" or param_d["type"] == "array" or param_d["type"] == "class":
        param_d["class"] = param_match.group("p_val")
        param_d["id"] = param_match.group("p_extra")
    else:
        param_d["value"] = param_match.group("p_val")
    return param_d

position_re = re.compile(r'^(?P<p_method><.*>):(?P<p_line>[-]?\d+):(?P<p_jimplestmt>[-]?\d+)$')
    
def position_to_dict(position_descriptor):
    position_dict = {}
    position_match = position_re.match(position_descriptor)
    if not position_match:
        raise Exception("Doesn't match position description RE: " + position_descriptor)
    position_dict["method_sig"] = position_match.group("p_method")
    position_dict["line"] = position_match.group("p_line")
    position_dict["jimplestmt"] = position_match.group("p_jimplestmt")
    return position_dict
    
    
# Object oriented API
        
_TYPE_TO_BCTYPE_DICT = {'byte' : 'B', 'char' : 'C', 'double' : 'D', 
                        'float' : 'F', 'int' : 'I', 'long' : 'J', 
                        'short' : 'S', 'void' : 'V', 'boolean' : 'Z'}
                        
def _typeToChordSignature(typeStr):
        if typeStr in _TYPE_TO_BCTYPE_DICT:
            return _TYPE_TO_BCTYPE_DICT[typeStr]
        elif typeStr.endswith("[]"):
            return '[' + _typeToChordSignature(typeStr[:-2])
        else:
            return 'L' + typeStr.replace('.', '/') + ";"
        return typeStr;

class MethodInfo(object):
    
    def __init__(self, method_sig):
        self.signature = method_sig
        method_dict = method_to_dict(method_sig)
        self.klass = method_dict["class"]
        self.returnType = method_dict["retval"]
        self.name = method_dict["name"]
        self.args = method_dict["args"]
    
    def toQualifiedName(self):
        return self.klass + "." + self.name
    
    def toChordSubsignature(self):
        chordSig = self.name + ":("
        for a in self.args:
            chordSig += _typeToChordSignature(a)
        chordSig += ")" + _typeToChordSignature(self.returnType)
        return chordSig
        
    def toChordSignature(self):
        return self.toChordSubsignature() + "@" + self.klass
    
    def __str__(self):
        return self.signature
    
    def __hash__(self):
        return hash(self.signature)
        
    def __eq__(self, other):
        return self.signature == other.signature
        
    def __ne__(self, other):
        return self.signature != other.signature

class FieldInfo(object):
    
    def __init__(self, field_sig):
        self.signature = field_sig
        field_dict = field_to_dict(field_sig)
        self.klass = field_dict["class"]
        self.type = field_dict["type"]
        self.name = field_dict["name"]
    
    # TODO?
    '''Not yet needed:
    def toChordSubsignature(self):
        
    def toChordSignature(self):
    '''

class ParamInfo(object):
    
    def __init__(self, param):
        param_dict = param_to_dict(param)
        self.type = param_dict['type']
        if self.type == "obj" or self.type == "array" or self.type == "class":
            self.klass = param_dict['class']
            self.id = param_dict['id']
        else:
            self.value = param_dict['value']

    def isObject(self):
        return self.type == "obj"
        
    def __str__(self):
        if self.type == "obj" or self.type == "array" or self.type == "class":
            return "%s:%s:%s" % (self.type, self.klass, self.id)
        else:
            return "%s:%s" % (self.type, self.value)
    
    def __hash__(self):
        if self.type == "obj" or self.type == "array" or self.type == "class":
            return hash((self.type, self.klass, self.id))
        else:
            return hash((self.type, self.value))
            
    def __eq__(self, other):
        if self.type != other.type:
            return False
        if self.type == "obj" or self.type == "array" or self.type == "class":
            if self.klass != other.klass:
                return False
            return self.id == other.id
        else:
            return self.value == other.value

    def __ne__(self, other):
        return not (self == other)
        
class PositionInfo(object):

    def __init__(self, position_dict):
        self.method = MethodInfo(position_dict["method_sig"])
        self.line = int(position_dict["line"])
        self.jimplestmt = int(position_dict["jimplestmt"])
    
    @staticmethod    
    def parse(at_string):
        return PositionInfo(position_to_dict(at_string))
    
    def getFile(self):
        filename = "/".join(self.method.klass.split('.')[:-1]) + "/"
        filename += self.method.klass.split('.')[-1].split('$')[0] + '.java'
        return filename
    
    def getLine(self):
        return self.line
    
    def getJimpleStmt(self):
        return self.jimplestmt
        
    def __str__(self):
        return "%s:%d:%d" % (self.method.signature, self.getLine(), self.getJimpleStmt())


class BaseLogRecord(object):
    
    def __init__(self, thread_id, position):
        object.__init__(self)
        self.thread = int(thread_id)
        self.position = PositionInfo.parse(position)
        self.tags = {}
    
    def hasTag(self, name):
        return name in self.tags
        
    def getTag(self, name):
        if name in self.tags: return self.tags[name]
        else: raise Exception("Tag not set.")
    
    def setTag(self, name, tag):
        self.tags[name] = tag
        
    def _className(self):
        return self.__class__.__name__
                
class BaseMethodRecord(BaseLogRecord):
    
    def __init__(self, thread_id, method_sig, position):
        BaseLogRecord.__init__(self, thread_id, position)
        self.method = MethodInfo(method_sig)
        self.name = self.method.signature

class BaseMethodCallOrStartRecord(BaseMethodRecord):
    
    def __init__(self, thread_id, method_sig, parameters, position):
        BaseMethodRecord.__init__(self, thread_id, method_sig, position)
        self.parameters = [ParamInfo(p) for p in parameters]
    
    def addInstance(self, instance):
        '''Add the object instance to this method call or start record. 
        This should only be called once for the constructor to add the 
        this instance parameter that cannot be captured at recording time.'''
        if self.isInstanceMethod():
            raise Exception("Instance parameter already present")
        elif self.method.name != "<init>":
            raise Exception("Not a constructor. addInstance(...) should be " \
                            "called only for constructors.")
        self.parameters = [instance] + self.parameters
    
    def isInstanceMethod(self):
        if len(self.method.args) == len(self.parameters): return False
        return True
        
    def __str__(self):
        parameters_str = '['+", ".join([str(p) for p in self.parameters])+']'
        return "[%s{Thread: %d,\n\t" \
	             "Name: %s,\n\t" \
	             "At: %s,\n\t" \
	             "Parameters: %s}]" % \
	    (self._className(), self.thread, self.name, 
	     self.position, parameters_str)

class MethodCallRecord(BaseMethodCallOrStartRecord):

    def __init__(self, thread_id, method_sig, parameters, parameterLocals, 
                 position):
        BaseMethodCallOrStartRecord.__init__(self, thread_id, method_sig, 
                                             parameters, position)
        self.locals = parameterLocals
        
    def __str__(self):
        parameters_str = '['+", ".join([str(p) for p in self.parameters])+']'
        locals_str = '['+", ".join(self.locals)+']'
        return "[%s{Thread: %d,\n\t" \
	             "Name: %s,\n\t" \
	             "At: %s,\n\t" \
	             "Parameters: %s,\n\t" \
	             "ParameterLocals: %s}]" % \
	    (self._className(), self.thread, self.name, 
	     self.position, parameters_str, locals_str)

class MethodStartRecord(BaseMethodCallOrStartRecord):
    
    def __init__(self, thread_id, method_sig, parameters, position):
        BaseMethodCallOrStartRecord.__init__(self, thread_id, method_sig, 
                                             parameters, position)
        
    def matchesCall(self, callrecord):
        m1 = self.method
        m2 = callrecord.method
        if m1.name != m2.name:
            return False
        if m1.args != m2.args:
            return False
        # Special case: string is final and treated by our code as a primitive 
        # type, not an object
        if m1.klass == "java.lang.String" or m2.klass == "java.lang.String":
            return m1.klass == m2.klass
        if callrecord.isInstanceMethod():
            # Same 'this' object?
            assert self.parameters[0].isObject() and \
                   callrecord.parameters[0].isObject()
            if self.parameters[0].id != callrecord.parameters[0].id:
                return False
        else:
            if m1.klass != m2.klass:
                return False
        return True
        
class MethodReturnRecord(BaseMethodRecord):
    
    def __init__(self, thread_id, method_sig, return_value, position):
        BaseMethodRecord.__init__(self, thread_id, method_sig, position)
        self.retval = ParamInfo(return_value)
        
    def __str__(self):
        return "[MethodReturnRecord{Thread: %d,\n\t" \
	            "Name: %s,\n\t" \
	            "At: %s,\n\t" \
	            "Returns: %s}]" % \
	    (self.thread, self.name, self.position, self.retval)
        
class MethodEndRecord(BaseMethodRecord):
    
    def __init__(self, thread_id, method_sig, return_value, position):
        BaseMethodRecord.__init__(self, thread_id, method_sig, position)
        self.retval = ParamInfo(return_value)
        
    def __str__(self):
        return "[MethodEndRecord{Thread: %d,\n\t" \
	            "Name: %s,\n\t" \
	            "At: %s,\n\t" \
	            "Returns: %s}]" % \
	    (self.thread, self.name, self.position, self.retval)

class FieldLSRecord(BaseLogRecord):
    
    def __init__(self, thread_id, field_sig, local_name, position, value):
        BaseLogRecord.__init__(self, thread_id, position)
        self.field = FieldInfo(field_sig)
        self.local = local_name
        self.value = ParamInfo(value)

class StaticLSRecord(FieldLSRecord):
    
    def __init__(self, thread_id, field_sig, local_name, position, value):
        FieldLSRecord.__init__(self, thread_id, field_sig, local_name, 
                               position, value)
                               
    def __str__(self):
        return "[%s{Thread: %d,\n\t" \
	            "Field: %s,\n\t" \
	            "Local: %s,\n\t" \
	            "At: %s,\n\t" \
	            "Value: %s}]" % \
	    (self._className(), self.thread, self.field.signature, 
	     self.local, self.position, self.value)

class InstanceLSRecord(FieldLSRecord):
    
    def __init__(self, thread_id, field_sig, local_name, position, instance, 
                 value):
        FieldLSRecord.__init__(self, thread_id, field_sig, local_name, 
                               position, value)
        self.instance = ParamInfo(instance)
                               
    def __str__(self):
        return "[%s{Thread: %d,\n\t" \
	            "Field: %s,\n\t" \
	            "Local: %s,\n\t" \
	            "At: %s,\n\t" \
	            "Instance: %s,\n\t" \
	            "Value: %s}]" % \
	    (self._className(), self.thread, self.field.signature, 
	     self.local, self.position, self.instance, self.value)

class ArrayLSRecord(BaseLogRecord):
    
    def __init__(self, thread_id, arrayLocal, arrayInstance, indexes, local, 
                 position, value):
        BaseLogRecord.__init__(self, thread_id, position)
        self.arrayLocal = arrayLocal
        self.arrayInstance = ParamInfo(arrayInstance)
        self.indexes = [int(s) for s in indexes]
        self.local = local
        self.value = ParamInfo(value)
                               
    def __str__(self):
        indexes_str = '['+", ".join([str(i) for i in self.indexes])+']'
        return "[%s{Thread: %d,\n\t" \
	            "ArrayLocal: %s,\n\t" \
	            "ArrayInstance: %s,\n\t" \
	            "Indexes: %s,\n\t" \
	            "Local: %s,\n\t" \
	            "At: %s,\n\t" \
	            "Value: %s}]" % \
	    (self._className(), self.thread, self.arrayLocal, self.arrayInstance, 
	     indexes_str, self.local, self.position, self.value)

class LoadStaticRecord(StaticLSRecord):
    
    def __init__(self, thread_id, field_sig, local_name, position, value):
        StaticLSRecord.__init__(self, thread_id, field_sig, local_name, 
                                position, value)

class LoadInstanceRecord(InstanceLSRecord):
    
    def __init__(self, thread_id, field_sig, local_name, position, instance, 
                 value):
        InstanceLSRecord.__init__(self, thread_id, field_sig, local_name, 
                                  position, instance, value)

class LoadUninitializedObjectRecord(StaticLSRecord):
    
    def __init__(self, thread_id, field_sig, local_name, position, value):
        StaticLSRecord.__init__(self, thread_id, field_sig, local_name, 
                                position, value)
                                
    def toInstanceRecord(self, instance):
        ir = LoadInstanceRecord(self.thread, self.field.signature, self.local, 
                                str(self.position), str(instance), 
                                str(self.value))
        return ir

class LoadArrayRecord(ArrayLSRecord):
    
    def __init__(self, thread_id, arrayLocal, arrayInstance, indexes, local, 
                 position, value):
        ArrayLSRecord.__init__(self, thread_id, arrayLocal, arrayInstance, 
                               indexes, local, position, value)

class StoreStaticRecord(StaticLSRecord):
    
    def __init__(self, thread_id, field_sig, local_name, position, value):
        StaticLSRecord.__init__(self, thread_id, field_sig, local_name, 
                                position, value)

class StoreInstanceRecord(InstanceLSRecord):
    
    def __init__(self, thread_id, field_sig, local_name, position, instance, 
                 value):
        InstanceLSRecord.__init__(self, thread_id, field_sig, local_name, 
                                  position, instance, value)

class StoreUninitializedObjectRecord(StaticLSRecord):
    
    def __init__(self, thread_id, field_sig, local_name, position, value):
        StaticLSRecord.__init__(self, thread_id, field_sig, local_name, 
                                position, value)
                                
    def toInstanceRecord(self, instance):
        ir = StoreInstanceRecord(self.thread, self.field.signature, self.local, 
                                str(self.position), str(instance), 
                                str(self.value))
        return ir

class StoreArrayRecord(ArrayLSRecord):
    
    def __init__(self, thread_id, arrayLocal, arrayInstance, indexes, local, 
                 position, value):
        ArrayLSRecord.__init__(self, thread_id, arrayLocal, arrayInstance, 
                               indexes, local, position, value)

class ArrayLengthRecord(BaseLogRecord):
    
    def __init__(self, thread_id, array_local, array_instance, local, position, 
                 value):
        BaseLogRecord.__init__(self, thread_id, position)
        self.arrayLocal = array_local
        self.arrayInstance = ParamInfo(array_instance)
        self.local = local
        self.value = ParamInfo(value)
                               
    def __str__(self):
        return "[%s{Thread: %d,\n\t" \
	            "ArrayLocal: %s,\n\t" \
	            "ArrayInstance: %s,\n\t" \
	            "Local: %s,\n\t" \
	            "At: %s,\n\t" \
	            "Value: %s}]" % \
	    (self._className(), self.thread, self.arrayLocal, self.arrayInstance, 
	     self.local, self.position, self.value)

class CaughtExceptionRecord(BaseLogRecord):
    
    def __init__(self, thread_id, local, position, exception):
        BaseLogRecord.__init__(self, thread_id, position)
        self.local = local
        self.exception = ParamInfo(exception)
                               
    def __str__(self):
        return "[%s{Thread: %d,\n\t" \
	            "Local: %s,\n\t" \
	            "At: %s,\n\t" \
	            "Exception: %s}]" % \
	    (self._className(), self.thread, self.local, self.position, 
	     self.exception)

class NewArrayRecord(BaseLogRecord):
    
    def __init__(self, thread_id, array_local, array_instance, dimensions, 
                 position):
        BaseLogRecord.__init__(self, thread_id, position)
        self.arrayLocal = array_local
        self.arrayInstance = ParamInfo(array_instance)
        self.dimensions = [int(s) for s in dimensions]
                               
    def __str__(self):
        dimensions_str = '['+", ".join([str(d) for d in self.dimensions])+']'
        return "[%s{Thread: %d,\n\t" \
	            "ArrayLocal: %s,\n\t" \
	            "ArrayInstance: %s,\n\t" \
	            "Dimensions: %s,\n\t" \
	            "At: %s}]" % \
	    (self._className(), self.thread, self.arrayLocal, self.arrayInstance, 
	     dimensions_str, self.position)

class NewObjectRecord(BaseLogRecord):
    
    def __init__(self, thread_id, local_name, position, object_type, 
                 object_instance=None):
        BaseLogRecord.__init__(self, thread_id, position)
        self.local = local_name
        self.type = object_type
        if object_instance:
            self.instance = ParamInfo(object_instance)
        else:
            self.instance = None
    
    def addInstance(self, instance):
        self.instance = instance
                               
    def __str__(self):
        if self.instance:
            return "[%s{Thread: %d,\n\t" \
	                "Local: %s,\n\t" \
	                "At: %s,\n\t" \
	                "Type: %s,\n\t" \
	                "Instance: %s}]" % \
	        (self._className(), self.thread, 
	         self.local, self.position, self.type, self.instance)
        else:
            return "[%s{Thread: %d,\n\t" \
	                "Local: %s,\n\t" \
	                "At: %s,\n\t" \
	                "Type: %s}]" % \
	        (self._className(), self.thread, 
	         self.local, self.position, self.type)

class InitializationCompleteRecord(BaseLogRecord):
    
    def __init__(self, thread_id, local_name, position, value):
        BaseLogRecord.__init__(self, thread_id, position)
        self.local = local_name
        self.value = ParamInfo(value)
                               
    def __str__(self):
        return "[%s{Thread: %d,\n\t" \
	            "Local: %s,\n\t" \
	            "At: %s,\n\t" \
	            "Value: %s}]" % \
	    (self._className(), self.thread, 
	     self.local, self.position, self.value)
        
class Log(object):
    
    def __init__(self, log_name, raw_mode=False):
        self.file = open(log_name, 'r')
        self._raw = raw_mode
    
    def __del__(self):
        self.file.close()
        
    def _entry_to_obj(self, entry_dict):
        ed = entry_dict
        if(ed['type'] == "MethodCallRecord"):
            return MethodCallRecord(ed['Thread'], ed['Name'], ed['Parameters'],
                                    ed['ParameterLocals'], ed['At'])
        elif(ed['type'] == "MethodStartRecord"):
            return MethodStartRecord(ed['Thread'], ed['Name'], ed['Parameters'],
                                    ed['At'])
        elif(ed['type'] == "MethodReturnRecord"):
            return MethodReturnRecord(ed['Thread'], ed['Name'], ed['Returns'],
                                      ed['At'])
        elif(ed['type'] == "MethodEndRecord"):
            return MethodEndRecord(ed['Thread'], ed['Name'], ed['Returns'],
                                   ed['At'])
        elif(ed['type'] == "LoadStaticRecord"):
            return LoadStaticRecord(ed['Thread'], ed['Field'], ed['Local'],
                                   ed['At'], ed['Value'])
        elif(ed['type'] == "LoadInstanceRecord"):
            return LoadInstanceRecord(ed['Thread'], ed['Field'], ed['Local'],
                                   ed['At'], ed['Instance'], ed['Value'])
        elif(ed['type'] == "LoadUninitializedObjectRecord"):
            return LoadUninitializedObjectRecord(ed['Thread'], ed['Field'], 
                                   ed['Local'], ed['At'], ed['Value'])
        elif(ed['type'] == "LoadArrayRecord"):
            return LoadArrayRecord(ed['Thread'], ed['ArrayLocal'],
                                   ed['ArrayInstance'], ed['Indexes'], 
                                   ed['Local'], ed['At'], ed['Value'])
        elif(ed['type'] == "StoreStaticRecord"):
            return StoreStaticRecord(ed['Thread'], ed['Field'], ed['Local'],
                                   ed['At'], ed['Value'])
        elif(ed['type'] == "StoreInstanceRecord"):
            return StoreInstanceRecord(ed['Thread'], ed['Field'], ed['Local'],
                                   ed['At'], ed['Instance'], ed['Value'])
        elif(ed['type'] == "StoreUninitializedObjectRecord"):
            return StoreUninitializedObjectRecord(ed['Thread'], ed['Field'], 
                                   ed['Local'], ed['At'], ed['Value'])
        elif(ed['type'] == "StoreArrayRecord"):
            return StoreArrayRecord(ed['Thread'], ed['ArrayLocal'],
                                   ed['ArrayInstance'], ed['Indexes'], 
                                   ed['Local'], ed['At'], ed['Value'])
        elif(ed['type'] == "ArrayLengthRecord"):
            return ArrayLengthRecord(ed['Thread'], ed['ArrayLocal'], 
                                  ed['ArrayInstance'], ed['Local'], ed['At'], 
                                  ed['Value'])
        elif(ed['type'] == "CaughtExceptionRecord"):
            return CaughtExceptionRecord(ed['Thread'], ed['Local'], ed['At'], 
                                         ed['Exception'])
        elif(ed['type'] == "NewObjectRecord"):
            return NewObjectRecord(ed['Thread'],  ed['Local'],
                                   ed['At'], ed['Type'])
        elif(ed['type'] == "NewArrayRecord"):
            return NewArrayRecord(ed['Thread'],  ed['ArrayLocal'], 
                                  ed['ArrayInstance'], ed['Dimensions'], 
                                  ed['At'])
        elif(ed['type'] == "InitializationCompleteRecord"):
            return InitializationCompleteRecord(ed['Thread'],  ed['Local'],
                                   ed['At'], ed['Value'])
        elif(ed['type'] == "IfRecord" or ed['type'] == "SwitchRecord"):
            # IfRecord and SwitchRecord are not supported in text instrumentation
            # just binary instrumentation.
            return None;
        elif(ed['type'] == "ThrowExceptionRecord"):
            # ThrowExceptionRecord is not supported in text instrumentation
            # just binary instrumentation.
            return None;
        elif(ed['type'] == "SimpleAssignmentRecord"):
            # SimpleAssignmentRecord is not supported in text instrumentation
            # just binary instrumentation.
            return None;
        elif(ed['type'] == "NegationRecord"):
            # NegationRecord is not supported in text instrumentation
            # just binary instrumentation.
            return None;
        elif(ed['type'] == "BinaryOperationRecord"):
            # BinaryOperationRecord is not supported in text instrumentation
            # just binary instrumentation.
            return None;
        elif(ed['type'] == "InstanceOfRecord"):
            # InstanceOfRecord is not supported in text instrumentation
            # just binary instrumentation.
            return None;
        elif(ed['type'] == "CastRecord"):
            # CastRecord is not supported in text instrumentation
            # just binary instrumentation.
            return None;
        elif(ed['type'] == "EnterMonitorRecord"):
            # EnterMonitorRecord is not supported in text instrumentation
            # just binary instrumentation.
            return None;
        elif(ed['type'] == "ExitMonitorRecord"):
            # ExitMonitorRecord is not supported in text instrumentation
            # just binary instrumentation.
            return None;
        else:
            raise Exception("Unsupported log entry type: " + ed['type'])
        
    def _raw__iter(self):
        entry_str = ""
        self.file.seek(0)
        line = self.file.readline()
        while(line != ""):
            line.strip()
            entry_str += line
            if(line.endswith('}]\n') or line.endswith('}]')):
                entry_dict = entry_to_dict(entry_str)
                entry_as_obj = self._entry_to_obj(entry_dict)
                if(entry_as_obj != None):
                    yield self._entry_to_obj(entry_dict)
                entry_str = ""
            line = self.file.readline()

    def __iter__(self):
        if self._raw:
            return self._raw__iter()
        else:
            log_filter = PerThreadFilter(InitializationFilter)
            return FilteredIterator(self._raw__iter, log_filter).__iter__()

    def filtered(self, logfilter):
        return FilteredIterator(self.__iter__, logfilter).__iter__()

class FilteredIterator(object):

    def __init__(self, iterator, iteratorFilter):
        self._iterator = iterator
        self._filter = iteratorFilter
    
    def __iter__(self):
        for e in self._iterator():
            while self._filter.ready():
                yield self._filter.pull()
            self._filter.push(e)

class TransparentFilter(object):

    def __init__(self):
        self._buffer = deque()
        
    def process(self, entry):
        pass

    def push(self, entry):
        self.process(entry)
        self._buffer.append(entry)
    
    def pull(self):
        if len(self._buffer) == 0: 
            raise Exception("Filter not ready.")
        return self._buffer.popleft()
    
    def ready(self):
        return len(self._buffer) != 0

class FilterChain(object):
    
    def __init__(self, filters):
        self.filters = filters

    def push(self, entry):
        self.filters[0].push(entry)
    
    def pull(self):
        if not self.ready():
            raise Exception("Filter not ready.")
        else:
            return self.filters[-1].pull()
    
    def ready(self):
        for i in range(0, len(self.filters) - 1):
            if self.filters[i].ready():
                self.filters[i+1].push(self.filters[i].pull())
        return self.filters[-1].ready()

class PerThreadFilter(object):

    def __init__(self, FilterClass):
        self._FilterClass = FilterClass
        self._thread_map = {}
        self._order = deque()

    def push(self, entry):
        thread = entry.thread
        if thread not in self._thread_map:
            self._thread_map[thread] = self._FilterClass()
        self._thread_map[thread].push(entry)
        self._order.append(thread)
    
    def pull(self):
        if len(self._order) == 0:
            raise Exception("Filter not ready.")
        thread = self._order.popleft()
        return self._thread_map[thread].pull()
    
    def ready(self):
        if len(self._order) == 0: return False
        thread = self._order[0]
        return self._thread_map[thread].ready()

class InitializationFilter(object):
    '''Match NewObjectRecord(s) to InitializationCompleteRecord(s) and 
    propagate the intialized object's id to corresponding 'Uninitialized' 
    accesses.'''
    
    def __init__(self):
        self._processed = deque()
        self._pending = deque()
        self._unmatched_news = {}
        self._unmatched_news_counter = 0
        self._new_to_call = {}
        self._call_to_return = {}
        self._unmatched_uninit = {}
        self._unmatched_uninit_counter = 0
        self._uninit_to_end = {}
    
    def _new_call_matches(self, new_e, call_e):
        assert type(new_e) is NewObjectRecord
        assert type(call_e) is MethodCallRecord
        if new_e.position.method != call_e.position.method:
            return False
        if new_e.type != call_e.method.klass:
            return False
        if len(call_e.locals) == 0:
            return False
        if new_e.local != call_e.locals[0]:
            return False
        return True
    
    def _call_return_matches(self, call_e, ret_e):
        assert type(call_e) is MethodCallRecord
        assert type(ret_e) is MethodReturnRecord
        if call_e.position.method != ret_e.position.method:
            return False
        if call_e.position.line != ret_e.position.line:
            return False
        if call_e.method != ret_e.method:
            return False
        return True
    
    def process(self):
        # Propagate instance value from Ret to Call to New.
        for new_e, call_e in self._new_to_call.items():
            assert call_e in self._call_to_return
            ret_e = self._call_to_return[call_e]
            call_e.addInstance(ret_e.retval)
            new_e.addInstance(ret_e.retval)
        # Shift each entry to the processed buffer, transforming uninitialized 
        # accesses into instance accesses.
        for entry in self._pending:
            if type(entry) is StoreUninitializedObjectRecord or \
               type(entry) is LoadUninitializedObjectRecord:
                assert entry in self._uninit_to_end
                end_e = self._uninit_to_end[entry]
                entry = entry.toInstanceRecord(end_e.retval)
            if type(entry) is MethodCallRecord or \
               type(entry) is MethodStartRecord:
                if entry in self._uninit_to_end:
                    end_e = self._uninit_to_end[entry]
                    entry.addInstance(end_e.retval)
            self._processed.append(entry)
        # Reset state
        self._pending = deque()
        self._unmatched_news = {}
        self._unmatched_news_counter = 0
        self._new_to_call = {}
        self._call_to_return = {}
        self._unmatched_uninit = {}
        self._unmatched_uninit_counter = 0
        self._uninit_to_end = {}
    
    def push(self, entry):
        # NewObjectRecords or Uninitialized loads and stores always start 
        # accumulating in the pending buffer.
        if type(entry) is NewObjectRecord:
            self._pending.append(entry)
            klass = entry.type
            if klass not in self._unmatched_news:
                self._unmatched_news[klass] = deque()
            self._unmatched_news[klass].append(entry)
            self._unmatched_news_counter += 1
            return
        
        if type(entry) is StoreUninitializedObjectRecord or \
             type(entry) is LoadUninitializedObjectRecord:
            self._pending.append(entry)
            initializer_m = entry.position.method.signature
            if initializer_m not in self._unmatched_uninit:
                self._unmatched_uninit[initializer_m] = deque()
            self._unmatched_uninit[initializer_m].append(entry)
            self._unmatched_uninit_counter += 1
            return
        
        # MethodCallRecords can start accumulating in the pending buffer in the 
        # special case in which they are calls to a superclass initializer for 
        # an uninitialized object
        if type(entry) is MethodCallRecord:
            if entry.method.name == "<init>":
                self._pending.append(entry)
                klass = entry.method.klass
                # Case 1: [New a -> Call a.<init> -> Ret a.<init>]
                if klass in self._unmatched_news and \
                    len(self._unmatched_news[klass]) > 0:
                    new_e = self._unmatched_news[klass][0]
                    if self._new_call_matches(new_e, entry):
                        self._new_to_call[new_e] = entry
                        return
                # Superclass initializers are threated as uninitialized 
                # accesses.
                # Case 2: [Start a.<init> -> Call super(a).<init> -> End a.<init>]
                initializer_m = entry.position.method.signature
                if initializer_m not in self._unmatched_uninit:
                    self._unmatched_uninit[initializer_m] = deque()
                self._unmatched_uninit[initializer_m].append(entry)
                self._unmatched_uninit_counter += 1
                return
        
        # MethodStartRecords can start accumulating in the pending when they 
        # are initializers
        if type(entry) is MethodStartRecord:
            if entry.method.name == "<init>":
                self._pending.append(entry)
                initializer_m = entry.position.method.signature
                if initializer_m not in self._unmatched_uninit:
                    self._unmatched_uninit[initializer_m] = deque()
                self._unmatched_uninit[initializer_m].append(entry)
                self._unmatched_uninit_counter += 1
                return
                
        
        # If there are no pending entries, other records cannot start 
        # accumulating in the pending buffer, so we shift them immediatly to 
        # the processed buffer.
        if len(self._pending) == 0:
            self._processed.append(entry)
            return
        
        # Otherwise, we are already accumulating entries, so we store the 
        # current entry in the pending buffer
        self._pending.append(entry)
        
        # MethodReturnRecords or MethodEndRecords can cause the filter to 
        # start the processing stage
        if type(entry) is MethodReturnRecord:
            klass = entry.method.klass
            if klass in self._unmatched_news and \
               len(self._unmatched_news[klass]) > 0:
                new_e = self._unmatched_news[klass][0]
                if new_e in self._new_to_call:
                    call_e = self._new_to_call[new_e]
                    if self._call_return_matches(call_e, entry):
                        self._call_to_return[call_e] = entry
                        self._unmatched_news[klass].popleft()
                        self._unmatched_news_counter -= 1
                if self._unmatched_news_counter == 0 and \
                   self._unmatched_uninit_counter == 0:
                   self.process()
            return
        
        if type(entry) is MethodEndRecord:
            initializer_m = entry.position.method.signature
            if initializer_m in self._unmatched_uninit:
                uninit_entries = self._unmatched_uninit[initializer_m]
                for e in uninit_entries:
                    self._uninit_to_end[e] = entry
                self._unmatched_uninit_counter -= len(uninit_entries)
                self._unmatched_uninit[initializer_m] = deque()
                if self._unmatched_news_counter == 0 and \
                   self._unmatched_uninit_counter == 0:
                   self.process()
            return
    
    def pull(self):
        if self.ready():
            return self._processed.popleft()
        else:
            raise Exception("Filter not ready.")
    
    def ready(self):
        return len(self._processed) > 0
