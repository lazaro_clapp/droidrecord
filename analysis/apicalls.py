# DroidRecord - Android tracing and dynamic analysis framework.
# 
# Copyright (c) 2015, Stanford University
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Stanford University nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
 
# Author: Lazaro Clapp

from lxml import etree
from optparse import OptionParser
import sys

import logparser
import timestamps

from util.reporthelpers import MethodsReportXmlHelper
from util.structures import CallAggregate
from util.structures import CallInvocation

def parseCmdLine():
    parser = OptionParser()
    parser.add_option("-o", "--output", dest="outfile",
                      help="write output XML file. This option requires -s|--static.", metavar="[PotentialCallbacks.xml]")
    return parser.parse_args()

class ApiCallsAnalysis(object):
    '''
    Provides the execution of the stand-alone api call analysis.
    
    Based on a very simplified version of CallbackAnalysis (callbacks.py)
    '''

    def __init__(self):
        self.apicalls = {}
    
    def _addApiCallEntry(self, entry):
        method = entry.method
        if method not in self.apicalls:
            self.apicalls[method] = CallAggregate(method)
        call = CallInvocation(entry)
        self.apicalls[method].addInvocation(call)
    
    def _setCallList(self, log):
        unmatchedcall = {}
        clinit_special_stacks = {}
        for entry in log.filtered(timestamps.TimeTagFilter()):
            if isinstance(entry, logparser.MethodCallRecord):
                # If there is a previous unmatched call, it must be an API call.
                # Call after call means the first call is an api call, 
                # otherwise it would have a corresponding start record in 
                # between:
                if entry.thread in unmatchedcall:
                    self._addApiCallEntry(unmatchedcall[entry.thread])
                unmatchedcall[entry.thread] = entry
                
            elif isinstance(entry, logparser.MethodStartRecord):
                # Special case: the class initializer can run between method 
                # invocation and method start, so if this MethodStartRecord is 
                # for <clinit>, we store the previous unmatched call and restore 
                # it when <clinit> finishes.
                if entry.method.name == '<clinit>':
                    if entry.thread not in clinit_special_stacks:
                        clinit_special_stacks[entry.thread] = []
                    if entry.thread in unmatchedcall:
                        unmatched_entry = unmatchedcall[entry.thread]
                        del unmatchedcall[entry.thread]
                    else:
                        unmatched_entry = None
                    clinit_special_stacks[entry.thread].append(unmatched_entry)
                
                if entry.thread in unmatchedcall:
                    match_to = unmatchedcall[entry.thread]
                    matched = entry.matchesCall(match_to)
                    if not matched:
                        # If we have a preceeding call that doesn't match the 
                        # starting method, then it was an API call. Furthermore 
                        # it was an API call that calls this callback:
                        # CALL m1 -> START m2, implies that m1 is not 
                        # instrumeted and calls m2.
                        self._addApiCallEntry(unmatchedcall[entry.thread])
                    del unmatchedcall[entry.thread]
                
            elif isinstance(entry, logparser.MethodEndRecord):
                # Restore state after the end of a class initializer method.
                # (see comment above)
                if entry.method.name == '<clinit>':
                    assert entry.thread in clinit_special_stacks
                    assert len(clinit_special_stacks[entry.thread]) > 0
                    unmatched_entry = clinit_special_stacks[entry.thread].pop()
                    if unmatched_entry != None:
                        unmatchedcall[entry.thread] = unmatched_entry
        
    def run(self, log):
        self._setCallList(log)

    def printApiCallInfo(self):
        for apicall in self.apicalls.values():
            print "API Call ", apicall.method


class STAMPApiCallsAnalysis(ApiCallsAnalysis):
    '''
    Provides the execution of the stamp-integrated api calls analysis.
    
    Allows outputing the api call information to XML.
    '''
    
    def __init__(self):
        ApiCallsAnalysis.__init__(self)
        
    def toXml(self):
        
        xmldoc = etree.XML("<root><title>API Call Method Information (Dynamic)</title></root>")
        root = xmldoc.xpath("/root")[0]
        
        helper = MethodsReportXmlHelper()
        
        def apicall_to_xml(parent_e, apicall):
            # Write basic call information
            value_e = helper.make_callaggregate_value_element(parent_e, apicall)
            
            # Make this item show a custom report page in STAMP
            short_name = apicall.method.klass.split('.')[-1] + "." + \
                         apicall.method.name
            value_e.set("showReport", "dynapicallsreport")
            value_e.set("reportNodeID", apicall.method.toChordSignature())
            value_e.set("reportNodeShortName", "API Call: " + apicall.method.name)
            label_e = etree.SubElement(value_e, "label")
            label_e.text = etree.CDATA(short_name)
            
            # Write additional (analysis specific) data
            data_e = etree.SubElement(value_e, "data")
            data_e.set("type", "dr_analysis_apicalls")
            position = apicall.invocations[0].entry.position
            helper.method_basic_info_to_xml(data_e, apicall.method, position)
            
            # Write the callback's invocations and parameters
            helper.method_invocations_to_xml(data_e, apicall.invocations)
        
        ns_struct = {} # Namespace structure
        for apicall in self.apicalls.values():
            category_e = helper.callaggregate_to_namespace_node(
                                                root,
                                                ns_struct,
                                                apicall,
                                                make_tuple_node=True)
            apicall_to_xml(category_e, apicall)
                           
        return xmldoc

def apiCallsAnalysis(log, options):
    analysis = STAMPApiCallsAnalysis()
    analysis.run(log)
    analysis.printApiCallInfo()
    if options.outfile != None:
        xmldoc = analysis.toXml()
        with open(options.outfile, 'w') as f:
            f.write(etree.tostring(xmldoc, pretty_print=True))

def main():
    (options, args) = parseCmdLine()    
    log = logparser.Log(args[0])
    apiCallsAnalysis(log, options)
    

if __name__ == "__main__":
    main()
