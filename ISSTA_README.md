Instructions for running ISSTA Modelgen model generation experiments:

Resulting models available at https://bitbucket.org/lazaro_clapp/modelgen-issta2015-specs

IMPORTANT: These instructions have only been tried before on Ubuntu Linux 12.04.4 
	and might not work elsewhere, also build-tools, Java 6, ant and python-lxml 
	are required dependencies.
	Also, since STAMP is open source at the moment, the following instructions 
	replicate only the results for experiment 5.1 of the ISSTA draft.

1) Download and initialize the Android 4.0.3 source code:

	e.g. (see https://source.android.com/source/downloading.html )

	curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
	export PATH=~/bin:$PATH
	chmod a+x ~/bin/repo
	mkdir android-source
	cd android-source/
	repo init -u https://android.googlesource.com/platform/manifest -b android-4.0.3_r1.1
	repo sync
	
2) (Optional, but recommended) Add our Dalvik source patch:
	
	Patch in [DROIDRECORD_DIR]/extra/patches/dalvik_stack_height.patch
	Destination: [ANDROID_SRC_DIR]/dalvik

3) Build the Android platform, Android SDK and Android Compatibility Test Suite (CTS):

	. build/envsetup.sh
	lunch full-eng
	make CC=gcc-4.4 CXX=g++-4.4 WITH_DEXPREOPT=false -j4
	lunch sdk-eng
	make CC=gcc-4.4 CXX=g++-4.4 WITH_DEXPREOPT=false -j4 sdk
	make CC=gcc-4.4 CXX=g++-4.4 WITH_DEXPREOPT=false -j4 cts
	
	export ANDROID_SDK_DIR=[ANDROID_SRC_DIR]/out/host/linux-x86/sdk/android-sdk_eng.ubuntu_linux-x86
	export ANDROID_BUILD_TOOLS=$ANDROID_SDK_DIR/build-tools/18.0.1
	export PATH=$PATH:$ANDROID_SDK_DIR/tools:$ANDROID_SDK_DIR/platform-tools:$ANDROID_BUILD_TOOLS
	
	(Feel free to add the exports to your shell configuration.)
	
4) Create an Android emulator instance:

	android create avd --force -n DR_SDK_INST_AVD -t 1 -hw.sdCard=yes -vm.heapSize=4096 -hw.ramSize=8192

5) Switch DroidRecord to issta2015 branch:

	git checkout issta2015

6) Build DroidRecord

	cd [DROIDRECORD_DIR]
	ant clean
	ant

7) Instrument SDK and CTS bytecode:

	./postsdkbuild
	./postctsbuild

8) Start the instrumented emulator:

	emulator -wipe-data -noaudio -avd DR_SDK_INST_AVD -debug all -ramdisk [DROIDRECORD_DIR]/ramdisk/ramdisk_new.img -system [DROIDRECORD_DIR]/droidrecord_sdk/system.img &

9) Run instrumented CTS tests and collect the recorded traces:

	adb shell mkdir /data/droidrecord
	[ANDROID_SRC_DIR]/out/host/linux-x86/cts/android-cts/tools/cts-tradefed run cts --class com.android.cts.droidrecord.modelgen.[CLASS_NAME]
	
	---- Where CLASS_NAME is the class for which you wish to run tests and collect traces. After cts-tradefed runs, collect the traces as follows:
	
	mkdir [CLASS_NAME]
	cd [CLASS_NAME]
	adb pull /data/droidrecord

10) Run modelgen on the traces:

	mkdir cutlog
	[DROIDRECORD_DIR]/droidrecord-cutlog [DROIDRECORD_DIR]/droidrecord_cts/templates/droidrecord.log.template -d [TRACES_DIR] cutlog
	[DROIDRECORD_DIR]/droidrecord-batch-modelgen -m [DROIDRECORD_DIR]/droidrecord-modelgen.basemodels [DROIDRECORD_DIR]/droidrecord_cts/templates/droidrecord.log.template cutlog
	
Models should have been created inside the cutlog directory.
